#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_bondsCommands.py,v 1.24 2013/01/23 23:43:18 annao Exp $
#
# $Id: test_bondsCommands.py,v 1.24 2013/01/23 23:43:18 annao Exp $
#

#############################################################################
#
# Authors: Sowjanya Karnati, Ruth Huey, Sophie Coon, Michel Sanner
#
# Copyright: M. Sanner TSRI 2000
#
#############################################################################




import sys,unittest,Pmv
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain
from string import split
mv = None
ct = 0
totalCt = 23

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class BondsBaseTests(unittest.TestCase):

    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, withShell=0, gui=hasGUI)
                                #withShell=False,
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                              package='Pmv')
            mv.browseCommands('deleteCommands',commands= ['deleteMol',],
                              package='Pmv') 
            mv.browseCommands('bondsCommands',package='Pmv')
            mv.browseCommands("interactiveCommands",package='Pmv')
        self.mv = mv
        self.mv.undo = mv.NEWundo

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None
        
class BuildBondsByDistanceTests(BondsBaseTests):
    """
    Class implementing function to test the BuildBondsByDistance command.
    """
    def test_loadBuildBonds(self):
        """
        Test that you load the buildBondsByDistance command and its
        dependencies properly
        """
        self.assertEqual(hasattr(mv, 'buildBondsByDistance'),True)
        
        
    def test_BuildBondsByDistance_invalid_input_nodes(self):
    
        """
        tests BuildBondsByDistance,invalid input returns None
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.buildBondsByDistance
        returnValue = c("pqrs",0)
        self.assertEqual(returnValue, [])
        
    def test_BuildBondsByDistance_invalid_input_dispaly(self):
        """
        tests BuildBondsByDistance,invalid input returns None
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.buildBondsByDistance
        returnValue = c("1crn",654)
        self.assertEqual(returnValue, [])
       
    def test_BuildBondsByDistance_empty_input_nodes(self):
        """
        tests BuildBondsByDistance,empty input returns None
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.buildBondsByDistance
        returnValue = c(" ",1)
        self.assertEqual(returnValue,[])

    
    def test_BuildBondsByDistance_get_selection(self):
       
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.select("1crn: :PRO36:N,CA,CB,CG,CD", negate=False,
                       only=False, log=0)
        self.mv.buildBondsByDistance(self.mv.getSelection(),display=1, log=0)
        mol = self.mv.getSelection()[0]
        self.assertEqual(len(mol.bonds),3)
        
    def test_simpleBuildBonds(self):
        """ Test buildBondsByDistance with simple molecule with no display"""
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.buildBondsByDistance("1crn", display = 0)
        self.mv.select("1crn")
        mol = self.mv.getSelection()[0]
        # 1- Test that the correct bonds have been created
        self.assertEqual(len(mol.allAtoms.bonds[0]),337)
        # 2- Delete the molecule
        self.mv.deleteMol("1crn")
        self.assertEqual(len(self.mv.Mols),0)

    def test_simpleBuildBonds_display(self):
        """ Test buildBondsByDistance with simple molecule with display"""
        # Read the molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # Call the buildBondsByDistance
        self.mv.buildBondsByDistance("1crn", display = 1)
        self.mv.select("1crn")
        mol = self.mv.getSelection()[0]
        # 1- Test that the correct bonds have been created
        self.assertEqual(len(mol.allAtoms.bonds[0]),337)
        # 2- The call has done what you wanted
        self.assertEqual(len(mol.geomContainer.atoms['bonded']),327)


    def test_alternateBuildBonds(self):
        """ Test buildBondsByDistance with a molecule 1bsrsmall that has some
        alternate location simple molecule"""
        # Read the molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # Call the buildBondsByDistance
        self.mv.buildBondsByDistance("1crn", display = 0)
        mol = self.mv.Mols[0]
         # Make sure that all the alternate atoms are not bound to each other
        # Get the atoms
        altA = mol.allAtoms.get(lambda x: x.name[-2:] == '@A')
        altB = mol.allAtoms.get(lambda x: x.name[-2:] == '@B')
        for atmA in altA:
            for atmB in altB:
                if atmA.isBonded(atmB):
                    self.assertRaises(AssertionError,atm1.isBonded,(atm2))


class AddBondsTests(BondsBaseTests):
    """
    Class implementing function to test the AddBond command.
    """
    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
            self.mv.setOnAddObjectCommands(['buildBondsByDistance',
                                            'displayLines'], log=0)
            
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        
    def test_loadAddBonds(self):
        """
        Test that you load the AddBonds command and its  dependencies
        properly
        """
        self.assertEqual(hasattr(mv, 'addBonds'),True)
        self.assertEqual(hasattr(mv, 'removeBonds'),True)
        
    
    def test_addbonds_invalid_input(self):
        self.mv.readMolecule("./Data/1crn.pdb")
        returnValue=self.mv.addBonds([("1crn: :ALA :NW","THR28:CG2")],
                                     bondOrder=[1], log=0)
        self.assertEqual(returnValue, 'ERROR')
        

    def test_addBonds(self):
        """tests bond is added between two atoms selected
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.select("1crn")
        mol = self.mv.Mols[0]
        old_bonds = len(mol.allAtoms.bonds[0])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        at1 = mol.NodesFromName("1crn: :ALA38:N")
        at2 = mol.NodesFromName("1crn: :THR28:CG2")
        self.mv.addBonds([(at1, at2)])
        new_bonds = len(mol.allAtoms.bonds[0])
        self.assertEqual(old_bonds<new_bonds, True)
           

    def test_addIntermolecularBond(self):
        """tests bond is not added between two atoms in different molecules
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        crn = self.mv.Mols[0]
        self.mv.readMolecule("./Data/ind.pdb")
        ind = self.mv.Mols[1]
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        at1 = crn.allAtoms[0]
        at2 = ind.allAtoms[0]
        old_bonds = len(crn.allAtoms.bonds[0]) + len(ind.allAtoms.bonds[0])
        self.mv.addBonds([(at1, at2)])
        new_bonds = len(crn.allAtoms.bonds[0]) + len(ind.allAtoms.bonds[0])
        self.assertEqual(old_bonds, new_bonds)


    def test_addBonds_atoms_are_bonded(self):
        self.mv.setOnAddObjectCommands(['buildBondsByDistance',
                                        'displayLines'], log=0)
        self.mv.readMolecule("./Data/small1crn.pdb")
        # Get a handle on the molecule
        allAtoms = self.mv.Mols[0].allAtoms
        # get two atoms not bonded
        atm1 = allAtoms.get(lambda x: x.full_name()=="small1crn: :THR1:O")[0]
        atm2 = allAtoms.get(lambda x: x.full_name()=="small1crn: :THR1:N")[0]
        # create a bond between these two atoms
        mv.addBonds([(atm1, atm2)], bondOrder=[1])
        # assert that these two atoms are now bonded.
        self.assertEqual( atm1.isBonded(atm2), True)

        # make sure that the bondOrder is the one you want.
        b = atm1.bonds.get(lambda x:x.atom1==atm2 or x.atom2==atm2)[0]
        self.assertEqual( b.bondOrder, 1)

        # make sure that the generated undo command is correct.
        self.mv.undo()
        self.assertEqual(atm1.isBonded(atm2),False)
        self.mv.deleteMol("small1crn")
        self.assertEqual(len(self.mv.Mols),0)


    def test_addBonds_UpdateGeom(self):
        """
        Display part of the molecule as sticks and balls, add a bond  between
        two atoms not represented by sticks and balls.
        Testing that these two atoms are not displayed by sticks and balls.
        """
        self.mv.setOnAddObjectCommands(['buildBondsByDistance',
                                        'displayLines'], log=0)
        self.mv.browseCommands("displayCommands",
                               commands=['displaySticksAndBalls','undisplaySticksAndBalls'])
        self.mv.readMolecule("./Data/small1crn.pdb")
        # select a part of the molecule and display by sticks and balls
        self.mv.select(mv.Mols[0].allAtoms[:5])
        self.mv.displaySticksAndBalls(self.mv.getSelection(), log=0,
                                      cquality=14, bquality=14, cradius=0.1,
                                      only=False, bRad=0.3,
                                      negate=False, bScale=0.0)
        self.mv.clearSelection()
        
        # Get a handle on the molecule
        allAtoms = self.mv.Mols[0].allAtoms
        # get two atoms not bonded
        atm1 = allAtoms.get(lambda x: x.full_name()=="small1crn: :THR2:O")[0]
        atm2 = allAtoms.get(lambda x: x.full_name()=="small1crn: :THR2:N")[0]
        # create a bond between these two atoms
        mv.addBonds([(atm1, atm2)], bondOrder=[1])

        self.failUnless(not atm1 in self.mv.Mols[0].geomContainer.atoms['balls'],
                        "%s should not be displayed as balls")
        self.failUnless(not atm2 in self.mv.Mols[0].geomContainer.atoms['balls'],
                        "%s should not displayed as sticksExpecting False")


    def xaddBonds_UpdateGeom(self):
        """
        THIS TEST MAKES NO SENSE TO ME
        Add a bond between two atoms displayed by sticks and balls.
        Make sure that new bonds is displayed as sticks and balls using the
        last Used values.
        """
        self.mv.setOnAddObjectCommands(['buildBondsByDistance',
                                        'displayLines'], log=0)
        self.mv.browseCommands("displayCommands")
        self.mv.readMolecule("./Data/small1crn.pdb")
        # select a part of the molecule and display by sticks and balls
        self.mv.select(mv.Mols[0].allAtoms[:5])
        self.mv.displaySticksAndBalls(self.mv.getSelection(), log=0,
                                      cquality=14, bquality=14, cradius=0.1,
                                      only=False, bRad=0.3,
                                      negate=False, bScale=0.0)
        self.mv.clearSelection()
        
        # Get a handle on the molecule
        allAtoms = self.mv.Mols[0].allAtoms
        
        # get two atoms not bonded
        atm1 = allAtoms.get(lambda x: x.full_name()=="small1crn: :THR1:O")[0]
        atm2 = allAtoms.get(lambda x: x.full_name()=="small1crn: :THR1:N")[0]
        # create a bond between these two atoms
        self.mv.addBonds([(atm1, atm2)], bondOrder=[1])

        self.failUnless(atm1 in self.mv.Mols[0].geomContainer.atoms['balls'],
                        "%s should be displayed as balls")
        self.failUnless(atm2 in self.mv.Mols[0].geomContainer.atoms['balls'],
                        "%s should displayed as sticks")
        
        

class RemoveBondsTests(BondsBaseTests):
    """
    Class implementing function to test the RemoveBonds command.
    """
    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
            self.mv.setOnAddObjectCommands(['buildBondsByDistance',
                                            'displayLines'], log=0)
        self.mv.browseCommands("displayCommands", ["displayLines","undisplayLines"], package='Pmv')
            
        for m in self.mv.Mols:
            self.mv.deleteMol(m)

    def test_loadRemoveBonds(self):
        """Test that you load the RemoveBonds command and its dependencies
        properly
        """
        self.assertEqual(hasattr(mv, 'removeBonds'),True)
               
        
    def test_removeBonds(self):
        """tests bond is removed between two atoms selected
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.buildBondsByDistance("1crn", display = 1)
        old_bonds=len(self.mv.allAtoms.bonds[0])
        # name of bond is at1.full_name() == at2.full_name()
        #self.mv.removeBonds("1crn: :TYR29:CD1","1crn: :TYR29:CE1", log=0)
        at1 = self.mv.expandNodes("1crn: :TYR29:CD1")[0]
        self.mv.removeBonds([at1.bonds[1]], log=0)
        new_bonds = len(mv.allAtoms.bonds[0])
        self.assertEqual(old_bonds>new_bonds,True)
        
    

    def test_removeBond_of_atom_selected(self):
        
        # Read small1crn
        self.mv.readMolecule("./Data/small1crn.pdb")
        # Get a handle on the molecule
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        mol = self.mv.Mols[0]
        # get two bonded atoms
        #atm1 = mol.allAtoms.get(lambda x: x.full_name()=="small1crn: :THR1:CA")[0]
        #atm2 = mol.allAtoms.get(lambda x: x.full_name()=="small1crn: :THR1:CB")[0]
        #self.mv.removeBonds(atm1,atm2)
        # bond : 'small1crn: :THR1:CA == small1crn: :THR1:CB'
        atm1 = self.mv.expandNodes("small1crn: :THR1:CA")[0]
        atm2 = self.mv.expandNodes("small1crn: :THR1:CB")[0]
        self.mv.removeBonds([atm1.bonds[0]], log=0)
        self.assertEqual(atm1.isBonded(atm2),False)
        self.mv.undo()
        # a bond is created by default with None for bondOrder....
        # when you undo
        # a removeBond command you create a bond with a bondOrder of 1.
        self.assertEqual(atm1.isBonded(atm2),True)

    def test_addbonds_undo_redo(self):
        """testting undo by adding,removing and undoing bonds """
        self.mv.readMolecule("./Data/ind.pdb")
        mol = self.mv.Mols[0]
        oldbonds=mol.allAtoms.bonds[0]
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        
        self.mv.addBondsGC("ind:I:IND201:H16", log=0)
        self.mv.addBondsGC("ind:I:IND201:H44", log=0)
        self.mv.addBondsGC("ind:I:IND201:H40", log=0)
        self.mv.addBondsGC("ind:I:IND201:H47", log=0)
        bonds = mol.allAtoms.bonds[0]
        self.assertEqual(len(bonds)-len(oldbonds), 2)
        self.mv.undo()
        self.mv.undo()
        bonds = mol.allAtoms.bonds[0]
        self.assertEqual(oldbonds, bonds)
        self.mv.redo()
        self.mv.redo()
        bonds=mol.allAtoms.bonds[0]
        self.assertEqual(len(bonds)-len(oldbonds), 2)
        

class LogBondsCommandsTest(BondsBaseTests):
    
    def test_BuildBondsByDistance_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        oldself =self
        self = mv
        s = "self.buildBondsByDistance('1crn', display=1, log=0)"
        exec(s)
        self=oldself
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['bonded'].vertexSet)>0,True)
        
    def test_BuildBondsByDistance_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.buildBondsByDistance("1crn",display=1)
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.buildBondsByDistance('1crn', display=1, log=0)")




if __name__ == '__main__':
    unittest.main()





































