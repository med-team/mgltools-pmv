#
#
# $Id: test_ConstrainAtomsAmber94.py,v 1.6 2009/11/10 22:59:02 annao Exp $
#
#############################################################################
#                                                                           #
#   Author:Sowjanya Karnati                                                 #
#   Copyright: M. Sanner TSRI 2000                                          #
#                                                                           #
#############################################################################

import sys,os
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
ct = 0
totalCt = 7
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class AmberBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, withShell=0, gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('amberCommands', package='Pmv')
            #set up links to shared dictionary and current instance
            from Pmv.amberCommands import Amber94Config, CurrentAmber94
            self.Amber94Config = Amber94Config
            self.CurrentAmber94 = CurrentAmber94
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        #for key,value in self.Amber94Config.items():
        #    del self.Amber94Config[key]
        #    try:
        #        del value
        #    except:
        #        print "exception in deleting ", value
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv


#####################################################
#       ConstrainAtomsAmber94 COMMAND TESTS         #
#####################################################

class ConstrainAtomsAmber94(AmberBaseTest):

    def xtest_constrain_atoms_amber94_widget(self):
        """checks widget is dispalyed for constrain atoms command
        FIX THIS
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        #self.mv.setup_Amber94("trp3_h:","ctest1",filename=None)
        #c1 = self.mv.constrainAtoms_Amber94
        #c1('ctest1', "trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0)
        #c1.buildForm('Set atoms to be constrained for "ctest1":',self.mv.Mols)
        ##Need to do this otherwise the form closes before assert
        ##but = c1.ifd.entryByName['anchorCB']['widget']
        ##but.wait_visibility(but)
        #self.assertEqual(c1.ifd.form.root.winfo_ismapped(),1)    
        #c1.Close_cb(c1.ifd)        
        

    def test_constrain_atoms_amber94_amberIds(self):
        """checks amberIds button in constrain atoms widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'ctest2','Data/trp3_h.prmtop')
        c1 = self.mv.constrainAtoms_Amber94
        c1('ctest2', "trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0)
        if self.mv.hasGui:
            c1.buildForm('Set atoms to be constrained for "ctest2":',self.mv.Mols)    
            AmberIdBut = c1.ifd.entryByName['amberIds']['widget']
            AmberIdBut.setentry('ctest2')
            self.assertEqual(AmberIdBut.get(),'ctest2')
            c1.Close_cb(c1.ifd) 
        
    def test_constrain_atoms_amber94_anchor(self):
        """checks anchor button in constrain atoms widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'ctest3','Data/trp3_h.prmtop')
        c1 = self.mv.constrainAtoms_Amber94
        c1('ctest3', "trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0)
        if self.mv.hasGui:
            c1.buildForm('Set atoms to be constrained for "ctest3":',self.mv.Mols)
            c1.ss.atomEntry.insert('end', 'C')
            c1.ss.showCurrent.set(1)
            c1.setAnchors()
            #Need to do this otherwise the form closes before assert
            #but = c1.ifd2.entryByName['cancelbut']['widget']
            #but.wait_visibility(but)
            #self.assertEqual(c1.adjAnchors.get(),1)
            self.assertEqual(c1.ifd2.form.root.winfo_exists(),1)
            c1.Accept_cb2()
            c1.Close_cb(c1.ifd)
        
    def test_constrain_atoms_amber94_wcons_tw(self):
        """checks setting a value for wcons_tw in constrain atoms widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'ctest4','Data/trp3_h.prmtop')
        c1 = self.mv.constrainAtoms_Amber94
        c1('ctest4', "trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0)
        if self.mv.hasGui:
            c1.buildForm('Set atoms to be constrained for "ctest4":',self.mv.Mols)
            c1.ss.atomEntry.insert('end', 'C')
            c1.ifd.entryByName['wcons_tw']['widget'].set(0.35)
            self.assertEqual(c1.ifd.entryByName['wcons_tw']['widget'].get(),0.35)
            c1.Close_cb(c1.ifd)

    def test_constrain_atoms_amber94_x_y_z_tw(self):
        """checks x_y_z_tw value in constrain atoms widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'ctest5','Data/trp3_h.prmtop')
        c1 = self.mv.constrainAtoms_Amber94
        c1('ctest5', "trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0)
        if self.mv.hasGui:
            c1.buildForm('Set atoms to be constrained for "ctest5":',self.mv.Mols)
            c1.ss.atomEntry.insert('end', 'C')
            c1.ss.showCurrent.set(1)
            c1.setAnchors()
            sph_vert = self.mv.Mols[0].geomContainer.geoms['bonded'].vertexSet.vertices.array[0]
            
            c1.x_tw.set(3)
            c1.y_tw.set(2)
            c1.z_tw.set(30)
        
            #Need to do this otherwise the form closes before assert
            #but = c1.ifd2.entryByName['cancelbut']['widget']
            #but.wait_visibility(but)
            sph_vert1 = c1.spheres.vertexSet.vertices.array[0]

            #self.assertEqual(sph_vert!=sph_vert1,True)
            import numpy
            self.assertFalse(numpy.alltrue(sph_vert==sph_vert1))
            c1.Accept_cb2()
            c1.Close_cb(c1.ifd)
        
        
#invalid input for constrainAtoms_Amber94       

    def test_constrain_atoms_amber94_invalid_key(self):
        """checks invalid key entry for constrainatoms
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        c1 = self.mv.constrainAtoms_Amber94
        self.assertEqual(c1('gjg',"trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0),'ERROR')
        
        
    def constrain_atoms_amber94_empty_key(self):
        """
        FIX THIS:
        checks empty key entry for constrainatoms
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        #c1 = self.mv.constrainAtoms_Amber94
        #self.assertEqual(c1(' ',"trp3_h: :LEU1:C;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[7.4189999999999996, -1.901, 30.295000000000002], [7.2220000000000004, -4.7229999999999999, 30.966000000000001], [4.5, -4.601, 28.879000000000001]], log=0),'ERROR')
        

    def test_constrain_atoms_amber94_invalid_Atoms(self):
        """checks invalid atoms entry for constrainatoms
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        c = self.mv.constrainAtoms_Amber94
        self.assertEqual(c('ctest6',"trp3_h: :LEU1:CAGH;trp3_h: :TRP2:C;trp3_h: :GLN3:C", [[0,1,1],[1,2,3],[2,3,4]]),'ERROR') 
         
    
    def test_constrain_atoms_amber94_invalid_vertices(self):
        """checks invalid vertices entry for constrainatoms
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'ctest7','Data/trp3_h.prmtop')
        c1 = self.mv.constrainAtoms_Amber94
        try:
            c1('ctest7',"trp3_h: :LEU1:,HG", [2,3,4], log=0)     
            raise IndexError
        except :
            print "."        

#end invalid input for constrainAtoms_Amber94

if __name__ == '__main__':
    test_cases = [
        'ConstrainAtomsAmber94',
        ]
    
    unittest.main( argv=([__name__] + test_cases) )




#if __name__ == '__main__':
#    unittest.main()






        
