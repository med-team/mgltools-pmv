#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_selectionCommands.py,v 1.33.2.1 2016/02/11 21:29:44 annao Exp $
#
# $Id: test_selectionCommands.py,v 1.33.2.1 2016/02/11 21:29:44 annao Exp $
#

import unittest, glob, os, Pmv.Grid, string,sys
import time
from string import split,strip,find
from MolKit.molecule import Atom,Molecule
from MolKit.protein import Residue,Chain, ProteinSet
from MolKit.sets import Sets
import Pmv

mv = None
klass = None
ct = 0
totalCt = 173
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class Pmvselection_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                    withShell=False, gui=hasGUI,
                                    trapExceptions=False)
            if ct > 0:
                from Pmv import selectionCommands
                reload(selectionCommands)
            mv.browseCommands('fileCommands', commands= ['readMolecule',],package= 'Pmv')
            mv.browseCommands('deleteCommands', commands=['deleteMol',],package= 'Pmv')
            mv.browseCommands("bondsCommands", commands= ["buildBondsByDistance"],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands("interactiveCommands", package='Pmv')
            # Don't want to trap exceptions and errors... the user pref is set to 1 by
            # default
            #need access to error messages
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            
            mv.browseCommands('selectionCommands',package= 'Pmv')
            mv.readMolecule("Data/ind.pdb")
            mv.readMolecule("Data/test_aa.pdb")
            mv.readMolecule("Data/barrel_1.pdb")
            mv.readMolecule("Data/1crn_hs.pdb")
            mv.readMolecule("Data/1CRN.cif")
            mv.readMolecule("Data/dnaexple.pdb")
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
                    
        if not hasattr(self, 'mv'):
            self.startViewer()
        try:
            self.mv.clearSelection(log=0)
        except:
            pass
        #for m in self.mv.Mols:
            #try:
                #self.mv.deleteMol(m)
            #except:
                #pass
        self.mv.sets = Sets()
        

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt

        #delete any molecules left due to errors
        #for m in self.mv.Mols:
            #try:
                #self.mv.deleteMol(m)
            #except:
                #pass
        ct = ct + 1
        #print 'ct =', ct
        self.mv.sets = Sets()
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None
        
#class Pmvselection_selectionTests(Pmvselection_BaseTests):
class Pmvselection_SaveSetsTests(Pmvselection_BaseTests):

#Save Current Selection as a set
#widget entries cannot be tested(buildForm() is absent)    
    
    def test_selection_saveset_keys(self):
        """checks the saved set is added to self.mv.sets,number of atoms,atom names
        """
        c=self.mv.saveSet
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        ats = self.mv.allAtoms[:3]
        c(ats,'atsset1',comments='NoDescription', log=0)
        #c(mv.allAtoms[:3],'atsset1',comments='NoDescription', log=0)
        self.assertEqual(self.mv.sets.keys()[0], 'atsset1')
        self.assertEqual(self.mv.sets.values()[0], mv.allAtoms[:3])
        self.assertEqual(self.mv.sets.values()[0].name, mv.allAtoms[:3].name)


   
    def test_selection_saveset_keys_select(self):
        """checks the saved set is added to self.mv.sets,number of atms ,atom names using select
        """
        c=self.mv.saveSet
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.select("ind:I:IND201:C19,H30", negate=False, only=False, log=0)
        c(self.mv.getSelection(),'atsset2',comments='NoDescription')
        self.assertEqual(self.mv.sets.keys()[0],'atsset2')
        self.assertEqual(self.mv.sets.values()[0], self.mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name, self.mv.getSelection().name)
    

    def test_selection_saveset_keys_select_from_string(self):
        """checks the saved set is added to self.mv.sets,number of atms ,atom names using select from string
        """
        c=self.mv.saveSet
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString(mols='ind',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        c(self.mv.getSelection(),'atsset3',comments='NoDescription')
        self.assertEqual(self.mv.sets.keys()[0],'atsset3')
        self.assertEqual(self.mv.sets.values()[0], mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name, mv.getSelection().name)
    

    def test_selection_saveset_keys_direct_select(self):
        """checks the saved set is added to self.mv.sets,number of atms ,atom names using direct select        """
        c=self.mv.saveSet
        self.mv.directSelect('ind')
        c(self.mv.getSelection(),'atsset4',comments='NoDescription')
        self.assertEqual(self.mv.sets.keys()[0],'atsset4')
        self.assertEqual(self.mv.sets.values()[0], mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name, mv.getSelection().name)

    
    def test_selection_saveset_invalid_input(self):
        """checks invalid input for saveset
        """
        c=self.mv.saveSet
        c(mv.allAtoms[352:456],'atsset5',comments='NoDescription')
        self.assertEqual(len(self.mv.selection), 0)

    
    def test_selection_saveset_invalid_input_select_from_string(self):
        """checks invalid input for saveset through select from string
        """
        c=self.mv.saveSet
        self.mv.selectFromString(mols='indty',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        c(self.mv.getSelection(),'atsset6',comments='NoDescription')
        self.assertEqual(len(self.mv.selection), 0)

    
    

#end Save Current Selection as a set

class Pmvselection_InvertSelectionTests(Pmvselection_BaseTests):
#invertSelection
    
    def test_selection_invert_Selection_get_selection(self):
        """checks selection inverted through getSelection command
        """
        self.mv.setSelectionLevel(Atom)
        self.mv.select("ind:I:IND201:C19,H30")
        all = self.mv.getSelection().name
        self.mv.invertSelection('all')
        new = self.mv.getSelection().name
        self.assertEqual(all!=new, True)
        self.mv.clearSelection()
        

    def test_selection_invert_Selection_atoms(self):
        """checks selection inverted
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.select(self.mv.allAtoms[:3])
        self.assertEqual(len(self.mv.selection), 3)
        c = self.mv.invertSelection
        c('all')
        #check that all but atoms are selected
        self.assertEqual(len(self.mv.selection), len(self.mv.allAtoms)-3)
        self.mv.clearSelection()
    

    def test_selection_invert_Selection_valid_input_select(self):
        """checks selection inverted,valid input through  selection command
        """
        self.mv.setSelectionLevel(Atom)
        self.mv.select(":::C19,H30")
        self.mv.invertSelection('all')
        self.assertEqual(len(self.mv.selection), 1567)
        #self.assertEqual(len(self.mv.selection),90)
#end invert selection



class Pmvselection_SelectTests(Pmvselection_BaseTests):
#select command

    def test_select(self):
        """checks select command selects atoms
        """
        c=self.mv.select
        oldatoms=mv.allAtoms[:3].name
        c(mv.allAtoms[:3])
        newatoms=mv.getSelection().name
        self.assertEqual(oldatoms,newatoms)

    def test_select_valid_input(self):
        """checks valid input for select command
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        self.assertEqual(len(self.mv.selection),3)

    def test_select_mmcif(self):
        """checks select command for mmcif 
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        self.assertEqual(len(self.mv.selection),3)


    def test_select_invalid_input(self):
        """checks invalid input for select command
        """
        c=self.mv.select
        c(mv.allAtoms[30000:33400])
        self.assertEqual(len(self.mv.selection),0)
        
    def test_select_chain(self):
        """checks select chain 
        """
        self.mv.select("ind:", negate=0, only=False, log=0)
        self.assertEqual(self.mv.getSelection().name,['I'])
        
    def test_select_mol(self):
        """checks select molecule
        """
        self.mv.select("ind", negate=0, only=False, log=0)
        self.assertEqual(self.mv.getSelection().name,['ind'])
        
    def test_select_atoms(self):
        """checks select atoms
        """
        self.mv.setSelectionLevel(Atom)
        self.mv.select(":::C19,H30")
        self.assertEqual(self.mv.getSelection().name,['C19', 'H30'])
    
    def test_select_residues(self):
        """checks select residues
        """
        self.mv.select("ind::", negate=False, only=False, log=0)
        self.assertEqual(self.mv.getSelection().name,['IND201'])
    
    def test_select_chain_valid_input(self):
        """checks select chain valid input
        """
        self.mv.select("ind:", negate=0, only=False, log=0)
        self.assertEqual(len(self.mv.selection),1)

        
    def test_select_mol_valid_input(self):
        """checks select molecule valid input
        """
        self.mv.select("ind", negate=0, only=False, log=0)
        self.assertEqual(len(self.mv.selection),1)


    def test_select_atoms__valid_input(self):
        """checks select atoms valid input
        """
        self.mv.setSelectionLevel(Atom)
        self.mv.select(":::C19,H30")
        self.assertEqual(len(self.mv.selection),2)
    

    def test_select_residues__valid_input(self):
        """checks select residues valid input
        """
        self.mv.select("ind::", negate=False, only=False, log=0)
        self.assertEqual(len(self.mv.selection),1)


    def test_select_chain_invalid_input(self):
        """checks select chain invalid input
        """
        self.mv.select("indtauer:", negate=0, only=False, log=0)
        self.assertEqual(len(self.mv.selection),0)
        
    def test_select_mol_invalid_input(self):
        """checks select molecule invalid input
        """
        self.mv.select("indyiuyf", negate=0, only=False, log=0)
        self.assertEqual(len(self.mv.selection),0)
        
    def test_select_atoms_invalid_input(self):
        """checks select atoms invalid input
        """
        self.mv.select("ind:Z:IND20623841:C19,H30", negate=False, only=False, log=0)
        self.assertEqual(len(self.mv.selection),0)
        
    def test_select_residues_invalid_input(self):
        """checks select residues invalid input
        """
        self.mv.select("indtrrew::", negate=False, only=False, log=0)
        self.assertEqual(len(self.mv.selection),0)
    
    def test_select_atoms_negate(self):
        """checks select atoms negate = true
        """
        self.mv.setSelectionLevel(Atom)
        self.mv.select(":::C19,H30", negate=True, only=False, log=0)
        self.assertEqual(len(self.mv.selection),0)
    
    def test_select_atoms_only(self):
        """checks select atoms only = true
        """
        self.mv.setSelectionLevel(Atom)
        self.mv.select(":::C19,H30", negate=False, only=True)
        self.assertEqual(len(self.mv.selection),2)

         
#end select command        


class Pmvselection_DeSelectTests(Pmvselection_BaseTests):
#deselect command

    def test_deselect_atoms(self):
        """checks deselect command
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        oldatoms=mv.getSelection().name
        c1=self.mv.deselect
        c1(mv.allAtoms[:3])
        newatoms=mv.getSelection().name
        self.assertEqual(oldatoms!=newatoms,True)
        
    def test_deselect_valid_input(self):
        """checks valid input for deselect command
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        c1=self.mv.deselect
        c1(mv.allAtoms[:3])
        self.assertEqual(len(self.mv.selection),0)

    def test_deselect_select(self):
        """ checks deselect with valid input through select command
        """
        c=self.mv.deselect
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.deselect(mv.allAtoms)
        self.mv.select("ind:I:IND201:C19,H30", negate=False, only=False, log=0)
        c(self.mv.getSelection())
        self.assertEqual(len(self.mv.selection),0)
    
    def test_deselect_valid_input_direct_select(self):
        """checks deselect with valid input through direct select
        """
        c1=self.mv.directSelect                         
        c1('ind')
        all=self.mv.getSelection().name
        c=self.mv.deselect
        c(self.mv.getSelection())
        self.assertEqual(len(mv.selection),0)

    def test_deselect_valid_input_select_from_string(self):
        """checks deselect,valid input through select from string
        """
        c=self.mv.deselect
        self.mv.selectFromString(mols='ind',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        all=self.mv.getSelection().name
        c(self.mv.getSelection())
        self.assertEqual(len(self.mv.selection),0)

    
#end deselect command


class Pmvselection_ClearSelectTests(Pmvselection_BaseTests):
#clearSelection


    def test_clearselection_atoms(self):
        """checks clear selection command
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        oldatoms=mv.getSelection().name
        c1=self.mv.clearSelection
        c1()
        newatoms=mv.getSelection().name
        self.assertEqual(oldatoms!=newatoms,True)
        
    def test_clearselection_atoms_valid_input(self):
        """checks clear selection command valid input
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        oldatoms=mv.getSelection().name
        c1=self.mv.clearSelection
        c1()
        self.assertEqual(len(self.mv.selection),0)
    
    def test_clear_selection_select(self):
        """ checks clear selection with valid input through select command
        """
        c=self.mv.clearSelection
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.select("ind:I:IND201:C19,H30", negate=False, only=False, log=0)
        c()
        self.assertEqual(len(self.mv.selection),0)
    
    def test_clear_selection_valid_input_direct_select(self):
        """checks clear selection  with valid input through direct select
        """
        c1=self.mv.directSelect                      
        c1('ind')
        all=self.mv.getSelection().name
        c=self.mv.clearSelection 
        c()
        self.assertEqual(len(mv.selection),0)

    def test_clear_selection_valid_input_select_from_string(self):
        """checks clear selection,valid input through select from string
        """
        c=self.mv.clearSelection
        self.mv.selectFromString(mols='ind',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        all=self.mv.getSelection().name
        c()
        self.assertEqual(len(self.mv.selection),0)

    

#end clearSelection

class Pmvselection_CreateSetIfNeededSelectTests(Pmvselection_BaseTests):
#create set if needed


    def test_create_set_if_needed_atoms(self):
        """checks create set if needed
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        c1=self.mv.createSetIfNeeded
        c1(mv.allAtoms[:3],"set1")
        self.assertEqual(self.mv.sets.keys()[0],'set1')
        self.assertEqual(self.mv.sets.values()[0],mv.allAtoms[:3])
        self.assertEqual(self.mv.sets.values()[0].name,mv.allAtoms[:3].name)

    def test_create_set_if_needed_select(self):
        """checks create set if needed ,input through get selection
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        c1=self.mv.createSetIfNeeded
        c1(mv.getSelection(),"set2")
        self.assertEqual(self.mv.sets.keys()[0],'set2')
        self.assertEqual(self.mv.sets.values()[0],mv.allAtoms[:3])
        self.assertEqual(self.mv.sets.values()[0].name,mv.allAtoms[:3].name)
    
    def test_create_set_if_needed_number_of_sets(self):
        """checks create set if needed when called twice with different sets,number of sets in self.mv.sets
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        c1=self.mv.createSetIfNeeded
        c1(mv.getSelection(),"set3")
        c1(mv.getSelection(),"set4")
        self.assertEqual(len(self.mv.sets.keys()),2)
        
        
    def test_selection_createSetIfNeeded_keys_select(self):
        """checks createSetIfNeeded the  is added to self.mv.sets,number of atms ,atom names using select
        """
        c=self.mv.createSetIfNeeded
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.select("ind:I:IND201:C19,H30", negate=False, only=False, log=0)
        c(self.mv.getSelection(),'set5')
        self.assertEqual(self.mv.sets.keys()[0],'set5')
        self.assertEqual(self.mv.sets.values()[0],mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name,mv.getSelection().name)
    
    def test_selection_createSetIfNeeded_keys_select_from_string(self):
        """checks the createSetIfNeeded is added to self.mv.sets,number of atms ,atom names using select from string
        """
        c=self.mv.createSetIfNeeded

        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString(mols='ind',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        c(self.mv.getSelection(),'set6')
        self.assertEqual(self.mv.sets.keys()[0],'set6')
        self.assertEqual(self.mv.sets.values()[0],mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name,mv.getSelection().name)
    
    def test_selection_createSetIfNeeded_keys_direct_select(self):
        """checks the createSetIfNeeded is added to self.mv.sets,number of atms ,atom names using direct select        """
        c=self.mv.createSetIfNeeded
        self.mv.directSelect('ind')
        c(self.mv.getSelection(),'set7')
        self.assertEqual(self.mv.sets.keys()[0],'set7')
        self.assertEqual(self.mv.sets.values()[0],mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name,mv.getSelection().name)

        

#end create set if needed
    
    
class Pmvselection_SelectSetSelectTests(Pmvselection_BaseTests):
#select Set
#widget entries cannot be tested since buildForm() is absent
    
    def test_selectSet_atoms(self):
        """checks select set 
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        c1=self.mv.createSetIfNeeded
        c1(mv.selection,"set10")
        #c1(mv.getSelection(),"set10")
        c2=self.mv.selectSet
        c2("set10")
        self.assertEqual(self.mv.sets.keys()[0],'set10')
        self.assertEqual(self.mv.sets.values()[0],mv.allAtoms[:3])
        self.assertEqual(self.mv.sets.values()[0].name,mv.allAtoms[:3].name)

    
    def test_selectSet_number_of_sets(self):
        """checks select set when called ,number of sets in self.mv.sets
        """
        c=self.mv.select
        c(mv.allAtoms[:3])
        c1=self.mv.createSetIfNeeded
        c1(mv.selection,"set11")
        #c1(mv.getSelection(),"set11")
        c2=self.mv.selectSet
        c2("set11")
        self.assertEqual(len(self.mv.sets.keys()),1)

    
    def test_selectSet_keys_select(self):
        """checks  selectSet the  is added to self.mv.sets,number of atms ,atom names using select
        """
        c=self.mv.createSetIfNeeded
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.select("ind:I:IND201:C19,H30", negate=False, only=False, log=0)
        c(self.mv.selection,'set12')
        c2=self.mv.selectSet
        c2("set12")
        self.assertEqual(self.mv.sets.keys()[0],'set12')
        self.assertEqual(self.mv.sets.values()[0],mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name,mv.getSelection().name)
    
    def test_select_set_keys_select_from_string(self):
        """checks the createSetIfNeeded is added to self.mv.sets,number of atms ,atom names using select from string
        """
        c=self.mv.createSetIfNeeded
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString(mols='ind',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        c(self.mv.selection,'set13')
        c2=self.mv.selectSet
        c2("set13")
        self.assertEqual(self.mv.sets.keys()[0],'set13')
        self.assertEqual(self.mv.sets.values()[0],mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name,mv.getSelection().name)
    
    def test_select_set_direct_select(self):
        """checks the createSetIfNeeded is added to self.mv.sets,number of atms ,atom names using direct select        """
        c=self.mv.createSetIfNeeded
        self.mv.directSelect('ind')
        c(self.mv.selection,'set14')
        c2=self.mv.selectSet
        c2("set14")
        self.assertEqual(self.mv.sets.keys()[0],'set14')
        self.assertEqual(self.mv.sets.values()[0],mv.getSelection())
        self.assertEqual(self.mv.sets.values()[0].name,mv.getSelection().name)

        

    def test_select_set_keys_saveset(self):
        """checks the select set input through saved set
        """
        c=self.mv.saveSet
        c(mv.allAtoms[:3],'set15',comments='NoDescription')
        c2=self.mv.selectSet
        c2("set15")
        self.assertEqual(self.mv.sets.keys()[0],'set15')
        self.assertEqual(self.mv.sets.values()[0],mv.allAtoms[:3])
        self.assertEqual(self.mv.sets.values()[0].name,mv.allAtoms[:3].name)



    def test_select_set_backbone(self):
        mol = self.mv.Mols[3]
        res = mol.chains.residues
        bb = res.atoms.get(lambda x:x.name =='C' or x.name == 'O' or x.name =='CA' or x.name =='N' or x.name=='HA')
        self.mv.select(bb)
        c1=self.mv.createSetIfNeeded
        c1(mv.selection,"backbone")
        self.assertEqual(self.mv.sets.keys()[0],'backbone')
        self.assertEqual(self.mv.sets.values()[0],bb)


    def test_select_set_sidechain(self):
        mol = self.mv.Mols[3]
        res = mol.chains.residues
        sc = res.atoms.get(lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name!='HA')
        self.mv.select(sc)
        c1=self.mv.createSetIfNeeded
        c1(mv.selection,"sidechain")
        self.assertEqual(self.mv.sets.keys()[0],'sidechain')
        self.assertEqual(self.mv.sets.values()[0],sc)
    

#end select set


class Pmvselection_DirectSelectTests(Pmvselection_BaseTests):
#direct select

    
    def test_direct_select_valid_input(self):
        """checks direct select with valid input
        """
        c=self.mv.directSelect                         
        c('ind')
        self.assertEqual(self.mv.getSelection().name,['ind'])

        
    def test_direct_select_widget(self):
        """checks direct select widget displays
        """
        if self.mv.hasGui:
            c=self.mv.directSelect                         
            c('ind')
            c.buildForm()
            self.assertEqual(c.ifd.form.root.winfo_exists(),1)
            c.Dismiss_cb()
    

    def test_direct_select_widget_chain_list(self):
        """checks direct select chain_list
        """
        c=self.mv.directSelect
        c('ind')
        #c.buildForm()
        self.mv.setSelectionLevel(Chain, KlassSet=None, log=0)
        self.assertEqual(self.mv.getSelection().name,['I'])
        #c.Dismiss_cb()
 

    def test_direct_select_invalid_input1(self):
        """checks direct select with invalid input  with non-existent
        string
        """
        self.mv.selectFromString(' ',chains='',res='',atoms='',
                                 negate=False, silent=True,log=0)
        c=self.mv.directSelect                         
        c('ind2')
        self.assertEqual(len(mv.selection),0)


    def test_direct_select_invalid_input2(self):
        """checks direct select with invalid input number
        """
        self.mv.selectFromString(' ',chains='',res='',atoms='',
                                 negate=False, silent=True,log=0)
        c=self.mv.directSelect                         
        c('2322')
        self.assertEqual(len(mv.selection),0)    

    def test_direct_select_empty_input(self):
        """checks direct select with empty input 
        """
        self.mv.selectFromString(' ',chains='',res='',atoms='',
                                 negate=False, silent=True,log=0)
        c=self.mv.directSelect                         
        c(' ')
        self.assertEqual(len(mv.selection),0)    
    
    def test_direct_select_mollist_name(self):
        """checks  deselecting through Mollist button
        """
        if not self.mv.hasGui: return
        c=self.mv.directSelect                         
        c('1crn_hs')
        c.buildForm()
        c.ifd.entryByName['Show SelSpheres']['widget'].select()
        c.ifd.entryByName['Show SelSpheres']['wcfg']['command']()
        mb=c.ifd.entryByName['Mol List']['widget']
        c.buildMolMenus()
        menu=mb.menu
        menu.invoke(1)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.assertEqual(len(self.mv.selection), 0)
        c.Dismiss_cb()


    def XXXtest_direct_select_chain_list_name(self):
        """checks direct select all atoms are selected through chain list
        command 
        """
        c=self.mv.directSelect                         
        c('ind')
        c.buildForm()
        c.ifd.entryByName['Show SelSpheres']['widget'].select()
        c.ifd.entryByName['Show SelSpheres']['wcfg']['command']()
        mb=c.ifd.entryByName['Chain List']['widget']
        c.buildChainMenus()
        menu=mb.menu
        menu.invoke(1)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.assertEqual(len(self.mv.selection), 419)
        c.Dismiss_cb()
   

    def XXXtest_direct_select_setlist_name(self):
        """checks direct select all atoms are selected through sets  list button
        """
        c=self.mv.directSelect                         
        c('ind')
        self.mv.saveSet(self.mv.selection,'atsset22',comments='NoDescription')

        c.buildForm()
        c.ifd.entryByName['Show SelSpheres']['widget'].select()
        c.ifd.entryByName['Show SelSpheres']['wcfg']['command']()
        mb=c.ifd.entryByName['Sets List']['widget']
        c.buildSetsMenus()
        menu=mb.menu
        menu.invoke(1)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.assertEqual(len(self.mv.selection), len(mv.Mols[0].allAtoms))
        c.Dismiss_cb()

    
#end directselect

class Pmvselection_SelectInSphereSelectTests(Pmvselection_BaseTests):
#select In Sphere
    def test_select_in_sphere(self):
        """checks select in spheres selected spheres with radius 1.0
        (selects only atoms at centers)
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=self.mv.allAtoms
        c(mv.allAtoms[:3].coords,1.,['all'])
        self.assertEqual(len(mv.selection), 3)

    def test_select_in_sphere_valid_input(self):
        """checks select in sphere with radius 2.0 (selects bonded atoms)
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=mv.selectInSphere
        all=mv.allAtoms
        c(mv.allAtoms[:3].coords,2.,['all'])
        self.assertEqual(len(mv.selection),10)

    def test_select_in_sphere_invalid_input(self):
        """checks select in sphere with invalid input centerList
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=mv.selectInSphere
        all=mv.allAtoms
        c(mv.allAtoms[5000:5234].coords,1.,['all'])
        self.assertEqual(len(mv.selection),0)

    def test_select_in_sphere_widget(self):
        """checks select in sphere widget displays
        """
        if not self.mv.hasGui: return
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        c.buildForm()
        c(mv.allAtoms[1].coords,1.,['all'])
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        c.clear_cb()
        c.close_cb()

    def test_select_in_sphere_an_atom_entry(self):
        """checks select in sphere an atom in widget
        """
        if not self.mv.hasGui: return
        
        self.mv.setSelectionLevel(Atom)
        c=self.mv.selectInSphere
        all=self.mv.allAtoms
        c.buildForm()
        ats=self.mv.select("ind:I:IND201:C1")
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        self.assertEqual(round(c.selSph.vertexSet.vertices.array[0][0]),round(self.mv.allAtoms[1].coords[0]))
        self.assertEqual(round(c.selSph.vertexSet.vertices.array[0][1]),round(self.mv.allAtoms[1].coords[1]))
        self.assertEqual(round(c.selSph.vertexSet.vertices.array[0][2]),round(self.mv.allAtoms[1].coords[2]))
        c.clear_cb()
        c.close_cb()

    def test_select_in_sphere_cur_selection_entry(self):
        """checks select in sphere cur selection  in widget
        """
        if not self.mv.hasGui: return
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        c(mv.allAtoms[:3].coords,1.,['all'])
        c.buildForm()
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),len(mv.getSelection()))
        c.clear_cb()
        c.close_cb()


    def test_select_in_sphere_rad_slider_entry(self):
        """checks select in sphere radius slider in widget
        """
        if not self.mv.hasGui: return
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        c(mv.allAtoms[:3].coords,5.,['all'])
        c.buildForm()
        c.ifd.entryByName['radSlider']['widget'].set(1.0)
        self.assertEqual(c.selSph.radius,5.0)
        c.clear_cb()
        c.close_cb()
        
    def test_select_in_sphere_all_molecules_cursel(self):
        """checks select in sphere all molecules, curselection in widget
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)

        c(ats._coords,1.,['all'])
        c.buildForm()
        c.ifd.entryByName['allRB']['widget'].select()
        c.ifd.entryByName['allRB']['wcfg']['command']()
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array), 12)
        c.clear_cb()
        c.close_cb()
        

    def test_select_in_sphere_all_molecules_atom(self):
        """checks select in sphere all molecules,  atom in widget
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats[3]._coords,1.,['all'])
        c.buildForm()
        c.ifd.entryByName['allRB']['widget'].select()
        c.ifd.entryByName['allRB']['wcfg']['command']()
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),1)
        c.clear_cb()
        c.close_cb()

    def test_select_in_sphere_all_molecules_names(self):
        """checks select in sphere all molecules names
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats._coords,1.,['all'])
        c.buildForm()
        c.ifd.entryByName['allRB']['widget'].select()
        c.ifd.entryByName['allRB']['wcfg']['command']()
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.assertEqual(mv.getSelection().name,['ind','test_aa', '1crn_hs', '1CRN','barrel_1'])
        c.clear_cb()
        c.close_cb()

    def test_select_in_sphere_from_list_entry1_atom(self):
        """checks select in sphere :atom,fromlist,1 st mol selected;
           vertexset length =1
        """
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats[3]._coords,1.,['all'])
        c.buildForm()
        #from list button
        c.selectionBase.set("fromList")
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        c.updateBase()
        #cur sel Button
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),1)
        c.clear_cb()
        c.close_cb()
    
    def test_select_in_sphere_from_list_entry_2_atom(self):
        """checks select in sphere atom,from list, 2 ndmol selected;
           vertexset length =1

        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)

        c(ats[7]._coords,1.,['all'])
        c.buildForm()
        #from list button
        c.selectionBase.set("fromList")
        
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        c.updateBase()
        #cur sel Button
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),1)
        c.clear_cb()
        c.close_cb()

    def test_select_in_sphere_from_list_entry2_cur_selection(self):
        """checks select in sphere : cursel,from list, 2 ndmol selected;
           vertexset length >1
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats._coords,1.,['all'])
        c.buildForm()
        #from list button
        c.selectionBase.set("fromList")
        
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        c.updateBase()
        #cursel Button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),4)
        c.clear_cb()
        c.close_cb()
    
    def test_select_in_sphere_from_list_entry1_cur_selection(self):
        """checks select in sphere :cursel,from list, 1 st mol selected;
           vertexset length >1

        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30", negate=False)
        c(ats._coords,1.,['all'])
        c.buildForm()
        #from list button
        c.selectionBase.set("fromList")
        
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('0')
        c.updateBase()
        #cur sel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),8)
        c.clear_cb()
        c.close_cb()


    def test_select_in_sphere_from_list_entry2_select(self):
        """checks select in sphere :cursel,from list, 2 ndmol selected,select;
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats[7]._coords,1.,['all'])
        c.buildForm()
        #from list button
        c.selectionBase.set("fromList")
        
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        #an atom button
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        len_verts_old=len(c.selSph.vertexSet.vertices.array)
        c.selList=['barrel_1']
        #select button
        c.select_cb()
        #cur sel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        len_verts_new=len(c.selSph.vertexSet.vertices.array)
        self.assertEqual(len_verts_old<len_verts_new,True)
        c.clear_cb()
        c.close_cb()

    def test_select_in_sphere_from_list_entry2_mol_name_select(self):
        """checks select in sphere :cursel,from list,  2 nd  mol
        selected,select : selected molecules name************
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats._coords,1.,['all'])
        c.buildForm()
        #from list button command
        c.selectionBase.set("fromList")
        
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        #cur sel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        c.selList=['barrel_1']
        #select button
        c.select_cb()
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.assertEqual(mv.getSelection().name,['barrel_1'])
        c.clear_cb()
        c.close_cb()
    
    def test_select_in_sphere_from_list_entry1_mol_name_select(self):
        """checks select in sphere :cursel,from list,  1st  mol
        selected,select : selected molecules name**********************
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats._coords,1.,['all'])
        c.buildForm()
        #from list button command
        c.selectionBase.set("fromList")
        c.ifd.entryByName['selectionRB']['widget'].select()
        
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        #cursel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        c.selList=['ind']
        #select button
        c.select_cb()
        #cursel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.assertEqual(mv.getSelection().name,['ind'])
        c.clear_cb()
        c.close_cb()



    def test_select_in_sphere_from_list_entry1_select(self):
        """checks select in sphere:atom,from list,  1st  mol
        selected,select,cursel

        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        c(ats[3]._coords,1.,['all'])
        c.buildForm()
        #from list button command
        c.selectionBase.set("fromList")
        
        c.oldval='set'
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('0')
        #an atom button
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        len_verts_old=len(c.selSph.vertexSet.vertices.array)
        c.selList=['ind']

        #select button
        c.select_cb()
        #cursel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        len_verts_new=len(c.selSph.vertexSet.vertices.array)
        self.assertEqual(len_verts_old<len_verts_new,True)
        c.clear_cb()
        c.close_cb()
        



    def test_select_in_sphere_save_set_atom(self):
        """checks select in sphere :all molecules,save set,atoms
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:H30", negate=False, only=False, log=0)
        self.mv.saveSet(self.mv.getSelection(),'newset5',comments='NoDescription')
        c(ats._coords,1.,['all'])
        c.buildForm()
        #an atom button
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),1)
        self.mv.sets={}
        c.selList=[]
    
    def test_select_in_sphere_save_set_cur_selection(self):
        """checks select in sphere :all molecules,save set,cur sel
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30", negate=False, only=False, log=0)
        self.mv.saveSet(self.mv.getSelection(),'newset4',comments='NoDescription')

        c(ats._coords,1.,['all'])
        c.buildForm()
        #an atom button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),8)
        self.mv.sets={}
        c.selList=[]
        
    def test_select_in_sphere_save_set_list_selection(self):
        """checks select in sphere using saved set option
        """
        self.mv.setSelectionLevel(Atom)
        c=self.mv.selectInSphere
        ats=self.mv.select("ind:I:IND201:H30")
        self.mv.saveSet(self.mv.getSelection(),'newset0',comments='NoDescription')
        c(ats._coords,1.,['all'])
        #an atom button
        c.buildForm()
        c.ifd.entryByName['atomRB']['widget'].select()
        c.ifd.entryByName['atomRB']['wcfg']['command']()
        #from list button command
        c.selectionBase.set("set")
        c.oldval='something'
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('0')
        #cur sel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.assertEqual(len(c.selSph.vertexSet.vertices.array),1)
        c.clear_cb()
        c.close_cb()
        self.mv.sets={}
        c.selList=[]

    def test_select_in_sphere_saved_set_entry2_mol_name_select(self):
        """checks select in sphere :cursel,saved set,  2 nd  mol
        selected,select : selected molecules name
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
       
        self.mv.saveSet(self.mv.getSelection(),'newset1',comments='NoDescription')
        c(ats._coords,1.,['all'])
        c.buildForm()
        #from list button command
        c.selectionBase.set("set")
        c.oldval='something'
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        #cur sel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        c.selList=['barrel_1']
        #select button
        c.select_cb()
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.assertEqual(mv.getSelection().name,['barrel_1'])
        c.clear_cb()
        c.close_cb()
        self.mv.sets={}
        c.selList=[]
        
    def test_select_in_sphere_saved_set_entry1_mol_name_select(self):
        """checks select in sphere :cursel,saved set,  1st  mol
        selected,select : selected molecules name
        """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        c=self.mv.selectInSphere
        all=mv.allAtoms
        ats=self.mv.select("ind:I:IND201:C15;ind:I:IND201:C19;ind:I:IND201:C20;ind:I:IND201:H24;ind:I:IND201:H30;barrel_1: :ALA55:HB1;barrel_1: :ALA57:CB;barrel_1: :ALA57:HB1;barrel_1: :ALA57:HB3", negate=False)
        self.mv.saveSet(self.mv.getSelection(),'newset2',comments='NoDescription')
        c(ats._coords,1.,['all'])
        c.buildForm()
        #from list button command
        c.selectionBase.set("fromList")
        c.ifd.entryByName['selectionRB']['widget'].select()
        c.oldval='something'
        c.updateBase()
        c.ifd.entryByName['baseMols']['widget'].lb.selection_set('1')
        #cursel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        c.selList=['ind']
        #select button
        c.select_cb()
        #cursel button
        c.ifd.entryByName['curselRB']['widget'].select()
        c.ifd.entryByName['curselRB']['wcfg']['command']()
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.assertEqual(mv.getSelection().name,['ind'])
        c.clear_cb()
        c.close_cb()
        c.selList=[]
        self.mv.sets={}
        
        




class Pmvselection_SelectFromStringSelectTests(Pmvselection_BaseTests):
#selection From String

#xor
    def test_SelectFromStringBackboneXorN(self):
        """
        select BackboneXorN
        """
        self.mv.selectFromString(mols='',chains='',res='',atoms='backbone',
                                 silent=True,log=0)
        self.mv.selectFromString(mols='',chains='',res='',atoms='N',
                                 xor=True, silent=True,log=0)
        #46residues of backbone atoms - N=>3*46 or 138
        self.assertEqual(len(self.mv.selection), 357)

#intersect
    def test_SelectFromStringBackboneIntersectALA(self):
        """
        select BackboneIntersectALA
        """
        self.mv.selectFromString(mols='',chains='',res='',atoms='backbone',
                                 silent=True,log=0)
        #ala_ats = self.mv.allAtoms.get("ALA*").atoms
        self.mv.selectFromString(mols='',chains='',res='ALA*',atoms='*',
                                 intersect=True, silent=True,log=0)
        #46residues of backbone atoms=>4*46 or 184
        self.assertEqual(len(self.mv.selection), 52)


    def test_SelectFromStringALA_CXorBackbone(self):
        """
        select Ala_CXorBackbone
        """
        self.mv.selectFromString(mols='',chains='',res='ALA*',atoms='C?',
                                 silent=True,log=0)
        self.mv.selectFromString(mols='',chains='',res='',atoms='backbone',
                                 xor=True, silent=True,log=0)
        #5ALA residuesCatoms(15atoms)xorbackbone atoms(184) =>184-5=>179
        self.assertEqual(len(self.mv.selection), 463)

#subtract
    def test_SelectFromStringALASubtractBackbone(self):
        """
        select AlaSubtractBackbone
        """
        self.mv.selectFromString(mols='',chains='',res='ALA*',atoms='*',
                                 silent=True,log=0)
        self.mv.selectFromString(mols='',chains='',res='',atoms='backbone',
                                 negate=True, silent=True,log=0)
        #5ALA residues(50atoms)- backbone atoms(20) =>30
        self.assertEqual(len(self.mv.selection), 49)


    def test_SelectFromStringMolByName(self):
        """
        select Mol by Name
        """
        self.mv.selectFromString('1crn_hs',chains='',res='',atoms='',
                                 negate=False, silent=True,log=0)
        self.assertEqual(len(self.mv.selection),1)

    def test_SelectFromStringMolByName1(self):
        """
        select Mol by Name from mmcif
        """
        self.mv.selectFromString('1CRN',chains='',res='',atoms='',
                                 negate=False, silent=True,log=0)
        self.assertEqual(len(self.mv.selection),1)


    def test_SelectFromStringFailMolByName(self):
        """
        fail select Mol by Name
        """
        self.mv.selectFromString('bdna',chains='',res='',atoms='',
                                 negate=0, silent=True,log=0)
        self.assertEqual(len(self.mv.selection),0)


    def test_SelectFromStringChains_proteic(self):
        """
        select proteic Chains
        """
        #chain
        self.mv.selectFromString(mols='',chains='proteic',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(self.mv.selection), 5)


    def test_SelectFromStringChains_dna(self):
        """
        select dna Chains
        """
        self.mv.selectFromString(mols='',chains='dna',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(self.mv.selection),1)



    def test_SelectFromStringAllChains(self):
        """
        select all Chains
        """
        #chain
        #mv.setSelectionLevel(Chain)
        self.mv.selectFromString(mols='',chains='*',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(self.mv.selection), 7)


    def test_SelectFromStringFailChainByName(self):
        """
        fail select Chain by Name
        """
        #to fail:
        mv = self.mv
        mv.selectFromString(mols='',chains='QQ',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    def test_SelectFromStringResByName(self):
        """
        select residue by name
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='THR1',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 3)


    def test_SelectFromStringFailResByName(self):
        """
        fail select residue by name
        """
        #to fail:
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='HIS5',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)



    def test_SelectFromStringAtomByName(self):
        """
        select atom by name
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='N',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 119)


    def test_SelectFromStringFailAtomByName(self):
        """
        fail select atom by name
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='Z',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    def test_SelectFromStringMolByNum(self):
        """
        select mol by number
        """
        mv = self.mv
        mv.selectFromString(mols='0',chains='',res='',atoms='',negate=0, silent=1,log=0)
        #mv.selectFromString(mols='1',chains='',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),1)


    def test_SelectFromStringFailMolByNum(self):
        """
        Fail select mol by number
        """
        mv = self.mv
        mv.selectFromString(mols='20',chains='',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    def test_SelectFromStringChainByNum(self):
        """
        select Chain by number
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='0',res='',atoms='',negate=0, silent=1,log=0)
        #mv.selectFromString(mols='',chains='1',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),1)


    def test_SelectFromStringFailChainByNum(self):
        """
        Fail select Chain by number
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='40',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 0)


    def test_SelectFromStringResByNum(self):
        """
        select Res by number
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='40',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),1)


    def test_SelectFromStringFailResByNum(self):
        """
        Fail select Res by number
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='400',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)
    

    def test_SelectFromStringAtomByNum(self):
        """
        select Atom by number
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='200',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),1)


    def test_SelectFromStringFailAtomByNum(self):
        """
        Fail select Atom by number
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='4000',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    #by range pass on mol, chain
    def test_SelectFromStringResByRange(self):
        """
        select Res by Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='1-10',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),10)


    def test_SelectFromStringFailResByRange(self):
        """
        Fail select Res by Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='500-530',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    def test_SelectFromStringAtomByRange(self):
        """
        select Atom by Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='1-10',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),10)


    def test_SelectFromStringFailAtomByRange(self):
        """
        Fail select Atom by Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='475463524-4344526',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    #by relative range: pass on mol, chain
    def test_SelectFromStringResByRelativeRange(self):
        """
        select Res by Relative Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='#1-#10',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 40)


    def test_SelectFromStringFailResByRelativeRange(self):
        """
        Fail select Res by Relative Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='#50-#53',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    #atom
    def test_SelectFromStringAtomByRelativeRange(self):
        """
        select Atom by Relative Range
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='#1-#10',negate=0, silent=1,log=0)
        #NB this selects first 10 in each residue...
        self.assertEqual(len(mv.selection), 730)


    #by resSeq
    def test_SelectFromStringResBySeq(self):
        """
        select Res by Sequence
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='PEA',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 6)


    #mol
    def test_SelectFromStringFailResByMolSeq(self):
        """
        Fail select Res by Mol Sequence
        """
        mv = self.mv
        mv.selectFromString(mols='PEA',chains='',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    #chain
    def test_SelectFromStringFailResByChainSeq(self):
        """
        Fail select Res by Chain Sequence
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='PEA',res='',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)


    #residue
    def test_SelectFromStringResBySeq_2(self):
        """
        select Res by Sequence 2
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='TTCC',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 8)


    def test_SelectFromStringFailResBySeq(self):
        """
        Fail select Res by Sequence 
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='PYT',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)

    def test_SelectFromStringResBySeq_3(self):
        """
        select Res by Sequence 3
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='TTCC,PYT,PEA',atoms='',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection), 14)

    #atom
    def test_SelectFromStringFailResByAtomSeq(self):
        """
        Fail select Res by Atom Sequence
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='PEA',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)

    def test_select_from_string_sidechain_ALA(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='ALA*', atoms='sidechain')
        res_ala = self.mv.Mols.chains.residues.get(lambda x:x.type =='ALA')
        scatoms_ala =res_ala.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='HA'))
        self.assertEqual(scatoms_ala,self.mv.getSelection())


    def test_select_from_string_sidechain_ARG(self):
        
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='ARG*',atoms='sidechain')
        res_arg = self.mv.Mols.chains.residues.get(lambda x:x.type =='ARG')
        scatoms_arg =res_arg.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='HA')) 
        self.assertEqual(scatoms_arg,self.mv.getSelection())
            
            
    def test__select_from_string_sidechain_ASN(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='ASN*',atoms='sidechain')
        res_asn = self.mv.Mols.chains.residues.get(lambda x:x.type =='ASN')
        scatoms_asn = res_asn.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='OXT' and x.name!='HA' and x.name!="\'O\'\'\'"))
        self.assertEqual(scatoms_asn, self.mv.getSelection())
        #self.assertEqual(len(self.mv.getSelection()), 36)
        

    def test_select_from_string_sidechain_ASP(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='ASP*',atoms='sidechain')
        res_asp = self.mv.Mols.chains.residues.get(lambda x:x.type =='ASP')
        scatoms_asp =res_asp.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='HA'))
        self.assertEqual(scatoms_asp,self.mv.getSelection())


    def test_select_from_string_sidechain_CYS(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='CYS*',atoms='sidechain')
        res_cys = self.mv.Mols.chains.residues.get(lambda x:x.type =='CYS')
        scatoms_cys =res_cys.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='HA'))
        self.assertEqual(scatoms_cys,self.mv.getSelection())


    def test_select_from_string_sidechain_GLU(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='GLU*',atoms='sidechain')
        res_glu = self.mv.Mols.chains.residues.get(lambda x:x.type =='GLU')
        scatoms_glu =res_glu.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='HA'))
        self.assertEqual(scatoms_glu,self.mv.getSelection())


    def test_select_from_string_sidechain_GLN(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='GLN*',atoms='sidechain')
        res_gln = self.mv.Mols.chains.residues.get(lambda x:x.type =='GLN')
        scatoms_gln =res_gln.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN' and x.name!='HA'))
        self.assertEqual(scatoms_gln,self.mv.getSelection())



    def test_select_from_string_sidechain_GLY(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='GLY*',atoms='sidechain')
        res_gly = self.mv.Mols.chains.residues.get(lambda x:x.type =='GLY')
        scatoms_gly =res_gly.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        #side chain selects no nonodes 
        


    def test_select_from_string_sidechain_HIS(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='HIS*',atoms='sidechain')
        res_his = self.mv.Mols.chains.residues.get(lambda x:x.type =='HIS')
        scatoms_his =res_his.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_his,self.mv.getSelection())


    def xtest_select_from_string_sidechain_ILE(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='ILE*',atoms='sidechain')
        res_ile = self.mv.Mols.chains.residues.get(lambda x:x.type =='ILE')
        scatoms_ile =res_ile.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_ile,self.mv.getSelection())



    def xtest_select_from_string_sidechain_LEU(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='LEU*',atoms='sidechain')
        res_leu = self.mv.Mols.chains.residues.get(lambda x:x.type =='LEU')
        scatoms_leu =res_leu.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_leu,self.mv.getSelection())


    def xtest_select_from_string_sidechain_LYS(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='LYS*',atoms='sidechain')
        res_lys = self.mv.Mols.chains.residues.get(lambda x:x.type =='LYS')
        scatoms_lys =res_lys.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_lys,self.mv.getSelection())


    def test_select_from_string_sidechain_MET(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='MET*',atoms='sidechain')
        res_met = self.mv.Mols.chains.residues.get(lambda x:x.type =='MET')
        scatoms_met =res_met.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_met,self.mv.getSelection())


    def xtest_select_from_string_sidechain_PHE(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='PHE*',atoms='sidechain')
        res_phe = self.mv.Mols.chains.residues.get(lambda x:x.type =='PHE')
        scatoms_phe =res_phe.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_phe,self.mv.getSelection())



    def xtest_select_from_string_sidechain_PRO(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='PRO*',atoms='sidechain')
        res_pro = self.mv.Mols.chains.residues.get(lambda x:x.type =='PRO')
        scatoms_pro =res_pro.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_pro,self.mv.getSelection())


    def xtest_select_from_string_sidechain_SER(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='SER*',atoms='sidechain')
        res_ser = self.mv.Mols.chains.residues.get(lambda x:x.type =='SER')
        scatoms_ser =res_ser.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_ser,self.mv.getSelection())


    def xtest_select_from_string_sidechain_THR(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='THR*',atoms='sidechain')
        res_thr = self.mv.Mols.chains.residues.get(lambda x:x.type =='THR')
        scatoms_thr =res_thr.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_thr,self.mv.getSelection())


    def test_select_from_string_sidechain_TRP(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='TRP*',atoms='sidechain')
        res_trp = self.mv.Mols.chains.residues.get(lambda x:x.type =='TRP')
        scatoms_trp =res_trp.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_trp,self.mv.getSelection())



    def xtest_select_from_string_sidechain_TYR(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='TYR*',atoms='sidechain')
        res_tyr = self.mv.Mols.chains.residues.get(lambda x:x.type =='TYR')
        scatoms_tyr =res_tyr.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_tyr,self.mv.getSelection())


    def xtest_select_from_string_sidechain_VAL(self):
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        res = self.mv.Mols.chains.residues
        self.mv.selectFromString(mols='',chains='',res='VAL*',atoms='sidechain')
        res_val = self.mv.Mols.chains.residues.get(lambda x:x.type =='VAL')
        scatoms_val =res_val.atoms.get((lambda x:x.name !='C' and  x.name != 'O' and  x.name !='CA' and  x.name !='N' and x.name != 'HN'))
        self.assertEqual(scatoms_val,self.mv.getSelection())


#end selection from string




class Pmvselection_SelectFromStringGUITests(Pmvselection_BaseTests):
    # These tests are added to 'ignore' list in __init__.py if hasGui==False
    def test_SelectFromStringGUIMolByName(self):
        """
        gui: select Mol by Name
        """
        self.mv.setSelectionLevel(Molecule)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        c.ifd.entryByName['molWid']['widget'].insert(0,'1crn_hs')
        c.callDoit()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        c.Dismiss_cb()
        self.assertEqual(len(self.mv.selection),1)
        c.Dismiss_cb()

    def test_SelectFromStringGUIFailMolByName(self):
        """
        gui: fail select GUI Mol by Name
        """
        self.mv.setSelectionLevel(Molecule)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        c.ifd.entryByName['molWid']['widget'].insert(0,'bdna')
        c.callDoit()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIAllChains(self):
        """
        gui: select all Chains
        """
        #chain
        self.mv.setSelectionLevel(Chain)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        c.ifd.entryByName['chainWid']['widget'].insert(0,'*')
        c.callDoit()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),7)
        c.Dismiss_cb()


    def test_SelectFromStringGUIFailChainByName(self):
        """
        gui: fail select Chain by Name
        """
        #to fail:
        self.mv.setSelectionLevel(Chain)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        c.ifd.entryByName['chainWid']['widget'].insert(0,'QQ')
        c.callDoit()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIResByName(self):
        """
        gui: select residue by name
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'THR1')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection), 3)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIResByName(self):
        """
        gui: fail select residue by name
        """
        #to fail:
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'HIS5')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIAtomByName(self):
        """
        gui:select atom by name
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()

        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'N')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection), 119)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIAtomByName(self):
        """
        gui:fail select atom by name
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()

        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'Z')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIMolByNum(self):
        """
        gui:select mol by number
        """
        self.mv.setSelectionLevel(Molecule)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()

        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        c.ifd.entryByName['molWid']['widget'].insert(0,'0')
        #c.ifd.entryByName['molWid']['widget'].insert(0,'1')
        c.callDoit()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),1)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIMolByNum(self):
        """
        gui:Fail select mol by number
        """
        self.mv.setSelectionLevel(Molecule)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        c.ifd.entryByName['molWid']['widget'].insert(0,'20')
        c.callDoit()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIChainByNum(self):
        """
        gui:select Chain by number
        """
        self.mv.setSelectionLevel(Chain)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()

        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        c.ifd.entryByName['chainWid']['widget'].insert(0,'0')
        #c.ifd.entryByName['chainWid']['widget'].insert(0,'1')
        c.callDoit()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),1)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIChainByNum(self):
        """
        gui:Fail select Chain by number
        """
        self.mv.setSelectionLevel(Chain)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        c.ifd.entryByName['chainWid']['widget'].insert(0,'40')
        c.callDoit()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIResByNum(self):
        """
        gui:select Res by number
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'40')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),1)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIResByNum(self):
        """
        gui:Fail select Res by number
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'400')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIAtomByNum(self):
        """
        gui:select Atom by number
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'200')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),1)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIAtomByNum(self):
        """
        gui:Fail select Atom by number
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'4000')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    #by range pass on mol, chain
    def test_SelectFromStringGUIResByRange(self):
        """
        gui:select Res by Range
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'1-10')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),10)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIResByRange(self):
        """
        gui:Fail select Res by Range
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'500-530')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIAtomByRange(self):
        """
        gui:select Atom by Range
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'1-10')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),10)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIAtomByRange(self):
        """
        gui:Fail select Atom by Range
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'4546324-4652314')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    #by relative range: pass on mol, chain
    def test_SelectFromStringGUIResByRelativeRange(self):
        """
        gui:select Res by Relative Range
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'#1-#10')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection), 40)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIResByRelativeRange(self):
        """
        gui:Fail select Res by Relative Range
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'#50-#53')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    #atom
    def test_SelectFromStringGUIAtomByRelativeRange(self):
        """
        gui:select Atom by Relative Range
        """
        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'#1-#10')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        #NB this selects first 10 atoms in each residue...
        self.assertEqual(len(self.mv.selection), 730)
        c.Dismiss_cb()


    #by resSeq
    def test_SelectFromStringGUIResBySeq(self):
        """
        gui:select Res by Sequence
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'PEA')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection), 6)
        c.Dismiss_cb()

    #mol
    def test_SelectFromStringFailGUIResByMolSeq(self):
        """
        gui:Fail select Res by Mol Sequence
        """
        self.mv.setSelectionLevel(Molecule)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        c.ifd.entryByName['molWid']['widget'].insert(0,'PEA')
        c.callDoit()
        c.ifd.entryByName['molWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    #chain
    def test_SelectFromStringFailGUIResByChainSeq(self):
        """
        gui:Fail select Res by Chain Sequence
        """
        self.mv.setSelectionLevel(Chain)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        c.ifd.entryByName['chainWid']['widget'].insert(0,'PEA')
        c.callDoit()
        c.ifd.entryByName['chainWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    #residue
    def test_SelectFromStringGUIResBySeq_2(self):
        """
        gui:select Res by Sequence 2
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'TTCC')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection), 8)
        c.Dismiss_cb()

    def test_SelectFromStringFailGUIResBySeq(self):
        """
        gui:Fail select Res by Sequence 
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'PYT')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()

    def test_SelectFromStringGUIResBySeq_3(self):
        """
        gui:select Res by Sequence 3
        """
        self.mv.setSelectionLevel(Residue)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        c.ifd.entryByName['resWid']['widget'].insert(0,'TTCC,PYT,PEA')
        c.callDoit()
        c.ifd.entryByName['resWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection), 14)
        c.Dismiss_cb()

    #atom
    def test_SelectFromStringFailGUIResByAtomSeq(self):
        """
        gui:Fail select Res by Atom Sequence
        """
        mv = self.mv
        mv.selectFromString(mols='',chains='',res='',atoms='PEA',negate=0, silent=1,log=0)
        self.assertEqual(len(mv.selection),0)

        self.mv.setSelectionLevel(Atom)
        c = self.mv.selectFromString
        #c.guiCallback()
        c.buildForm()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        c.ifd.entryByName['atomWid']['widget'].insert(0,'PEA')
        c.callDoit()
        c.ifd.entryByName['atomWid']['widget'].delete(0,'end')
        self.assertEqual(len(self.mv.selection),0)
        c.Dismiss_cb()



#######Log Tests
class SelectionLogTests(Pmvselection_BaseTests):
    def test_selection_save_set_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        ats = self.mv.allAtoms[:3]
        self.mv.saveSet(ats,'atsset1',comments='NoDescription')
        #self.mv.saveSet(ats,'atsset1',comments='NoDescription', log=0)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        sst = 'self.saveSet("ind,test_aa,barrel_1,1crn_hs,1CRN,dnaexple:::0-2", \'atsset1\', log=0, comments=\'NoDescription\')' 
        self.assertEqual(split(last_entry,'\n')[0], sst)


    def test_selection_save_set_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        ats = self.mv.allAtoms[:3]
        oldself = self
        self =mv
        #s ="self.saveSet(ats,'atsset1',comments='NoDescription', log=0)"
        s = 'self.saveSet("ind:::0-2", \'atsset1\', log=0, comments=\'NoDescription\')'
        exec(s)
        oldself.assertEqual(1,1)
        
        
    def XXXtest_selection_invert_Selection_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.setSelectionLevel(Atom)
        self.mv.select("ind:I:IND201:C19,H30")
        all=self.mv.selection.name
        self.mv.invertSelection('all')
        if not self.mv.hasGui: return
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],"self.invertSelection('all', log=0)")
    
    def XXXtest_selection_invert_Selection_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.setSelectionLevel(Atom)
        self.mv.select("ind:I:IND201:C19,H30")
        all=self.mv.selection.name
        oldself=self
        self =mv
        s = "self.invertSelection('all', log=0)"
        exec(s)
        oldself.assertEqual(1,1)

    def test_select_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        #self.mv.select(mv.allAtoms[:3])
        self.mv.selectSet('set1')
        if not self.mv.hasGui: return
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        sst = "self.selectSet('set1', negate=False, only=False, log=0)"
        self.assertEqual(split(last_entry,'\n')[0], sst)
        

    def test_select_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s  = 'self.select("ind:::0-2", negate=False, only=False, xor=False, log=0, intersect=False)'
        exec(s)
        oldself.assertEqual(1,1)


    def test_deselect_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.select(mv.allAtoms[:3])
        self.mv.deselect(mv.allAtoms[:3])
        if not self.mv.hasGui: return
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        sst='self.select("ind,test_aa,barrel_1,1crn_hs,1CRN,dnaexple:::0-2", negate=1, only=False, xor=False, log=0, intersect=False)' 
        self.assertEqual(split(last_entry,'\n')[0],sst)

        
    def test_deselect_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.select(mv.allAtoms[:3])
        oldself=self
        self =mv
        s = 'self.select("ind:::0-2", negate=1, only=False, xor=False, log=0, intersect=False)'
        exec(s)
        oldself.assertEqual(1,1)
    
    def test_clearselection_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        c=self.mv.select
        c(mv.allAtoms)
        self.mv.clearSelection()
        if not self.mv.hasGui: return
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],"self.clearSelection(log=0)")

    def test_clearselection_log_checks_that_it_runs(self):
        """Checking log string runs """
        c=self.mv.select
        c(mv.allAtoms[:3])
        self.mv.clearSelection()
        oldself=self
        self =mv
        s = "self.clearSelection(log =0)"
        exec(s)
        oldself.assertEqual(1,1)
    
    def test_create_set_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        c=self.mv.select
        c(mv.allAtoms[:3])
        self.mv.createSetIfNeeded(mv.allAtoms[:3],"set1")
        if not self.mv.hasGui: return
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.createSetIfNeeded("ind,test_aa,barrel_1,1crn_hs,1CRN,dnaexple:::0-2", \'set1\', log=0, comments=\'No description\')')

    def test_create_set_log_checks_that_it_runs(self):
        """Checking log string runs """
        c=self.mv.select
        c(mv.allAtoms[:3])
        self.mv.createSetIfNeeded(mv.allAtoms[:3],"set1")
        oldself=self
        self =mv
        s = 'self.createSetIfNeeded("ind:::0-2", \'set1\', log=0, comments=\'No description\')'
        exec(s)
        oldself.assertEqual(1,1)

    def test_selectSet_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        c=self.mv.select
        c(mv.allAtoms[:3])
        self.mv.createSetIfNeeded(mv.selection,"set12")
        self.mv.selectSet("set12")
        if not self.mv.hasGui: return
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],"self.selectSet('set12', negate=False, only=False, log=0)")

    def test_create_set_log_checks_that_it_runs_1(self):
      """Checking log string runs """
      c=self.mv.select
      c(mv.allAtoms[:3])
      self.mv.createSetIfNeeded(mv.allAtoms[:3],"set1")
      oldself=self
      self =mv
      s = "self.selectSet('set1', negate=False, only=False, log=0)"
      exec(s)
      oldself.assertEqual(1,1)
      
    def test_direct_select_log_checks_expected_log_string(self):
      """checks expected log string is written
      """
      self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
      if not self.mv.hasGui: return
      self.mv.directSelect('ind')                
      tx = self.mv.GUI.MESSAGE_BOX.tx
      last_index = tx.index('end')
      last_entry_index = str(float(last_index)-2.0)
      last_entry = tx.get(last_entry_index, 'end')    
      self.assertEqual(split(last_entry,'\n')[0],"self.directSelect('ind', log=0)")

    def test_direct_select_set_log_checks_that_it_runs(self):
      """Checking log string runs """
      oldself=self
      self =mv
      s = "self.directSelect('ind', log=0)" 
      exec(s)
      oldself.assertEqual(1,1)



if __name__ == '__main__':
    #unittest.main()
    test_cases = [
        'Pmvselection_SaveSetsTests',
        'Pmvselection_InvertSelectionTests',
        'Pmvselection_SelectTests',
        'Pmvselection_DeSelectTests',
        'Pmvselection_ClearSelectTests',
        'Pmvselection_CreateSetIfNeededSelectTests',
        'Pmvselection_SelectSetSelectTests',
        'Pmvselection_DirectSelectTests',
        'Pmvselection_SelectInSphereSelectTests',
        'Pmvselection_SelectFromStringSelectTests',
        'SelectionLogTests'
            ]
    unittest.main( argv=([__name__ ,] + test_cases) )

