#
#
# $Id: test_FreezeAtoms_Amber94.py,v 1.5 2009/11/10 22:59:02 annao Exp $
#
#############################################################################
#                                                                           #
#   Author:Sowjanya Karnati                                                 #
#   Copyright: M. Sanner TSRI 2000                                          #
#                                                                           #
#############################################################################

import sys,os
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
ct = 0
totalCt = 7
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class AmberBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                    trapExceptions=False, withShell=0, gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('amberCommands', package='Pmv')
            #set up links to shared dictionary and current instance
            from Pmv.amberCommands import Amber94Config, CurrentAmber94
            self.Amber94Config = Amber94Config
            self.CurrentAmber94 = CurrentAmber94
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        #for key,value in self.Amber94Config.items():
        #    del self.Amber94Config[key]
        #    try:
        #        del value
        #    except:
        #        print "exception in deleting ", value
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None


class FreezeAtoms_Amber94(AmberBaseTest):

    def test_freeze_atoms_widget(self):
        """tests freeze atoms widget
        FIX THIS
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        ##call setup_Amber94 command
        #self.mv.setup_Amber94("trp3_h:","fztest1",filename=None)
        #c1 = self.mv.freezeAtoms_Amber94
        #self.mv.freezeAtoms_Amber94('fztest1', "trp3_h: :LEU1:CA;trp3_h: :TRP2:CA;trp3_h: :GLN3:CA")
        #c1.buildForm('Set atoms to be constrained for "fztest1":',self.mv.Mols,c1.CurrentAmber94)
        ##Need to do this otherwise the form closes before assert
        ##but = c1.ifd.entryByName[4]['widget']
        ##but.wait_visibility(but)
        #self.assertEqual(c1.ifd.form.root.winfo_ismapped(),1)    
        #c1.Close_cb(c1.ifd)    


    def test_freeze_atoms_amberIds(self):
        """tests freeze atoms widget entry amberIds
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:","fztest2",filename=None)
        c1 = self.mv.freezeAtoms_Amber94
        self.mv.freezeAtoms_Amber94('fztest2', "trp3_h: :LEU1:CA;trp3_h: :TRP2:CA;trp3_h: :GLN3:CA")
        if self.mv.hasGui:
            c1.buildForm('Set atoms to be constrained for "fztest2":',self.mv.Mols,c1.CurrentAmber94)   
            AmberIdBut = c1.ifd.entryByName['amberIds']['widget']
            AmberIdBut.setentry('fztest2')
            self.assertEqual(AmberIdBut.get(),'fztest2')
            c1.Close_cb(c1.ifd)

    def test_freeze_atoms_coords(self):
        """tests freeze atoms command ,by freezing 3 atoms,calling minimize
        command and compare coords of frozen atoms before and after minimizing
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:","fztest3",filename=None)
        c1 = self.mv.freezeAtoms_Amber94
        self.mv.freezeAtoms_Amber94('fztest3', "trp3_h: :LEU1:CA;trp3_h: :TRP2:CA;trp3_h: :GLN3:CA")
        ats = self.mv.Mols[0].allAtoms.get(lambda x : x.name == 'CA')
        c = self.mv.minimize_Amber94
        c('fztest3', dfpred=10.0, callback_freq='10', callback=1, drms=1e-06, maxIter=10000, log=0)
        ats1 = self.mv.Mols[0].allAtoms.get(lambda x : x.name == 'CA')
        self.assertEqual(ats.coords,ats1.coords)
        if self.mv.hasGui:
            c1.Close_cb(c1.ifd)

    def test_freeze_atoms_invalid_key(self):
        """tests freeze atoms command ,invalid key entry
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        c1 = self.mv.freezeAtoms_Amber94
        self.assertEqual(c1('gjg',"trp3_h: :LEU1:CA"),'ERROR')
        

    def test_freeze_atoms_empty_key(self):
        """tests freeze atoms command ,empty key entry
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        c1 = self.mv.freezeAtoms_Amber94
        #from Pmv.amberCommands import Amber94Config
        #c1.CurrentAmber94 = Amber94Config.values()[0][0]    
        self.assertEqual(c1(' ',"trp3_h: :LEU1:CA"),'ERROR')
        
    def test_freeze_atoms_invalid_atoms(self):
        """tests freeze atoms command ,with invalid atoms
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        c1 = self.mv.freezeAtoms_Amber94
        returnValue = c1('fztest4', "trp3_h: :LEU1:CAN;trp3_h: :TRP2:CAN;trp3_h: :GLN3:CA")
        self.assertEqual(returnValue,'ERROR')    

    def test_freeze_atoms_empty_atoms(self):
        """tests freeze atoms command ,with empty atoms
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        c1 = self.mv.freezeAtoms_Amber94
        returnValue = c1('fztest5', " ")
        self.assertEqual(returnValue,'ERROR')


if __name__ == '__main__':
    test_cases = [
        'FreezeAtoms_Amber94',
        ]
    
    unittest.main( argv=([__name__ ] + test_cases) )




#if __name__ == '__main__':
#    unittest.main()






        
