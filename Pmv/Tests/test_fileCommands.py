#
# $Id: test_fileCommands.py,v 1.48.2.1 2016/02/11 21:29:43 annao Exp $
#
#############################################################################
#
# Author:Sowjanya KARNATI,Ruth HUEY
#
# Copyright: M. Sanner TSRI 2000
#
#############################################################################



import sys,os,string,re
import unittest,string
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from Pmv.extruder import Sheet2D
from string import split
from DejaVu.Spheres import Spheres
from MolKit.mmcifParser import MMCIFParser
from MolKit.mmcifWriter import MMCIFWriter
mv= None
klass = None
ct = 0
totalCt = 94

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1
    
class FileBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
[(1.0, 1.0, 1.0)]       """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                verbose=False, trapExceptions=False,
                                withShell=0, gui=hasGUI)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('fileCommands', package='Pmv')
            mv.browseCommands('displayCommands',
                              commands=['displaySticksAndBalls','undisplaySticksAndBalls',
                                        'displayCPK', 'undisplayCPK',
                                        'displayLines','undisplayLines',
                                        'displayBackboneTrace','undisplayBackboneTrace',
                                        'DisplayBoundGeom'
                                        ], package='Pmv')
            mv.browseCommands('editCommands', package='Pmv')
            mv.browseCommands('msmsCommands', package='Pmv')
        self.mv = mv 
           
        

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None




##########################################################
#       Write PDBQ Command Tests                         #
##########################################################



class WritePDBQ(FileBaseTest):
    def test_write_pdbq_1(self):
        """tests pdbq file exists
        """
        cmd = "rm -f ./testfile.pdbq"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ
        c('1crn','testfile.pdbq')
        self.assertEqual(os.path.exists('testfile.pdbq'),True)
    def test_write_pdbq_pdbq_rec(self):
        """tests write pdbq,in pdbq file each line contains ATOM or HETATM or CONECT
        """
        cmd = "rm -f ./testfile.pdbq"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ
        c('1crn','testfile.pdbq',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        z= 0
        # open testfile for reading
        fptr = open("testfile.pdbq")
        alllines = fptr.readlines()
        #compute how many lines has HETATM or ATOM or CONECT
        for i in range(0,len(alllines)):
            y = string.find(str(split(alllines[i])),'HETATM') 
            y1= string.find(str(split(alllines[i])),'CONECT')
            y2 = string.find(str(split(alllines[i])),'ATOM')
            if y>0 or y1>0 or y2>0:
                z=z+1
        #number of lines having HETATM or ATOM or CONECT is number of lines in file - TER line 
        self.assertEqual(len(alllines)-2,z)
                
    def test_write_pdbq_bond_origin(self):
        """tests write PDBQ,setting bond origin
        """
        cmd = "rm -f ./testfile.pdbq"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ
        c('1crn','testfile.pdbq',sort = False,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = (1.0,2.0,3.0),ssOrigin = 'File')
        self.assertEqual(c.getLastUsedValues()['bondOrigin'],(1.0,2.0,3.0))
        
        
        
    def test_write_pdbq_ss_origin(self):
        """tests write PDBQ,setting ssorigin
        """
        cmd = "rm -f ./testfile.pdbq"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ
        c('1crn','testfile.pdbq',sort = False,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = 'File')
        self.assertEqual(c.getLastUsedValues()['ssOrigin'],'File')
        self.assertEqual(c.getLastUsedValues()['sort'],False)
        
    def test_write_pdbq_more_than_one_molecule(self):
        """tests write pdbq,when more than one mol is selected returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.addKollmanCharges("ind:::")
        self.mv.displayCPK("1crn:::")
        self.mv.select(self.mv.allAtoms)
        c = self.mv.writePDBQ
        returnValue = c(self.mv.getSelection(),'testfile.pdbq',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)

    def test_write_pdbq_mol_with_no_charges(self):
        """tests write pdbq,when mol with no charges is selected returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ
        returnValue = c(self.mv.getSelection(),'testfile.pdbq',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)

#tests invalid input for writePDBQ command
    
    def test_write_pdbq_invalid_nodes(self):
        """tests write pdbq with invalid nodes returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ 
        returnValue = c('hello','testfile.pdbq',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)

    def test_write_pdbq_invalid_file_name(self):
        """tests write pdbq with invalid file name returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ 
        returnValue = c('1crn','testfile.abc',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)
        
    def test_write_pdbq_invalid_sort(self):
        """tests write pdbq with invalid sort returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ 
        returnValue = c('1crn','testfile.pdbq',sort = 'hdsk',pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)        
    

    def test_write_pdbq_invalid_pdbRec(self):
        """tests write pdbq with invalid pdbRec returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::")
        c = self.mv.writePDBQ 
        returnValue = c('1crn','testfile.pdbq',sort = True,pdbRec = 'hai',bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)


    def test_write_pdbq_invalid_bond_origin(self):
        """tests write pdbq with invalid bond origin returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ 
        returnValue = c('1crn','testfile.pdbq',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'hello',ssOrigin = None)
        self.assertEqual(returnValue,None)


    def test_write_pdbq_invalid_ss_origin(self):
        """tests write pdbq with invalid ss origin returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQ 
        returnValue = c('1crn','testfile.pdbq',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = 'hai')
        self.assertEqual(returnValue,None)

########################################################
#       Write PDB Command Tests                        #
########################################################



class writePDB(FileBaseTest):
    
    def test_write_pdb_1(self):
        """tests pdb file exists
        """
        cmd = "rm -f ./testfile.pdb"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB
        c('1crn','testfile.pdb')
        self.assertEqual(os.path.exists('testfile.pdb'),True)

    def test_write_pdb_pdb_rec(self):
        """tests write pdb,in pdb file each line contains ATOM or HETATM or CONECT except starting and ending lines
        """
        cmd = "rm -f ./testfile.pdb"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB
        c('1crn','testfile.pdb',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        z= 0
        #open testfile  for reading
        fptr = open("testfile.pdb")
        alllines = fptr.readlines()
        #compute how many lines has HETATM or ATOM
        for i in range(0,len(alllines)):
            y = string.find(str(split(alllines[i])),'HETATM')
            y1 = string.find(str(split(alllines[i])),'CONECT')
            y2 = string.find(str(split(alllines[i])),'ATOM')
            if y>0 or y1>0 or y2>0:
                z=z+1
        #number of lines having HETATM or ATOM is number of lines - TER line 
        self.assertEqual(len(alllines) - 2,z)
                
    def test_write_pdb_bond_origin(self):
        """tests write PDB,setting bond origin 
        """
        cmd = "rm -f ./testfile.pdb"
        cmd1 = "rm -f ./testfile1.pdb"
        os.system(cmd)
        os.system(cmd1)
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.writePDB 
        c('1crn','testfile.pdb',sort = False,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = (1.0,2.0,1.0),ssOrigin = 'File')
        self.assertEqual(c.getLastUsedValues()['bondOrigin'],(1.0,2.0,1.0))
        
        
    def test_write_pdb_ss_origin(self):
        """tests write pdb ,setting ss origin
        """
        cmd = "rm -f ./testfile.pdb"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB
        c('1crn','testfile.pdb',sort = False,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = 'File')
        self.assertEqual(c.getLastUsedValues()['ssOrigin'],'File')
        self.assertEqual(c.getLastUsedValues()['sort'],False)
        
    def test_write_pdb_more_than_one_molecule(self):
        """tests write pdb ,when more than one mol selected
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.displayCPK("1crn:::")
        self.mv.displayCPK("ind:::")
        self.mv.select(self.mv.allAtoms)
        c = self.mv.writePDB
        returnValue = c(self.mv.getSelection(),'testfile.pdb',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue, "ERROR")
                    
#tests invalid input for writePDB command
    
    def test_write_pdb_invalid_nodes(self):
        """tests write pdb with invalid nodes returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB 
        returnValue = c('hello','testfile.pdb',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,"ERROR")

    def test_write_pdb_invalid_file_name(self):
        """tests write pdb with invalid filename returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB 
        returnValue = c('1crn','testfile.abc',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)
        
    def test_write_pdb_invalid_sort(self):
        """tests write pdb with invalid sort returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB 
        returnValue = c('1crn','testfile.pdb',sort = 'hdsk',pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)        
    

    def test_write_pdb_invalid_pdbRec(self):
        """tests write pdb with invalid pdbRec returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB 
        returnValue = c('1crn','testfile.pdb',sort = True,pdbRec = 'hai',bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)


    def test_write_pdb_invalid_bond_origin(self):
        """tests write pdb with invalid bond origin returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB 
        returnValue = c('1crn','testfile.pdb',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'hello',ssOrigin = None)
        self.assertEqual(returnValue,None)


    def test_write_pdb_invalid_ss_origin(self):
        """tests write pdb with invalid ss origin returns None
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDB 
        returnValue = c('1crn','testfile.pdb',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = 'hai')
        self.assertEqual(returnValue,None)
        
#########################################################
#       Write PDBQS Command Tests                       #
#########################################################

class WritePDBQS(FileBaseTest):
    
    def test_write_pdbqs_1(self):
        """tests pdbqs file exists
        """
        cmd = "rm -f ./testfile.pdbqs"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS
        c('1crn','testfile.pdbqs')
        self.assertEqual(os.path.exists('testfile.pdbqs'),True)

    def test_write_pdbqs_pdbqs_rec(self):
        """tests write pdbqs,in pdbqs file each line contains ATOM or HETATM or CONECT
        """
        cmd = "rm -f ./testfile.pdbqs"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS
        c('1crn','testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        z= 0
        # open testfile for reading
        fptr = open("testfile.pdbqs")
        alllines = fptr.readlines()
        #compute how many lines has HETATM or ATOM or CONECT
        for i in range(0,len(alllines)):
            y = string.find(str(split(alllines[i])),'HETATM') 
            y1= string.find(str(split(alllines[i])),'CONECT')
            y2= string.find(str(split(alllines[i])),'ATOM')
            if y>0 or y1>0 or y2 >0:
                z=z+1
        #number of lines having HETATM or ATOM or CONECT is number of lines in file -TER line
        self.assertEqual(len(alllines)-2,z)
                
    def test_write_pdbqs_bond_origin(self):
        """tests write PDBQSQS,setting bond origin
        """
        cmd = "rm -f ./testfile.pdbqs"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS
        c('1crn','testfile.pdbqs',sort = False,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = (1.0,2.0,3.0),ssOrigin = 'File')
        self.assertEqual(c.getLastUsedValues()['bondOrigin'],(1.0,2.0,3.0))
        
        
        
    def test_write_pdbqs_ss_origin(self):
        """tests write PDBQSQS,setting ssorigin
        """
        cmd = "rm -f ./testfile.pdbqs"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS
        c('1crn','testfile.pdbqs',sort = False,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = 'File')
        self.assertEqual(c.getLastUsedValues()['ssOrigin'],'File')
        self.assertEqual(c.getLastUsedValues()['sort'],False)
        
    def test_write_pdbqs_more_than_one_molecule(self):
        """tests write pdbqs,when more than one mol is selected returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.readMolecule("Data/piece_modterm.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.addKollmanCharges("piece_modterm:::")
        self.mv.displayCPK("1crn:::")
        self.mv.select(self.mv.allAtoms)
        c = self.mv.writePDBQS
        returnValue = c(self.mv.getSelection(),'testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)

    def test_write_pdbqs_mol_with_no_charges(self):
        """tests write pdbqs,when mol with no charges is selected returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS
        returnValue = c(self.mv.getSelection(),'testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)

    def test_write_pdbqs_mol_with_no_sol_par(self):
        """tests write pdbqs,when mol with no sol par is selected returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS
        returnValue = c(self.mv.getSelection(),'testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)    
#tests invalid input for writePDBQSQS command
    
    def test_write_pdbqs_invalid_nodes(self):
        """tests write pdbqs with invalid nodes returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS 
        returnValue = c('hello','testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)

    def test_write_pdbqs_invalid_file_name(self):
        """tests write pdbqs with invalid file name returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS 
        returnValue = c('1crn','testfile.abc',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)
        
    def test_write_pdbqs_invalid_sort(self):
        """tests write pdbqs with invalid sort returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS 
        returnValue = c('1crn','testfile.pdbqs',sort = 'hdsk',pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)        
    

    def test_write_pdbqs_invalid_pdbRec(self):
        """tests write pdbqs with invalid pdbRec returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        c = self.mv.writePDBQS 
        returnValue = c('1crn','testfile.pdbqs',sort = True,pdbRec = 'hai',bondOrigin = 'all',ssOrigin = None)
        self.assertEqual(returnValue,None)


    def test_write_pdbqs_invalid_bond_origin(self):
        """tests write pdbqs with invalid bond origin returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS 
        returnValue = c('1crn','testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'hello',ssOrigin = None)
        self.assertEqual(returnValue,None)


    def test_write_pdbqs_invalid_ss_origin(self):
        """tests write pdbqs with invalid ss origin returns None
        """
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::")
        self.mv.displayCPK("1crn:::")
        c = self.mv.writePDBQS 
        returnValue = c('1crn','testfile.pdbqs',sort = True,pdbRec = ['ATOM', 'HETATM' ,'CONECT'],bondOrigin = 'all',ssOrigin = 'hai')
        self.assertEqual(returnValue,None)        
        
#########################################################
#       Write PQR Command Tests                       #
#########################################################

class WritePQR(FileBaseTest):

    def test_write_pqr_1(self):
        """tests pqr file exists
        """
        cmd = "rm -f ./testfile.pqr"
        os.system(cmd)
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.displayCPK("mead:::")
        c = self.mv.writePQR
        c('testfile.pqr','mead')
        self.assertEqual(os.path.exists('./testfile.pqr'),True)

    def test_write_pqr_pqr_rec(self):
        """tests write pqr,in pqr file each line contains ATOM or HETATM or CONECT
        """
        cmd = "rm -f ./testfile.pqr"
        os.system(cmd)
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.displayCPK("mead:::")
        c = self.mv.writePQR
        c('testfile.pqr','mead',sort = True,recType = ['ATOM', 'HETATM' ,'CONECT'])
        z= 0
        # open testfile for reading
        fptr = open("testfile.pqr")
        alllines = fptr.readlines()
        #compute how many lines has HETATM or ATOM or CONECT
        for i in range(0,len(alllines)):
            y = string.find(str(split(alllines[i])),'HETATM') 
            y1= string.find(str(split(alllines[i])),'CONECT')
            y2= string.find(str(split(alllines[i])),'ATOM')
            if y>0 or y1>0 or y2 >0:
                z=z+1
        #number of lines having HETATM or ATOM or CONECT is number of lines in file
        self.assertEqual(len(alllines),z)
                
   
        
    def test_write_pqr_more_than_one_molecule(self):
        """tests write pqr,when more than one mol is selected returns None
        """
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.readMolecule("Data/mead1.pqr")
        self.mv.displayCPK("mead:::")
        self.mv.displayCPK("mead1:::")
        self.mv.select(self.mv.allAtoms)
        c = self.mv.writePQR('testfile.pqr',self.mv.getSelection(),sort = True)
        self.assertEqual(c,None)
        

        
#tests invalid input for writePQR command
    
    def test_write_pqr_invalid_nodes(self):
        """tests write pqr with invalid nodes returns None
        """
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.displayCPK("mead:::")
        c = self.mv.writePQR 
        returnValue = c('testfile.pqr','hello',sort = True,recType = ['ATOM', 'HETATM' ,'CONECT'])
        self.assertEqual(returnValue,None)

    def test_write_pqr_invalid_file_name(self):
        """tests write pqr with invalid file name returns None
        """
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.displayCPK("mead:::")
        c = self.mv.writePQR 
        returnValue = c('testfile.abc','mead',sort = True,recType = ['ATOM', 'HETATM' ,'CONECT'])
        self.assertEqual(returnValue,None)
        
    def test_write_pqr_invalid_sort(self):
        """tests write pqr with invalid sort returns None
        """
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.displayCPK("mead:::")
        c = self.mv.writePQR 
        returnValue = c('testfile.pqr','mead',sort = 'hdsk',recType = ['ATOM', 'HETATM' ,'CONECT'])
        self.assertEqual(returnValue,None)        
    

    def test_write_pqr_invalid_recType(self):
        """tests write pqr with invalid recType returns None
        """
        self.mv.readMolecule("Data/mead.pqr")
        c = self.mv.writePQR 
        returnValue = c('testfile.pqr','mead',sort = True,recType = 'hai')
        self.assertEqual(returnValue,None)


           
########################################################
#       Read Molecule Command Tests                    #
########################################################

class ReadMolecule(FileBaseTest):

    def test_read_molecule_pdb(self):
        """tests readMolecule command ,reading a pdb file
        """
        c = self.mv.readMolecule
        c("Data/1crn.pdb")
        self.assertEqual(os.path.exists("./Data/1crn.pdb"),True)
        self.assertEqual(len(mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'1crn')
        self.assertEqual(c.fileExt,'.pdb')
        
    def test_read_molecule_pdbq(self):
        """tests readMolecule command ,reading a pdbq file
        """
        c = self.mv.readMolecule
        c("Data/1crn.pdbq")
        self.assertEqual(os.path.exists("./Data/1crn.pdbq"),True)
        self.assertEqual(len(mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'1crn')
        self.assertEqual(c.fileExt,'.pdbq')
        
    def test_read_molecule_pdbqs(self):
        """tests readMolecule command ,reading a pdbqs file
        """
        c = self.mv.readMolecule
        c("Data/1crn.pdbqs")
        self.assertEqual(os.path.exists("./Data/1crn.pdbqs"),True)
        self.assertEqual(len(mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'1crn')
        self.assertEqual(c.fileExt,'.pdbqs')
        
    def test_read_molecule_pqr(self):
        """tests readMolecule command ,reading a pqr file
        """
        c = self.mv.readMolecule
        c("Data/mead.pqr")
        self.assertEqual(os.path.exists("./Data/mead.pqr"),True)
        self.assertEqual(len(mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'mead')
        self.assertEqual(c.fileExt,'.pqr')
        
    def test_read_molecule_cif(self):
        """tests readMolecule command ,reading a cif file
        """
        c = self.mv.readMolecule
        c("Data/1CRN.cif")
        self.assertEqual(os.path.exists("./Data/1CRN.cif"),True)
        self.assertEqual(len(mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'1CRN')
        self.assertEqual(c.fileExt,'.cif')
        
    def test_read_molecule_mol2(self):
        """tests readMolecule command ,reading a mol2 file
        """
        c = self.mv.readMolecule
        c("Data/hpi1s.mol2")
        self.assertEqual(os.path.exists("./Data/hpi1s.mol2"),True)
        self.assertEqual(len(mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'hpi1s')
        self.assertEqual(c.fileExt,'.mol2')


    def test_read_molecule_invalid_file(self):
        """tests readMolecule command with non existent file name
        """
        c = self.mv.readMolecule
        returnValue = c('hai')
        self.assertEqual(returnValue,None)


    def test_read_molecule_other_exts_1_to_pdb(self):
        """tests readMolecule command
        """
        c = self.mv.readMolecule    
        c.doit('Data/test_barrel_1_2_hbonds.hbnd',parser ='PDB')
        self.assertEqual(len(self.mv.Mols),1)
        self.assertEqual(self.mv.Mols[0].name,'test_barrel_1_2_hbonds')
    
   
###########################################################
#       Write VRML2 COMMAND TESTS                         #
###########################################################


class WriteVRML(FileBaseTest):


    def test_write_VRML_1(self):
        """tests write VRML ,file exists,no.of lines greater than zero
        """
        cmd ='rm -f testfile.wrl'
        os.system(cmd)
        self.mv.readMolecule('ind.pdb')
        self.mv.displayCPK("ind:::")
        self.mv.writeVRML2('testfile.wrl')
        self.assertEqual(os.path.exists('./testfile.wrl'),True)
        fptr = open('testfile.wrl')
        alllines = fptr.readlines()
        self.assertEqual(len(alllines) > 0 ,True)
        
    def test_write_VRML_2(self):
        """tests write VRML with invalid input
        """
        self.mv.readMolecule('ind.pdb')
        c = self.mv.writeVRML2
        returnValue = c('testfile.abc')
        self.assertEqual(returnValue,None)
        
# This test fails on Windows with  "No such file or directory: ' ' " error 
##     def test_write_VRML_3(self):
##         """tests write VRML with empty input
##         """
##         self.mv.readMolecule('ind.pdb')
##         c = self.mv.writeVRML2
##         returnValue = c(' ')
##         self.assertEqual(returnValue,None)


#####################################################
#   Read PDB Command Tests                          #
#####################################################

class ReadPDB(FileBaseTest):
    
    def test_read_pdb_1(self):
        """tests reading a pdb file 
        """
        c = self.mv.readPDB
        c("Data/ind.pdb")
        self.assertEqual(os.path.exists('./Data/ind.pdb'),True)
        self.assertEqual(len(self.mv.Mols),1)
        
#####################################################
#   Read PDBQ Command Tests                         #
#####################################################

class ReadPDBQ(FileBaseTest):
    
    def test_read_pdbq_1(self):
        """tests reading a pdbq file
        """
        c = self.mv.readPDBQ
        c("Data/1crn.pdbq")
        self.assertEqual(os.path.exists('./Data/1crn.pdbq'),True)
        self.assertEqual(len(self.mv.Mols),1)

    def test_read_pdbq_4(self):
        """tests read pdbq, write pdb, read pdb and make sure that 
        chemical elements are not changed, especially aromatic carbons 
        """
        self.mv.readMolecule("Data/ind.pdbq")
        self.mv.writePDB(self.mv.Mols[0],"Data/ind_1.pdb")
        self.mv.readMolecule("Data/ind_1.pdb")    
        TrueFalse = [x.chemElem for x in self.mv.Mols[0].allAtoms.data] == \
                             [y.chemElem for y in self.mv.Mols[1].allAtoms.data]
        self.assertEqual(TrueFalse,True)        
        os.remove("Data/ind_1.pdb")


###################################################
#   Read PDBQS Command Tests                      #
###################################################

class ReadPDBQS(FileBaseTest):
    
    def test_read_pdbqs_1(self):
        """tests reading pdbqs file 
        """
        c = self.mv.readPDBQS
        c("Data/1crn.pdbqs")
        self.assertEqual(os.path.exists('./Data/1crn.pdbqs'),True)
        self.assertEqual(len(self.mv.Mols),1)

###################################################
#   Read PQR Command Tests                        #
###################################################

class ReadPQR(FileBaseTest):
    
    def test_read_pqr_1(self):
        """tests reading a pqr file
        """
        c = self.mv.readPQR
        c("Data/mead.pqr")
        self.assertEqual(os.path.exists('./Data/mead.pqr'),True)
        self.assertEqual(len(self.mv.Mols),1)


###################################################
#   Read MMCIF Command Tests                      #
###################################################

class ReadMMCIF(FileBaseTest):
    
    def test_read_cif_1(self):
        """tests reading a cif file
        """
        mol = self.mv.readMMCIF("Data/1CRN.cif")
        self.assertEqual(len(mol),1)

###################################################
#   Read MOL2 Command Tests                       #
###################################################

class ReadMOL2(FileBaseTest):
    
    def test_read_mol2_1(self):
        """tests reading a mol2 file
        """
        c = self.mv.readMOL2
        c("Data/hpi1s.mol2")
        self.assertEqual(os.path.exists('./Data/hpi1s.mol2'),True)
        self.assertEqual(len(self.mv.Mols),1)

###################################################
#   Write MMCIF Command Tests                     #
###################################################

class WriteMMCIF(FileBaseTest):
    
    def test_write_MMCIF_1(self):
        """tests writing MMCIF,path exists
        """
        cmd ='rm -f testfile.cif'
        os.system(cmd)
        self.mv.readMolecule("Data/1CRN.cif")
        self.mv.displayCPK("1CRN:::")
        self.mv.writeMMCIF('testfile.cif','1CRN')
        self.assertEqual(os.path.exists('./testfile.cif'),True)
        

    def test_write_MMCIF_2(self):
        """tests write MMCIF and no.of lines > 0
        """
        cmd ='rm -f testfile.cif'
        os.system(cmd)
        self.mv.readMolecule("Data/1CRN.cif")
        self.mv.displayCPK("1CRN:::")
        self.mv.writeMMCIF('testfile.cif','1CRN')
        fptr =open('testfile.cif')
        alllines = fptr.readlines()
        self.assertEqual(len(alllines)>0,True)
        z =0
        #computes how many lines has HETATM or ATOM or CONECT
        for i in range(0,len(alllines)):
            y = string.find(str(split(alllines[i])),'HETATM') 
            y1= string.find(str(split(alllines[i])),'CONECT')
            y2 = string.find(str(split(alllines[i])),'ATOM')
            if y>0 or y1>0 or y2>0:
                z=z+1
        self.assertEqual(z>0,True)
        
        
    def test_write_MMCIF_invalid_1(self):
        """tests write MMCIF with invalid input
        """
        self.mv.readMolecule("Data/1CRN.cif")
        c = self.mv.writeMMCIF
        returnValue = c('testfile.abc','1CRN')
        self.assertEqual(returnValue,None)
        
    
    def test_write_MMCIF_invalid_2(self):
        """tests write MMCIF ,filename with invalid extension as input
        """
        self.mv.readMolecule("Data/1CRN.cif")
        c = self.mv.writeMMCIF
        returnValue = c('testfile.abc','1CRN')
        self.assertEqual(returnValue,None) 


###################################################
#   Write STL Command Tests                       #
###################################################

class WriteSTL(FileBaseTest):


    def test_write_STL_1(self):
        """tests writing STL,len af lines in stl file >0
        """
        cmd ='rm -f testfile.stl'
        os.system(cmd)
        self.mv.readMolecule('ind.pdb')
        self.mv.displayCPK("ind:::")
        self.mv.writeSTL('testfile.stl')
        self.assertEqual(os.path.exists('./testfile.stl'),True)
        fptr = open('testfile.stl')
        alllines = fptr.readlines()
        self.assertEqual(len(alllines) > 0 ,True)
        
    def test_write_STL_2(self):
        """tests write stl with invalid input
        """
        self.mv.readMolecule('ind.pdb')
        c = self.mv.writeSTL
        returnValue = c('testfile.abc')
        self.assertEqual(returnValue,None)
        
    def test_write_STL_3(self):
        """tests write stl with computeMSMS
        """
        self.mv.readMolecule('ind.pdb')
        self.mv.computeMSMS("ind")
        c = self.mv.writeSTL
        returnValue = c('testfile.stl')
        self.assertEqual(returnValue,None)

# This test fails on Windows
##     def test_write_STL_3(self):
##         """tests write stl with empty input
##         """
##         self.mv.readMolecule('ind.pdb')
##         c = self.mv.writeSTL
##         returnValue = c(' ')
##         self.assertEqual(returnValue,None)

#class FileCommandsLogBaseTest(FileBaseTest):
class FileCommandsLogWriteTest(FileBaseTest):

    def test_write_pdbq_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        cmd = "rm -f ./testfile.pdbq"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        self.mv.writePDBQ('1crn','testfile.pdbq')        
        oldself=self
        self =mv
        s = "self.writePDBQ('1crn','testfile.pdbq')"
        exec(s)
        oldself.assertEqual(1,1)      
        
        
#    def test_write_pdbq_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """
#        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
#        cmd = "rm -f ./testfile.pdbq"
#        os.system(cmd)
#        self.mv.readMolecule("Data/1crn.pdb")
#        self.mv.addKollmanCharges("1crn:::", log=0)
#        self.mv.displayCPK("1crn:::")
#        self.mv.writePDBQ('1crn','testfile.pdbq')
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],"self.writePDBQ('1crn', sort=True, ssOrigin=None, log=0, filename='testfile.pdbq', transformed=False, bondOrigin='all')")


#    def test_write_pdb_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """        
#        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
#        cmd = "rm -f ./testfile.pdb"
#        os.system(cmd)
#        self.mv.readMolecule("Data/1crn.pdb")
#        self.mv.displayCPK("1crn:::")
#        self.mv.writePDB('1crn','testfile.pdb') 
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],"self.writePDB('1crn', sort=True, ssOrigin=None, log=0, filename='testfile.pdb', pdbRec=['ATOM', 'HETATM', 'CONECT'], transformed=False, bondOrigin='all')" )
        

    def test_write_pdb_log_checks_that_it_runs(self):
        """Checking log string runs """
        cmd = "rm -f ./testfile.pdb"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.displayCPK("1crn:::")
        oldself=self
        self =mv
        s = "self.writePDB('1crn','testfile.pdb')"
        exec(s)
        oldself.assertEqual(1,1)
        
#    def test_write_pdbqs_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """
#        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
#        cmd = "rm -f ./testfile.pdbqs"
#        os.system(cmd)
#        self.mv.readMolecule("Data/1crn.pdbqs")
#        self.mv.addKollmanCharges("1crn:::", log=0)
#        self.mv.displayCPK("1crn:::")
#        self.mv.writePDBQS('1crn','testfile.pdbqs')        
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],"self.writePDBQS('1crn', sort=True, ssOrigin=None, log=0, filename='testfile.pdbqs', transformed=False, bondOrigin='all')")


    def test_write_pdbqs_log_checks_that_it_runs(self):
        """Checking log string runs """        
        cmd = "rm -f ./testfile.pdbqs"
        os.system(cmd)
        self.mv.readMolecule("Data/1crn.pdbqs")
        self.mv.addKollmanCharges("1crn:::", log=0)
        self.mv.displayCPK("1crn:::")
        oldself=self
        self =mv
        s = "self.writePDBQS('1crn','testfile.pdbqs')"            
        exec(s)
        oldself.assertEqual(1,1)

            
#    def test_write_pqr_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """
#        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        #        cmd = "rm -f ./testfile.pqr"
#        os.system(cmd)
#        self.mv.readMolecule("Data/mead.pqr")
#        self.mv.displayCPK("mead:::")
#        self.mv.writePQR('testfile.pqr','mead')
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],"self.writePQR('testfile.pqr', 'mead', sort=True, log=0)")

    def test_write_pqr__log_checks_that_it_runs(self):
        """Checking log string runs """
        cmd = "rm -f ./testfile.pqr"
        os.system(cmd)
        self.mv.readMolecule("Data/mead.pqr")
        self.mv.displayCPK("mead:::")
        oldself=self
        self =mv
        s = "self.writePQR('testfile.pqr','mead')"
        exec(s)
        oldself.assertEqual(1,1)


#    def test_write_VRML_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """
        #self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        #        cmd ='rm -f testfile.wrl'
#        os.system(cmd)
#        self.mv.readMolecule('ind.pdb')
#        self.mv.displayCPK("ind:::")
#        self.mv.writeVRML2('testfile.wrl')
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],"self.writeVRML2('testfile.wrl', log=0, cylinderQuality=10, saveNormals=0, colorPerVertex=True, useProto=0, sphereQuality=2)")

    def test_write_VRML_log_checks_that_it_runs(self):
        """Checking log string runs """
        if not self.mv.hasGui: return
        cmd ='rm -f testfile.wrl'
        os.system(cmd)
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.displayCPK("ind:::")
        oldself=self
        self =mv
        s="self.writeVRML2('testfile.wrl')"
        exec(s)
        oldself.assertEqual(1,1)

    #    def test_write_MMCIF_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """
#        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)          
#        cmd ='rm -f testfile.cif'
#        os.system(cmd)
#        self.mv.readMolecule("Data/1CRN.cif")
#        self.mv.displayCPK("1CRN:::")
#        self.mv.writeMMCIF('testfile.cif','1CRN')
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],'self.writeMMCIF(\'testfile.cif\', "1CRN", log=0)')
        

    def test_write_MMCIF_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/1CRN.cif")
        oldself=self
        self =mv
        s = "self.writeMMCIF('testfile.cif','1CRN')"
        exec(s)
        oldself.assertEqual(1,1)

#    def test_write_STL_log_checks_expected_log_string(self):
#        """checks expected log string is written
#        """    
#        cmd ='rm -f testfile.stl'
#        os.system(cmd)
#        self.mv.readMolecule('ind.pdb')
#        self.mv.displayCPK("ind:::")
#        self.mv.writeSTL('testfile.stl')
#        tx = self.mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],"self.writeSTL('testfile.stl', log=0)")

    def test_write_STL_log_checks_that_it_runs(self):
        """Checking log string runs """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')
        oldself=self
        self =mv
        s = "self.writeSTL('testfile.stl')"
        exec(s)
        oldself.assertEqual(1,1)
        

class FileCommandsLogReadTest(FileBaseTest):

    def test_read_molecule_log_checks_expected_log_string(self):
        """checks expected log string is written
        """        
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/1crn.pdb")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readMolecule('Data/1crn.pdb', ask=0, parser=None, log=0)")
        
    def test_read_molecule_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s = "self.readMolecule('Data/1crn.pdb')"
        exec(s)
        oldself.assertEqual(1,1)  
    
        
    def test_read_pdb_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readPDB("Data/ind.pdb")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readPDB('Data/ind.pdb', ask=0, log=0)")


    def test_read_pdb_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s ="self.readPDB('Data/ind.pdb')"
        exec(s)
        oldself.assertEqual(1,1)

    

               
    def test_read_pdbq_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
   
        self.mv.readPDBQ("Data/1crn.pdbq")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readPDBQ('Data/1crn.pdbq', ask=0, log=0)")
            
    def test_read_pdbq_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s ="self.readPDBQ('Data/1crn.pdbq')"
        exec(s)
        oldself.assertEqual(1,1)

        
    def test_read_pdbqs_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readPDBQS("Data/1crn.pdbqs")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readPDBQS('Data/1crn.pdbqs', ask=0, log=0)")
        
    def test_read_pdbqs_log_checks_that_it_runs(self):
        """Checking log string runs """
        
        oldself=self
        self =mv
        s ="self.readPDBQS('Data/1crn.pdbqs')"
        exec(s)
        oldself.assertEqual(1,1)

    def test_read_pqr_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readPQR("Data/mead.pqr")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readPQR('Data/mead.pqr', ask=0, log=0)")
        
    
    def test_read_pqr_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s="self.readPQR('Data/mead.pqr')"
        exec(s)
        oldself.assertEqual(1,1)


    def test_read_cif_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMMCIF("Data/1CRN.cif")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readMMCIF('Data/1CRN.cif', ask=0, log=0)")


    def test_read_cif_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s ="self.readMMCIF('Data/1CRN.cif')"
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_read_mol2_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMOL2("Data/hpi1s.mol2")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.readMOL2('Data/hpi1s.mol2', ask=0, log=0)")

    def test_read_mol2_log_checks_that_it_runs(self):
        """Checking log string runs """
        oldself=self
        self =mv
        s="self.readMOL2('Data/hpi1s.mol2')"
        exec(s)
        oldself.assertEqual(1,1)



    ###TESTING FILE WITH NO. OF MODELS
    def test_read_file_with_no_of_models(self):
        """reading molecule with no.of models and testing for ,
        number of models=number of  mols
        number of atoms in model=number of atoms in molecule
        """
        self.mv.readMolecule("./Data/1AY3.pdb")    
        self.assertEqual((len(mv.Mols)),27)
        #testing no.of mols in viewer equal to number of models
        #open 1AY3.pdb file readlines ,pattern search for MODEL 
        #find last MODEL line which contains last model number
        fptr=open("./Data/1AY3.pdb")
        data=fptr.readlines()
        pat = re.compile('MODEL')
        c=[]
        for line in data:
             match = pat.search(line)
             if match:
                c.append(line)
        
        self.assertEqual(str(len(self.mv.Mols)),c[-1].split()[-1])      
        #checking for no.of atoms equal in model and molecule
        #serach for pattern match MODEL, ENDMDL and count lines in between them that startswith ATOM or HETATM compare with list of length of atoms in molecules.
        newlist=[]
        list=0
        pat = re.compile('MODEL')
        pat1=re.compile('ENDMDL')
        for line in range(len(data)):
            match = pat.search(data[line])
            match1 = pat1.search(data[line])
            match2 = data[line].startswith('ATOM')
            match3=data[line].startswith('HETATM')
            if match:
                var=1
            if match2 or match3:
                list=list+1
            if match1:
                var=0    
                newlist.append(list)
                list=0
        molatmlist=[]
        for l in range(len(mv.Mols)):
            molatmlist.append(len(mv.Mols[l].allAtoms))
        self.assertEqual(newlist,molatmlist)


    ##Tests for readFromWeb Command
class ReadFromWebTest(FileBaseTest):

    def test_readFromWebCommand_pdbid(self):
        """testing calling command with pdbid"""
        mol = self.mv.fetch('1crn', log=0)
        self.assertEqual(mol[0].name,'1crn')

    def test_readFromWebCommand_keyword(self):
        """testing calling with keyword CRAMBIN"""
        if not self.mv.hasGui:
            return
        
        c = self.mv.fetch
        
        def call_enter():
            #f = c.showForm('Fetch',modal =0,blocking = 0)
            form = c.cmdForms['Fetch']
            ebn = form.descr.entryByName
            ebn['keyword']['widget'].setentry('CRAMBIN')
            form.OK_cb()
                        
        def modifiedInit(arg):            
            self.mv.GUI.ROOT.after(1, call_enter)            
        c.initFunc = modifiedInit
        c.guiCallback()
        
    def test_readFromWebCommand_clearPDBCache(self):
        """testing readFromWebCommand,clearPDBCache """
        if not self.mv.hasGui:
            return
        c = self.mv.fetch
        f = c.showForm('Fetch',modal =0,blocking = 0)
        form = c.cmdForms['Fetch']
        ebn = form.descr.entryByName
        #ebn['PDBCachedir']['widget'].invoke()
        c.clearPDBCache()
        d = c.getPDBCachedir()
        if d:
            if os.path.exists(d):
                filenames = os.listdir(d)
                self.assertEqual(len(filenames),0)
            
        

if __name__ == '__main__': 
    unittest.main() 



