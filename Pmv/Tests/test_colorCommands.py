#
#
# $Id: test_colorCommands.py,v 1.38 2012/03/15 23:29:38 annao Exp $
#
#
#############################################################################
#
# Author: Sophie COON, Sowjanya KARNATI
#
# Copyright: M. Sanner TSRI 2000
#
#############################################################################
import sys,unittest,Pmv,time
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from mglutil.util.defaultPalettes import MolColors
mv = None
klass = None
ct = 0
totalCt = 99
from string import split
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class ColorBaseTests(unittest.TestCase):

    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, gui=hasGUI)
            #mv = MoleculeViewer(customizer = './.empty', logMode = 'no', withShell=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands',package= 'Pmv')
            mv.browseCommands('deleteCommands',package='Pmv') 
            mv.browseCommands('colorCommands',package='Pmv')
            mv.browseCommands("interactiveCommands",package='Pmv')
            mv.browseCommands('displayCommands',
                              commands=['displaySticksAndBalls','undisplaySticksAndBalls',
                                        'displayCPK', 'undisplayCPK',
                                        'displayLines','undisplayLines',
                                        'displayBackboneTrace','undisplayBackboneTrace',
                                        'DisplayBoundGeom'
                                        ],package='Pmv', topCommand=0)
            #mv.browseCommands("displayCommands",package = 'Pmv')
            mv.browseCommands('selectionCommands')
            mv.browseCommands("bondsCommands",
                          commands=['buildBondsByDistance'],
                          package='Pmv', log = 0)
        
            mv.setOnAddObjectCommands(['buildBondsByDistance',
                                       'displayLines'],
                                  log=0)
        self.mv = mv
        self.mv.undo = mv.NEWundo

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            print 'setup: destroying mv'
            if mv and mv.hasGui:
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name 
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv 
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            print "total count:", ct
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None
########################### COLOR COMMAND TESTS ###############################

class ColorTests(ColorBaseTests):

#widget buttons  check    
    def xtest_color_geomsGUI_widget(self):
        """checks color widget is diplayed
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.assertEqual(len(self.mv.Mols), 1)
        c=self.mv.color
        # Create the Select Geometry Inputform
        f= c.showForm('geomsGUI', modal=0, blocking=0, force=1)
        # this is to leanup the self.expamndednodes_____Atoms etc...
        c.cleanup()
        ebn = f.descr.entryByName
        # Need to do this otherwise the form closes before the assert
        button = ebn[0]['widget']
        button.wait_visibility(button)
        # Testing that the form is open
        self.assertEqual(f.root.winfo_ismapped(),1)
        f.withdraw()
        
    def xtest_choose_color_widget(self):
        """checks choose color widget is displayed
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        form=c.showForm('chooseColor',modal=0,blocking=0)
        c.cleanup()
        ebn = form.descr.entryByName
        # Need to do this otherwise the form closes before the assert
        button = form.descr.entryByName['dismiss']['widget']
        button.wait_visibility(button)
        self.assertEqual(form.root.winfo_ismapped(),1)
        form.withdraw()

    def xtest_color_all_geometries(self):
        """checks AllGeometries Button
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c = self.mv.color
        #assert not self.vf.color.cmdForms.has_key('geomsGUI')
        f = self.mv.color.showForm('geomsGUI', modal=0, blocking=0, force=1)
        c.cleanup()
        allGeombut = c.idf.entryByName[0]['widget']
        #allGeombut.invoke()
        c.selectall_cb()
        val = f.checkValues()
        allGeombut.wait_visibility(allGeombut)
        self.assertEqual(val['geoms'], ('lines',))
        f.withdraw()
    
    def xtest_color_No_geometries(self):
        """checks NoGeometries Button
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        f = self.mv.color.showForm('geomsGUI', modal=0, blocking=0, force=1)
        c.cleanup()
        NoGeombut = c.idf.entryByName[1]['widget']
        #NoGeometries Command
        c.deselectall_cb()
        val = f.checkValues()
        NoGeombut.wait_visibility(NoGeombut)
        self.assertEqual(val['geoms'], ( ))
        f.withdraw()

    def xtest_color_show_undispalyed_geometries_on(self):
        """checks ShowUnDisGeometries Button On
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        #self.mv.color(mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ['balls'])
        f = self.mv.color.showForm('geomsGUI', modal=0, blocking=0, force=1)
        c.cleanup()
        ShowUndisGeombut = f.descr.entryByName['showUndis']['widget']
        ShowUndisGeombut.invoke()
        
        val = f.checkValues()
        ShowUndisGeombut.wait_visibility(ShowUndisGeombut)
        #ShowUndisGeombut On
        self.assertEqual(c.idf.entryByName['showUndis']['wcfg']['variable'].get(),1)
        f.withdraw()

    def xtest_color_show_undispalyed_geometries_allgeom_selected(self):
        """checks ShowUnDisGeometries Button all geomtries selected
        """
        self.mv.browseCommands("displayCommands",
                               commands=['displaySticksAndBalls',],
                               package='Pmv')
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.browseCommands("displayCommands", commands=['displayCPK',],
                               package='Pmv')
        c=self.mv.color
        f = self.mv.color.showForm('geomsGUI', modal=0, blocking=0, force=1)
        c.cleanup()
        ShowUndisGeombut = f.descr.entryByName['showUndis']['widget']
        #ShowUndisGeombut On
        ShowUndisGeombut.invoke()
        #All Geometries button On
        c.idf.entryByName[0]['widget'].invoke()
        val = f.checkValues()
        ShowUndisGeombut.wait_visibility(ShowUndisGeombut)
        self.assertEqual(val['geoms'],('balls', 'sticks', 'lines', 'cpk'))
        f.withdraw()
    
    def xtest_color_show_undispalyed_geometries_Nogeom_selected(self):
        """checks ShowUnDisGeometries Button No geomtries selected
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        f = self.mv.color.showForm('geomsGUI', modal=0, blocking=0, force=1)
        c.cleanup()
        ShowUndisGeombut = f.descr.entryByName['showUndis']['widget']
        #ShowUndisGeombut On
        ShowUndisGeombut.invoke()
        #No Geometries button On
        c.idf.entryByName[1]['widget'].invoke()
        val = f.checkValues()
        ShowUndisGeombut.wait_visibility(ShowUndisGeombut)
        self.assertEqual(val['geoms'],())
        f.withdraw()

    def xtest_color_show_undispalyed_geometries_selected_geom(self):
        """checks ShowUnDisGeometries Button ,geomtries selected
        """
        self.mv.browseCommands("displayCommands",
                               commands=['displaySticksAndBalls',],
                               package='Pmv')
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.browseCommands("displayCommands", commands=['displayCPK',],
                               package='Pmv')
        c=self.mv.color
        f = self.mv.color.showForm('geomsGUI', modal=0, blocking=0, force=1)
        c.cleanup()
        ShowUndisGeombut = f.descr.entryByName['showUndis']['widget']
        #ShowUndisGeombut On
        ShowUndisGeombut.invoke()
        # selecting some Geometries 
        c.idf.entryByName['geoms']['widget'].invoke(0)
        c.idf.entryByName['geoms']['widget'].invoke(3)
        val = f.checkValues()
        ShowUndisGeombut.wait_visibility(ShowUndisGeombut)
        self.assertEqual(val['geoms'],('balls', 'cpk'))
        f.withdraw()
#end widget buttons check        
    
#color command    
    def test_color_balls(self):
        """checks color balls
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        #dispalying as sticks and balls        
        self.mv.displaySticksAndBalls("1crn") 
        #color command
        self.mv.color(self.mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ['balls'])
        self.assertEqual(mv.allAtoms[0].colors['balls'],(1.0, 0.0, 0.0, 1.0))
    
    def test_color_lines(self):
        """checks color lines
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        #color command
        self.mv.color(self.mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ['lines'])
        self.assertEqual(mv.allAtoms[0].colors['lines'],(1.0, 0.0, 0.0, 1.0))
               
    def test_color_sticks(self):
        """checks color sticks
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        #dispalying as sticks and balls
        self.mv.displaySticksAndBalls("1crn")
        #color command
        self.mv.color(self.mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ('all',))
        self.assertEqual(mv.allAtoms[0].colors['sticks'],(1.0, 0.0, 0.0, 1.0))
   
    def test_color_cpk(self):
        """checks color cpk
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        #displaying as cpk
        self.mv.displayCPK("1crn",only = 0, 
                           log = 0, negate = 0, scaleFactor = 1.0,
                           quality = 10)
        #color command
        self.mv.color(self.mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ['cpk'])
        self.assertEqual(mv.allAtoms[0].colors['cpk'],(1.0, 0.0, 0.0, 1.0))

    
#tests all colors in choose color widget        
    def test_choose_color_white(self):
        """colors check in choose color White
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        #color command with color white 
        self.mv.color(self.mv.Mols[0], [(1.,1.,1.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(1.0, 1.0, 1.0, 1.0))

    def test_choose_color_black(self):
        """colors check in choose color Black
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # color command  with color black
        self.mv.color(self.mv.Mols[0], [(0.,0.,0.,0.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(0.0,0.0,0.0,0.0))

    def test_choose_color_blue(self):
        """colors check in choose color Blue
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # color command with color blue
        self.mv.color(self.mv.Mols[0], [(0.,0.,1.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(0.0, 0.0, 1.0, 1.0))


    def test_choose_color_green(self):
        """colors check in choose color Green
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # color command with color green
        self.mv.color(self.mv.Mols[0], [(0.,1.,0.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(0.0, 1.0, 0.0, 1.0))

    def test_choose_color_red(self):
        """colors check in choose color Red
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # color command with color red
        self.mv.color(self.mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(1.0, 0.0, 0.0, 1.0))
   
    def test_choose_color_greenandblue(self):
        """colors check in choose color green+blue
        """
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")
        # color command with color green+blue
        self.mv.color(self.mv.Mols[0], [(0.,1.,1.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(0.0, 1.0, 1.0, 1.0))

    def test_choose_color_redandblue(self):
        """colors check in choose color red+blue
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        self.mv.color(self.mv.Mols[0], [(1.,0.,1.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(1.0, 0.0, 1.0, 1.0))

    def test_choose_color_yellow(self):
        """colors check in choose color yellow
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        self.mv.color(self.mv.Mols[0], [(1.,1.,0.,1.),],geomsToColor = ['lines'])
        self.assertEqual(self.mv.allAtoms[0].colors['lines'],(1.0, 1.0, 0.0, 1.0))
        
## #end tests all colors in choose color widget

## #invalid input for color command    
    def test_color_invalid_input_mols(self):
        """tests invalid input for mv.Mols[0]s in color command
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        returnValue=c('hai', [(1.,0.,0.,1.),],geomsToColor = ['sticks'])
        self.assertEqual(returnValue,'ERROR')
        

    def test_color_emtpy_input_mols(self):
        """tests empty input for mv.Mols[0]s in color command
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        returnValue=c(" ", [(1.,0.,0.,1.),],geomsToColor = ['sticks'])
        self.assertEqual(returnValue,'ERROR')

    
    def test_color_invalid_input_colors(self):
        """tests empty input for colors in color command
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        returnValue=c(self.mv.Mols[0], [(1.0,1.0),],geomsToColor = ['sticks'])
        self.assertEqual(returnValue,None)
    
    
    def test_color_emtpy_input_colors(self):
        """tests empty input for colors in color command
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        returnValue=c(self.mv.Mols[0], [(),],geomsToColor = ['sticks'])
        self.assertEqual(returnValue,None)
         
    def test_color_invalid_input_geomsTocolors(self):
        """tests empty input for geomsTocolors in color command
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        returnValue=c(self.mv.Mols[0], [(1.0,1.0,1.0),], geomsToColor = ['spheres'])
        self.assertEqual(returnValue,None)
    
    
    def test_color_emtpy_input_geomsTocolors(self):
        """tests empty input for geomsTocolors in color command
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c=self.mv.color
        returnValue=c(self.mv.Mols[0], [(1.0,1.0,1.0),], geomsToColor = [' '])
        self.assertEqual(returnValue,'ERROR')

#end invalid input for color command
    
    def test_color_1(self):
        """
        Test calling all the color command with a TreeNode as an argument
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.color(mol, [(1.,1.,0.,1.),],
                      geomsToColor = ['lines'])
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.color, 'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.color, 'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.color, 'expandedNodes____Nodes'))
        self.mv.undo()
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.color, 'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.color, 'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.color, 'expandedNodes____Nodes'))

    def test_color_secondary_structure(self): 
        """
        Test color secondary structure geometry with a RGB color
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.browseCommands('secondaryStructureCommands',
                               commands=['ribbon',],
                               package='Pmv')
        self.mv.ribbon(self.mv.getSelection())
        self.mv.color(self.mv.getSelection(), [(1.,0.,1.),],
                      geomsToColor = ['secondarystructure'])
        self.assertEqual(self.mv.allAtoms[0].colors['secondarystructure'],(1.,0.,1.))

    def test_addOnAddObject_colorByAtomType(self): 
        mv.addOnAddObjectCmd(mv.colorByAtomType)
        self.mv.readMolecule('Data/1crn.pdb')

###################### COLOR BY ATOM TYPE COMMAND TESTS#######################

class ColorByAtomTypeTests(ColorBaseTests):

#invalid input for ColorByAtomType command

    def test_color_by_atom_invalid_input_nodes(self):
        """checks invalid input for nodes in
        ColorByAtomType************************
        """
        self.mv.readMolecule('Data/1crn.pdb')
        returnValue=self.mv.colorByAtomType("hello", geomsToColor = 'all')
        self.assertEqual(returnValue,'ERROR')
        
    def test_color_by_atom_invalid_input_geomsToColor(self):
        """checks invalid input for geomsToColor in ColorByAtomType
        """
        self.mv.readMolecule('Data/1crn.pdb')
        returnValue=self.mv.colorByAtomType("1crn", geomsToColor = 'hai')
        self.assertEqual(returnValue,None)
        
    def test_color_by_atom_empty_input_nodes(self):
        """checks empty input for nodes in ColorByAtomType
        """
        self.mv.readMolecule('Data/1crn.pdb')
        returnValue=self.mv.colorByAtomType("", geomsToColor = 'all')
        self.assertEqual(returnValue,None)
        
    def test_color_by_atom_empty_input_geomsToColor(self):
        """checks empty input for geomsToColor in ColorByAtomType
        """
        self.mv.readMolecule('Data/1crn.pdb')
        returnValue=self.mv.colorByAtomType("1crn", geomsToColor = '')
        self.assertEqual(returnValue,'ERROR')
        
#end invalid input for ColorByAtomType command

    def test_color_by_atom_type_get_Selection(self):
        """checks input through getSelection() 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select(self.mv.allAtoms[0])
        self.mv.colorByAtomType(self.mv.getSelection())
        #atom0 is N
        from Pmv.pmvPalettes import AtomElements
        Ncol = AtomElements['N']
        #self.assertEqual(mv.allAtoms[0].colors['lines'],(0.,0.,1.))
        self.assertEqual(mv.allAtoms[0].colors['lines'], Ncol)
        
#    def test_colorByAtomType_String(self):
#        """checks colors by atom type
#        """
#        # Load  different display commands.
#        self.mv.browseCommands("selectionCommands",
#                               commands=['selectFromString',],package='Pmv')
#        self.mv.browseCommands("labelCommands", commands=['labelByProperty',],
#                               package='Pmv')
#
#        # Read a molecule
#        self.mv.readMolecule("Data/1crn.pdb")
#        self.mv.colorByAtomType("1crn", geomsToColor='all')
#        mol = self.mv.Mols[0]
#        allAtms = mol.allAtoms
#        allC = allAtms.get(lambda x: x.element=="C")
#        allN = allAtms.get(lambda x: x.element=="N")
#        allO = allAtms.get(lambda x: x.element=="O")
#        allS = allAtms.get(lambda x: x.element=="S")
#        self.assertEqual( filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                 allC.colors['lines']), [])
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['lines']),[])
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['lines']), [])
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
#                                allS.colors['lines']), [])
        
#    def test_colorByAtomType_1(self):
#        # Load  different display commands.
#        self.mv.browseCommands("selectionCommands",
#                               commands=['selectFromString',],
#                               package='Pmv')
#        self.mv.browseCommands("labelCommands", commands=['labelByProperty',],
#                               package='Pmv')
#
#        # Read a molecule
#        self.mv.readMolecule("Data/1crn.pdb")
#        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
#        self.mv.selectFromString('','','20-30','',
#                                 negate=False, silent=True)
#        # display part of it as CPK
#        self.mv.displayCPK(self.mv.getSelection(),only = 0,
#                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10)
#        self.mv.clearSelection()
#        mol = self.mv.Mols[0]
#        # color all the geometry representing these residues by atom type
#        self.mv.colorByAtomType(self.mv.getSelection(), geomsToColor='all')
#        # 'all' will color only the displayed geometry in this case lines
#        # and cpk not sticks and balls
#        allAtms = mol.allAtoms
#        allC = allAtms.get(lambda x: x.element=="C")
#        allN = allAtms.get(lambda x: x.element=="N")
#        allO = allAtms.get(lambda x: x.element=="O")
#        allS = allAtms.get(lambda x: x.element=="S")
#        self.assertEqual( filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                 allC.colors['lines']), [])
#        self.assertEqual(filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                allC.colors['cpk']), [])
#
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['lines']),[])
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['cpk']),[])
#
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['lines']), [])
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['cpk']), [])
#
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
#                                allS.colors['lines']), [])
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
#                                allS.colors['cpk']), [])
#
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 1.0),
#                                allAtms.colors['sticks']),[])
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 1.0),
#                                allAtms.colors['balls']),[])
#
#        gc = mol.geomContainer
#        licol = gc.geoms['bonded'].materials[1028].prop[1]
#        liatms  = gc.atoms['bonded']
#        licol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                    licol.tolist())
#        liatmcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                       liatms.colors['lines'] )
#        self.assertEqual(licol,liatmcol)
#
#        cpkcol = gc.geoms['cpk'].materials[1028].prop[1]
#        cpkatms = gc.atoms['cpk']
#        cpkcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                     cpkcol.tolist())
#        cpkatmscol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                         cpkatms.colors['cpk'])
#        self.assertEqual(cpkcol,cpkatmscol)
###         self.mv.deleteMol('1crn')
#        self.mv.undo()

#    def test_colorByAtomType_2(self):
#        # Load  different display commands.
#        self.mv.browseCommands("selectionCommands",
#                               commands=['selectFromString'],package='Pmv')
#        self.mv.browseCommands("labelCommands", commands=['labelByProperty',], package='Pmv')
#
#        # Read a molecule
#        self.mv.readMolecule("Data/1crn.pdb")
#        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
#        self.mv.selectFromString(res='20-30',negate=False, silent=True)
#
#        # display part of it as CPK
#        self.mv.displayCPK(self.mv.getSelection(),only = 0, 
#                           log = 0, negate = 0, scaleFactor = 1.0,
#                           quality = 10)
#
#        # display another part of it as sticks and balls
#        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
#        self.mv.selectFromString(res='5-15', negate=False, silent=True )
#        self.mv.displaySticksAndBalls(self.mv.getSelection())
#        self.mv.clearSelection()
#
#        # select residue 10 through 25
#        self.mv.selectFromString(res='10-25', negate=False, silent=True)
#        mol = self.mv.Mols[0]
#        selAtms = self.mv.getSelection().atoms[:]
#        # color the cpk, sticks and balls 
#        self.mv.colorByAtomType(self.mv.getSelection(), geomsToColor = ['sticks', 'balls', 'lines'])
#        # 'all' will color only the displayed geometry in this case lines and cpk not sticks and balls
#        allC = selAtms.get(lambda x: x.element=="C")
#        allN = selAtms.get(lambda x: x.element=="N")
#        allO = selAtms.get(lambda x: x.element=="O")
#        allS = selAtms.get(lambda x: x.element=="S")
#        self.assertEqual( filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                 allC.colors['lines']),[])
#        self.assertEqual(filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                allC.colors['sticks']),[])
#        self.assertEqual(filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                allC.colors['balls']), [])
#
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['lines']),[])
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['sticks']),[])
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['balls']), [])
#
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['lines']), [])
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['sticks']),[])
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['balls']), [])
#
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
#                                allS.colors['lines']),[])
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
#                                allS.colors['sticks']),[])
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
#                                allS.colors['balls']), [])
#
#
#        gc = mol.geomContainer
#        licol = gc.geoms['bonded'].materials[1028].prop[1]
#        liatms  = gc.atoms['bonded']
#        licol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                    licol.tolist())
#        liatmcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                       liatms.colors['lines'] )
#        self.assertEqual( licol , liatmcol)
#
#        cpkcol = gc.geoms['cpk'].materials[1028].prop[1]
#        cpkatms = gc.atoms['cpk']
#        cpkcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                     cpkcol.tolist())
#        cpkatmscol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                         cpkatms.colors['cpk'])
#        self.assertEqual( cpkcol, cpkatmscol)
#
#        stickscol = gc.geoms['sticks'].materials[1028].prop[1]
#        sticksatms = gc.atoms['sticks']
#        stickscol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                        stickscol.tolist())
#        sticksatmscol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                            sticksatms.colors['sticks'])
#        self.assertEqual(cpkcol, cpkatmscol)
#
#        ballscol = gc.geoms['balls'].materials[1028].prop[1]
#        ballsatms = gc.atoms['balls']
#        ballscol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                       ballscol.tolist())
#        ballsatmscol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                           ballsatms.colors['balls'])
#        self.assertEqual(ballscol,ballsatmscol)
        

#    def test_colorLabels_atoms(self):
#        self.mv.browseCommands("selectionCommands",
#                               commands=['selectFromString'],package='Pmv')
#        self.mv.browseCommands("labelCommands", commands=['labelByProperty',], package='Pmv')
#        self.mv.readMolecule("Data/1crn10.pdb")
#        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
#        self.mv.selectFromString(atoms='*', negate=False, silent=True)
#        self.mv.labelByProperty(self.mv.getSelection(),
#                                only = 0, 
#                                textcolor = (0.0, 0.0, 1.0),
#                                location = 'Center', 
#                                format = None, 
#                                negate = 0, 
#                                font = 'arial1.glf', 
#                                properties = ['element'], 
#                                log = 0)
#        self.mv.clearSelection()
#        self.mv.colorByAtomType("1crn10", geomsToColor=["AtomLabels",])
#        mol = self.mv.Mols[0]
#        allAtms = mol.allAtoms
#        allC = allAtms.get(lambda x: x.element=="C")
#        allN = allAtms.get(lambda x: x.element=="N")
#        allO = allAtms.get(lambda x: x.element=="O")
#        allS = allAtms.get(lambda x: x.element=="S")
#        #print filter(lambda x: not x == (0.7, 0.7, 0.7), allC.colors['AtomLabels'])==[]
#        self.assertEqual(filter(lambda x: not x == (0.7, 0.7, 0.7),
#                                allC.colors['AtomLabels']),[])
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                allN.colors['AtomLabels']), [])
#        self.assertEqual(filter(lambda x: not x == (1.0, 0.0, 0.0),
#                                allO.colors['AtomLabels']) ,[])
#        self.assertEqual( filter(lambda x: not x == (1.0, 1.0, 0.),
#                                 allS.colors['AtomLabels']),[])


#################### COLOR BY RESIDUE TYPE COMMAND TESTS######################



class ColorByResidueType(ColorBaseTests):
    
    def test_color_by_residue_type_1(self):
        """
        Test calling all the color command with a TreeNode as an argument
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorByResidueType(mol)
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorByResidueType,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorByResidueType,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorByResidueType,
                                   'expandedNodes____Nodes'))
        self.mv.undo()
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorByResidueType,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorByResidueType,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorByResidueType,
                                   'expandedNodes____Nodes'))

#tests invalid input for colorByResidueType command
    def test_color_by_residue_type_invalid_input_nodes(self):
        """tests color by Residue Type,invalid input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByResidueType
        returnValue = c("abc")
        self.assertEqual(returnValue,'ERROR')
        
    
    def test_color_by_residue_type_empty_input_nodes(self):
        """tests color by Residue Type,empty input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByResidueType
        returnValue = c("")
        self.assertEqual(returnValue,None)
        
                              
    def test_color_by_residue_type_invalid_input_geomsToColor(self):
        """tests color by Residue Type,invalid input,geomsToColor
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByResidueType
        returnValue = c(mv.Mols[0],geomsToColor = ['hai'])
        self.assertEqual(returnValue,None)


    def test_color_by_residue_type_empty_input_geomsToColor(self):
        """tests color by Residue Type,empty input,geomsToColor
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByResidueType
        returnValue = c(mv.Mols[0],geomsToColor = [''])
        self.assertEqual(returnValue,'ERROR')
#end tests invalid input for colorByResidueType command

    def test_color_by_residue_type_for_residues_lines(self):
        """tests color by residue type, coloring lines
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByResidueType
        c(mv.Mols[0],geomsToColor = ['lines'])
        #finding residues
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        resTHRcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resTHR.atoms.colors['lines'])
        resCYScol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resCYS.atoms.colors['lines'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['lines'])
        resSERcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resSER.atoms.colors['lines'])
        resILEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resILE.atoms.colors['lines'])
        resVALcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resVAL.atoms.colors['lines'])
        resALAcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resALA.atoms.colors['lines'])
        resARGcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resARG.atoms.colors['lines'])                 
        resASNcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resASN.atoms.colors['lines'])
        resPHEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPHE.atoms.colors['lines'])
        resLEUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resLEU.atoms.colors['lines'])
        resGLYcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLY.atoms.colors['lines'])
        resGLUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLU.atoms.colors['lines'])
        self.assertEqual(resTHRcol[0], (1.0,1.0,0.0))#orange
        self.assertEqual(resCYScol[0], (1.0,1.0,0.0))#yellow
        self.assertEqual(resPROcol[0], (1.0,1.0,1.0))#flesh
        self.assertEqual(resSERcol[0], (1.0,1.0,0.0))#orange
        self.assertEqual(resILEcol[0], (0.0,1.0,0.0))#green
        self.assertEqual(resVALcol[0], (0.0,1.0,0.0))#green
        self.assertEqual(resALAcol[0], (1.0,1.0,1.0))#dark grey
        self.assertEqual(resARGcol[0], (0.0,0.0,1.0))#blue
        self.assertEqual(resASNcol[0], (0.0,1.0,1.0))#cyan
        self.assertEqual(resPHEcol[0], (0.0,0.0,1.0))#mid blue
        self.assertEqual(resLEUcol[0], (0.0,1.0,0.0))#green
        self.assertEqual(resGLYcol[0], (1.0,1.0,1.0))#light grey
        self.assertEqual(resGLUcol[0], (1.0,0.0,0.0))#bright red



    def test_color_by_residue_type_for_residues_cpk(self):
        """tests color by residue type ,coloring cpk
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displayCPK("1crn",only = 0, 
                           log = 0, negate = 0, scaleFactor = 1.0,
                           quality = 10)
        c=self.mv.colorByResidueType
        c(mv.Mols[0],geomsToColor = ['cpk'])
        #finding residues
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        resTHRcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resTHR.atoms.colors['cpk'])
        resCYScol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resCYS.atoms.colors['cpk'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['cpk'])
        resSERcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resSER.atoms.colors['cpk'])
        resILEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resILE.atoms.colors['cpk'])
        resVALcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resVAL.atoms.colors['cpk'])
        resALAcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resALA.atoms.colors['cpk'])
        resARGcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resARG.atoms.colors['cpk'])                 
        resASNcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resASN.atoms.colors['cpk'])
        resPHEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPHE.atoms.colors['cpk'])
        resLEUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resLEU.atoms.colors['cpk'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['cpk'])
        resGLYcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLY.atoms.colors['cpk'])
        resGLUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLU.atoms.colors['cpk'])
        self.assertEqual(resTHRcol[0], (1.0,1.0,0.0))
        self.assertEqual(resCYScol[0], (1.0,1.0,0.0))
        self.assertEqual(resPROcol[0], (1.0,1.0,1.0))
        self.assertEqual(resSERcol[0], (1.0,1.0,0.0))
        self.assertEqual(resILEcol[0], (0.0,1.0,0.0))
        self.assertEqual(resVALcol[0], (0.0,1.0,0.0))
        self.assertEqual(resALAcol[0], (1.0,1.0,1.0))
        self.assertEqual(resARGcol[0], (0.0,0.0,1.0))
        self.assertEqual(resASNcol[0], (0.0,1.0,1.0))
        self.assertEqual(resPHEcol[0], (0.0,0.0,1.0))
        self.assertEqual(resLEUcol[0], (0.0,1.0,0.0))
        self.assertEqual(resPROcol[0], (1.0,1.0,1.0))
        self.assertEqual(resGLYcol[0], (1.0,1.0,1.0))
        self.assertEqual(resGLUcol[0], (1.0,0.0,0.0))

    def test_color_by_residue_type_for_residues_sticks(self):
        """tests color by residue type ,coloring sticks
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displaySticksAndBalls("1crn")
        c=self.mv.colorByResidueType
        c(mv.Mols[0],geomsToColor = ['sticks','balls'])
        #finding residues
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        resTHRcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resTHR.atoms.colors['sticks'])
        resCYScol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resCYS.atoms.colors['sticks'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['sticks'])
        resSERcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resSER.atoms.colors['sticks'])
        resILEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resILE.atoms.colors['sticks'])
        resVALcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resVAL.atoms.colors['sticks'])
        resALAcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resALA.atoms.colors['sticks'])
        resARGcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resARG.atoms.colors['sticks'])                 
        resASNcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resASN.atoms.colors['sticks'])
        resPHEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPHE.atoms.colors['sticks'])
        resLEUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resLEU.atoms.colors['sticks'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['sticks'])
        resGLYcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLY.atoms.colors['sticks'])
        resGLUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLU.atoms.colors['sticks'])
        self.assertEqual(resTHRcol[0], (1.0,1.0,0.0))
        self.assertEqual(resCYScol[0], (1.0,1.0,0.0))
        self.assertEqual(resPROcol[0], (1.0,1.0,1.0))
        self.assertEqual(resSERcol[0], (1.0,1.0,0.0))
        self.assertEqual(resILEcol[0], (0.0,1.0,0.0))
        self.assertEqual(resVALcol[0], (0.0,1.0,0.0))
        self.assertEqual(resALAcol[0],(1.0,1.0,1.0))
        self.assertEqual(resARGcol[0],(0.0,0.0,1.0))
        self.assertEqual(resASNcol[0],(0.0,1.0,1.0))
        self.assertEqual(resPHEcol[0],(0.0,0.0,1.0))
        self.assertEqual(resLEUcol[0],(0.0,1.0,0.0))
        self.assertEqual(resPROcol[0],(1.0,1.0,1.0))
        self.assertEqual(resGLYcol[0],(1.0,1.0,1.0))
        self.assertEqual(resGLUcol[0],(1.0,0.0,0.0))    

    def test_color_by_residue_type_for_residues_balls(self):
        """tests color by residue type ,coloring balls
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displaySticksAndBalls("1crn")
        c=self.mv.colorByResidueType
        c(mv.Mols[0],geomsToColor = ['sticks','balls'])
        #finding residues
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        resTHRcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resTHR.atoms.colors['balls'])
        resCYScol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resCYS.atoms.colors['balls'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['balls'])
        resSERcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resSER.atoms.colors['balls'])
        resILEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resILE.atoms.colors['balls'])
        resVALcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resVAL.atoms.colors['balls'])
        resALAcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resALA.atoms.colors['balls'])
        resARGcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resARG.atoms.colors['balls'])                 
        resASNcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resASN.atoms.colors['balls'])
        resPHEcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPHE.atoms.colors['balls'])
        resLEUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resLEU.atoms.colors['balls'])
        resPROcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resPRO.atoms.colors['balls'])
        resGLYcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLY.atoms.colors['balls'])
        resGLUcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         resGLU.atoms.colors['balls'])
        self.assertEqual(resTHRcol[0], (1.0,1.0,0.0))
        self.assertEqual(resCYScol[0], (1.0,1.0,0.0))
        self.assertEqual(resPROcol[0], (1.0,1.0,1.0))
        self.assertEqual(resSERcol[0], (1.0,1.0,0.0))
        self.assertEqual(resILEcol[0], (0.0,1.0,0.0))
        self.assertEqual(resVALcol[0], (0.0,1.0,0.0))
        self.assertEqual(resALAcol[0],(1.0,1.0,1.0))
        self.assertEqual(resARGcol[0],(0.0,0.0,1.0))
        self.assertEqual(resASNcol[0],(0.0,1.0,1.0))
        self.assertEqual(resPHEcol[0],(0.0,0.0,1.0))
        self.assertEqual(resLEUcol[0],(0.0,1.0,0.0))
        self.assertEqual(resPROcol[0],(1.0,1.0,1.0))
        self.assertEqual(resGLYcol[0],(1.0,1.0,1.0))
        self.assertEqual(resGLUcol[0],(1.0,0.0,0.0))
    
    def test_colorLabels_residues(self):
        """tests coloring residue labels
        """
        self.mv.browseCommands("selectionCommands",
                               commands=['selectFromString'],package='Pmv')
        self.mv.browseCommands("labelCommands", commands=['labelByProperty',], package='Pmv')
        self.mv.readMolecule("Data/1crn10.pdb")
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(res = 'THR*')
        self.mv.labelByProperty(self.mv.getSelection(),only = 0, 
                                textcolor = (0.0, 0.0, 1.0),
                                location = 'Center', 
                                format = None, 
                                negate = 0, 
                                font = 'arial1.glf')
        self.mv.colorByResidueType("1crn10",geomsToColor=["ResidueLabels",])
        
        res=self.mv.Mols[0].chains.residues.get(lambda x: x.type=='THR')
        rescol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         res.atoms.colors['ResidueLabels'])

        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 0.),
                                 rescol),[])


##########COLOR RESIDUES USING SHAPELY COMMAND TESTS##########################


class ColorResiduesUsingShapely(ColorBaseTests):

    def test_colorResiduesUsingShapely_1(self):
        """
        Test calling all the color command with a TreeNode as an argument
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorResiduesUsingShapely(mol)
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorResiduesUsingShapely,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorResiduesUsingShapely,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorResiduesUsingShapely,
                                   'expandedNodes____Nodes'))
        self.mv.undo()
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorResiduesUsingShapely,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorResiduesUsingShapely,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorResiduesUsingShapely,
                                   'expandedNodes____Nodes'))
#tests invalid input for colorResiduesUsingShapely command

    def test_color_residues_using_shapely_invalid_nodes(self):
        """checks invalid input nodes for colorResiduesUsingShapely
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorResiduesUsingShapely
        returnValue =c("abcd")
        self.assertEqual(returnValue,'ERROR')
        
    def test_color_residues_using_shapely_empty_nodes(self):
        """checks empty input nodes for colorResiduesUsingShapely
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorResiduesUsingShapely
        returnValue =c("")
        self.assertEqual(returnValue,None)

    def test_color_residues_using_shapely_invalid(self):
        """checks invalid input geomsToColor for colorResiduesUsingShapely
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorResiduesUsingShapely
        returnValue =c("1crn",geomsToColor = ['abcd'])
        self.assertEqual(returnValue,None)

    def test_color_residues_using_shapely_empty(self):
        """checks empty input geomsToColor for colorResiduesUsingShapely
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorResiduesUsingShapely
        returnValue =c("1crn",geomsToColor = [''])
        self.assertEqual(returnValue,'ERROR')
#end invalid input for colorResiduesUsingShapely command       

    def test_color_residues_using_shapely_lines(self):
        """tests color residues shapely colrs using shapely coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        #coloring using colorResiduesUsingShapely
        c=self.mv.colorResiduesUsingShapely
        c(mv.Mols[0],geomsToColor = ['lines'])
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        #finding colors of residues
        resTHRcolS = resTHR.atoms.colors['lines']
        resCYScolS = resCYS.atoms.colors['lines']
        resPROcolS = resPRO.atoms.colors['lines']
        resSERcolS = resSER.atoms.colors['lines']
        resILEcolS = resILE.atoms.colors['lines']
        resVALcolS = resVAL.atoms.colors['lines']
        resALAcolS = resALA.atoms.colors['lines']
        resARGcolS = resARG.atoms.colors['lines']                 
        resASNcolS = resASN.atoms.colors['lines']
        resPHEcolS = resPHE.atoms.colors['lines']
        resLEUcolS = resLEU.atoms.colors['lines']
        resGLYcolS = resGLY.atoms.colors['lines']
        resGLUcolS = resGLU.atoms.colors['lines']
        #coloring using colorByResidueType
        self.mv.colorByResidueType(mv.Mols[0],geomsToColor = ['lines'])
        resTHRcol = resTHR.atoms.colors['lines']
        resCYScol = resCYS.atoms.colors['lines']
        resPROcol = resPRO.atoms.colors['lines']
        resSERcol = resSER.atoms.colors['lines']
        resILEcol = resILE.atoms.colors['lines']
        resVALcol = resVAL.atoms.colors['lines']
        resALAcol = resALA.atoms.colors['lines']
        resARGcol = resARG.atoms.colors['lines']                 
        resASNcol = resASN.atoms.colors['lines']
        resPHEcol = resPHE.atoms.colors['lines']
        resLEUcol = resLEU.atoms.colors['lines']
        resGLYcol = resGLY.atoms.colors['lines']
        resGLUcol = resGLU.atoms.colors['lines']
        #comparing colors of same reisidues 
        self.assertEqual(resTHRcol != resTHRcolS,True)
        self.assertEqual(resCYScol != resCYScolS,True)
        self.assertEqual(resPROcol != resPROcolS,True)
        self.assertEqual(resSERcol != resSERcolS,True)
        self.assertEqual(resILEcol != resILEcolS,True)
        self.assertEqual(resVALcol != resVALcolS,True)
        self.assertEqual(resALAcol != resALAcolS,True)
        self.assertEqual(resARGcol != resARGcolS,True)
        self.assertEqual(resASNcol != resASNcolS,True)
        self.assertEqual(resPHEcol != resPHEcolS,True)
        self.assertEqual(resLEUcol != resLEUcolS,True)
        self.assertEqual(resGLYcol != resGLYcolS,True)
        self.assertEqual(resGLUcol != resGLUcolS,True)
    
    def test_color_residues_using_shapely_sticks(self):
        """tests color residues shapely colrs using shapely coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        #coloring using colorResiduesUsingShapely
        self.mv.displaySticksAndBalls(self.mv.Mols[0])
        c=self.mv.colorResiduesUsingShapely
        c(mv.Mols[0],geomsToColor = ['sticks','balls'])
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        #finding colors of residues
        resTHRcolS = resTHR.atoms.colors['sticks']
        resCYScolS = resCYS.atoms.colors['sticks']
        resPROcolS = resPRO.atoms.colors['sticks']
        resSERcolS = resSER.atoms.colors['sticks']
        resILEcolS = resILE.atoms.colors['sticks']
        resVALcolS = resVAL.atoms.colors['sticks']
        resALAcolS = resALA.atoms.colors['sticks']
        resARGcolS = resARG.atoms.colors['sticks']                 
        resASNcolS = resASN.atoms.colors['sticks']
        resPHEcolS = resPHE.atoms.colors['sticks']
        resLEUcolS = resLEU.atoms.colors['sticks']
        resGLYcolS = resGLY.atoms.colors['sticks']
        resGLUcolS = resGLU.atoms.colors['sticks']
        #coloring using colorByResidueType
        self.mv.colorByResidueType(mv.Mols[0],geomsToColor = ['sticks'])
        resTHRcol = resTHR.atoms.colors['sticks']
        resCYScol = resCYS.atoms.colors['sticks']
        resPROcol = resPRO.atoms.colors['sticks']
        resSERcol = resSER.atoms.colors['sticks']
        resILEcol = resILE.atoms.colors['sticks']
        resVALcol = resVAL.atoms.colors['sticks']
        resALAcol = resALA.atoms.colors['sticks']
        resARGcol = resARG.atoms.colors['sticks']                 
        resASNcol = resASN.atoms.colors['sticks']
        resPHEcol = resPHE.atoms.colors['sticks']
        resLEUcol = resLEU.atoms.colors['sticks']
        resGLYcol = resGLY.atoms.colors['sticks']
        resGLUcol = resGLU.atoms.colors['sticks']
        #comparing colors of same reisidues 
        self.assertEqual(resTHRcol != resTHRcolS,True)
        self.assertEqual(resCYScol != resCYScolS,True)
        self.assertEqual(resPROcol != resPROcolS,True)
        self.assertEqual(resSERcol != resSERcolS,True)
        self.assertEqual(resILEcol != resILEcolS,True)
        self.assertEqual(resVALcol != resVALcolS,True)
        self.assertEqual(resALAcol != resALAcolS,True)
        self.assertEqual(resARGcol != resARGcolS,True)
        self.assertEqual(resASNcol != resASNcolS,True)
        self.assertEqual(resPHEcol != resPHEcolS,True)
        self.assertEqual(resLEUcol != resLEUcolS,True)
        self.assertEqual(resGLYcol != resGLYcolS,True)
        self.assertEqual(resGLUcol != resGLUcolS,True)
    
    def test_color_residues_using_shapely_balls(self):
        """tests color residues shapely colrs using shapely coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        #coloring using colorResiduesUsingShapely
        self.mv.displaySticksAndBalls(self.mv.Mols[0])
        c=self.mv.colorResiduesUsingShapely
        c(mv.Mols[0],geomsToColor = ['balls'])
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        #finding colors of residues
        resTHRcolS = resTHR.atoms.colors['balls']
        resCYScolS = resCYS.atoms.colors['balls']
        resPROcolS = resPRO.atoms.colors['balls']
        resSERcolS = resSER.atoms.colors['balls']
        resILEcolS = resILE.atoms.colors['balls']
        resVALcolS = resVAL.atoms.colors['balls']
        resALAcolS = resALA.atoms.colors['balls']
        resARGcolS = resARG.atoms.colors['balls']                 
        resASNcolS = resASN.atoms.colors['balls']
        resPHEcolS = resPHE.atoms.colors['balls']
        resLEUcolS = resLEU.atoms.colors['balls']
        resGLYcolS = resGLY.atoms.colors['balls']
        resGLUcolS = resGLU.atoms.colors['balls']
        #coloring using colorByResidueType
        self.mv.colorByResidueType(mv.Mols[0],geomsToColor = ['balls'])
        resTHRcol = resTHR.atoms.colors['balls']
        resCYScol = resCYS.atoms.colors['balls']
        resPROcol = resPRO.atoms.colors['balls']
        resSERcol = resSER.atoms.colors['balls']
        resILEcol = resILE.atoms.colors['balls']
        resVALcol = resVAL.atoms.colors['balls']
        resALAcol = resALA.atoms.colors['balls']
        resARGcol = resARG.atoms.colors['balls']                 
        resASNcol = resASN.atoms.colors['balls']
        resPHEcol = resPHE.atoms.colors['balls']
        resLEUcol = resLEU.atoms.colors['balls']
        resGLYcol = resGLY.atoms.colors['balls']
        resGLUcol = resGLU.atoms.colors['balls']
        #comparing colors of same reisidues 
        self.assertEqual(resTHRcol != resTHRcolS,True)
        self.assertEqual(resCYScol != resCYScolS,True)
        self.assertEqual(resPROcol != resPROcolS,True)
        self.assertEqual(resSERcol != resSERcolS,True)
        self.assertEqual(resILEcol != resILEcolS,True)
        self.assertEqual(resVALcol != resVALcolS,True)
        self.assertEqual(resALAcol != resALAcolS,True)
        self.assertEqual(resARGcol != resARGcolS,True)
        self.assertEqual(resASNcol != resASNcolS,True)
        self.assertEqual(resPHEcol != resPHEcolS,True)
        self.assertEqual(resLEUcol != resLEUcolS,True)
        self.assertEqual(resGLYcol != resGLYcolS,True)
        self.assertEqual(resGLUcol != resGLUcolS,True)    
        
    def test_color_residues_using_shapely_cpk(self):
        """tests color residues shapely colors using shapely coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        #coloring using colorResiduesUsingShapely
        self.mv.displayCPK(mv.Mols[0],only = 0,
                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10)
        c=self.mv.colorResiduesUsingShapely
        c(mv.Mols[0],geomsToColor = ['cpk'])
        resTHR = mv.Mols[0].chains.residues.get(lambda x : x.type == 'THR')
        resCYS = mv.Mols[0].chains.residues.get(lambda x : x.type == 'CYS')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resSER = mv.Mols[0].chains.residues.get(lambda x : x.type == 'SER')
        resILE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ILE')
        resVAL = mv.Mols[0].chains.residues.get(lambda x : x.type == 'VAL')
        resALA = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ALA')
        resARG = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ARG')
        resASN = mv.Mols[0].chains.residues.get(lambda x : x.type == 'ASN')
        resPHE = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PHE')
        resLEU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'LEU')
        resPRO = mv.Mols[0].chains.residues.get(lambda x : x.type == 'PRO')
        resGLY = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLY')
        resGLU = mv.Mols[0].chains.residues.get(lambda x : x.type == 'GLU')
        #finding colors of residues
        resTHRcolS = resTHR.atoms.colors['cpk']
        resCYScolS = resCYS.atoms.colors['cpk']
        resPROcolS = resPRO.atoms.colors['cpk']
        resSERcolS = resSER.atoms.colors['cpk']
        resILEcolS = resILE.atoms.colors['cpk']
        resVALcolS = resVAL.atoms.colors['cpk']
        resALAcolS = resALA.atoms.colors['cpk']
        resARGcolS = resARG.atoms.colors['cpk']                 
        resASNcolS = resASN.atoms.colors['cpk']
        resPHEcolS = resPHE.atoms.colors['cpk']
        resLEUcolS = resLEU.atoms.colors['cpk']
        resGLYcolS = resGLY.atoms.colors['cpk']
        resGLUcolS = resGLU.atoms.colors['cpk']
        #coloring using colorByResidueType
        self.mv.colorByResidueType(mv.Mols[0],geomsToColor = ['cpk'])
        resTHRcol = resTHR.atoms.colors['cpk']
        resCYScol = resCYS.atoms.colors['cpk']
        resPROcol = resPRO.atoms.colors['cpk']
        resSERcol = resSER.atoms.colors['cpk']
        resILEcol = resILE.atoms.colors['cpk']
        resVALcol = resVAL.atoms.colors['cpk']
        resALAcol = resALA.atoms.colors['cpk']
        resARGcol = resARG.atoms.colors['cpk']                 
        resASNcol = resASN.atoms.colors['cpk']
        resPHEcol = resPHE.atoms.colors['cpk']
        resLEUcol = resLEU.atoms.colors['cpk']
        resGLYcol = resGLY.atoms.colors['cpk']
        resGLUcol = resGLU.atoms.colors['cpk']
        #comparing colors of same reisidues 
        self.assertEqual(resTHRcol != resTHRcolS,True)
        self.assertEqual(resCYScol != resCYScolS,True)
        self.assertEqual(resPROcol != resPROcolS,True)
        self.assertEqual(resSERcol != resSERcolS,True)
        self.assertEqual(resILEcol != resILEcolS,True)
        self.assertEqual(resVALcol != resVALcolS,True)
        self.assertEqual(resALAcol != resALAcolS,True)
        self.assertEqual(resARGcol != resARGcolS,True)
        self.assertEqual(resASNcol != resASNcolS,True)
        self.assertEqual(resPHEcol != resPHEcolS,True)
        self.assertEqual(resLEUcol != resLEUcolS,True)
        self.assertEqual(resGLYcol != resGLYcolS,True)
        self.assertEqual(resGLUcol != resGLUcolS,True)

#coloring selected residue
    
    def test_color_residues_using_shapely_one_residue_lines(self):
        """test coloring ,selected residue lines
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.colorResiduesUsingShapely(self.mv.Mols[0].chains.residues[2])
        
        atms = self.mv.Mols[0].chains[0].residues[2].atoms
        atmcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                       atms.colors['lines'] )
        self.assertEqual(atmcol[0],(1.0,1.0,0.0))

    def test_color_residues_using_shapely_one_residue_cpk(self):
        """test coloring ,selected residue cpk
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displayCPK(mv.Mols[0].chains.residues[2],only = 0,
                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10)
        self.mv.colorResiduesUsingShapely(self.mv.Mols[0].chains.residues[2],geomsToColor
        = ['cpk'])
        atms = self.mv.Mols[0].chains[0].residues[2].atoms
        atmcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                       atms.colors['cpk'] )
        self.assertEqual(atmcol[0],(1.0,1.0,0.0))
        
    def test_color_residues_using_shapely_one_residue_sticks_balls(self):
        """test coloring ,selected residue sticks
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displaySticksAndBalls(self.mv.Mols[0].chains.residues[2])
        self.mv.colorResiduesUsingShapely(self.mv.Mols[0].chains.residues[2],geomsToColor
        = ['sticks'])

        atms = self.mv.Mols[0].chains[0].residues[2].atoms
        atmcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                       atms.colors['sticks'] )
        self.assertEqual(atmcol[0],(1.0,1.0,0.0))
#end coloring selected residue


############## COLOR ATOMS USING DG COMMAND TESTS ############################

class ColorAtomsUsingDG(ColorBaseTests):

    def test_colorAtomsUsingDG_1(self):
        """
        Test calling all the color command with a TreeNode as an argument
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorAtomsUsingDG(mol)
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorAtomsUsingDG,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorAtomsUsingDG,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorAtomsUsingDG,
                                   'expandedNodes____Nodes'))
        self.mv.undo()
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorAtomsUsingDG,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorAtomsUsingDG,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorAtomsUsingDG,
                                   'expandedNodes____Nodes'))
#invalid input for colorAtomsUsingDG command
    
    def test_colorAtomsUsingDG_invalid_input_nodes(self):
        """checks invalid input nodes,colorAtomsUsingDG 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorAtomsUsingDG                           
        returnValue=c("abcd")
        self.assertEqual(returnValue,'ERROR')
        
    def test_colorAtomsUsingDG_invalid_input(self):
        """checks empty input nodes,colorAtomsUsingDG
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorAtomsUsingDG                           
        returnValue=c("")
        self.assertEqual(returnValue,None)        
    
    def test_colorAtomsUsingDG_invalid_input_geomsToColor(self):
        """checks invalid input geomsToColor,colorAtomsUsingDG 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorAtomsUsingDG                           
        returnValue=c("1crn",geomsToColor = ['fgds'])
        self.assertEqual(returnValue,None) 
    
    def test_colorAtomsUsingDG_empty_input_geomsToColor(self):
        """checks empty input geomsToColor,colorAtomsUsingDG 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorAtomsUsingDG                           
        returnValue=c("1crn",geomsToColor = [''])
        self.assertEqual(returnValue,'ERROR')
#end invalid input for colorAtomsUsingDG command
    def test_colorAtomsUsingDG_DGatoms_lines(self):
        """coloring DG atoms lines, ASPOD1,ASPOD2,GLUOE1,GLUOE2,ARGNE,ARGNH1,ARGNH2
        """
        self.mv.readMolecule('Data/1crn.pdb')
        #getting residues
        resASP = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASP')
        resGLU = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLU')
        resSER = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='SER')
        resTHR = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='THR')
        resLYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='LYS')
        resARG = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ARG')
        resGLN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLN')
        resASN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASN')
        resCYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='CYS')
        #finding colors of DGatoms
        old_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['lines']
        old_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['lines']
        old_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['lines']
        old_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['lines']
        old_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['lines']
        old_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['lines']
        old_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['lines']
        #colorng DG atoms using  colorAtomsUsingDG
        c=self.mv.colorAtomsUsingDG
        c("1crn")
        new_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['lines']
        new_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['lines']
        new_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['lines']
        new_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['lines']
        new_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['lines']
        new_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['lines']
        new_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['lines']
        #comparing DGatoms after coloring with colorAtomsUsingDG
        self.assertNotEqual(old_ASPOD1 , new_ASPOD1)
        self.assertNotEqual(old_ASPOD2 , new_ASPOD2)
        self.assertNotEqual(old_GLUOE1 , new_GLUOE1)
        self.assertNotEqual(old_GLUOE2 , new_GLUOE2)
        self.assertNotEqual(old_ARGNE , new_ARGNE)
        self.assertNotEqual(old_ARGNH1 , new_ARGNH1)
        self.assertNotEqual(old_ARGNH2 , new_ARGNH2)
        
    def test_colorAtomsUsingDG_DGatoms_cpk(self):
        """coloring DG atoms cpk, ASPOD1,ASPOD2,GLUOE1,GLUOE2,ARGNE,ARGNH1,ARGNH2
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displayCPK(mv.Mols[0],only = 0,
                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10)
        #getting residues
        resASP = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASP')
        resGLU = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLU')
        resSER = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='SER')
        resTHR = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='THR')
        resLYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='LYS')
        resARG = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ARG')
        resGLN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLN')
        resASN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASN')
        resCYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='CYS')
        #finding colors of DGatoms
        old_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['cpk']
        old_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['cpk']
        old_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['cpk']
        old_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['cpk']
        old_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['cpk']
        old_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['cpk']
        old_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['cpk']
        #colorng DG atoms using  colorAtomsUsingDG
        c=self.mv.colorAtomsUsingDG
        c("1crn",geomsToColor=['cpk'])
        new_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['cpk']
        new_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['cpk']
        new_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['cpk']
        new_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['cpk']
        new_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['cpk']
        new_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['cpk']
        new_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['cpk']
        #comparing DGatoms after coloring with colorAtomsUsingDG
        self.assertNotEqual(old_ASPOD1 , new_ASPOD1)
        self.assertNotEqual(old_ASPOD2 , new_ASPOD2)
        self.assertNotEqual(old_GLUOE1 , new_GLUOE1)
        self.assertNotEqual(old_GLUOE2 , new_GLUOE2)
        self.assertNotEqual(old_ARGNE , new_ARGNE)
        self.assertNotEqual(old_ARGNH1 , new_ARGNH1)
        self.assertNotEqual(old_ARGNH2 , new_ARGNH2)

    def test_colorAtomsUsingDG_DGatoms_sticks(self):
        """coloring DG atoms sticks, ASPOD1,ASPOD2,GLUOE1,GLUOE2,ARGNE,ARGNH1,ARGNH2
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displaySticksAndBalls(self.mv.Mols[0])
        #getting residues
        resASP = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASP')
        resGLU = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLU')
        resSER = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='SER')
        resTHR = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='THR')
        resLYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='LYS')
        resARG = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ARG')
        resGLN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLN')
        resASN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASN')
        resCYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='CYS')
        #finding colors of DGatoms
        old_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['sticks']
        old_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['sticks']
        old_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['sticks']
        old_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['sticks']
        old_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['sticks']
        old_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['sticks']
        old_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['sticks']
        #colorng DG atoms using  colorAtomsUsingDG
        c=self.mv.colorAtomsUsingDG
        c("1crn",geomsToColor=['sticks','balls'])
        new_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['sticks']
        new_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['sticks']
        new_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['sticks']
        new_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['sticks']
        new_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['sticks']
        new_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['sticks']
        new_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['sticks']
        #comparing DGatoms after coloring with colorAtomsUsingDG
        self.assertNotEqual(old_ASPOD1 , new_ASPOD1)
        self.assertNotEqual(old_ASPOD2 , new_ASPOD2)
        self.assertNotEqual(old_GLUOE1 , new_GLUOE1)
        self.assertNotEqual(old_GLUOE2 , new_GLUOE2)
        self.assertNotEqual(old_ARGNE , new_ARGNE)
        self.assertNotEqual(old_ARGNH1 , new_ARGNH1)
        self.assertNotEqual(old_ARGNH2 , new_ARGNH2)

    def test_colorAtomsUsingDG_DGatoms_balls(self):
        """coloring DG atoms balls, ASPOD1,ASPOD2,GLUOE1,GLUOE2,ARGNE,ARGNH1,ARGNH2 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displaySticksAndBalls(self.mv.Mols[0])
        #getting residues
        resASP = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASP')
        resGLU = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLU')
        resSER = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='SER')
        resTHR = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='THR')
        resLYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='LYS')
        resARG = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ARG')
        resGLN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='GLN')
        resASN = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='ASN')
        resCYS = self.mv.Mols[0].chains.residues.get(lambda x:x.type =='CYS')
        #finding colors of DGatoms
        old_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['balls']
        old_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['balls']
        old_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['balls']
        old_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['balls']
        old_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['balls']
        old_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['balls']
        old_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['balls']
        #colorng DG atoms using  colorAtomsUsingDG
        c=self.mv.colorAtomsUsingDG
        c("1crn",geomsToColor=['sticks','balls'])
        new_ASPOD1 = resASP.atoms.get(lambda x: x.name == 'OD1').colors['balls']
        new_ASPOD2 = resASP.atoms.get(lambda x: x.name == 'OD2').colors['balls']
        new_GLUOE1 = resGLU.atoms.get(lambda x: x.name == 'OE1').colors['balls']
        new_GLUOE2 = resGLU.atoms.get(lambda x: x.name == 'OE2').colors['balls']
        new_ARGNE = resARG.atoms.get(lambda x: x.name == 'NE').colors['balls']
        new_ARGNH1 = resARG.atoms.get(lambda x: x.name == 'NH1').colors['balls']
        new_ARGNH2 = resARG.atoms.get(lambda x: x.name == 'NH2').colors['balls']
        #comparing DGatoms after coloring with colorAtomsUsingDG
        self.assertNotEqual(old_ASPOD1 , new_ASPOD1)
        self.assertNotEqual(old_ASPOD2 , new_ASPOD2)
        self.assertNotEqual(old_GLUOE1 , new_GLUOE1)
        self.assertNotEqual(old_GLUOE2 , new_GLUOE2)
        self.assertNotEqual(old_ARGNE , new_ARGNE)
        self.assertNotEqual(old_ARGNH1 , new_ARGNH1)
        self.assertNotEqual(old_ARGNH2 , new_ARGNH2)


#################### COLOR BY CHAINS COMMAND TESTS ###########################


class ColorByChains(ColorBaseTests):
    
    def test_color_by_chains_1(self):
        """
        Test calling all the color command with a TreeNode as an argument
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorByChains(mol)
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorByChains,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorByChains,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorByChains,
                                   'expandedNodes____Nodes'))
        self.mv.undo()
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorByChains,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorByChains,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorByChains,
                                   'expandedNodes____Nodes'))

#invalid input for colorByChains command   
    
    def test_colorByChains_invalid_input_nodes(self):
        """checks invalid input nodes,colorByChains 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByChains                           
        returnValue=c("abcd")
        self.assertEqual(returnValue,'ERROR')
        
    def test_colorByChains_invalid_input(self):
        """checks empty input nodes,colorByChains
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByChains                           
        returnValue=c("")
        self.assertEqual(returnValue,None)        
    
    def test_colorByChains_invalid_input_geomsToColor(self):
        """checks invalid input geomsToColor,colorByChains 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByChains                           
        returnValue=c("1crn",geomsToColor = ['fgds'])
        self.assertEqual(returnValue,None) 
    
    def test_colorByChains_empty_input_geomsToColor(self):
        """checks empty input geomsToColor,colorByChains 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByChains                           
        returnValue=c("1crn",geomsToColor = [''])
        self.assertEqual(returnValue,'ERROR')        
#end invalid input for colorByChains command

#molecule with only one chain
    def test_color_by_chains_lines(self):
        """checks  colorByChains
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.colorByChains("1crn")
        self.assertEqual(self.mv.Mols[0].chains.residues.atoms.colors['lines'],self.mv.allAtoms.colors['lines']) 
        
    def test_color_by_chains_cpk(self):
        """checks  colorByChains,cpk
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displayCPK(mv.Mols,only = 0,
                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10)
        self.mv.colorByChains("1crn",geomsToColor = ['cpk'])
        self.assertEqual(self.mv.Mols[0].chains.residues.atoms.colors['cpk'],self.mv.allAtoms.colors['cpk'])
        
    def test_color_by_chains_sticks_balls(self):
        """checks  colorByChains,sticks,balls
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.colorByChains("1crn")
        self.mv.displaySticksAndBalls(self.mv.Mols)
        self.assertEqual(self.mv.Mols[0].chains.residues.atoms.colors['sticks'],self.mv.allAtoms.colors['sticks'])
        self.assertEqual(self.mv.Mols[0].chains.residues.atoms.colors['balls'],self.mv.allAtoms.colors['balls'])
        
    def test_color_by_chains_getSelection(self):
        """checks  colorByChains,input through getSelection
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select(self.mv.allAtoms[20:50])
        self.mv.colorByChains(self.mv.getSelection())
        self.assertEqual(self.mv.allAtoms[20:50].colors,self.mv.getSelection().colors)
#end molecule with only one chain

#molecule with more than one chain
    def test_color_by_chains_more_than_one_chain(self):
        """checks  colorByChains for mol with two chains
        """
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.colorByChains(mv.Mols[0])
        self.assertEqual(self.mv.Mols[0].chains[0].residues.atoms[0].colors['lines'],(1.0,0.5,1.0))        
        self.assertEqual(self.mv.Mols[0].chains[1].residues.atoms[0].colors['lines'], MolColors['1'])

    def test_color_by_chains_more_than_one_chain_lines(self):
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.colorByChains(mv.select(self.mv.Mols[0].chains[0]))
        self.assertEqual(self.mv.Mols[0].chains[0].residues.atoms[0].colors['lines'],(1.0,0.5,1.0))        

    def test_color_by_chains_more_than_one_chain_cpk(self):
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.displayCPK(mv.Mols[0].chains[0],only = 0,
                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10) 
        self.mv.colorByChains(mv.select(self.mv.Mols[0].chains[0]))
        self.assertEqual(self.mv.Mols[0].chains[0].residues.atoms[0].colors['lines'],(1.0,0.5,1.0))        
        self.assertEqual(self.mv.Mols[0].chains[1].residues.atoms[0].colors['lines'],(1.0,1.0,1.0))


    def test_color_by_chains_more_than_one_chain_sticks(self):
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.displaySticksAndBalls(self.mv.Mols[0].chains[0],only = 1)
        self.mv.colorByChains(mv.select(self.mv.Mols[0].chains[0]))
        self.assertEqual(self.mv.Mols[0].chains[0].residues.atoms[0].colors['lines'],(1.0,0.5,1.0))        
        self.assertEqual(self.mv.Mols[0].chains[1].residues.atoms[0].colors['lines'],(1.0,1.0,1.0))

    def test_color_by_chains_more_than_one_chain_balls(self):
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.browseCommands("displayCommands",
                               commands=['displaySticksAndBalls',],
                               package='Pmv')
        self.mv.displaySticksAndBalls(self.mv.Mols[0].chains[0],only = 1)
        self.mv.colorByChains(mv.select(self.mv.Mols[0].chains[0]))
        self.assertEqual(self.mv.Mols[0].chains[0].residues.atoms[0].colors['lines'],(1.0,0.5,1.0))        
        self.assertEqual(self.mv.Mols[0].chains[1].residues.atoms[0].colors['lines'],(1.0,1.0,1.0))

#end molecules with more than one chain
    def test_colorLabels_chains(self):
        """tests coloring chain labels
        """
        self.mv.browseCommands("selectionCommands",
                               commands=['selectFromString'],package='Pmv')
        self.mv.browseCommands("labelCommands", commands=['labelByProperty',], package='Pmv')
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setSelectionLevel(Chain, KlassSet=None, log=0)
        self.mv.selectFromString(chains = '*')
        self.mv.labelByProperty(self.mv.getSelection(),only = 0, 
                                textcolor = (0.0, 0.0, 1.0),
                                location = 'Center', 
                                format = None, 
                                negate = 0, 
                                font = 'arial1.glf')
        self.mv.colorByChains("hsg1",geomsToColor=["ChainLabels",])
#        chains = self.mv.Mols[0].chains.get(lambda x: x.name == 'A')
#        chains1 = self.mv.Mols[0].chains.get(lambda x: x.name == 'B')
#        chaincol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                         chains.residues.atoms.colors['ChainLabels'])
#        chaincol1 = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                         chains1.residues.atoms.colors['ChainLabels'])
#        self.assertEqual(filter(lambda x: not x == (1.0, 1.0, 1.0),
#                                 chaincol),[])
#        self.assertEqual(filter(lambda x: not x == (0.0, 1.0, 0.0),
#                                 chaincol1),[])

###################### COLOR BY MOLECULES COMMAND TESTS ######################
from mglutil.util.defaultPalettes import MolColors
class ColorByMolecules(ColorBaseTests):
    
    def test_colorByMolecules_1(self):
        """
        Test calling all the color command with a TreeNode as an argument
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorByMolecules(mol)
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorByMolecules,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorByMolecules,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorByMolecules,
                                   'expandedNodes____Nodes'))
        self.mv.undo()
        # Make sure that the cleanup works properly.
        self.failUnless(not hasattr(self.mv.colorByMolecules,
                                   'expandedNodes____Atoms'))
        self.failUnless(not hasattr(self.mv.colorByMolecules,
                                   'expandedNodes____Molecules'))
        self.failUnless(not hasattr(self.mv.colorByMolecules,
                                   'expandedNodes____Nodes'))

#invalid input for colorByMolecules
    def test_colorByMolecules_invalid_input_nodes(self):
        """checks invalid input nodes,colorByMolecules 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByMolecules                           
        returnValue=c("abcd")
        self.assertEqual(returnValue,'ERROR')
        
    def test_colorByMolecules_invalid_input(self):
        """checks empty input nodes,colorByMolecules
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByMolecules                           
        returnValue=c("")
        self.assertEqual(returnValue,None)        
    
    def test_colorByMolecules_invalid_input_geomsToColor(self):
        """checks invalid input geomsToColor,colorByMolecules 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByMolecules                           
        returnValue=c("1crn",geomsToColor = ['fgds'])
        self.assertEqual(returnValue,None) 
    
    def test_colorByMolecules_empty_input_geomsToColor(self):
        """checks empty input geomsToColor,colorByMolecules 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        c=self.mv.colorByMolecules                           
        returnValue=c("1crn",geomsToColor = [''])
        self.assertEqual(returnValue,'ERROR')        

#end invalid input for colorByMolecules

    def test_colorByMolecules_lines(self):
        """checks colorByMolecules,coloring lines
        """
        self.mv.readMolecule('Data/small1crn.pdb')
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.colorByMolecules(mv.Mols)
        self.assertEqual(self.mv.Mols[0].allAtoms[0].colors['lines'],MolColors['0'])

    def test_colorByMolecules_cpk(self):
        """checks colorByMolecules,coloring cpk
        """
        self.mv.readMolecule('Data/small1crn.pdb')
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.browseCommands("displayCommands", commands=['displayCPK'],
                               package='Pmv')
        self.mv.displayCPK(mv.Mols,only = 0,
                           log = 0, negate = 0, scaleFactor = 1.0, quality = 10)
        self.mv.colorByMolecules(mv.Mols,geomsToColor = ['cpk'])
        self.assertEqual(self.mv.Mols[0].allAtoms[0].colors['cpk'], MolColors['0'])


    def test_colorByMolecules_sticks_balls(self):
        """checks colorByMolecules,coloring balls
        """
        self.mv.readMolecule('Data/small1crn.pdb')
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.displaySticksAndBalls(self.mv.Mols)
        self.mv.colorByMolecules(mv.Mols,geomsToColor=['sticks','balls'])
        self.assertEqual(self.mv.Mols[0].allAtoms[0].colors['sticks'],MolColors['0'])
        self.assertEqual(self.mv.Mols[0].allAtoms[0].colors['balls'],MolColors['0'])

        
    def test_colorByMolecules_getSelection(self):
        """checks colorByMolecules input through getSelection()
        """
        self.mv.readMolecule('Data/small1crn.pdb')
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.select(self.mv.allAtoms[0:50])
        self.mv.colorByMolecules(mv.Mols)
        self.assertEqual(self.mv.Mols[0].allAtoms[0].colors['lines'],MolColors['0'])
        self.assertEqual(self.mv.Mols[1].allAtoms[0].colors['lines'],MolColors['1'])
    
    def test_colorLabels_molecules(self):
        """tests coloring molecules labels
        """
        self.mv.browseCommands("selectionCommands",
                               commands=['selectFromString'],package='Pmv')
        self.mv.browseCommands("labelCommands", commands=['labelByProperty',], package='Pmv')
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.mv.selectFromString(mols = 'hsg1')
        self.mv.labelByProperty(self.mv.getSelection(),only = 0,
                                textcolor=(1.0, 0.0, 0.0),location = 'Center',
                                format = None, negate = 0, font = 'arial1.glf')
        self.mv.colorByMolecules("hsg1",geomsToColor=["ProteinLabels",])
#        mols=self.mv.Mols[0]
#        molcol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
#                         mols.chains.residues.atoms.colors['ProteinLabels'])
#        self.assertEqual(filter(lambda x: not x == (0.0, 0.0, 1.0),
#                                 molcol),[])

class ColorByProperty(ColorBaseTests):

    def test_color_by_property_1(self):
        """
        Test the __call__ of a colorByProperty command
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.loadColorMap('Data/rw256_map.py', log = 0)
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, log = 0, KlassSet = None)
        self.mv.colorByProperty(self.mv.getSelection(), ['lines'],
                                'temperatureFactor',
                                colormap='rw256', mini=3.38, maxi=23.98)
         
        # in the new colormap these two test are not correct                        
        #self.assertEqual(self.mv.colorMaps['rw256'].mini,3.38)
        #self.assertEqual(self.mv.colorMaps['rw256'].maxi,23.98)

#log TESTS

class ColorLogTests(ColorBaseTests):

    def test_color_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("./Data/1crn.pdb")
        #dispalying as sticks and balls        
        self.mv.displaySticksAndBalls("1crn") 
        #color command
        self.mv.color(self.mv.Mols[0], [(1.,0.,0.,1.),],geomsToColor = ['balls'])
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.color("1crn", [(1.0, 0.0, 0.0, 1.0)], [\'balls\'], log=0)')    
    


    def test_color_log_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        #dispalying as sticks and balls        
        self.mv.displaySticksAndBalls("1crn") 
        #color command
        oldself=self
        self=mv
        s = 'self.color("1crn", [(1.0, 0.0, 0.0, 1.0)], [\'balls\'], log=0)'
        exec(s)
        oldself.assertEqual(mv.allAtoms[0].colors['balls'],(1.0, 0.0, 0.0, 1.0))

    def test_colorByAtomType_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select(self.mv.allAtoms[0])
        self.mv.colorByAtomType(self.mv.getSelection(), "all")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorByAtomType("1crn: :THR1:N", [\'lines\'], log=0)')
        
    def test_colorByAtomType_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select(self.mv.allAtoms[0])
        oldself=self
        self=mv
        s ='mv.colorByAtomType("1crn: :THR1:N", [\'lines\'], log=0)'
        exec(s)
        #atom0 is N
        from Pmv.pmvPalettes import AtomElements
        Ncol = AtomElements['N']
        oldself.assertEqual(mv.allAtoms[0].colors['lines'], Ncol)
        #oldself.assertEqual(mv.allAtoms[0].colors['lines'],(0.,0.,1.))
    
    def test_colorByResidueType_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorByResidueType(mol, "all")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorByResidueType("1crn:::", [\'lines\'], log=0)')
        
    def test_colorByResidueType_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self=mv
        s ='self.colorByResidueType("1crn", [\'lines\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_colorByResiduesUsingShapely_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
  
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorResiduesUsingShapely(mol, "all")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorResiduesUsingShapely("1crn:::", [\'lines\'], log=0)')

    def test_colorByResiduesUsingShapely_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self=mv
        s = 'self.colorResiduesUsingShapely("1crn", [\'lines\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
        
    def test_colorAtomsUsingDG_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorAtomsUsingDG(mol, "all")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorAtomsUsingDG("1crn:::", [\'lines\'], log=0)')
        
    def test_colorAtomsUsingDG_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self=mv
        s = 'self.colorResiduesUsingShapely("1crn", [\'lines\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)
    
    def test_color_by_chains_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorByChains(mol, "all")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorByChains("1crn:::", [\'lines\'], log=0)')
        
    def test_color_by_chains_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self=mv
        s = 'self.colorByChains("1crn", [\'lines\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_colorByMolecules_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        self.mv.colorByMolecules(mol, "all")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorByMolecules("1crn:::", [\'lines\'], log=0)')

    def test_colorByMolecules_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self=mv
        s = 'self.colorByMolecules("1crn", [\'lines\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_color_by_property_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.loadColorMap('Data/rw256_map.py', log = 0)
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, log = 0, KlassSet = None)
        self.mv.colorByProperty(self.mv.getSelection(), ['lines'],
                                'temperatureFactor',
                                colormap='rw256', mini=3.38, maxi=23.98)
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorByProperty("1crn", [\'lines\'], \'temperatureFactor\', mini=3.38, maxi=23.98, colormap=\'rw256\', propertyLevel=None, log=0)')

        
    def test_color_by_property_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self=mv
        s = 'self.colorByProperty("1crn: :THR1:N;", [\'lines\'], \'temperatureFactor\', mini=3.38, maxi=23.98, colormap=\'rw256\', log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
## class ColorTests(ColorBaseTests):
##     def test_addOnAddObject_colorByAtomType(self): 
##         mv.addOnAddObjectCmd(mv.colorByAtomType)
##         self.mv.readMolecule('Data/1crn.pdb')
        
if __name__ == '__main__':
    unittest.main()
