#############################################################################
#
# Author: Sophie I. COON, Michel F. SANNER
#
# Copyright: M. Sanner TSRI 2000
#
#############################################################################
#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_moleculeViewer.py,v 1.10 2011/06/10 21:14:54 sargis Exp $
#
# $Id: test_moleculeViewer.py,v 1.10 2011/06/10 21:14:54 sargis Exp $
#
import sys
import unittest
from MolKit.protein import Protein, Chain, Residue
from MolKit.molecule import Molecule, Atom

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

    
mv= None
ct = 0
totalCt = 10

class MoleculeViewerTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, withShell=0, gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            #mv.browseCommands('deleteCommands',commands=['deleteMol',],
            #                   package='Pmv')
            #mv.browseCommands("bondsCommands",
            #                   commands=["buildBondsByDistance",],
            #                   package="Pmv")
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.loadCommand('fileCommands', 'readMolecule', 'Pmv')
            mv.loadCommand('deleteCommands','deleteMol', 'Pmv')
            mv.loadCommand("bondsCommands", "buildBondsByDistance", "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)

            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            #set up links to shared dictionary and current instance
        self.mv = mv 


    def setUp(self):
        """
        setUp
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        mols = self.mv.readMolecule("Data/1crn.pdb")
        self.mols = mols
    

    def tearDown(self):
        """
        cleanUp
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                self.mv.Exit(0)
            del self.mv


class SelectionTests(MoleculeViewerTests):

    def test_getSelection_mol(self):
        """ test getSelection single molecule"""
        self.mv.select("1crn")
        sel = self.mv.getSelection()[0]
        self.assertEqual(sel, self.mv.Mols[0])
        

    def test_setSelectionLevel_Atom(self):
        """ test setSelection to Atom from single molecule"""
        self.mv.select("1crn")
        self.mv.setSelectionLevel(Atom, log = 0, KlassSet = None)
        self.assertEqual(self.mv.selection, self.mv.allAtoms)
        sel = self.mv.getSelection()[:]
        self.assertEqual(sel, self.mv.allAtoms)
    

    def test_setSelectionLevel_Chain(self):
        """ test setSelection to Chain from single molecule"""
        self.mv.select("1crn")
        self.mv.setSelectionLevel(Chain, log = 0, KlassSet = None)
        self.assertEqual(self.mv.selection, self.mv.Mols.chains)
        sel = self.mv.getSelection()[:]
        self.assertEqual(sel, self.mv.Mols.chains)


    def test_setSelectionLevel_Residue(self):
        """ test setSelection to Residue from single molecule"""
        self.mv.select("1crn")
        self.mv.setSelectionLevel(Residue, log = 0, KlassSet = None)
        self.assertEqual(self.mv.selection, self.mv.Mols.chains.residues)
        sel = mv.getSelection()[:]
        self.assertEqual(sel, self.mv.Mols[0].chains.residues)

    
class IComLevelTests(MoleculeViewerTests):

    def test_changeIcomLevel_Protein(self):
        """ test setting IcomLevel to Protein doesnot change atom selection"""
        atmset = self.mv.allAtoms[12:30]
        self.mv.select(atmset)
        self.assertEqual(self.mv.selection, atmset)
        mv.setIcomLevel(Protein, log = 0, KlassSet = None)
        sel = mv.getSelection()
        self.assertEqual(sel,self.mv.Mols)


    
class GetMolFromNameTests(MoleculeViewerTests):

    def test_getMolFromName_success(self):
        """ test successfully accessing molecule by its name"""
        mv.readMolecule("Data/1crn.pdb")
        mol = mv.getMolFromName("1crn")
        self.assertEqual(mol, mv.Mols[0])


    def test_getMolFromName_None(self):
        """ test None is returned when accessing byName a non-existent name"""
        mv.readMolecule("Data/1crn.pdb")
        mol = mv.getMolFromName("1crn.pdb")
        self.assertEqual(mol, None)




if __name__ == '__main__':
    test_cases = [
        'SelectionTests',
        'IComLevelTests',
        'GetMolFromNameTests',
        ]
    
    unittest.main( argv=([__name__] + test_cases) )
    #to get verbose output use this line instead:
    #unittest.main( argv=([__name__, '-v'] + test_cases) )

