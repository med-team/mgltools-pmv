#$Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_APBSCommands.py,v 1.25 2010/05/26 22:25:18 annao Exp $
#
#$Id: test_APBSCommands.py,v 1.25 2010/05/26 22:25:18 annao Exp $
import unittest, sys, os, time
mv = None
ct = 0
totalCt = 5
#pf = ""
#sys.path.insert(0,'../..')
class APBSBaseTest(unittest.TestCase):
    """Base class for APBS unittest"""
    def startViewer(self):
        """start Viewer"""

        global mv
        #if mv is None:
        from Pmv.moleculeViewer import MoleculeViewer
        mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                            withShell=0, trapExceptions=False)
        mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        mv.browseCommands("APBSCommands")
        mv.browseCommands("editCommands")
        mv.browseCommands("fileCommands")
        mv.browseCommands('deleteCommands')
        mv.browseCommands("msmsCommands")

        self.mv = mv 

    def setUp(self):
        """set-up"""

        global mv
        if mv is None:
        #if not hasattr(self, 'mv'):
            self.startViewer()
        else:
           self.mv = mv 
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
            
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv    
            
class APBSTests(APBSBaseTest):
    """class APBSTests runs various unittest for APBS"""
    def test_1(self):
        """Tests APBSRun and APBSSave_Profile"""
        self.mv.readMolecule('Data/1crn.pdb')
        #this is not working because test dicectory path is too long
        #self.mv.APBSRun()

        self.mv.APBSSetup.params.projectFolder='apbs-'+self.mv.Mols[0].name

        self.mv.APBSRun()

        self.assertEqual(os.path.exists(self.mv.APBSSetup.params.projectFolder),
                                                                           True)
        #pf = os.path.join(self.mv.APBSSetup.params.projectFolder,'test')
        self.mv.APBSSaveProfile(Profilename = 'test')
 
    def test_2(self):
        """Tests APBSLoad_Profile and APBSMap_Potential_to_MSMS (mol is a string)"""
        pf = os.path.join("apbs-1crn","test.apbs.pf")
        dx = os.path.join("apbs-1crn","1crn.potential.dx")
        counter = 1 
        while not os.path.exists(dx):
            time.sleep(30)
            counter += 1
            if counter > 10:
                print >> sys.stderr, "APBS run is taking too long. Can't wait no longer"
                break
        self.mv.APBSLoadProfile(filename = pf)
        self.assertEqual(self.mv.Mols[0].name, '1crn')
        self.mv.APBSMapPotential2MSMS(potential = dx,mol = '1crn')

    def test_3(self):
        """Tests APBSLoad_Profile and APBSMap_Potential_to_MSMS (mol is a molecule object)"""
        pf = os.path.join("apbs-1crn","test.apbs.pf")
        dx = os.path.join("apbs-1crn","1crn.potential.dx")
        counter = 1 
        while not os.path.exists(dx):
            time.sleep(30)
            counter += 1
            if counter > 10:
                print >> sys.stderr, "APBS run is taking too long. Can't wait no longer"
                break
        self.mv.APBSLoadProfile(filename = pf)
        self.mv.APBSMapPotential2MSMS(potential = dx,mol = self.mv.Mols[0])

    def test_4(self):
        """Tests APBSDisplay_Isocontours"""
        pf = os.path.join("apbs-1crn","test.apbs.pf")
        dx = os.path.join("apbs-1crn","1crn.potential.dx")
        counter = 1 
        while not os.path.exists(dx):
            time.sleep(30)
            counter += 1
            if counter > 10:
                print >> sys.stderr, "APBS run is taking too long. Can't wait no longer"
                break
        self.mv.APBSLoadProfile(filename = pf)
        self.mv.APBSDisplayIsocontours(potential = dx)
        self.mv.APBSDisplayIsocontours.guiCallback()
        self.assertEqual(self.mv.GUI.VIEWER.FindObjectByName('root|+polygons').name,"+polygons")
        self.assertEqual(self.mv.GUI.VIEWER.FindObjectByName('root|-polygons').name,"-polygons")
        
    def test_APBS_Binding_Energy(self):
        """Tests if we can handle Binding Energy calculation with 3 molecules"""
        if sys.platform == 'win32': return
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.readMolecule('Data/small1crn.pdb')
        self.mv.readMolecule('Data/1bsrsmall.pdb')    
        self.mv.APBSSetup( calculationType='Binding energy')
        self.mv.APBSRun('small1crn', '1bsrsmall', '1crn')
        self.mv.APBSSetup.cmd.com.wait()
        
if __name__ == '__main__':
    try:
        uname = os.uname()
        if uname[0] == 'Darwin' and uname[2][0] == '7':
            pass
        else:
            unittest.main()
    finally:
        for root, dirs, files in os.walk('apbs-1crn', topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
   #     os.rmdir('apbs-1crn')

    
