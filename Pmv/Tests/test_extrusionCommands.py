#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_extrusionCommands.py,v 1.14.4.1 2016/02/11 21:29:43 annao Exp $
#
# $Id: test_extrusionCommands.py,v 1.14.4.1 2016/02/11 21:29:43 annao Exp $
#

import sys,unittest
import string
from string import split
"""
The test_extrusionCommands module implements a serie of function to test the
Pmv/extrusionCommands module.
"""
mv= None

ct = 0
totalCt = 12
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class ExtrusionBaseTest(unittest.TestCase):
    def startViewer(self):
        global mv
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode =
            'no',trapExceptions=False, withShell=0, gui=hasGUI)
            mv.browseCommands('fileCommands', commands=['readMolecule'],package= 'Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol'],package= 'Pmv')
            mv.browseCommands("bondsCommands", commands=["buildBondsByDistance"],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands("interactiveCommands", package='Pmv')
            # Don't want to trap exceptions and errors... the user pref is set to 1 by
            # default
            
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
    
            mv.browseCommands('selectionCommands', commands=['select', 'clearSelection'], package='Pmv')
            mv.browseCommands('extrusionCommands', package='Pmv')
        self.mv = mv

    
    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None


class ExtrusionTests(ExtrusionBaseTest):

    def test_computeSheet2D_1(self):
        """
        Function to test the ComputeSheet2D command on an empty viewer.
        Expect not to crash :)
        """
        self.mv.computeSheet2D(self.mv.getSelection(), 'test', 'CA', 'O')

    def test_computeSheet2D_2(self):
        """
            Function to test the computeSheet2D command on 1crn10.pdb with the
        following arguments.
        nodes = 1crn10, traceName = 'catrace', ctlAtmName = 'CA', torsAtmName = 'O'
        Expect to create a sheet2D attribute to the chain with a key 'catrace',
        the sheet2D should not be None
        All the residues of the chain should have been used to compute the sheet2D.
        """
        from Pmv.extruder import Sheet2D
        if not len(self.mv.Mols) ==0:
            self.mv.deleteMol(self.mv.Mols)
        self.assertEqual(len(self.mv.Mols)==0,True)    
        # Read 1crn10.pdb
        self.mv.readMolecule('./Data/1crn10.pdb')
        self.mv.select('1crn10')
        # Call the computeSheet2D command with the current selection
        # sheet2DName = 'catrace'
        # ctlAtmName  = 'CA'
        # torsAtmName = 'O'
        # buildIsHelix =0  nbrib = 2 nbchords = 4, width = 1.5, offset = 1.2
        self.mv.computeSheet2D(self.mv.getSelection(), 'catrace', 'CA', 'O')
        # Expect that the chain has an attribute sheet2D which is a dictionnary
        # with one entry at least called 'catrace' not None.
        chain = self.mv.getSelection()[0].chains[0]
        s2D = chain.sheet2D['catrace']
        self.assertEqual(s2D!=None,True)
        # Assert that sheet2DRes is a ResidueSet containing all the residues of the
        # chain.
        self.assertEqual(s2D.resInSheet == chain.residues,True)
        # Create a Sheet2D object and compute the sheet2D with the parameter
        # that should have been passed by the command.
        coords = []
        inHelix = [0,]*len(chain.residues)
        for r in chain.residues:
            coords = coords + r.getAtmsAndCoords(['CA', 'O'])[1]
        s = Sheet2D()
        s.compute(coords, inHelix, nbrib=2, nbchords=4, width=1.5, offset=1.2)
        # Check if the two sheet contains the same data...
        self.assertEqual(s.coords == s2D.coords,True)
        self.assertEqual(s.smooth.tolist() == s2D.smooth.tolist(),True)
        self.assertEqual(s.verts2D_flat.tolist() == s2D.verts2D_flat.tolist(),True)
        self.assertEqual(s.faces2D.tolist() == s2D.faces2D.tolist(),True)
        self.assertEqual(s.path.tolist() == s2D.path.tolist(),True)
        self.assertEqual(s.matrixTransfo.tolist() == s2D.matrixTransfo.tolist(),True)

        self.mv.deleteMol('1crn10')
        self.assertEqual(len(self.mv.Mols) == 0,True)
    
    def test_computeSheet2D_3(self):
        """
        Test the computeSheet2D command with a chain where the 1st residue cannot   
        be considered for Sheet2D. 
        This test is the result of bug occuring when a chain starts with residues
        that can't be used to compute sheet2D. These residues should then be
        skipped until you can compute a Sheet2D.
        Expect a sheet2D computed with only the [1:] residues.
        """
        from Pmv.extruder import Sheet2D
        if not len(self.mv.Mols) ==0:
            self.mv.deleteMol(self.mv.Mols)
        self.assertEqual(len(self.mv.Mols)==0,True)    
        self.mv.readMolecule('Data/2tbvA.pdb')
        self.mv.select('2tbvA')
        mol = self.mv.getSelection()[0]
        self.mv.computeSheet2D(mol, 'ssSheet2D', 'CA', 'O',
                      buildIsHelix=0)
        chain =  mol.chains[0]
        s2D = chain.sheet2D['ssSheet2D']
        self.assertEqual(s2D!=None,True)
        res = chain.residues[1:]
        self.assertEqual(s2D.resInSheet == res,True)
        # Compute the sheet2D test
        coords = []
        inHelix = [0,]*len(res)
        for r in res:
            coords = coords + r.getAtmsAndCoords(['CA', 'O'])[1]
        s = Sheet2D()
        s.compute(coords, inHelix, nbrib=2, nbchords=4, width=1.5, offset=1.2)

        # Check if the two sheet contains the same data...
        self.assertEqual(s.coords == s2D.coords,True)
        self.assertEqual(s.smooth.tolist() == s2D.smooth.tolist(),True)
        self.assertEqual(s.verts2D_flat.tolist() == s2D.verts2D_flat.tolist(),True)
        self.assertEqual(s.faces2D.tolist() == s2D.faces2D.tolist(),True)
        self.assertEqual(s.path.tolist() == s2D.path.tolist(),True)
        self.assertEqual(s.matrixTransfo.tolist() == s2D.matrixTransfo.tolist(),True)
        self.mv.deleteMol('2tbvA')
        self.assertEqual(len(self.mv.Mols)==0,True)
    
    
    def test_computeSheet2D_4(self):
        """
        Function to test the computeSheet2D command using ctlAtmName and
        torsAtmName not found in the chain.
        Expect the sheet2D['DNAtrace'] to be None.
        """
        from Pmv.extruder import Sheet2D
        # load crambin in the viewer.
        self.mv.readMolecule('Data/1crn10.pdb')
        self.mv.select('1crn10')
        # Compute the general sheet2D with all the CA and all the O 
        self.mv.computeSheet2D(self.mv.getSelection(), 'DNAtrace', "P", "C1'")
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(chain.sheet2D['DNAtrace']==None,True)
    

    def test_computeSheet2D_8(self):
        """
        Function to test the computeSheet2D command on a molecule where the
        torsAtmName can't be found.
        Expect the sheet2D to be None.
        """
        self.mv.readMolecule('Data/1crnnosheet2D.pdb')
        self.mv.select('1crnnosheet2D')
        self.mv.computeSheet2D(self.mv.getSelection(), 'test','CA', 'O')
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(chain.sheet2D['test']==None,True)

##############################################################################
##  TEST ARGUMENT TYPE....
##############################################################################
    
    def test_computeSheet2D_9(self):
        """
        Function to test the input of traceName. Should be a string and we are
        passing a list of string.
        We expect to not create the sheet2D attribute.
        """
        self.mv.readMolecule('Data/1crn10.pdb')
        self.mv.select('1crn10')
        # Compute the general sheet2D with all the CA and all the O 
        self.mv.computeSheet2D(self.mv.getSelection(), ['test'], "CA", "O")
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain, 'sheet2D'),False)

    def test_computeSheet2D_10(self):
        """
        Function to test the input type of the ctlAtmName. Expect a string and
        passing a integer instead.
        Expect no sheet2D attribute.
        """
        # load molecule
        self.mv.readMolecule('Data/1crn10.pdb')
        self.mv.select('1crn10')
        # Compute the general sheet2D with all the CA and all the O 
        self.mv.computeSheet2D(self.mv.getSelection(), 'test', 4, "O")
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain, 'sheet2D'),False)

    def test_computeSheet2D_11(self):
        """
        Function to test the input type of the torsAtmName. Expect a string and
        passing a list of string instead.
        Expect no sheet2D attribute.
        """
        # bad tracename
        # load crambin in the viewer.
        self.mv.readMolecule('Data/1crn10.pdb')
        self.mv.select('1crn10')
        # Compute the general sheet2D with all the CA and all the O 
        self.mv.computeSheet2D(self.mv.getSelection(), 'test', "CA", ["0,"])
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain, 'sheet2D'),False)


####Log Tests####

class LogExtrusionTests(ExtrusionBaseTest):
      
    
    def test_ComputeSheet2D_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule("./Data/1crn10.pdb")
        self.mv.select("1crn10")
        chain = self.mv.Mols[0].chains[0]
        oldself =self
        self = mv
        s = "self.computeSheet2D(self.getSelection(), 'test', 'CA','O')"
        exec(s)
        self =oldself 
        self.assertEqual(hasattr(chain, 'sheet2D'),True)
        
    def test_ComputeSheet2D_checks_expected_log_string(self):
        """checks expected log string is wrtten"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("./Data/1crn10.pdb")
        self.mv.select("1crn10")
        self.mv.computeSheet2D(self.mv.getSelection(), "test", "CA","O")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeSheet2D("1crn10", \'test\', \'CA\', \'O\', nbchords=4, nbrib=2, log=0, width=1.5, buildIsHelix=False, offset=1.2)')
            

    def test_displayPath3D_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule("./Data/1crn10.pdb")    
        self.mv.select("1crn10")
        self.mv.computeSheet2D(self.mv.getSelection(), 'test', 'CA','O')
        chain = self.mv.Mols[0].chains[0]
        oldself =self
        self = mv
        s = "self.displayPath3D(self.getSelection())"
        exec(s)
        self =oldself 
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['path '].vertexSet), 81)
        
    def test_displayPath3D__checks_expected_log_string(self):
        """checks expected log string is wrtten"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("./Data/1crn10.pdb")
        self.mv.select("1crn10")
        self.mv.computeSheet2D(self.mv.getSelection(), "test", "CA","O")
        self.mv.displayPath3D(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displayPath3D("1crn10", negate=False, only=False, log=0)')



if __name__ == '__main__':
    unittest.main()
