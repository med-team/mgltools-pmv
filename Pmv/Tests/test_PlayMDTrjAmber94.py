#
#
# $Id: test_PlayMDTrjAmber94.py,v 1.5 2009/11/10 22:59:02 annao Exp $
#
#############################################################################
#                                                                           #
#   Author:Sowjanya Karnati                                                 #
#   Copyright: M. Sanner TSRI 2000                                          #
#                                                                           #
#############################################################################

import sys,os
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
ct = 0
totalCt = 1
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class AmberBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                    trapExceptions=False, withShell=0, gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('amberCommands', package='Pmv')
            #set up links to shared dictionary and current instance
            from Pmv.amberCommands import Amber94Config, CurrentAmber94
            self.Amber94Config = Amber94Config
            self.CurrentAmber94 = CurrentAmber94
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        #for key,value in self.Amber94Config.items():
        #    del self.Amber94Config[key]
        #    try:
        #        del value
        #    except:
        #        print "exception in deleting ", value
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None



###########################################################
#           PlayMdTrjAmber94 COMMAND TESTS                #
###########################################################

class PlayMdTrjAmber94(AmberBaseTest):

         
    
    def xplay_md_trj_amber94_widget(self):
        """checks PlayMdTrjAmber94 widget displayed
        FIX THIS
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:", 'pltest1', log=0, filename=None)
        #c = self.mv.play_md_trj_Amber94
        ##c('pltest1', 'Data/trp3_h1.trj', log=0)
        #c.buildForm()
        ##Need to do this otherwise the form closes before assert
        ##but = c.ifd.entryByName[4]['widget']
        ##but.wait_visibility(but)
        #self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        #c.Close_cb(c.ifd)
        
        
    def xtest_play_md_trj_amber94_amberids(self):
        """checks PlayMdTrjAmber94 widget amberIds button
        FIX THIS:
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:", 'pltest2', log=0, filename=None)
        #c = self.mv.play_md_trj_Amber94
        #c('pltest2', 'Data/trp3_h1.trj', log=0)    
        #c.buildForm()
        #AmberIdBut = c.ifd.entryByName['amberIds']['widget']
        #AmberIdBut.setentry('pltest2')
        #self.assertEqual(AmberIdBut.get(),'pltest2')
        #c.Close_cb(c.ifd)
    

    def xtest_play_md_trj_amber94_invalid_input(self):
        """
        FIX THIS:
        checks PlayMdTrjAmber94 invalid input for key
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:", 'pltest3', log=0, filename=None)
        #c = self.mv.play_md_trj_Amber94
        #self.assertEqual(c('gjg',"Data/trp3_h.pdb"),'ERROR')
       

    def xtest_play_md_trj_amber94_empty_input(self):
        """
        FIX THIS:
        checks PlayMdTrjAmber94 empty input for key
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:", 'pltest4', log=0, filename=None)
        #c = self.mv.play_md_trj_Amber94
        #self.assertEqual(c(' ',"Data/trp3_h.pdb"),'ERROR')
        

    def xplay_md_trj_amber94_invalid_input_trj_file(self):
        """
        FIX THIS: it freezes
        checks PlayMdTrjAmber94 invalid input for trj file
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:", 'pltest5')
        #c = self.mv.play_md_trj_Amber94
        #from Pmv.amberCommands import Amber94Config
        #c.CurrentAmber94 = Amber94Config.values()[0][0]
        #self.assertEqual(c("pltest5",'tuyty'),'ERROR')
        
 

    def xtest_play_md_trj_amber94_empty_input_trj_file(self):
        """
        FIX THIS:
        checks PlayMdTrjAmber94 empty input for trj file
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #c = self.mv.play_md_trj_Amber94
        #self.assertEqual(c("pltest6",'        '),'ERROR') 
    
    
    def test_play_md_trj_amber94_trj_file(self):
        """checks trp3_h1.trj file exists
        """
        filename ='Data/trp3_h1.trj'
        cmd = "rm -f " + filename
        self.mv.readMolecule("Data/trp3_h.pdb")
        self.mv.setup_Amber94("trp3_h:", 'pltest7', log=0, filename=None)    
        self.mv.setmdOpts_Amber94('pltest7', tautp=0.2, log=0, zerov=0, tempi=0.0, verbosemd=1, idum=-1, temp0=300.0, t=0.0, ntpr_md=10, dt=0.001, ntwx=300, vlimit=10.0)
        c1 = self.mv.md_Amber94
        c1('pltest7', 349, callback=1, filename='Data/trp3_h1.trj', log=0, callback_freq=10)
        if self.mv.hasGui:
            c = self.mv.play_md_trj_Amber94
            c('pltest7', 'Data/trp3_h1.trj', log=0)
            self.assertEqual(os.path.exists(filename), True)
        

    def xtest_play_md_trj_amber94_trj_file_get(self):
        """checks CurrentAmber94 filename
        FIX THIS:
        """
        filename ='Data/trp3_h1.trj'
        #cmd = "rm -f " + filename
        #self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:", 'pltest8', log=0, filename=None)    
        #self.mv.setmdOpts_Amber94('pltest8', tautp=0.2, log=0, zerov=0, tempi=0.0, verbosemd=1, idum=-1, temp0=300.0, t=0.0, ntpr_md=10, dt=0.001, ntwx=300, vlimit=10.0)
        #c1 = self.mv.md_Amber94
        #c1('pltest8', 349, callback=1, filename='Data/trp3_h1.trj', log=0, callback_freq=10)
        #c = self.mv.play_md_trj_Amber94
        #c('pltest8', 'Data/trp3_h1.trj', log=0)
        #self.assertEqual(c1.CurrentAmber94.filename,'Data/trp3_h1.trj')


if __name__ == '__main__':
    test_cases = [
        'PlayMdTrjAmber94',
        ]
    
    unittest.main( argv=([__name__] + test_cases) )




#if __name__ == '__main__':
#    unittest.main()






        
