#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_editCommands.py,v 1.23.2.1 2016/02/11 21:29:43 annao Exp $
#
# $Id: test_editCommands.py,v 1.23.2.1 2016/02/11 21:29:43 annao Exp $
#


import unittest, glob, os, string,sys
import time, numpy
from string import split,strip,find
from MolKit.molecule import Atom
from MolKit.protein import Residue
mv = None
ct = 0
totalCt = 55
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class EditBaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
        
    def startViewer(self):
        global mv
        if mv is None:
            from MolKit import Read
            import Tkinter
            from  Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(verbose=False, trapExceptions=False,
                                withShell=0, gui=hasGUI)
            mv.loadModule('editCommands', 'Pmv')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
        self.mv.setIcomLevel(Atom, KlassSet=None)



    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            #print "deleting ", m
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            if self.mv.hasGui:
                self.mv.Exit(0)
            del self.mv


class EditCommandsTests(EditBaseTests):

    def test_computeGasteiger_1(self):
        """ Bug when calling computeGasteiger __call__ with a string"""
        self.mv.readMolecule("./Data/ind.pdb", log=0)
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self.assertEqual(len(self.mv.Mols), 1)
        self.assertEqual(mol.name, 'ind')
        #assert len(mv.Mols) == 1 and mv.Mols[0].name == 'ind'
        self.mv.add_hGC("ind", method='noBondOrder', renumber=1, polarOnly=0)
        self.mv.computeGasteiger('ind')
        for a in mol.allAtoms:
            self.assertEqual(a.chargeSet, 'gasteiger')


    def test_edit_type_atom_atom_type(self):
        """tests atom 1 is same in hsg1.pdb and viewer
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom1 = self.mv.allAtoms[0]
        fptr=open("Data/hsg1.pdb")
        alllines=fptr.readlines()
        ll=split(alllines[0])
        self.assertEqual(atom1.name,ll[2])
        

    def test_edit_type_atom_invalidname(self):
        """checks nothing breaks and nothing is returned
        when invalid name entered in typeatoms
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self.assertEqual(hasattr(mol.allAtoms[0], 'babel_type'), False)
        val = self.mv.typeAtoms("gjsdg:::")
        self.assertEqual(val, None)
        self.assertEqual(hasattr(mol.allAtoms[0], 'babel_type'), False)
   

    def test_edit_type_atom_babel_list_viewer(self):
        """tests all atoms are same in babel list and viewer
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.typeAtoms("hsg1")
        fptr = open('Data/babel_list')
        alllines = fptr.readlines()
        from string import strip
        for x, y in zip(self.mv.allAtoms.babel_type, alllines):
	        self.assertEqual(x,strip(y))


    def test_edit_edit_atom_type_changedatom(self):
        """checks atom type is changed or not
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        changedAtom = self.mv.allAtoms[0]
        self.mv.editAtomType(changedAtom, 'Oany')
        self.assertEqual(changedAtom.babel_type,'Oany')


    def test_edit_edit_atom_type_invalid_atom(self):
        """checks when invalid atom type is entered
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        changedAtom = self.mv.allAtoms[0]
        self.mv.editAtomType(changedAtom, 'Oany')
        self.assertNotEqual(changedAtom.babel_type,'hello')
    
        
    def test_edit_edit_atom_type_visible(self):
        
        """checks theselected sphere is visible
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
        self.mv.editAtomType(atom0,"O")
        self.assertEqual(self.mv.editAtomTypeGC.spheres.visible,1)
        

    def test_edit_edit_atom_type_vertices(self):
        
        """checks vertices of selected atom
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        c=self.mv.editAtomType
        atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
        c(atom0, 'Oany')
        ato=self.mv.Mols[0].allAtoms.coords
        ind = self.mv.allAtoms.index(atom0)
        #FIX THIS!!:
        self.assertEqual(round(ato[ind][0]),round(atom0.coords[0]))
        self.assertEqual(round(ato[ind][1]),round(atom0.coords[1]))
        self.assertEqual(round(ato[ind][2]),round(atom0.coords[2]))

               
    def XXtest__edit_edit_atom_type_widget(self):
        """checks widget exists
        FIX THIS WHEN WE FIGURE OUT HOW TO GET AROUND
        "BLOCKING" input forms...
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        x = self.mv.editAtomTypeGC
        atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
        self.mv.editAtomTypeGC(atom0,log=0)
        x.ifd.form.root.wm_deiconify()
        self.assertEqual(x.ifd.form.root.winfo_ismapped(),1)
        x.ifd.form.root.wm_withdraw()
        

    def XXtest__edit_edit_atom_type_chem_Elem(self):
        """checks in edit atom type atoms chem elems are O,C,B,S,H
        FIX THIS WHEN WE FIGURE OUT HOW TO GET AROUND
        "BLOCKING" input forms...
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        y="Oany"
        atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
        self.mv.editAtomType(atom0,y)
        x=self.mv.editAtomTypeGC
        for i in x.atmTypeDict :
            if find(i,y)==0:
                break;
            else:
                self.assertEqual(atom0.chemElem, 'O')


    def test_edit_add_kollman_charges_added(self):
        """checks whether kollman charges are added are not
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.assertEqual(mv.allAtoms[0]._charges,{})
        self.mv.addKollmanCharges("hsg1:::")
        self.assertEqual(round(self.mv.allAtoms[0]._charges.values()[0],3),0.017)

        
    def test_edit_add_kollman_charges_charge(self):
        """checks charge added is kollman
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.addKollmanCharges("hsg1:::")
        atom0 = self.mv.allAtoms[0]
        self.assertEqual(atom0.chargeSet,"Kollman")
    

    def test_edit_add_kollman_charges_no_charge_added(self):
        """checks for charge  when not added
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom0 = self.mv.allAtoms[0]
        self.assertEqual(atom0._charges , {})
        self.assertEqual(hasattr(atom0,'charge'),False)
            

    def test_edit_add_kollman_charges_charge_list(self):
        """checks charges are same in list and mv.addKollmanCharges.q
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.addKollmanCharges("hsg1:::")
        fptr = open('Data/charge_list')
        alllines = fptr.readlines()
        from string import strip
        for x, y in zip(self.mv.addKollmanCharges.q, alllines):
	        self.assertEqual(strip(x),strip(y))


    def test_edit_add_kollman_charges_totalcharge(self):
        """checks Total kollman charge added
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.assertEqual(round(self.mv.addKollmanCharges(self.mv.getSelection()),2),8.00)


    def test_edit_add_kollman_charges_to_1dwb_rec(self):
        """checks adding kollman charges to molecule missing Cterminus O
        """
        self.mv.readMolecule("Data/1dwb_rec.pdbqt")
        #sum of charges in pdbqt files=-1.925
        pdbqt_charges = numpy.add.reduce(self.mv.allAtoms.charge)
        print "pdbqt_charges", pdbqt_charges
        total = self.mv.addKollmanCharges("1dwb_rec:::")
        print 'total:', total
        #sum of Kollman charges = -0.336
        Kollman_charges = numpy.add.reduce(self.mv.allAtoms.charge)
        print "Kollman_charges:", Kollman_charges
        self.assertEqual(round(Kollman_charges-pdbqt_charges, 3), 1.589)


    def test_edit_compute_gasteiger_added(self):
        """checks whether gasteiger charges are added are not
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.assertEqual(mv.allAtoms[0]._charges,{})
        self.mv.computeGasteiger("hsg1:::")
        self.assertEqual(round(self.mv.allAtoms[0]._charges.values()[0],3),-0.038)


    def test_edit_compute_gasteiger__charge(self):
        """checks charge added is gasteiger
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom0 = self.mv.allAtoms[0]
        self.mv.computeGasteiger("hsg1:::")
        self.assertEqual(atom0.chargeSet,"gasteiger")
    

    def test_edit_compute_gasteiger_no_charge_added(self):
        """checks for charge and charge set when gasteiger charges not added
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom0 = self.mv.allAtoms[0]
        self.assertEqual(atom0._charges , {})
        self.assertEqual(hasattr(atom0,'charge'),False)
                

    def test_edit_check_totals_on_residues_hydrogens_added(self):
        """checks totals on residues when polar H are added
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        #self.mv.add_hGC("hsg1:::", polarOnly=1, renumber=1, method='noBondOrder', log=0)
        self.mv.addKollmanCharges("hsg1:::")
        self.mv.checkResCharges("hsg1::")
        atom_charges=mv.Mols[0].allAtoms.charge
        #summ = numpy.add.reduce(self.mv.Mols[0].allAtoms.charge)
        summ = 0
        for i in range(0,len(atom_charges)):
            summ=summ+atom_charges[i]
        self.assertEqual((abs(summ-(round(summ,0))))<0.01,True)


    def test_edit_check_raises_key_error(self):
        """checks keyError is raised when atoms have no charges
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.assertRaises(KeyError,self.mv.checkResCharges,"hsg1::")
        

    def test_edit_check_totals_on_residues_hydrogens_not_added(self):
        """checks totals on residues when polar H are removed
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.deleteHydrogens(self.mv.allAtoms)
        self.mv.addKollmanCharges("hsg1")
        totalCharge = numpy.add.reduce(self.mv.allAtoms.charge)
        self.assertEqual((abs(totalCharge-(round(totalCharge,0))))<0.01, False)
        

    def test_edit_set_charge_field_kollman(self):
        """checks set charge field when set to kollman
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.addKollmanCharges("hsg1:::")
        self.mv.setChargeSet("hsg1:::", 'Kollman')
        atoms=mv.allAtoms[0]
        self.assertEqual(atoms.chargeSet,"Kollman")


    def test_edit_set_charge_field_gasteiger(self):
        """checks set charge field when set to gasteiger
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.computeGasteiger("hsg1:::")
        self.mv.setChargeSet("hsg1:::", 'gasteiger')
        atoms=mv.allAtoms[0]
        self.assertEqual(atoms.chargeSet,"gasteiger")


    def test_edit_set_charge_field__invalid_name(self):
        """checks set charge field when set to invalid entry
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.assertRaises(KeyError,self.mv.setChargeSet, self.mv.allAtoms, 'hai')
        

    def test_edit_add_hydrogens_number(self):
        """checks no .of hydrogens
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        #self.mv.add_hGC("hsg1:::", polarOnly=1, renumber=1)
        hs = self.mv.allAtoms.get(lambda x: x.element=='H')
        self.assertEqual(len(hs.name),330)
            

    def test_edit_add_hydrogens_coords(self):
        """checks coordinates of hydrogens
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        #self.mv.add_hGC("hsg1:::", polarOnly=1, renumber=1)
        hs = self.mv.allAtoms.get(lambda x: x.element=='H')
        self.assertEqual(hs.coords[0],mv.allAtoms[1].coords)
                

    def test_edit_add_hydrogens_list_sort(self):
        """checks sorted list of hydrogens
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        #self.mv.add_hGC("hsg1:::", polarOnly=1, renumber=1)
        hs = self.mv.allAtoms.get(lambda x: x.element=='H')
        hs.name.sort()
        fptr = open('Data/hydrogen.name')
        alllines = fptr.readlines()
        from string import strip
        for x, y in zip(hs.name, alllines):
	        self.assertEqual(strip(x),strip(y))
         

    def test_edit_merge_non_polar_hydrogens_number_of_atoms(self):
        """checks number of atoms
        """
        self.mv.readMolecule("Data/ld.pdb")
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        #self.mv.add_hGC("hsg1:::", polarOnly=0, renumber=1)
        self.mv.addKollmanCharges(mol)
        orig_num_atoms=len(mol.allAtoms)
        nphs=mol.allAtoms.get(lambda x:x.element=='H' and (x.bonds[0].atom1.element=='C' or x.bonds[0].atom2.element=='C'))
        len_nphs=len(nphs)
        self.mv.mergeNPHS("ld:A:MET36:HA")
        self.assertEqual(len(mol.allAtoms)!=(len_nphs),True)
    

    def test_edit_merge_non_polar_hydrogens_charges(self):
        """checks atom charges after merging
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        #self.mv.add_hGC("hsg1:::", polarOnly=0, renumber=1)
        self.mv.addKollmanCharges(mol)
        #make a record of the charges on each nonpolarCarbon + its hydrogens
        c_nhps=mol.allAtoms.get(lambda x:x.element=='C' and x.findHydrogens() > 0)
        #this dict keys are the nonpolarCarbons
        #its values are their united charge [orig charge + charge on each nph]
        c_charges={}
        for c_atom in c_nhps:
            hs=c_atom.findHydrogens()
            h_tot_charge=0
            for h in hs:
                h_tot_charge+=h.charge
            c_charges[c_atom]=c_atom.charge+h_tot_charge
        for c_atom in c_nhps:
            self.assertEqual(c_atom.charge, c_charges[c_atom])




    def test_edit_fixHNames(self):
        """checks for corrected H names 
        """
        self.mv.readMolecule("Data/piece_1tmn.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.assertEqual(self.mv.allAtoms[1].name,'HT1')
        self.assertEqual(self.mv.allAtoms[2].name,'HT2')
        self.assertEqual(self.mv.allAtoms[3].name,'HT3')
        self.mv.fixHNames("piece_1tmn:::")
        self.assertEqual(self.mv.allAtoms[1].name,'HN1')
        self.assertEqual(self.mv.allAtoms[2].name,'HN2')
        self.assertEqual(self.mv.allAtoms[3].name,'HN3')


    def test_edit_fix_names_list(self):
        """checks fixed atom names vs reference file's names
        """
        self.mv.readMolecule("Data/piece_1tmn.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.fixHNames("piece_1tmn:::")
        ats = self.mv.allAtoms
        ats.sort()
        newnames = ats.name
        fptr = open('Data/atomnames1.list')
        alllines1 = fptr.readlines()
        fptr.close()
        #compare the canned atom names with the current atoms' names
        for x, y in zip(alllines1, ats.name):
	        self.assertEqual(strip(x), y)
        

    def test_edit_mergelonepairs_Ld1_Ld2(self):
        """checks lone pairs are ld1,ld2
        """
        self.mv.readMolecule("Data/ld.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.computeGasteiger("ld::", log=0)
        lpats = self.mv.allAtoms.get(lambda x: x.name[0]=='L')
        self.assertEqual(lpats.name[0],'LD1')
        self.assertEqual(lpats.name[1],'LD2')
        

    def test_edit_mergelonepairs_atm_names(self):
        """checks for atms names in list and mv.allAtoms         
        """
        self.mv.readMolecule("Data/ld.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.computeGasteiger("ld::", log=0)
        self.mv.mergeLPS("ld:A:MET36:LD1,LD2")
        ats = self.mv.allAtoms
        ats.sort()
        newnames = ats.name
        fptr = open('Data/atomnames2.list')
        alllines1 = fptr.readlines()
        fptr.close()
        #compare the canned atom names with the current atoms' names
        for x, y in zip(alllines1, ats.name):
	        self.assertEqual(strip(x), y)


    def test_edit_mergelonepairs_length_s(self):
        """checks  charge  after merging lone pairs 
        """
        self.mv.readMolecule("Data/ld.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.computeGasteiger("ld::", log=0)
        for at in self.mv.allAtoms:
            at._charges['new']=1.0
        self.assertEqual(self.mv.allAtoms.get(lambda x: x.element=='S')._charges['new'],[1.0])
        self.mv.mergeLPS("ld:A:MET36:LD1,LD2")    
        self.assertEqual(self.mv.allAtoms.get(lambda x: x.element=='S')._charges['new'],[-0.1734])
    

    def test_edit_mergelonepairs_length(self):
        """checks no.of charges after merging lone pairs
        """
        self.mv.readMolecule("Data/ld.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        lpats = self.mv.allAtoms.get(lambda x: x.name[0]=='L')
        self.mv.computeGasteiger("ld::", log=0)
        old=self.mv.allAtoms.charge
        self.mv.mergeLPS("ld:A:MET36:LD1,LD2")
        new=self.mv.allAtoms._charges
        self.assertEqual(len(old)!=len(new),True)
    

    def test_edit_split_nodes_residues(self):
       """checks number of residues after splitting nodes
       """
       self.mv.readMolecule("Data/ld.pdb")
       old_len=len(self.mv.Mols.chains.residues)
       self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
       self.mv.splitNodes("ld:A:ARG41:CZ,NH1,NH2,HH11,HH12,HH21,HH22", Residue, "ld", renumber=1, log=0)
       new_len=len(self.mv.Mols.chains.residues)
       self.assertEqual(new_len, old_len + 1)
       #self.assertEqual((old!=new),True)


    def test_edit_split_nodes_atoms_in_residue(self):
       """checks atoms in residue which is split
       """
       self.mv.readMolecule("Data/ld.pdb")
       old=self.mv.Mols.chains.residues[5].atoms.name
       self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
       self.mv.splitNodes("ld:A:ARG41:CZ,NH1,NH2,HH11,HH12,HH21,HH22", Residue, "ld", renumber=1, log=0)
       new=self.mv.Mols.chains.residues[5].atoms.name
       self.assertEqual(old!=new,True)

class AssignAtomsRadiiTests(EditBaseTests):
    def test_overwriteTrue(self):
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.browseCommands("displayCommands", commands=["displayCPK"],
                               package="Pmv")
        # assign the radii
        self.mv.displayCPK("1crn")
        self.mv.allAtoms.radius = 1.0
        self.assertEqual(self.mv.allAtoms.radius, [1.0,]*len(self.mv.allAtoms),"Expecting all the radius to be 1.0")
        self.mv.assignAtomsRadii("1crn", united=1, log=0, overwrite=True)
        self.assertEqual(self.mv.allAtoms[0].radius,1.7, "expecting self.mv.allAtoms[0].radius == 1.7 got %s"%self.mv.allAtoms[0].radius)
                      

class EditCommandsLogTests(EditBaseTests):

    def test_add_hGC_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("./Data/ind.pdb", log=0)
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self.mv.add_hGC("ind", method='noBondOrder', renumber=1, polarOnly=0)
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.add_hGC('ind', 0, 'noBondOrder', 1, log=0)")
        
    def test_add_hGC_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule("./Data/ind.pdb", log=0)
        oldself =self
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        s ="self.add_hGC('ind', 0, 'noBondOrder', 1, log=0)"
        self= mv
        exec(s)
        self=oldself
        oldself.assertEqual(1,1) 

    def test_typeAtoms_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)

        self.mv.readMolecule("./Data/hsg1.pdb", log=0)
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self.mv.typeAtoms("hsg1")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.typeAtoms("hsg1", log=0)')
           
    def test_typeAtoms_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule("./Data/ind.pdb", log=0)
        oldself =self
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self = mv
        s = 'self.typeAtoms("hsg1", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_editAtomType_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
        self.mv.editAtomType(atom0, 'Oany')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.editAtomType("hsg1:A:PRO1:O", \'Oany\', log=0)')
    

    def test_editAtomType_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
        oldself =self
        self = mv
        s ='self.editAtomType("hsg1:A:PRO1:O", \'Oany\', log=0)'
        exec(s)
        oldself.assertEqual(1,1)
    #def test_editAtomtypeGC_log(self):
     #   self.mv.readMolecule("Data/hsg1.pdb")
     #   self.mv.Mols[0].buildBondsByDistance()
     #   x = self.mv.editAtomTypeGC
     #   atom0 = self.mv.allAtoms.get(lambda x: x.element=='O')[0]
     #   self.mv.editAtomTypeGC(atom0,log=0)    
     #   last_index = tx.index('end')
     #   last_entry_index = str(float(last_index)-2.0)
     #   last_entry = tx.get(last_entry_index, 'end')    
     #   self.assertEqual(split(last_entry,'\n')[0],
        
    def test_edit_add_kollman_charges_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.addKollmanCharges("hsg1:::")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.addKollmanCharges('hsg1:::', log=0)")

    def test_edit_add_kollman_charges_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        oldself =self
        self =mv
        s = "self.addKollmanCharges('hsg1:::', log=0)"
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_compute_gasteiger_charge_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.computeGasteiger("hsg1:::")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeGasteiger("hsg1:::", log=0)')


    def test_compute_gasteiger_charge_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        oldself=self
        self = mv
        s = 'self.computeGasteiger("hsg1:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_check_Res_charges_log_checks_expected_log_string(self):
         """checks expected log string is written
         """
         self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
         self.mv.readMolecule("Data/hsg1.pdb")
         self.mv.Mols[0].buildBondsByDistance()
         self.mv.addKollmanCharges("hsg1:::")
         self.mv.checkResCharges("hsg1::")
         if self.mv.hasGui:
             tx = self.mv.GUI.MESSAGE_BOX.tx
             last_index = tx.index('end')
             last_entry_index = str(float(last_index)-2.0)
             last_entry = tx.get(last_entry_index, 'end')    
             self.assertEqual(split(last_entry,'\n')[0],'self.checkResCharges("hsg1::;", log=0)')

    def test_check_Res_charges_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()    
        self.mv.addKollmanCharges("hsg1:::")
        oldself=self
        self =mv
        s = 'self.checkResCharges("hsg1::;", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_set_chargeSet_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.addKollmanCharges("hsg1:::")
        self.mv.setChargeSet("hsg1:::", 'Kollman')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.setChargeSet("hsg1:::", \'Kollman\', log=0)')
        
        
    def test_set_chargeSet_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.addKollmanCharges("hsg1:::")
        oldself=self
        self = mv
        s = 'self.setChargeSet("hsg1:::", \'Kollman\', log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_Merge_nphs_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/ld.pdb")
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self.mv.addKollmanCharges(mol)
        self.mv.mergeNPHS("ld:A:MET36:HA")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-3.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.mergeNPHS("ld:A:MET36:HA", log=0)')


    def test_Merge_nphs_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/ld.pdb")
        mol = self.mv.Mols[0]
        mol.buildBondsByDistance()
        self.mv.addKollmanCharges(mol)
        oldself=self
        self =mv
        s = 'self.mergeNPHS("ld:A:MET36:HA", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_edit_fix_names_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/piece_1tmn.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        self.mv.fixHNames("piece_1tmn:::")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.fixHNames('piece_1tmn:::', log=0)")

    def test_edit_fix_names_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/piece_1tmn.pdb")
        self.mv.Mols[0].buildBondsByDistance()
        oldself=self
        self =mv
        s = "self.fixHNames('piece_1tmn:::', log=0)"
        exec(s)
        oldself.assertEqual(1,1)
        
    

if __name__ == '__main__':
    unittest.main()

