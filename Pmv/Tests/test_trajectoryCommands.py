############################################################################
#
# Author: Anna Omelchenko
#
# Copyright: M. Sanner TSRI 2000
#
#############################################################################


import unittest,string
mv =None

class TestCommand(unittest.TestCase):
#class TestCommand():
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """

    def startViewer(self, totalCt):
        """
        start Molecule Viewer
        """
        global mv
        self.totalCt = totalCt 
        if mv is None:
            print "starting Viewer"
            from Pmv.moleculeViewer import MoleculeViewer

            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                       withShell=0, verbose=False, trapExceptions=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', package='Pmv')
            mv.browseCommands('deleteCommands', commands=['deleteMol',],package= 'Pmv')
            mv.browseCommands('colorCommands', commands=('colorByAtomType',),
			  log=0, package='Pmv')
            mv.browseCommands("bondsCommands", commands= ["buildBondsByDistance"],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands('trajectoryCommands', package='Pmv')
            self.ct = 0
            mv.readMolecule('./Data/after_full.gro')
            mv.colorByAtomType("after_full", ['lines'])
            
        self.mv = mv

    def tearDown(self):
        """
        clean-up
        """
        #delete any molecules left due to errors
        #for m in self.mv.Mols:
        #   self.mv.deleteMol(m)
        if hasattr(self, "ct"):
            self.ct = self.ct + 1
            if self.ct==self.totalCt:
                print 'destroying mv'
                self.mv.Exit(0)
                del self.mv

class TestOpenTrajectory(TestCommand):

    def setUp(self):
        if not hasattr(self, 'mv'):
            print "creating viewer"
            self.startViewer(2)


    def test_read_trrFile(self):
        """ test: read Gromacs trr trajectory file."""
        
        self.mv.openTrajectory('./Data/full.trr', log=1)
        assert self.mv.Trajectories.has_key('full.trr')
        tr = self.mv.Trajectories['full.trr']
        # chech number of frames (lenghth of coordinates array)
        self.assertEqual(tr.nbFrames, 21)
        # chech number of atoms in the trajectory
        self.assertEqual(tr.nbAtoms, 2741)
        self.assertEqual(tr.parser.headers[0]['natoms'] , tr.nbAtoms)

        
    def read_xtcFile(self):
        pass


class TestPlayTrajectory(TestCommand):
    def setUp(self):
        if not hasattr(self, 'mv'):
            self.startViewer(3)
        if not hasattr(self, "traj"):
            self.mv.openTrajectory('./Data/full.trr', log=1)
            #self.mv.readMolecule('./Data/after_full.gro')
            self.mv.GUI.VIEWER.Redraw()
            self.traj = self.mv.Trajectories['full.trr']
                        

    def test_1(self):
        """ test : open trajectory player """
        mol = self.mv.Mols[0]
        assert mol.name == "after_full"
        print mol
        print type(mol.allAtoms)
        assert self.traj.nbAtoms == len(mol.allAtoms)
        
        self.mv.playTrajectory('after_full', 'full.trr', log=1)
        from Pmv.trajectoryCommands import TrajPlayerCached   
        player = self.traj.player
        assert isinstance(player, TrajPlayerCached)
        assert player.form is not None

    def test_2(self):
        """ test: play trajectory """
        
        mol = self.mv.Mols[0]
        assert mol.name == "after_full"
        assert self.traj.nbAtoms == len(mol.allAtoms)
        
        self.mv.playTrajectory('after_full', 'full.trr', log=1)
        player = self.traj.player
        player.Play_cb()
        self.assertEqual(player.currentFrameIndex, 21)
        assert len(player.dpyLists) == 21
        # reverse play
        player.PlayRev_cb()
        self.assertEqual(player.currentFrameIndex,0)
        # clear cache
        player.clearCache_cb()
        assert len(player.dpyLists) == 0


    def test_3(self):
        """ test: go to # frame"""
        
        mol = self.mv.Mols[0]
        assert mol.name == "after_full"
        assert self.traj.nbAtoms == len(mol.allAtoms)
        
        self.mv.playTrajectory('after_full', 'full.trr', log=1)
        player = self.traj.player
        player.clearCache_cb()
        ind = 10
        player.nextFrame(ind)
        self.assertEqual(player.currentFrameIndex,ind)
        assert player.dpyLists.has_key(ind-1)
        assert player.dpyLists[ind-1] is not None
        # test that slider at the bottom of the player is set correctly
        slider = player.form.descr.entryByName['slider']['widget']
        self.assertEqual (int(slider.get()), ind)


if __name__ == '__main__':
    unittest.main()
    
