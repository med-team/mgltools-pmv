#$Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_loadCommands.py,v 1.4 2009/11/10 22:59:02 annao Exp $
# Author: Sargis Dallakyan - sargis@scripps.edu
# Summary: Makes sure that all Pmv modules can be loaded.
#$Id: test_loadCommands.py,v 1.4 2009/11/10 22:59:02 annao Exp $
import unittest, sys, os, time
from mglutil.util.packageFilePath import findModulesInPackage
import Pmv
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1
    
class LoadTest(unittest.TestCase):
    """Base class for unittest"""
    def test_loadAllModules(self):
        from Pmv.moleculeViewer import MoleculeViewer
        mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                            withShell=0, trapExceptions=False, gui=hasGUI)
        mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        PmvPath = Pmv.__path__[0]
        modules = findModulesInPackage(PmvPath,"^def initModule")[PmvPath]
        failedTest = []
        for module_py in modules:
            module = os.path.splitext(module_py)[0]
            print 'loading...' + module
            try:
                if not mv.hasGui:
                    if module in ["hbondCommands"]:
                        continue
                    mv.loadModule(module, 'Pmv')
            except Exception, inst:
                print inst
                failedTest.append(module_py)
            
        self.assertEqual(failedTest,[])
        if mv.hasGui:
            mv.Exit(0)

            
if __name__ == '__main__':
        unittest.main()

    
