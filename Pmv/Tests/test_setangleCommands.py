#
#
#  $Id: test_setangleCommands.py,v 1.23.4.1 2016/02/11 21:29:44 annao Exp $
#
#

import unittest, glob, os, Pmv.Grid, string,sys
import time, Tkinter, math
import numpy
from string import split,strip,find
from MolKit.molecule import Atom
from MolKit.protein import Residue
from math import pi
mv = None
ct = 0
totalCt = 36
klass = None

class Pmvsetangle_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(logMode='no', verbose=False,
                                 trapExceptions=False, withShell=0)
            mv.loadModule('setangleCommands', 'Pmv')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            #need access to error messages
            mv.setOnAddObjectCommands(['buildBondsByDistance', 'displayLines'])
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
            
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        c =self.mv.setTorsionGC
        c.snakeLength = 1
        c.oldValue = None
        c.torsionType = Tkinter.StringVar()
        c.torsionType.set('1')
        c.newAngList = Tkinter.StringVar()
        c.TAHdata = None
        c.molecule = None
        #c.bondString = Tkinter.StringVar()
        c.callbackDict = {}
        c.callbackDict['measureDistanceGC'] = 'update'
        c.callbackDict['measureAngleGC'] = 'update'
        c.callbackDict['measureTorsionGC'] = 'update'
        c.atomList = []
        try:
            c.form.withdraw()
        except:
            pass


    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt

        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)

        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv

    def assertArrayEqual(self, a1, a2):
        self.assertEqual(len(a1), len(a2))
        d = abs(a1-a2)
        for v in d:
            for i in range(len(v)):
                self.assertTrue(v[i]< 1.e-5)
                
                    


class Pmvsetangle_setangleTests1(Pmvsetangle_BaseTests):


    def test_set_torsion_widget(self):
        """checks setangle widget displays
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        c("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        c.Done_cb()

    

    def test_setangle_invalid_input(self):
        """checks set angle invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        returnvalue=c("small_1crn_hs: :GLY345:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        self.assertEqual(returnvalue, None)
        #self.assertEqual(returnvalue,'ERROR')
    

    def test_setangle_empty_input(self):
        """checks set angle empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        returnvalue=c(" ", None, log=0)
        self.assertEqual(returnvalue,'ERROR')
    

    def test_set_angleGC_spheres(self):
        """checks geoms visible
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        self.assertEqual(c.spheres.visible,1)
        

    def test_set_torsion_update_lines(self):
        """
        checks geoms vertex set changes when torsion angle changes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.selectFromString(silent=True, res='', mols='', atoms='1-4', negate=0, chains='', log=False)
        c=self.mv.setTorsionGC
        self.mv.setIcomLevel(Atom,KlassSet=None)
        c.guiCallback()
        self.mv.setTorsionGC("1crn: :THR1:O",None)
        self.mv.setTorsionGC("1crn: :THR1:C",None)
        self.mv.setTorsionGC("1crn: :THR1:CA",None)
        self.mv.setTorsionGC("1crn: :THR1:N",None)
        oldvert=c.spheres.vertexSet.vertices.array[:]
        lineoldvert = self.mv.Mols[0].geomContainer.atoms['bonded'].coords
        
        x=c.ifd.entryByName
        ##set type to Absolute
        x['rdbut1']['widget'].select()
        ##set torsion to a specific value
        x['extslider']['widget'].set(190)
        self.mv.GUI.VIEWER.Redraw()
        newvert=c.spheres.vertexSet.vertices.array[:]
        linenewvert=self.mv.Mols[0].geomContainer.atoms['bonded'].coords
        #comparing sphere vertices
        self.assertEqual(str(oldvert[3:][0])!=str(newvert[3:][0]),True)
        #comparing line vertices of atom N
        #print lineoldvert,linenewvert
        self.assertEqual(lineoldvert[0]!=linenewvert[0],True)
        #comparing stick verices

        
    def test_set_torsion_update_balls(self):
        """checks balls and spheres vertex set changes when torsion angle changes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        #self.mv.selectFromString(silent=True, res='', mols='', atoms='1-4', negate=0, chains='', log=False)
        self.mv.select(self.mv.allAtoms[:4])
        c=self.mv.setTorsionGC
        self.mv.setIcomLevel(Atom,KlassSet=None)
        c.guiCallback()
        self.mv.setTorsionGC("1crn: :THR1:O",None)
        self.mv.setTorsionGC("1crn: :THR1:C",None)
        self.mv.setTorsionGC("1crn: :THR1:CA",None)
        self.mv.setTorsionGC("1crn: :THR1:N",None)
        self.mv.displaySticksAndBalls(self.mv.getSelection(), log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        oldstickvert=self.mv.Mols[0].geomContainer.atoms['sticks'].coords[:]
        oldballvert=self.mv.Mols[0].geomContainer.atoms['balls'].coords[:]
        x=c.ifd.entryByName
        ##set type to Absolute
        x['rdbut1']['widget'].select()
        ##set torsion to a specific value
        x['extslider']['widget'].set(100)
        self.mv.GUI.VIEWER.Redraw()
        self.mv.GUI.VIEWER.master.update_idletasks()
        newstickvert=self.mv.Mols[0].geomContainer.atoms['sticks'].coords[:]
        newballvert=self.mv.Mols[0].geomContainer.atoms['balls'].coords[:]
        self.assertNotEqual(oldstickvert[0],newstickvert[0])
        self.assertNotEqual(oldballvert[0], newballvert[0])
        
    def test_set_torsion_update_cpk(self):
        """checks cpk vertex set changes when torsion angle changes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        #self.mv.selectFromString(silent=True, res='', mols='', atoms='1-4', negate=0, chains='', log=False)
        self.mv.select(self.mv.allAtoms[:4])
        c=self.mv.setTorsionGC
        self.mv.setIcomLevel(Atom,KlassSet=None)
        c.guiCallback()
        self.mv.setTorsionGC("1crn: :THR1:O",None)
        self.mv.setTorsionGC("1crn: :THR1:C",None)
        self.mv.setTorsionGC("1crn: :THR1:CA",None)
        self.mv.setTorsionGC("1crn: :THR1:N",None)
        self.mv.displayCPK(self.mv.getSelection(), log=0, cpkRad=0.0, scaleFactor=1.0, only=True, negate=False, quality=10)
        oldcpkvert = self.mv.Mols[0].geomContainer.atoms['cpk'].coords
        x=c.ifd.entryByName
        ##set type to Absolute
        x['rdbut1']['widget'].select()
        ##set torsion to a specific value
        x['extslider']['widget'].set(-100)
        self.mv.GUI.VIEWER.master.update_idletasks()
        self.mv.GUI.VIEWER.Redraw()
        newcpkvert=self.mv.Mols[0].geomContainer.atoms['cpk'].coords
        self.assertEqual(oldcpkvert[0]!=newcpkvert[0],True)
    
    def test_set_angleGC_slider(self):
        """checks value in slider
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        c("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        x=c.ifd.entryByName
        #set type to Absolute
        x['rdbut1']['widget'].select()
        c.rdSet()
        #settorsion to a specific file
        x['extslider']['widget'].set(123)
        self.assertEqual(c.extslider.val,123)
        c.Done_cb()

        
    def test_set_angleGC_anglelistEnt(self):
        """checks value in angle list entry
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        c("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        x=c.ifd.entryByName
        #set type to Absolute
        x['rdbut1']['widget'].select()
        c.rdSet()
        self.assertEqual(x['angListEnt']['widget'].get(),' -70.207')
        self.assertEqual(c.angListEnt.get(),' -70.207')
        c.Done_cb()

        
    def test_set_angleGC_torsion_type_absolute(self):
        """ checks torsion type is absolute and angle is -70.207
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        c("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        #c.buildForm()
        c.guiCallback()
        x=c.ifd.entryByName
        #set type to Absolute
        x['rdbut1']['widget'].select()
        c.rdSet()
        x['rdbut1']['widget'].update_idletasks()
        self.assertEqual(c.torsionType.get(),'1')
        self.assertEqual(str(round(c.extslider.val,3)),'-70.207')
        c.Done_cb()
        

    def test_set_angleGC_torsion_type_relative(self):
        """ checks torsion type is relative and default torsion angle 0
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        c("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        #x=c.ifd.entryByName
        #set type to Relative
        c.torsionType.set('0')
        wid = c.ifd.entryByName['rdbut2']['widget']
        wid.select()
        c.rdSet()
        wid.update_idletasks()
        #x['rdbut2']['widget'].select()
        self.assertEqual(c.torsionType.get(),'0')
        self.assertEqual(c.extslider.val,0.00)
        c.Done_cb()
        

    def test_set_angleGC_history_list(self):
        """ checks history list and angle
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        #build some history
        c("small_1crn_hs: :PRO41:CA;small_1crn_hs: :PRO41:C;small_1crn_hs: :PRO41:O;small_1crn_hs: :PRO41:CB;small_1crn_hs: :PRO41:HB2;small_1crn_hs: :PRO41:CG;small_1crn_hs: :PRO41:HG2;small_1crn_hs: :GLY42:N;small_1crn_hs: :GLY42:CA;small_1crn_hs: :GLY42:HA1;small_1crn_hs: :GLY42:HA2;small_1crn_hs: :GLY42:HN", None, log=0)
        c("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        #at this point, 2nd listbox entry is selected
        x=c.ifd.entryByName
        x['rdbut1']['widget'].select()
        c.rdSet()
        x['historyList']['widget'].lb.selection_clear('0','end')
        x['historyList']['widget'].lb.selection_set(0)
        self.assertEqual(c.historyList.curselection()[0],'0')
        self.assertEqual(c.angListEnt.get(),' -70.207')
        self.assertEqual(str(round(c.extslider.val,3)),'-70.207')
        c.Done_cb()

        
    def test_set_angleGC_spheres_new_torsion(self):
        """checks new torsion command when called geoms are invisible
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        x=c.ifd.entryByName
        x['rdbut1']['widget'].select()
        c.rdSet()
        c.new_Tors()
        x['historyList']['widget'].lb.selection_clear('0','end')

        self.assertEqual(len(c.spheres.vertexSet),0)
        c.Done_cb()
       

    def test_set_angleGC_resetAll(self):
        """checks reset button,changes selection in history list
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        c("small_1crn_hs: :PRO41:CA;small_1crn_hs: :PRO41:C;small_1crn_hs: :PRO41:O;small_1crn_hs: :PRO41:CB;small_1crn_hs: :PRO41:HB2;small_1crn_hs: :PRO41:CG;small_1crn_hs: :PRO41:HG2;small_1crn_hs: :GLY42:N;small_1crn_hs: :GLY42:CA;small_1crn_hs: :GLY42:HA1;small_1crn_hs: :GLY42:HA2;small_1crn_hs: :GLY42:HN", None, log=0)

        x=c.ifd.entryByName
        x['rdbut1']['widget'].select()
        c.rdSet()
        x['historyList']['widget'].lb.selection_clear('0','end')
        x['historyList']['widget'].lb.selection_set('0')
        c.resetAll()
        self.assertEqual(c.historyList.curselection()[0],'1')
        c.Done_cb()
   
    
    def test_set_angleGC_step_back(self):
        """checks stepback button:
        
        1.anglistentry is same as before setting extslider val and after calling stepback command

        2.geoms vertices are same before setting extslider val and after calling stepback command

        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        oldangListEnt=c.angListEnt.get()
        oldvert=c.spheres.vertexSet.vertices.array
        x=c.ifd.entryByName
        x['rdbut1']['widget'].select()
        c.rdSet()
        x['extslider']['widget'].set(149)
        #stepBack
        c.stepBack()
        newvert=c.spheres.vertexSet.vertices.array
        newangListEnt=c.angListEnt.get()
        self.assertEqual(oldangListEnt,newangListEnt)
        self.assertArrayEqual(oldvert, newvert)
        #self.assertTrue(numpy.alltrue(oldvert == newvert))
        c.Done_cb()

        

    def test_set_angleGC_start_over(self):
        """checks start overbutton:
        1.anglistentry is same as before setting extslider val and after         calling startover command

        2.geoms vertices are same before setting extslider val and afte          r calling startover command
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        oldangListEnt=string.strip(c.angListEnt.get())
        oldvert=c.spheres.vertexSet.vertices.array
        x=c.ifd.entryByName
        x['rdbut1']['widget'].select()
        c.rdSet()
        x['extslider']['widget'].set(67.29)
        #startOver
        c.startOver()
        newvert=c.spheres.vertexSet.vertices.array
        newangListEnt=string.strip(c.angListEnt.get())
        self.assertEqual(oldangListEnt,'-70.207')
        self.assertEqual(newangListEnt,'-70.207')
        self.assertArrayEqual(oldvert,newvert)
        c.Done_cb()
        
        
    def test_set_angleGC_move(self):
        """checks move button by setting slider to some value ,
        vertices of last selected geom changes
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        c=self.mv.setTorsionGC
        #c.buildForm()
        c.guiCallback()
        self.mv.setTorsionGC("small_1crn_hs: :PRO41:HG2;small_1crn_hs: :PRO41:CG;small_1crn_hs: :PRO41:CB;small_1crn_hs: :PRO41:HB2", None, log=0)
        oldvert=c.spheres.vertexSet.vertices.array[3][:]
        x=c.ifd.entryByName
        #set to Relative
        x['rdbut2']['widget'].select()
        c.torsionType.set('0')
        c.rdSet()
        x['extslider']['widget'].set(180)
        #this would move everything back to where it was
        #c.repeat_transTors()
        self.mv.GUI.VIEWER.Redraw()
        newvert=c.spheres.vertexSet.vertices.array[3][:]
        #self.assertNotEqual(oldvert, newvert)
        self.assertFalse(numpy.alltrue(oldvert==newvert))
        #Move command???
        #c.repeat_transTors()
        #c.repeat_transTors()
        #self.assertEqual(oldvert == newvert,True)
        c.Done_cb()

class Pmvsetangle_setangleTests2(Pmvsetangle_BaseTests):
        
#SetRelativeTorsion


    def test_set_relative_torsion_invalid_input(self):
        """checks set relative torsion invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        c=self.mv.setRelativeTorsion
        returnvalue=c('CB1','HB2',40)
        self.assertEqual(returnvalue,'ERROR')

    
    def test_set_relative_torsion_empty_input(self):
        """checks set relative torsion empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        c=self.mv.setRelativeTorsion
        returnvalue=c(' ',' ',40)
        self.assertEqual(returnvalue,'ERROR')

    def test_set_relative_torsion_coords(self):
        """checks for new coords after setting relative torsion
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        c=self.mv.setRelativeTorsion
        oldcoords0=mv.allAtoms[0].coords
        c("small_1crn_hs: :PRO41:HG2","small_1crn_hs: :PRO41:CG",70)
        newcoords0=mv.allAtoms[0].coords
        self.assertNotEqual(oldcoords0,newcoords0)
    
#end SetRelativeTorsion


#Set Translation

    def test_set_translation_invalid_input(self):
        """checks set translation invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTranslation
        returnvalue=c(mv.allAtoms[90:],(2.0,3.0,5.0))
        self.assertEqual(returnvalue,'ERROR')

    
    def test_set_translation_empty_input(self):
        """checks set translation empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTranslation
        returnvalue=c(' ',(2.0,3.0,5.0))
        self.assertEqual(returnvalue,'ERROR')

    
    def test_set_translation_coords_values(self):
        """checks for new coords after setting translation +1 in z-axis
        """
        self.mv.setOnAddObjectCommands([])
        self.mv.readMolecule("Data/setangle_model.pdb")
        oldz1=mv.allAtoms.coords[0][2]
        oldz2=mv.allAtoms.coords[1][2]
        oldz3=mv.allAtoms.coords[2][2]
        oldz4=mv.allAtoms.coords[3][2]
        c=self.mv.setTranslation
        c(mv.allAtoms,(0.0,0.0,1.0))
        newz1=mv.allAtoms.coords[0][2]
        newz2=mv.allAtoms.coords[1][2]
        newz3=mv.allAtoms.coords[2][2]
        newz4=mv.allAtoms.coords[3][2]
        #z-co_ordinate is increased by 1
        self.assertEqual(newz1,oldz1+1)
        self.assertEqual(newz2,oldz2+1)
        self.assertEqual(newz3,oldz3+1)
        self.assertEqual(newz4,oldz4+1)

        

    
        
#end setTranslation



#set Quaternion
    def test_set_Quaternion_invalid_input(self):
        """checks set Quaternion invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setQuaternion
        returnvalue=c(mv.allAtoms[90:],(2.0,3.0,5.0))
        self.assertEqual(returnvalue,'ERROR')

    
    def test_set_Quaternion_empty_input(self):
        """checks set Quaternion empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setQuaternion
        returnvalue=c(' ',(1.0,3.0,5.0))
        self.assertEqual(returnvalue,'ERROR')

    def test_set_Quaternion_coords(self):
        """checks for new coords after setting Quaternion about x-axis
        """
        
        self.mv.readMolecule("Data/setangle_model1.pdb")
        oldcoords1=self.mv.allAtoms[0].coords[1]
        oldcoords2=self.mv.allAtoms[1].coords[1]
        oldcoords3=self.mv.allAtoms[2].coords[1]
        self.mv.setQuaternion(self.mv.allAtoms, (1, 0, 0,pi))
        newcoords1=self.mv.allAtoms[0].coords[1]
        newcoords2=self.mv.allAtoms[1].coords[1]
        newcoords3=self.mv.allAtoms[2].coords[1]
        self.assertEqual(oldcoords1,-(newcoords1))
        self.assertEqual(oldcoords2,newcoords2)
        self.assertEqual(oldcoords3,-(newcoords3))

#end setQuaternion

#set Torsion

    def test_set_Torsion_invalid_input(self):
        """checks set  invalid input Torsion
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsion
        returnvalue=c(self.mv.allAtoms[70:],self.mv.allAtoms[2],self.mv.allAtoms[3],self.mv.allAtoms[4],30,0)

        self.assertEqual(returnvalue,'ERROR')

    
    def test_set_Torsion_empty_input(self):
        """checks set  empty input Torsion
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c=self.mv.setTorsion
        returnvalue=c(' ',' ', ' ',' ',30,0)
        self.assertEqual(returnvalue,'ERROR')

    def test_set_Torsion_coords(self):
        """checks for new coords after setting Torsion to 180
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        c=self.mv.setTorsion
        oldcoords4=self.mv.allAtoms[4].coords
        c(self.mv.allAtoms[1],self.mv.allAtoms[2],self.mv.allAtoms[3],self.mv.allAtoms[4],180,0)
        newcoords4=self.mv.allAtoms[4].coords
        self.assertEqual(oldcoords4!=newcoords4,True)
        

######################################################
### Log Tests
######################################################

class SetangleCommandsLogTest(Pmvsetangle_BaseTests):

    #def test_set_TorsionGC_log_checks_expected_log_string(self):
        #"""checks expected log string is written
        #"""
        #self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        #self.mv.readMolecule("Data/small_1crn_hs.pdb")
        #self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)
        #tx = self.mv.GUI.MESSAGE_BOX.tx
        #last_index = tx.index('end')
        #last_entry_index = str(float(last_index)-2.0)
        #last_entry = tx.get(last_entry_index, 'end')    
        #self.assertEqual(split(last_entry,'\n')[0],'self.mv.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)')

        
        
    #def test_set_TorsionGC_log_checks_that_it_runs(self):
        #"""Checking log string runs """
        #self.mv.readMolecule("Data/small_1crn_hs.pdb")
        #oldself=self
        #self =mv
        #s = 'self.setTorsionGC("small_1crn_hs: :GLY42:C;small_1crn_hs: :ASP43:N;small_1crn_hs: :TYR44:N;small_1crn_hs: :TYR44:HN", None, log=0)'
        #exec(s)
        #oldself.assertEqual(1,1)


        
    def test_set_relative_torsion_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        self.mv.setRelativeTorsion("small_1crn_hs: :PRO41:HG2","small_1crn_hs: :PRO41:CG",70)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.setRelativeTorsion("small_1crn_hs: :PRO41:HG2", "small_1crn_hs: :PRO41:CG", 70, None, 0, log=0)')
        


    def test_set_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        oldself=self
        self =mv
        s ='self.setRelativeTorsion("small_1crn_hs: :PRO41:HG2", "small_1crn_hs: :PRO41:CG", 70, None, 0, log=0)'
        exec(s)
        oldself.assertEqual(1,1)


    def test_set_translation_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.setOnAddObjectCommands([])
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.setTranslation(mv.allAtoms,(0.0,0.0,1.0))       
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.setTranslation("small_1crn_hs:::", (0.0, 0.0, 1.0,), log=0)')
        
    
    def test_set_translation_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.setOnAddObjectCommands([])
        self.mv.readMolecule("Data/setangle_model.pdb")
        oldself=self
        self =mv
        s = 'self.setTranslation("setangle_model:::", (0.0, 0.0, 1.0), log=0)'
        exec(s)
        oldself.assertEqual(1,1)
    

    def test_set_Quaternion_log_checks_expected_log_string_1(self):
        """checks expected log string is written
        """        
        self.mv.readMolecule("Data/setangle_model1.pdb")
        oldself=self
        self =mv
        s ='self.setQuaternion("small_1crn_hs:::", (1, 0, 0, 3.1415926535897931), (0.0, 0.0, 0.0), (0.0, 0.0, 0.0), log=0)'
        exec(s)
        oldself.assertEqual(1,1)
       
    def test_set_Torsion_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        self.mv.setTorsion(self.mv.allAtoms[1],self.mv.allAtoms[2],self.mv.allAtoms[3],self.mv.allAtoms[4],180,0)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.setTorsion("small_1crn_hs: :PRO41:C", "small_1crn_hs: :PRO41:O", "small_1crn_hs: :PRO41:CB", "small_1crn_hs: :PRO41:HB2", 180, 0, log=0)')

    def test_set_Torsion_log_checks_expected_log_string_1(self):
        """checks expected log string is written
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.Mols[-1].buildBondsByDistance()
        oldself=self
        self =mv
        s ='self.setTorsion("small_1crn_hs: :PRO41:C", "small_1crn_hs: :PRO41:O", "small_1crn_hs: :PRO41:CB", "small_1crn_hs: :PRO41:HB2", 180, 0, log=0)'
        exec(s)
        oldself.assertEqual(1,1)

if __name__ == '__main__':
    unittest.main()

