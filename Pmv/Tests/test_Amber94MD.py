#
#
# $Id: test_Amber94MD.py,v 1.7 2009/11/10 22:59:02 annao Exp $
#
#############################################################################
#                                                                           #
#   Author:Sowjanya Karnati                                                 #
#   Copyright: M. Sanner TSRI 2000                                          #
#                                                                           #
#############################################################################

import sys,os
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
ct = 0
totalCt = 6 #10

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class AmberBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, withShell=0, gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('amberCommands', package='Pmv')
            #set up links to shared dictionary and current instance
            from Pmv.amberCommands import Amber94Config, CurrentAmber94
            self.Amber94Config = Amber94Config
            self.CurrentAmber94 = CurrentAmber94
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        #for key,value in self.Amber94Config.items():
        #    del self.Amber94Config[key]
        #    try:
        #        del value
        #    except:
        #        print "exception in deleting ", value
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv


##################################################################
#           MD_Amber94 COMMAND TESTS                             #
##################################################################

class Amber94MD(AmberBaseTest):

    def xtest_md_amber94_widget(self):
        """tests md_Amber94 widget is mapped
        FIX THIS
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        #self.mv.setup_Amber94("trp3_h:",'mdtest1','Data/trp3_h.prmtop')
        #c1 = self.mv.md_Amber94
        ##call md_Amber94 command
        #c1('mdtest1', 349, callback=1, filename='0', log=0, callback_freq=10)
        #c1.buildForm()
        ##Need to do this otherwise the form closes before assert
        ##but = c1.ifd.entryByName['setfroz_cb']['widget']
        ##but.wait_visibility(but)
        #self.assertEqual(c1.ifd.form.root.winfo_ismapped(),1)    
        #c1.Accept_cb()
        

    def xtest_md_amber94_set_md_opts(self):
        """tests md_Amber94 widget entry setminopts, when on shows
        setminopts widget
        FIX THIS
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        #self.mv.setup_Amber94("trp3_h:",'mdtest2','Data/trp3_h.prmtop') 
        #c= self.mv.setmdOpts_Amber94
        #c('mdtest2')
        #c1 = self.mv.md_Amber94
        ##call md_Amber94 command
        #c1('mdtest2', 349, callback=1, filename='0', log=0, callback_freq=10)
        #c1.buildForm()
        ##toggle setminopts button
        #c1.setMdOpts()
        #Need to do this otherwise the form closes before assert
        #but = c.ifd.entryByName['zerov_cb']['widget']
        #but.wait_visibility(but)
        #checks setminopts widget is dispalyed
        #self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        #c.Accept_cb()
        #c1.Close_cb(c1.ifd)
    
    
    def xtest_amber94_md_set_cons(self):
        """tests md_Amber94 widget entry setconstrainedatoms, when on shows
        setconstrainedatoms widget
        FIX THIS
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        #self.mv.setup_Amber94("trp3_h:",'mdtest3','Data/trp3_h.prmtop')
        #c= self.mv.constrainAtoms_Amber94
        #c('mdtest3', "trp3_h: :LEU1:CA;trp3_h: :TRP2:CA;trp3_h: :GLN3:CA", [[7.3717881770581828, -1.4355121812459293, 29.157618041258299], [8.0758698590763469, -3.9904585482337724, 31.994182820197484], [4.592990279264443, -4.731006081632029, 30.914477999132139]], log=0)
        #c1 = self.mv.md_Amber94
        ##call md_Amber94 command
        #c1('mdtest3', 349, callback=1, filename='0', log=0, callback_freq=10)
        #c1.buildForm()
        #toggle setConstrainedAtoms button
        #c1.setConstrainedAtoms()
        ##Need to do this otherwise the form closes before assert
        ##but = c.ifd.entryByName['constrAts']['widget']
        ##but.wait_visibility(but)
        ###checks setConstrainedAtoms widget is displayed
        #self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        #c.Accept_cb()
        #c1.Close_cb(c1.ifd)
        
    def xtest_amber94_md_set_freeze_atoms(self):
        """tests md_Amber94 widget entry setfreezeatoms, when on shows
        setfreezeatoms widget
        FIX THIS
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        ##call setup_Amber94 command
        #self.mv.setup_Amber94("trp3_h:",'mdtest4','Data/trp3_h.prmtop')
        #c= self.mv.freezeAtoms_Amber94
        #c1 = self.mv.md_Amber94
        ##call md_Amber94 command
        #c1('mdtest4', 349, callback=1, filename='0', log=0, callback_freq=10)
        #c1.buildForm()
        ##toggle setfreezeatoms button
        #c1.setFrozenAtoms()
        ##Need to do this otherwise the form closes before assert
        ##but = c.ifd.entryByName['frozAts']['widget']
        ##but.wait_visibility(but)
        ##checks setfreezeatoms widget is displayed
        #self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        #c.Accept_cb()
        #c1.Close_cb(c1.ifd)

    def test_md_amber94_opts_update_geom(self):
        """checks toggle updategeom
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest5','Data/trp3_h.prmtop')
        c = self.mv.md_Amber94
        #call md_Amber94 command
        c('mdtest5', 349, callback=1, filename='0', log=0, callback_freq=10)
        if self.mv.hasGui:
            c.buildForm()
            #toggle update geom button
            c.ifd.entryByName['callback_cb']['widget'].invoke()
            self.assertEqual(c.callback.get(),1)
            c.Accept_cb()
            
        
    def test_md_amber94_opts_update_frequency(self):
        """checks setting update freq when update geom is on
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest6','Data/trp3_h.prmtop')
        #call md_Amber94 command
        c = self.mv.md_Amber94
        c('mdtest6', 349, callback=1, filename='0', log=0, callback_freq=10)
        if self.mv.hasGui:
            c.buildForm()
            #toggle update geom button on
            c.ifd.entryByName['callback_cb']['widget'].invoke()
            #setting a value to update frequency
            c.callbackFreq.set(20)
            self.assertEqual(c.callbackFreq.get(),'20')
            c.Accept_cb()

    def test_md_amber94_amber_Ids(self):
        """checks by setting some key value in amberIds button
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest7','Data/trp3_h.prmtop')
        c1 = self.mv.md_Amber94
        #call md_Amber94 command
        c1('mdtest7', 349, callback=1, filename='0', log=0, callback_freq=10)
        if self.mv.hasGui:
            c1.buildForm()
            #amberIds button
            AmberIdBut = c1.ifd.entryByName['amberIds']['widget']
            AmberIdBut.setentry('mdtest7')
            self.assertEqual(AmberIdBut.get(),'mdtest7')
            c1.Accept_cb()

        
    def test_md_amber94_max_step_thumb_wheel(self):
        """checks by setting maximum iterations 
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest8','Data/trp3_h.prmtop')
        #call md_Amber94 command
        c = self.mv.md_Amber94
        c('mdtest8', 349, callback=1, filename='0', log=0, callback_freq=10)
        if self.mv.hasGui:
            c.buildForm()
            #setting a value to maxiter thumb wheel
            c.ifd.entryByName['maxStep_tw']['widget'].set(400)
            self.assertEqual(c.ifd.entryByName['maxStep_tw']['widget'].get(),400)
            c.Accept_cb()    


    def test_md_amber94_wcons_thumb_wheel(self):
        """checks by setting wcons - restraint weight for keeping atoms close
        to their position in xyz-ref
        """
        #read molecule

        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest9','Data/trp3_h.prmtop')
        #call md_Amber94 command
        c = self.mv.md_Amber94
        c('mdtest9', 349, callback=1, filename='0', log=0, callback_freq=10)
        if self.mv.hasGui:
            c.buildForm()
            #setting a value to wcons
            c.ifd.entryByName['wcons_tw']['widget'].set(0.50)
            self.assertEqual(c.ifd.entryByName['wcons_tw']['widget'].get(),0.50)
            c.Accept_cb()
    
    def test_md_amber94_normal(self):
        """checks md_Amber94
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest10','Data/trp3_h.prmtop')
        #call setmdopts_Amber94
        c = self.mv.md_Amber94
        c('mdtest10', 349, callback=1, filename='0', log=0, callback_freq=10)
        self.assertEqual(hasattr(c,'CurrentAmber94'),True)


    def xmd_amber94_empty_key(self):
        """
        FIX THIS:
        checks setmdopts_Amber94 with empty key
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:",'mdtest11','Data/trp3_h.prmtop')
        #c = self.mv.md_Amber94
        #self.assertEqual(c(' ',349, callback=1, filename='0', log=0, callback_freq=10),'ERROR')
        

    def xmd_amber94_invalid_key(self):
        """checks setmdopts_Amber94 with invalid key
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:",'mdtest12','Data/trp3_h.prmtop')
        #c = self.mv.md_Amber94
        #self.assertEqual(c('gjg',349, callback=1, filename='0', log=0, callback_freq=10),'ERROR')



        
if __name__ == '__main__':
    test_cases = [
        'Amber94MD',
        
        ]
    
    unittest.main( argv=([__name__] + test_cases) )




#if __name__ == '__main__':
#    unittest.main()






        
