#
#########################################################################
#   Authors: Sowjanya KARNATI ,Michel F Sanner , Ruth Huey
#########################################################################
# $Id: test_msmsCommands.py,v 1.50 2011/11/04 23:19:08 annao Exp $
#
#
import sys,unittest,Pmv,time,os
import Tkinter
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from string import split
import numpy
mv =None
klass = None
ct =0
totalCt = 76
from time import sleep
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1


class MSMSBaseTest(unittest.TestCase):
    
    def startViewer(self):
        import Pmv
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer='./.empty', logMode='no', verbose=False, 
                                withShell=0, gui=hasGUI,
                                trapExceptions=False)
            mv.browseCommands('fileCommands', commands=['readMolecule',],package= 'Pmv')
            mv.browseCommands('deleteCommands',commands =['deleteMol',], package='Pmv')
            mv.browseCommands("bondsCommands", commands=["buildBondsByDistance",],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands("interactiveCommands", package='Pmv')
            # Don't want to trap exceptions and errors... the user pref is set to 1 by
            # default
            
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('msmsCommands', package='Pmv')
            mv.browseCommands('colorCommands',package='Pmv')
            # mv.browseCommands('displayCommands',package='Pmv')
            mv.browseCommands("selectionCommands",commands= ["clearSelection",
                                                             "selectFromString"],
                              package="Pmv")
            
        self.mv =mv

    def setUp(self):
        """
        clean-up
        """
           
        #os.system("rm -f *.vert *.face")
        if os.name == 'nt': #sys.platform == 'win32':
            os.system("del /F *.vert *.face")
        else:
            os.system("rm -f *.vert *.face")
        
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
            
    def tearDown(self):
        """
        clean-up
        """
        
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None
        #self.mv.Exit(0)


class ComputeMSMS(MSMSBaseTest):
    
    def test_compute_msms_on_empty_viewer(self):
        """tests compute msms on empty viewer
        """
        if self.mv.computeMSMS.flag & 1:    
            self.mv.computeMSMS(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.computeMSMS cannot be called with only self.mv.getSelection()")
        

    def test_compute_msms_first_molecular_surface(self):
        """tests compute msms for a first molecular surface for the entire
        molecule,but we don't display the msms 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                            pRadius=1.5, display=0)
        self.assertEqual(gc.msms.has_key(sName),True)
        self.assertEqual(gc.msmsAtoms.has_key(sName),True)
        self.assertEqual(mol.geomContainer.msmsAtoms[sName],mol.allAtoms)
        self.assertEqual(gc.atoms.has_key(sName),True)
        self.assertEqual(len(mol.geomContainer.atoms[sName]) , 0)
        self.assertEqual(gc.geoms.has_key(sName),True)
        
    def test_compute_msms_molecular_surface_with_a_subselection(self):
        """Compute msms for  a molecular surface with a subselection
            but for the entire molecule,display
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 1 through 10
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMS(nodes, sName, perMol=1, density=3.0,
                   pRadius=1.5, display=1)
        self.assertEqual(gc.msmsAtoms.has_key(sName),True)
        self.assertEqual(mol.geomContainer.msmsAtoms[sName],mol.allAtoms)

    def test_compute_msms_surface_with_a_subselection_atoms(self):
        """compute msms for a selection and checks selected atoms and msms
        atoms are same 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 1 through 10
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMS(nodes, sName, perMol=1, density=3.0,
                            pRadius=1.5, display=1)
        from MolKit.molecule import Atom
        atms = nodes.findType(Atom)
        atms.sort()
        self.assertEqual(gc.atoms.has_key(sName),True)
        at2 = gc.atoms[sName][:]
        at2.sort()
        self.assertEqual(atms, at2)

    
    def test_compute_msms_surface_current_selection(self):
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 1 through 15
        self.mv.selectFromString('','','1-15', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMS(nodes, sName, perMol=1, density=3.0,
                            pRadius=1.5, display=1) 
        self.assertEqual(gc.msmsAtoms.has_key(sName),True)
        self.assertEqual(gc.msms['1crnMSMS1'][1] , 0)
     
    def test_compute_msms_surface_selected_atoms(self): 
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 1 through 15
        self.mv.selectFromString('','','1-15', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMS(nodes, sName, perMol=1, density=3.0,
                            pRadius=1.5, display=1)
        #msmsm atoms 
        msAtms = mol.geomContainer.atoms['1crnMSMS1']
        msAtms.sort()
        #selected atoms
        selAtms=nodes.findType(Atom)
        selAtms.sort()
        self.assertEqual(selAtms, msAtms)

    def test_compute_msms_surface_default_arguments(self):
        """checks compute msms with default arguements
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 25 through 35
        self.mv.selectFromString('','','25-35', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        # surfName=None, perMol=1, density = 1.0, pRadius = 1.5 and display=1
        self.mv.computeMSMS(nodes)        
        #sName = mol.name+'-MSMS'
        sName = 'MSMS-MOL'
        self.assertEqual(gc.msms.has_key(sName),True)
        #all atoms 
        allatms = mol.allAtoms
        allatms.sort()
        #selected atoms
        satms = gc.msmsAtoms[sName]
        satms.sort()
        self.assertEqual(satms, allatms)
        
#tests invalid input for compute msms
    def test_compute_msms_invalid_nodes(self):
        """tests nodes invalid input for compute msms 
        """
        self.mv.readMolecule('Data/1crn.pdb')   
        command = self.mv.computeMSMS
        returnValue = command('hello')
        self.assertEqual(returnValue,None)
        
    def test_compute_msms_empty_nodes(self):
        """tests nodes empty input for compute msms
        """
        self.mv.readMolecule('Data/1crn.pdb')   
        command = self.mv.computeMSMS
        returnValue = command(' ')
        self.assertEqual(returnValue,None)
        
    def test_compute_msms_empty_surf_name(self):
        """tests surf name empty input for compute msms
        """
        self.mv.readMolecule('Data/1crn.pdb')   
        command = self.mv.computeMSMS
        returnValue = command(self.mv.Mols[0],' ')
        self.assertEqual(returnValue,None)


    def test_compute_msms_invalid_display(self):
        """tests display invalid input for compute msms
        """
        self.mv.readMolecule('Data/1crn.pdb')   
        command = self.mv.computeMSMS
        returnValue = command(self.mv.Mols[0],display = 10)
        self.assertEqual(returnValue,None)
            
#end invalid input for compute msms

class DisplayMSMS(MSMSBaseTest):

    def test_display_msms_empty_viewer(self):
        """tests display msms on empty viewer
        """
        if self.mv.displayMSMS.flag & 1:    
            self.mv.displayMSMS(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.dipslayMSMS cannot be called with only self.mv.getSelection()")    

    def test_display_msms_before_computing(self):
        """tests display msms with out compute msms
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        # displayMSMS before computing any msms surface with default values
        self.mv.displayMSMS('1crn')
        self.assertEqual(self.mv.Mols[0].geomContainer.msms,{})
    
    def test_display_msms_default_values(self):
        """tests display msms with default values
        """
        self.mv.readMolecule('Data/1crn.pdb')    
        self.mv.computeMSMS('1crn')
        self.mv.displayMSMS('1crn')
        self.assertNotEqual(self.mv.Mols[0].geomContainer.msms , {})
    
    def test_display_msms_entire_molecule(self):
        """tests display msms for entire molecule
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        # Compute a first MSMS for the entire molecule'
        sName = '1crnMSMS'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0, pRadius=1.5)
        self.mv.displayMSMS('1crn',surfName=sName, nbVert=1, only=1, negate=0)
        g = gc.geoms[sName]
        #self.assertEqual(len(g.vertexSet),6041)
        self.assertEqual(len(g.vertexSet),6044)
        #self.assertEqual(len(g.faceSet),12078)
        self.assertEqual(len(g.faceSet),12084)

    def test_display_msms_get_selection(self):
        """tests display msms for selected residues
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        # Compute a MSMS for the selected atoms
        sName = '1crnMSMS'
        self.mv.selectFromString('', '', '1-10', '', negate=False,silent=True)
        self.mv.computeMSMS(self.mv.getSelection(), surfName=sName, perMol=1, density=3.0, pRadius=1.5)
        #nbVert = 1
        self.mv.displayMSMS(self.mv.getSelection(),surfName=sName, nbVert=1, only=1, negate=0)
        g = gc.geoms[sName]
        self.assertEqual(len(g.vertexSet),6044)
        self.assertEqual(len(g.faceSet),2235)
        #nbVert = 2
        self.mv.displayMSMS(self.mv.getSelection(), surfName=sName,nbVert=2, only=1, negate=0)
        self.assertEqual(len(g.vertexSet),6044)
        self.assertEqual(len(g.faceSet),2046)
        #nbVert = 3
        self.mv.displayMSMS(self.mv.getSelection(),surfName=sName, nbVert=3, only=1, negate=0)
        self.assertEqual(len(g.vertexSet),6044)
        print "2:len(g.faceSet)=", len(g.faceSet)
        self.assertEqual(len(g.faceSet),1864)
        nodes = self.mv.getSelection()
        selatms = nodes.findType(Atom)
        selatms.sort()
        atms2 = gc.atoms['1crnMSMS']
        atms2.sort()
        self.assertEqual(atms2, selatms)
        
    def test_display_msms_only(self):
        """tests display msms when only = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName='1crnMSMS1'
        self.mv.selectFromString('','','20-40', '',negate=False,silent=True)
        self.mv.computeMSMS(self.mv.getSelection(), surfName=sName, perMol=1, density=3.0, pRadius=1.5)
        self.mv.displayMSMS(self.mv.getSelection(), surfName=sName, only=1, negate=0)
        nodes = self.mv.getSelection()
        selatms = nodes.findType(Atom)
        selatms.sort()
        satms1 = gc.atoms['1crnMSMS1']
        satms1.sort()
        self.assertEqual(satms1,selatms)
        
    def test_display_msms_negate(self):
        """tests display msms when negate = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        self.mv.selectFromString('','','20-40', '',negate=False,silent=True)
        self.mv.computeMSMS(self.mv.getSelection(), surfName=sName, perMol=1, density=3.0, pRadius=1.5)
        self.mv.displayMSMS(self.mv.getSelection(),surfName=sName,only=0,negate=1)
        self.assertEqual(gc.atoms['1crnMSMS1'], [])
        
    def test_display_msms__only_negate(self):
        """tests display msms with only = 1, negate = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName='1crnMSMS1'
        self.mv.selectFromString('','','20-40', '',negate=False,silent=True)
        self.mv.computeMSMS(self.mv.getSelection(), surfName=sName, perMol=1, density=3.0, pRadius=1.5)
        self.mv.displayMSMS(self.mv.getSelection(),surfName=sName,only=1,negate=1)
        self.assertEqual(gc.atoms['1crnMSMS1'], [])
        
    def test_display_msms_two_molecules_one_displayed(self):
        """tests display msms by reading two molecules
        """
        self.mv.readMolecule('./Data/1bsr.pdb')
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        self.mv.computeMSMS('1bsr', surfName='1bsrMSMS')
        self.mv.selectFromString('1bsr','A','', '', negate=False,silent=True)
        nodes=self.mv.getSelection()
        self.mv.displayMSMS(nodes, surfName='1bsrMSMS', only=1,negate=0)
        selatms = nodes.findType(Atom)
        #selatms = AtomSet([x for x in selatms if not x.hetatm])
        selatms.sort()
        satms = gc.atoms['1bsrMSMS']
        satms.sort()
        self.assertEqual(selatms, satms)

        
    def test_display_msms_color_msms(self):
        """tests display msms by coloring 
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.computeMSMS("1crn", surfName="1crnMSMS", perMol=1,display = 1)
        old_color = self.mv.Mols[0].chains.residues.atoms.colors['1crnMSMS']
        self.mv.colorByAtomType(self.mv.Mols[0],geomsToColor = ['1crnMSMS'])
        self.mv.displayMSMS("1crn", surfName = "1crnMSMS")
        new_color = self.mv.Mols[0].chains.residues.atoms.colors['1crnMSMS']
        self.assertNotEqual(old_color,new_color)

        
    
    def test_display_msms_two_molecules_two_displayed(self):
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.readMolecule('./Data/1bsr.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        self.mv.selectFromString('','','20-40', '',negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMS("1crn", surfName="E", perMol=1,display = 0)
        self.mv.computeMSMS("1bsr", surfName="T", perMol=1,display = 0)
        self.mv.displayMSMS(self.mv.getSelection(),negate = 0, only = 1, surfName = ('E',), log = 0, nbVert = 1)
        self.mv.displayMSMS(self.mv.getSelection(),negate = 0, only = 1, surfName = ('T',), log = 0, nbVert = 1)
        selatms = nodes.findType(Atom)
        gc1 = self.mv.Mols[1].geomContainer
        len_satms1 = len(gc.atoms['E'])+len(gc1.atoms['T'])
        self.assertEqual(len_satms1,len(selatms))

#tests invalid input  for display msms    
    
    def test_display_msms_invalid_input_nodes(self):
        self.mv.readMolecule('Data/1crn.pdb')
        command = self.mv.displayMSMS
        returnValue =command('abcd')
        self.assertEqual(returnValue,None)

    def test_display_msms_empty_input_nodes(self):
        self.mv.readMolecule('Data/1crn.pdb')
        command = self.mv.displayMSMS
        returnValue =command(' ')
        self.assertEqual(returnValue,None)    
    
    def test_display_msms_invalid_input_only(self):
        self.mv.readMolecule('Data/1crn.pdb')        
        command =self.mv.displayMSMS
        returnValue =command('1crn',only ='hai')
        self.assertEqual(returnValue,None)
        
    def test_display_msms_invalid_input_negate(self):
        self.mv.readMolecule('Data/1crn.pdb')        
        command =self.mv.displayMSMS
        returnValue =command('1crn',negate ='hai')
        self.assertEqual(returnValue,None)    
    
    def test_display_msms_invalid_input_surf_name(self):
        self.mv.readMolecule('Data/1crn.pdb')        
        command =self.mv.displayMSMS
        returnValue =command('1crn',surfName = ' ')
        self.assertEqual(returnValue,None)
        
    def test_display_msms_invalid_input_nb_vert(self):
        self.mv.readMolecule('Data/1crn.pdb')        
        command =self.mv.displayMSMS
        returnValue =command('1crn',nbVert = 'hai')
        self.assertEqual(returnValue,None)

#end invalid input for display msms


class saveMSMS(MSMSBaseTest):
    
    def test_save_msms_None(self):
        command = self.mv.saveMSMS
        returnValue = command(None,None,None)
        self.assertEqual(returnValue,None)

    def test_save_msms_non_existent(self):
        # Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        sName = '1crnMSMS'
        molName = self.mv.Mols[0].name
        # try to output a surface that doesn't exist yet.
        outputName = 'outMSMS'
        command = self.mv.saveMSMS
        returnValue = command(outputName, molName, sName)
        self.assertEqual(returnValue,None)
        
    def test_save_msms_os_path_exists(self):
        # Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        sName = '1crnMSMS'
        molName = self.mv.Mols[0].name
        outputName = 'outMSMS'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=True)
        self.mv.saveMSMS(outputName, molName, sName)
        self.assertEqual(os.path.exists('./outMSMS.face'),True)
        self.assertEqual(os.path.exists('./outMSMS.vert'),True)
        f = open("./outMSMS.face")
        faceslines = f.readlines()
        #self.assertEqual(faceslines[2] ,'  12078     327  1.50  3.00\n')
        self.assertEqual(faceslines[2] ,'  12084     327  1.50  3.00\n')
        #self.assertEqual(len(faceslines),12081)
        self.assertEqual(len(faceslines),12087)
        f.close()
        f2=open("./outMSMS.vert")
        vertlines = f2.readlines()
        #self.assertEqual(len(vertlines), 6044)
        self.assertEqual(len(vertlines), 6047)
        f2.close()

    def test_save_msms_non_existent_component(self):
        self.mv.readMolecule('Data/1crn.pdb')
        outputName2= 'outMSMS2'
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=True)
        self.mv.saveMSMS(outputName2, molName, 'test',component = 3)
        self.assertEqual(os.path.exists("./outMSMS2.face"),False)
        self.assertEqual(os.path.exists("./outMSMS2.vert"),False)
        
    def test_save_msms_save_without_header(self):
        self.mv.readMolecule('Data/1crn.pdb')
        outputName2= 'outMSMS2'
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=True)
        self.mv.saveMSMS(outputName2, molName, sName, withHeader=0, component=0)
        self.assertEqual(os.path.exists("./outMSMS2.face"),True)
        self.assertEqual(os.path.exists("./outMSMS2.vert"),True)
        f = open("./outMSMS2.face")
        fLines = f.readlines()
        #self.assertEqual(len(fLines),12078)
        self.assertEqual(len(fLines),12084)
        f.close()
        f = open("./outMSMS2.vert")
        vLines = f.readlines()
        #self.assertEqual(len(vLines),6041)
        self.assertEqual(len(vLines),6044)

#invalid input for saveMSMS

    def test_save_msms_invalid_input_nodes(self):
        """tests invalid molname for save msms
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        outputName2= 'outMSMS2'
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command(outputName2,sName,"abcd")
        self.assertEqual(returnValue,None)

    def test_save_msms_empty_input_nodes(self):
        """tests empty molname for savemsms
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        outputName2= 'outMSMS2'
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command(outputName2,sName," ")
        self.assertEqual(returnValue,None)

    def test_save_msms_invalid_input_file_name(self):
        """tests invalid file name for save msms
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command("abcd",sName,molName)
        self.assertEqual(returnValue,None)

    def test_save_msms_empty_input_file_name(self):
        """tests empty file name for save msms
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command(" ",sName,molName)
        self.assertEqual(returnValue,None)   

    def test_save_msms_empty_input_surf_name(self):
        """tests empty surf name for save msms
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        outputName2= 'outMSMS2'
        molName = self.mv.Mols[0].name
        command = self.mv.saveMSMS
        returnValue = command(outputName2," ",molName)
        self.assertEqual(returnValue,None)

    def test_save_msms_invalid_input_with_header(self):
        """tests save msms invalid input for with header ,
        default is 0 or 1
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        outputName2= 'outMSMS2'
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command(outputName2,sName,molName,withHeader = 'hai')
        self.assertEqual(returnValue,None)

    def test_save_msms_invalid_input_component(self):
        """tests save msms invalid input for component
        default componenet = 0
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        outputName2= 'outMSMS2'
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command(outputName2,sName,molName,component = 'hai')
        self.assertEqual(returnValue,None)   

    def test_save_msms_invalid_input_format(self):
        """tests save msms invalid input for format
        format should be selected from ['MS_TSES_ASCII', 
                        'MS_ASES_ASCII', 
                        'MS_TSES_ASCII_AVS',
                        'MS_ASES_ASCII_AVS'
                        ]
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        outputName2= 'outMSMS2'
        molName = self.mv.Mols[0].name
        sName = '1crnMSMS'
        command = self.mv.saveMSMS
        returnValue = command(outputName2,sName,molName,format = 'hai')
        self.assertEqual(returnValue,None)

#end invalid input for save msms

class UnDisplayMSMS(MSMSBaseTest):
    
    def test_undisplay_msms_empty_viewer(self):
        if self.mv.undisplayMSMS.flag & 1:    
            self.mv.undisplayMSMS(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.undipslayMSMS cannot be called with only self.mv.getSelection()")
    
    def test_undisplay_msms_invalid_nodes(self):
        self.mv.readMolecule("./Data/1crn.pdb")
        command = self.mv.undisplayMSMS
        returnValue = command("fdsfg")
        self.assertEqual(returnValue,None)
    
    def test_undisplay_msms_empty_nodes(self):
        self.mv.readMolecule("./Data/1crn.pdb")
        command = self.mv.undisplayMSMS
        returnValue = command(" ")
        self.assertEqual(returnValue,None)        
            
    def test_undisplay_msms_nodes(self):
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.computeMSMS('1crn', '1crnMSMS')
        self.mv.undisplayMSMS('1crn')
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['1crnMSMS'].vertexSet.vertices),0)
        
        
    def test_display_msms_two_molecules(self):
        
        self.mv.readMolecule('./Data/1bsr.pdb')
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[1]
        self.mv.computeMSMS('1crn', surfName='1crnMSMS')
        self.mv.computeMSMS('1bsr', surfName='1bsrMSMS')
        self.mv.undisplayMSMS(mol)
        self.assertEqual(len(self.mv.Mols[1].geomContainer.atoms['1crnMSMS'].vertexSet.vertices),0)
        self.assertEqual(len(self.mv.Mols[1].geomContainer.atoms['1crnMSMS'].faceSet),0)
        self.assertNotEqual(len(self.mv.Mols[0].geomContainer.geoms['1bsrMSMS'].vertexSet.vertices),0)



class ReadMSMS(MSMSBaseTest):

    def test_read_msms_no_header(self):
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.computeMSMS(self.mv.getSelection(), surfName='TESTMSMS')
        gc = self.mv.Mols[0].geomContainer
        vertLen = len(gc.geoms['TESTMSMS'].vertexSet.vertices.array)
        faceLen = len(gc.geoms['TESTMSMS'].faceSet.faces.array)
        # no header
        self.mv.saveMSMS('./Data/surf_noheader', '1crn', 'TESTMSMS',
                format='MS_TSES_ASCII', component=0,  withHeader=0, log=0)
        self.mv.deleteMol("1crn")
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.readMSMS('./Data/surf_noheader.vert',
                         './Data/surf_noheader.face',
                         molName='1crn', log=0)
        gc = self.mv.Mols[0].geomContainer
        self.assertEqual(gc.geoms.has_key('surf_noheader_msms') ,True)
        srfG = gc.geoms['surf_noheader_msms']
        self.assertEqual(len(srfG.vertexSet.vertices.array), vertLen)
        self.assertEqual(len(srfG.faceSet.faces.array),faceLen)
        os.system('rm -f ./Data/surf_noheader.vert ./Data/surf_noheader.face')
    
    def test_read_msms_header(self):
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.computeMSMS(self.mv.getSelection(), surfName='TESTMSMS')
        gc = self.mv.Mols[0].geomContainer
        vertLen = len(gc.geoms['TESTMSMS'].vertexSet.vertices.array)
        faceLen = len(gc.geoms['TESTMSMS'].faceSet.faces.array)
        # with header
        self.mv.saveMSMS('./Data/surf_header', '1crn', 'TESTMSMS',
                format='MS_TSES_ASCII', component=0,  withHeader=0, log=0)
        self.mv.deleteMol("1crn")
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.readMSMS('./Data/surf_header.vert', './Data/surf_header.face',
                molName='1crn', log=0)
        gc = self.mv.Mols[0].geomContainer
        self.assertEqual(gc.geoms.has_key('surf_header_msms'),True)
        srfG = gc.geoms['surf_header_msms']
        self.assertEqual(len(srfG.vertexSet.vertices.array),vertLen)
        self.assertEqual(len(srfG.faceSet.faces.array), faceLen)
        os.system('rm -f ./Data/surf_header.vert ./Data/surf_header.face')

class computeMSMSApprox(MSMSBaseTest):   
    
    def xtest_compute_msms_approx_on_empty_viewer(self):
        """tests compute msms on empty viewer
        """
        if self.mv.computeMSMSApprox.flag & 1:    
            self.mv.computeMSMSApprox(self.mv.getSelection(),None)
        else:
            raise ValueError("WARNING: self.mv.computeMSMSapprox cannot be called with only self.mv.getSelection()")
        

    def xtest_compute_msms_approx_first_molecular_surface(self):
        """tests compute msms approx for a first molecular surface for the entire
        molecule,but we don't display the msms 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        self.mv.computeMSMSApprox('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=0)
        self.assertEqual(gc.msms.has_key(sName),True)
        self.assertEqual(gc.msmsAtoms.has_key(sName),True)
        self.assertEqual(gc.atoms.has_key(sName),True)
        self.assertEqual(len(mol.geomContainer.atoms[sName]), 0)  #changed 2/23 display is 0!
        self.assertEqual(gc.geoms.has_key(sName),True)
        
    def xtest_compute_msms_approx_molecular_surface_with_a_subselection(self):
        """Compute msms for  a molecular surface with a subselection
            but for the entire molecule,display
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 1 through 10
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMSApprox(nodes, sName, perMol=1, density=3.0,
                   pRadius=1.5, display=1)
        self.assertEqual(gc.msmsAtoms.has_key(sName),True)
        self.assertEqual(len(mol.geomContainer.msmsAtoms[sName]),46)

    def xtest_compute_msms_approx_surface_with_a_subselection_atoms(self):
        """compute msms for a selection and checks selected atoms 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        # Select residue 1 through 10
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMSApprox(nodes, sName, perMol=1, density=3.0,
                   pRadius=1.5, display=1)
        self.assertEqual(gc.atoms.has_key(sName),True)
        at2 = gc.atoms[sName][:]
        at2.sort()
        self.assertEqual(len(at2), 46)  #2/23: added support for "display" 
                                        #parameter to computeMSMSapprox

    
    def xtest_compute_msms_approx_surface_default_arguments(self):
        """checks compute msms with default arguements
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        #sName = '1crnMSMS1'
        # Select residue 25 through 35
        self.mv.selectFromString('','','25-35', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        # surfName=None, perMol=1, density = 1.0, pRadius = 1.5 and display=1
        self.mv.computeMSMSApprox(nodes)        
        #sName = mol.name+'-MSMS'
        sName = 'MSMS-MOL'
        self.assertEqual(gc.msms.has_key(sName),True)
        
 
class IdentifyBuriedVertices(MSMSBaseTest):

    def test_identify_buried_vertices_sas_ses(self):
        """identify the portion of msms surface computed for chain A which is buried by chain B""" 
        self.mv.readMolecule('Data/hsg1.pdb')
        mol = self.mv.Mols[0]
        self.mv.computeMSMS("hsg1:A", perMol=False, density=1.0)
        Value =self.mv.identifyBuriedVertices("MSMS-MOL", mol, mol.chains[1])
        self.assertEqual(round(Value['sas'][0]), 2010.0)
        #self.assertEqual(round(Value['ses'][0]), 1559.0)
        self.assertEqual(round(Value['ses'][0]), 1557.0)

    def test_identify_buried_vertices_invalid_mol_name(self):
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = '1crnMSMS1'
        self.mv.selectFromString('','','25-35', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        self.mv.computeMSMS("1crn")
        self.assertRaises(AssertionError,self.mv.identifyBuriedVertices,"MSMS-MOL", "abcd",nodes)
          
    

class DisplayBuriedTriangles(MSMSBaseTest):
        
    def test_display_buried_triangles(self):
        """
        check that displayBuriedTriangles displays fewer
        triangles than displayMSMS when buriedVertices
        have been identified
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal','indMSMS')
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        old = gc.geoms[sName].getFaces()
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select('hsg1')
        self.mv.identifyBuriedVertices('indMSMS', 'ind_crystal', 'hsg1')
        self.mv.displayBuriedTriangles("ind_crystal", 'indMSMS')
        new = gc.geoms[sName].getFaces()
        print "old=", old, " and new=", new
        self.assertNotEqual(len(old), len(new))
        
        
    def test_subset_only_display_buried_triangles(self):
        """
        check that subset only displayBuriedTriangles displays fewer triangles 
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal','indMSMS', density=1.0)
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        old = gc.geoms[sName].getFaces()
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select('hsg1')
        self.mv.identifyBuriedVertices('indMSMS', 'ind_crystal', 'hsg1')
        self.mv.displayBuriedTriangles("ind_crystal:::1-10", 'indMSMS', only=1)
        new = gc.geoms[sName].getFaces()
        self.assertEqual(len(new), 278)
        
        
    def test_negate_subset_display_buried_triangles(self):
        """
        check that negate subset displayBuriedTriangles displays complement triangles 
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal','indMSMS', density=1.0)
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        old = gc.geoms[sName].getFaces()
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.select('hsg1')
        self.mv.identifyBuriedVertices('indMSMS', 'ind_crystal', 'hsg1')
        self.mv.displayBuriedTriangles("ind_crystal:::1-10", 'indMSMS', negate=1)
        new = gc.geoms[sName].getFaces()
        self.assertEqual(len(new), 152)
        

    def xtest_undo_display_buried_triangles(self):
        """
        check that displayBuriedTriangles is undoable
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal','indMSMS')
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        old = gc.geoms[sName].getFaces()
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.identifyBuriedVertices('indMSMS', 'ind_crystal', 'hsg1')
        self.mv.displayBuriedTriangles("ind_crystal", 'indMSMS')
        new = gc.geoms[sName].getFaces()
        self.assertNotEqual(len(old), len(new))
        self.mv.undo()
        restored = gc.geoms[sName].getFaces()
        self.assertEqual(len(old), len(restored))
        for x,y in zip(old, restored):
            self.assertTrue(numpy.alltrue(x==y))
        

    def xtest_undo_displayMSMS_after_buried_triangles(self):
        """
        check that displayMSMS after displayBuriedTriangles is undoable
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal',surfName=sName)
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        all = gc.geoms[sName].getFaces()
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.identifyBuriedVertices(sName, 'ind_crystal', 'hsg1')
        self.mv.displayBuriedTriangles("ind_crystal", sName)
        buried = gc.geoms[sName].getFaces()
        self.assertNotEqual(len(all), len(buried))
        #self.mv.select('ind_crystal')
        #test restoring complete msms display
        self.mv.displayMSMS('ind_crystal', surfName=sName)
        #self.mv.displayMSMS('ind_crystal', surfName='indMSMS')
        new = gc.geoms[sName].getFaces()
        self.assertEqual(len(all), len(new))
        for x,y in zip(all, new):
            self.assertTrue(numpy.alltrue(x==y))
        #test restoring buried display
        self.mv.undo()
        current = gc.geoms[sName].getFaces()
        self.assertEqual(len(current), len(buried))
        #for x,y in zip(current, buried):
        #    self.assertEqual(x,y)
       

    def xtest_colors_undo_displayMSMS_after_buried_triangles(self):
        """
        check that colors of displayMSMS after displayBuriedTriangles are correctly restored after multiple colorings and undos...
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal',surfName=sName)
        g = gc.geoms[sName]
        self.mv.browseCommands('colorCommands',package='Pmv')
        self.mv.colorByAtomType("ind_crystal",['indMSMS'])
        #self.mv.GUI.VIEWER.Redraw()
        #sleep(2)
        #save the colors for the surface, colored by atom type
        atom_type_colors = g.materials[GL.GL_FRONT].getState()
        at_diffuse = atom_type_colors['diffuse']
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.identifyBuriedVertices(sName, 'ind_crystal', 'hsg1')
        self.mv.assignBuriedAreas("indMSMS", 'ind_crystal')
        self.mv.displayBuriedTriangles("ind_crystal", sName)
        self.mv.colorByProperty(mol.allAtoms,['indMSMS'], 'buriedSES', mini=0.0, maxi=20, colormap='rgb256')
        #self.mv.GUI.VIEWER.Redraw()
        #sleep(2)
        prop_type_colors = g.materials[GL.GL_FRONT].getState()
        prop_diffuse = prop_type_colors['diffuse']
        #check that there are equal numbers of  colors 
        self.assertEqual(len(at_diffuse), len(prop_diffuse))
        #but check that all the colors are not the same
        ct = 0
        for i, j in zip(at_diffuse, prop_diffuse):
            if i==j: ct+=1
        self.assertNotEqual(len(at_diffuse), ct)

        #color by another coloring scheme
        self.mv.colorByMolecules("ind_crystal",['indMSMS'])
        #self.mv.GUI.VIEWER.Redraw()
        #sleep(2)
        mol_type_colors = g.materials[GL.GL_FRONT].getState()
        mol_diffuse = mol_type_colors['diffuse']

        #check that there are unequal numbers of  colors 
        #645 vs 1
        self.assertNotEqual(len(prop_diffuse), len(mol_diffuse))
        #undo1 back to prop colors and check
        self.mv.undo()
        #self.mv.GUI.VIEWER.Redraw()
        #sleep(2)
        undo1_type_colors = g.materials[GL.GL_FRONT].getState()
        undo1_diffuse = undo1_type_colors['diffuse']
        #check that there are equal numbers of  colors 
        self.assertEqual(len(prop_diffuse), len(undo1_diffuse))
        #and check that the restored colors are the same 
        ct = 0
        for i, j in zip(undo1_diffuse, prop_diffuse):
            if i==j: ct+=1
        self.assertEqual(len(prop_diffuse), ct)
        #undo and check again
        self.mv.undo()
        #self.mv.GUI.VIEWER.Redraw()
        #sleep(3)
        undo2_type_colors = g.materials[GL.GL_FRONT].getState()
        undo2_diffuse = undo2_type_colors['diffuse']
        #check that there are equal numbers of  colors 
        self.assertEqual(len(at_diffuse), len(undo2_diffuse))
        #but check that the restored colors are the same 
        ct = 0
        for i, j in zip(at_diffuse, undo2_diffuse):
            if i==j: ct+=1
        self.assertEqual(len(at_diffuse), ct)
       
        
    def test_undo_display_buried_after_buried_triangles(self):
        """
        check that displayBuriedTriangles after displayBuriedTriangles is undoable
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        sName = 'indMSMS'
        self.mv.computeMSMS('ind_crystal','indMSMS')
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        #get complete list of possible faces
        all = gc.geoms[sName].getFaces()
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.identifyBuriedVertices('indMSMS', 'ind_crystal', 'hsg1')
        self.mv.displayBuriedTriangles("ind_crystal", 'indMSMS')
        #get list of faces buried by hsg1
        buried = gc.geoms[sName].getFaces()
        self.assertNotEqual(len(all), len(buried))
        self.mv.identifyBuriedVertices('indMSMS', 'ind_crystal', 'hsg1:A')
        self.mv.displayBuriedTriangles("ind_crystal", 'indMSMS')
        #get list of faces buried by hsg1 chainA only
        buried_by_A = gc.geoms[sName].getFaces()
        self.assertNotEqual(len(buried), len(buried_by_A))
        

#################DisplayIntermolecularBuriedTriangles Tests ####################

class DisplayIntermolecularBuriedTriangles(MSMSBaseTest):
           
    def test_display_intermolecular_buried_triangles(self):
        """
        check displayIntermolecularBuriedTriangles, precomputed
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        sName1 = 'indMSMS'
        self.mv.computeMSMS('ind_crystal',sName1, density=1.0)
        indMSMS = self.mv.Mols[0].geomContainer.geoms[sName1]
        ind_all_faces = indMSMS.getFaces()
        #self.assertEqual(len(ind_all_faces), 1286)
        self.assertEqual(len(ind_all_faces), 1302)
        self.mv.readMolecule('Data/hsg1.pdb')
        self.mv.identifyBuriedVertices(sName1, 'ind_crystal', 'hsg1')
        self.mv.assignBuriedAreas("indMSMS", 'ind_crystal')
        sName2 = 'hsgMSMS'
        self.mv.computeMSMS('hsg1',sName2, density=1.0)
        hsgMSMS = self.mv.Mols[1].geomContainer.geoms[sName2]
        hsg_all_faces = hsgMSMS.getFaces()
        #self.assertEqual(len(hsg_all_faces), 20546)
        self.assertEqual(len(hsg_all_faces), 20840)
        self.mv.identifyBuriedVertices(sName2, 'hsg1', 'ind_crystal')
        self.mv.assignBuriedAreas("hsgMSMS", 'hsg1')
        self.mv.displayIntermolecularBuriedTriangles('hsg1', sName2, 'ind_crystal', sName1)
        #these numbers are hardwired
        ind_buried = indMSMS.getFaces()
        #self.assertEqual(len(ind_buried), 1218)
        self.assertEqual(len(ind_buried), 1234)
        hsg_buried = hsgMSMS.getFaces()
        #self.assertEqual(len(hsg_buried), 1715)
        self.assertEqual(len(hsg_buried), 1721) 

    
    def test_whole_process_display_intermolecular_buried_triangles(self):
        """
        check whole_process_displayIntermolecularBuriedTriangles, msms not precomputed
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        sName1 = 'indMSMS'
        self.mv.readMolecule('Data/hsg1.pdb')
        sName2 = 'hsgMSMS'
        self.mv.displayIntermolecularBuriedTriangles('hsg1', sName2, 'ind_crystal', sName1)
        #these numbers are hardwired
        indMSMS = self.mv.Mols[0].geomContainer.geoms[sName1]
        ind_buried = indMSMS.getFaces()
        #self.assertEqual(len(ind_buried), 1218)
        self.assertEqual(len(ind_buried),2849) #!!!
        hsgMSMS = self.mv.Mols[1].geomContainer.geoms[sName2]
        hsg_buried = hsgMSMS.getFaces()
        #self.assertEqual(len(hsg_buried), 1715)
        self.assertEqual(len(hsg_buried), 3809)# !!!
    
    def xtest_undo_whole_process_display_intermolecular_buried_triangles(self):
        """
        check undo_whole_process_displayIntermolecularBuriedTriangles, msms not precomputed
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        sName1 = 'indMSMS'
        self.mv.readMolecule('Data/hsg1.pdb')
        sName2 = 'hsgMSMS'
        self.mv.displayIntermolecularBuriedTriangles('hsg1', sName2, 'ind_crystal', sName1)
        #these numbers are hardwired
        indMSMS = self.mv.Mols[0].geomContainer.geoms[sName1]
        ind_buried = indMSMS.getFaces()
        #self.assertEqual(len(ind_buried), 1218)
        self.assertEqual(len(ind_buried),2849) #!!!
        hsgMSMS = self.mv.Mols[1].geomContainer.geoms[sName2]
        hsg_buried = hsgMSMS.getFaces()
        #self.assertEqual(len(hsg_buried), 1715)
        self.assertEqual(len(hsg_buried), 3809)
        self.mv.undo()
        #if self.mv.hasGui:
        #    self.mv.GUI.VIEWER.Redraw()
        ind_all_faces = indMSMS.getFaces()
        #print "len(ind_all_faces)=", len(ind_all_faces)
        #self.assertEqual(len(ind_all_faces), 1286)
        self.assertEqual(len(ind_all_faces), 3028)#!!!
        hsg_all_faces = hsgMSMS.getFaces()
        #print "len(hsg_all_faces)=", len(hsg_all_faces)
        #self.assertEqual(len(hsg_all_faces), 20546)
        self.assertEqual(len(hsg_all_faces), 47286)#!!!

    
    def XXXFIXTHISXXXtest_undo_whole_process_with_negate(self):
        """
        check undo_whole_process_with_negate, msms not precomputed
        """
        self.mv.readMolecule('Data/ind_crystal.pdb')
        sName1 = 'indMSMS'
        self.mv.readMolecule('Data/hsg1.pdb')
        sName2 = 'hsgMSMS'
        self.mv.displayIntermolecularBuriedTriangles('hsg1', sName2, 'ind_crystal', sName1, negate=1)
        #these numbers are hardwired
        indMSMS = self.mv.Mols[0].geomContainer.geoms[sName1]
        ind_exposed = indMSMS.getFaces()
        print "len(ind_exposed)=", len(ind_exposed)
        #self.assertEqual(len(ind_exposed), 68)
        hsgMSMS = self.mv.Mols[1].geomContainer.geoms[sName2]
        hsg_exposed = hsgMSMS.getFaces()
        print "len(hsg_exposed)=", len(hsg_exposed)
        #self.assertEqual(len(hsg_exposed), 18831)
        self.mv.undo()
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        ind_all_faces = indMSMS.getFaces()
        print "len(ind_all_faces)=", len(ind_all_faces)
        self.assertEqual(len(ind_all_faces), 1286)
        hsg_all_faces = hsgMSMS.getFaces()
        print "len(hsg_all_faces)=", len(hsg_all_faces)
        sleep(5)
        self.assertEqual(len(hsg_all_faces), 20546)


#################Log Tests ####################
from Pmv.Tests import compareArguments

class MSMSLogTests(MSMSBaseTest):

    def test_compute_msms_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        
        sName = '1crnMSMS1'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                            pRadius=1.5, display=0)
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            if hasattr(last_index, 'string'):
                last_index = last_index.string
            last_entry_index = str(float(last_index)-2.0)
            last_entry = split(tx.get(last_entry_index, 'end') , '\n')[0]
            self.assertTrue(compareArguments(last_entry, 'self.computeMSMS("1crn", hdensity=6.0, hdset=\'None\', redraw=0, density=3.0, pRadius=1.5, perMol=1, noHetatm=False, display=0, surfName=\'1crnMSMS1\')' ) )

    def test_compute_msms_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self =mv
        s = 'self.computeMSMS("1crn", log=0, density=3.0, pRadius=1.5, perMol=1, display=0, surfName=\'1crnMSMS1\')'
        exec(s)
        oldself.assertEqual(1,1)
    
    
    def test_display_msms_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        
        # displayMSMS before computing any msms surface with default values
        self.mv.displayMSMS('1crn')
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            if hasattr(last_index, 'string'):
                last_index = last_index.string
            last_entry_index = str(float(last_index)-2.0)
            last_entry = split(tx.get(last_entry_index, 'end') ,'\n')[0]
            self.assertTrue(compareArguments(last_entry, 'self.displayMSMS("1crn", negate=False, only=False, surfName=\'all\', log=0, nbVert=1)') )


    def test_display_msms_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crn.pdb')
        mol = self.mv.Mols[0]
        oldself=self
        self =mv
        s = 'self.displayMSMS("1crn", negate=False, only=False, surfName=\'all\', log=0, nbVert=1)'
        exec(s)
        oldself.assertEqual(1,1)


    def test_save_msms_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.readMolecule('Data/1crn.pdb')
        sName = '1crnMSMS'
        molName = self.mv.Mols[0].name
        outputName = 'outMSMS'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=True)
        self.mv.saveMSMS(outputName, molName, sName)
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            if hasattr(last_index, 'string'):
                last_index = last_index.string
            last_entry_index = str(float(last_index)-2.0)
            last_entry = split(tx.get(last_entry_index, 'end'), '\n')[0]
            self.assertTrue(compareArguments(last_entry, "self.saveMSMS('outMSMS', '1crn', '1crnMSMS', format='MS_TSES_ASCII', component=None, log=0, withHeader=True)") )

        

    def test_save_msms_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crn.pdb')
        sName = '1crnMSMS'
        molName = self.mv.Mols[0].name
        outputName = 'outMSMS'
        self.mv.computeMSMS('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=True)
        oldself=self
        self =mv
        s ="self.saveMSMS('outMSMS', '1crn', '1crnMSMS', format='MS_TSES_ASCII', component=None, log=0, withHeader=True)"
        exec(s)
        oldself.assertEqual(1,1)

        
    def test_undisplay_msms_log_checks_expected_log_string(self):
        """checks expected log string is written
        """   
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.computeMSMS('1crn', '1crnMSMS')
        self.mv.undisplayMSMS('1crn')
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            if hasattr(last_index, 'string'):
                last_index = last_index.string
            last_entry_index = str(float(last_index)-2.0)
            last_entry = split(tx.get(last_entry_index, 'end'), '\n')[0]
            self.assertTrue(compareArguments(last_entry, 'self.displayMSMS("1crn", negate=1, only=False, surfName=\'all\', log=0, nbVert=1)'))

        
        
    def test_undisplay_msms_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.computeMSMS('1crn', '1crnMSMS')
        self.mv.undisplayMSMS('1crn')
        oldself=self
        self =mv
        s = 'self.displayMSMS("1crn", negate=1, only=False, surfName=\'all\', log=0, nbVert=1)'
        exec(s)
        oldself.assertEqual(len(oldself.mv.Mols[0].geomContainer.atoms['1crnMSMS'].vertexSet.vertices),0)        


##    def test_read_msms_log_checks_expected_log_string(self):
##        """checks expected log string is written
##        """
##        self.mv.readMolecule("./Data/1crn.pdb")
##        self.mv.computeMSMS(self.mv.getSelection(), surfName='TESTMSMS')
##        self.mv.saveMSMS('./Data/surf_noheader', '1crn', 'TESTMSMS',
##                format='MS_TSES_ASCII', component=0,  withHeader=0, log=0)
##        self.mv.deleteMol("1crn")
##        self.mv.readMolecule("./Data/1crn.pdb")
##        self.mv.readMSMS('./Data/surf_noheader.vert', './Data/surf_noheader.face', molName='1crn', log=0)
#

    def xtest_compute_msms_approx_first_molecular_surface(self):
        """tests compute msms approx for a first molecular surface for the entire molecule,but we don't display the msms 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        sName = '1crnMSMS1'
        self.mv.computeMSMSApprox('1crn', surfName=sName, perMol=1, density=3.0,
                   pRadius=1.5, display=0)
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            if hasattr(last_index, 'string'):
                last_index = last_index.string
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')
            self.assertTrue(compareArguments(last_entry, 'self.computeMSMSApprox("1crn", nbSphPerRes=1, redraw=1, density=3.0, pRadius=1.5, perMol=1, display=0, surfName=\'1crnMSMS1\')') )


    def xtest_compute_msms_approx_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.readMolecule('Data/1crn.pdb')
        sName = '1crnMSMS1'
        oldself=self
        self =mv
        s = 'self.computeMSMSApprox("1crn", nbSphPerRes=1, log=0, density=3.0, pRadius=1.5, perMol=1, display=0, surfName=\'1crnMSMS1\')'
        exec(s)
        oldself.assertEqual(1,1)



if __name__ == '__main__':
    #unittest.main()
    test_cases = [
        'ComputeMSMS',
        'DisplayMSMS',
        'saveMSMS',
        #'UndisplayMSMS',
        'ReadMSMS',
        'computeMSMSApprox',
        'IdentifyBuriedVertices',
        'DisplayBuriedTriangles',
        'DisplayIntermolecularBuriedTriangles',
        'MSMSLogTests',
    ]
    unittest.main( argv=([__name__,] + test_cases) )
    #unittest.main( argv=([__name__,'-v'] + test_cases) )



        
