#
#
#Author : Sowjanya Karnati
#
#
# $Id: test_helpCommands.py,v 1.6 2010/09/07 22:12:11 sargis Exp $


import unittest
from string import split
import urllib2

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1
    
"""
This module implements a set of function to test the commands of the
traceCommands module
"""
class HelpBaseTest(unittest.TestCase):
    def setUp(self):
        from Pmv.moleculeViewer import MoleculeViewer
        self.mv = MoleculeViewer(customizer = './.empty', logMode = 'no', verbose=False,trapExceptions=False, withShell=0, gui=hasGUI)
        self.mv.browseCommands('fileCommands', package='Pmv')
        self.mv.browseCommands('helpCommands', package='Pmv')
        self.mv.browseCommands("helpCommands", commands = ("mailingListsCommand",))

    def tearDown(self):
        if self.mv.hasGui:
            self.mv.Exit(0)

class Help_MailingListTest(HelpBaseTest):


    def test_mailinglist_invalid_args_1(self):
        self.mv.browseCommands('helpCommands', package='Pmv') 
        
        c = self.mv.mailingListsCommand([],[])
        error ="ERROR: pack or page are not string type"
        #exec(c)
        #print "ok"
        self.assertEqual(c,error)
        
        

    def test_mailinglist_invalid_args_2(self):
        
        c = self.mv.mailingListsCommand("Pmv","abc")
        error1 = "ERROR: Invalid page name"
        self.assertEqual(c,error1)


    def test_mailinglist_invalid_args_3(self):
        
        c = self.mv.mailingListsCommand("abc","Login Page")
        error2 = "ERROR: Invalid pack name"
        self.assertEqual(c,error2)

    def test_web_page_exists_pmv_login_page(self):
        pmvloginurl = "http://mgldev.scripps.edu/mailman/listinfo/pmv"
        conn = urllib2.urlopen(pmvloginurl)
        r1 = conn.url
        self.assertEqual(r1,pmvloginurl)
    
    def test_web_page_exists_vision_login_page(self):
        visionloginurl = "http://mgldev.scripps.edu/mailman/listinfo/vision"
        conn = urllib2.urlopen(visionloginurl)
        r1 = conn.url
        self.assertEqual(r1,visionloginurl)
        
    def test_web_page_exists_pmv_archive_page(self):
        pmvarchiveurl = 'http://mgldev.scripps.edu/pipermail/pmv/'
        conn = urllib2.urlopen(pmvarchiveurl)
        r1 = conn.url
        self.assertEqual(r1,pmvarchiveurl)
        
    def test_web_page_exists_vision_archive_page(self):
        visionarchiveurl = 'http://mgldev.scripps.edu/pipermail/vision/'
        conn = urllib2.urlopen(visionarchiveurl)
        r1 = conn.url
        self.assertEqual(r1,visionarchiveurl)

    
    ################## Widget Tests #####################

    def test_mailingLists_widget_1(self):
        """tests selected pages"""
        if not self.mv.hasGui:
            return
        c = self.mv.mailingListsCommand
        form = c.showForm('Show MailingLists',modal =0,blocking = 0)

        cf = c.cmdForms['Show MailingLists']
        ebn = cf.descr.entryByName
        cmdW1 = ebn['pmvlist']['widget']
        cmdW1.select_clear(0,last=1)
        #cmdW1.selectItem(0)
        cmdW1.select_set(0)
        page = cmdW1.getcurselection()[0]
        self.assertEqual(page,'Login Page')
        cmdW1.select_clear(0,last=1)
        #cmdW1.selectItem(1)
        cmdW1.select_set(1)
        page = cmdW1.getcurselection()[0]
        self.assertEqual(page,'Archive Page')
        
        #vision Pages
        cmdW2 = ebn['visionlist']['widget']
        cmdW2.select_clear(0,last=1)
        #cmdW1.selectItem(0)
        cmdW2.select_set(0)
        page = cmdW2.getcurselection()[0]
        self.assertEqual(page,'Login Page')
        
        cmdW2.select_clear(0,last=1)
        #cmdW1.selectItem(1)
        cmdW2.select_set(1)
        page = cmdW2.getcurselection()[0]
        self.assertEqual(page,'Archive Page')
        form.withdraw()


if __name__ == '__main__':
    unittest.main()
        
