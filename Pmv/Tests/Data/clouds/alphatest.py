#!/usr/bin/python
from Tkinter import *
from PIL import Image, ImageTk

root = Tk()
canvas = Canvas(root, bg='blue')
canvas.pack()

im = Image.open('image.tif').convert("RGB")
imageString = ''.join([['\xff','\x00'][int(all([v > 245 for v in p]))] for p in list(im.getdata())])
im.putalpha(Image.fromstring("L", im.size, imageString) )

graphic = ImageTk.PhotoImage(image=im)
canvas.create_image(100, 100, image=graphic)
root.mainloop()
