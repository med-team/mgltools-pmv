import Image
import numpy
from DejaVu.Texture import Texture
from DejaVu.IndexedPolygons import IndexedPolygons
from opengltk.OpenGL.GL import *

im = Image.open('Data/clouds/image.tif').convert('RGB')
imageString = ''.join([['\xff','\x00'][int(all([v > 245 for v in p]))] for p in list(im.getdata())])
im.putalpha(Image.fromstring("L", im.size, imageString))

im2D = [ord(c) for c in list(im.tostring())]
im2D = numpy.array(im2D, 'B')
im2D.shape = im.size+(4,)
im2D2 = numpy.array(im2D[:1024, :1024])

t = Texture(enable=1, image=im2D2, format=GL_RGBA)

#t.Set(genModS = GL_OBJECT_LINEAR, genModT = GL_OBJECT_LINEAR)
#t.Set(wrapS = GL_CLAMP, wrapT = GL_CLAMP)
#t.Set(envMode = GL_DECAL)

# Set up square...
coords = [[0.0, 0.0, 0.0], [10.0, 0.0, 0.0], [10.0, 10.0, 0.0], [0.0, 10.0, 0.0]]
indices = [[0,1,2,3]]
materials = ( (1,1,1,1),(1,1,1,1),(1,1,1,1),(1,1,1,1) )
texturecoords = [[0,0],[1,0],[1,1],[0,1]]

text = IndexedPolygons('Image')

text.Set(texture = t, textureCoords = texturecoords, vertices=coords,faces=indices , materials = materials, inheritMaterial=0, transparent=1)
text.Set(shading = 'smooth', frontPolyMode='fill', backPolyMode='fill', culling = 0)

self.setbackgroundcolor([1, 0, 0,], log=0)
self.GUI.VIEWER.AddObject(text)

#disable depthcueing
self.GUI.VIEWER.currentCamera.fog.Set(enabled=False)
