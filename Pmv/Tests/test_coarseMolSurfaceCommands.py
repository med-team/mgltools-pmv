
import unittest,sys,os,time
mv = None
surfName = "1crnCS"
ct = 0
totalCt = 11
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class coarseMolSurfaceTest(unittest.TestCase):
    """Base class for coarseMolSurface unittest"""
    def startViewer(self):
        """start Viewer"""

        global mv
        #if mv is None:
        print "start viewer"
        from Tkinter import Tk
        #root = Tk()    
        #root.withdraw()
        print "hasGUI:", hasGUI
        from Pmv.moleculeViewer import MoleculeViewer
        mv = MoleculeViewer(customizer='./.empty', logMode='no', #master=root,
                            verbose=False, 
                            withShell=0, gui=hasGUI,
                            trapExceptions=False)
        self.mv = mv
        
        mv.browseCommands("visionCommands", commands=('vision',), topCommand=0, package='Pmv')
        mv.browseCommands('fileCommands', commands=['readMolecule',],package= 'Pmv')
        mv.browseCommands('deleteCommands',commands =['deleteMol',], package='Pmv')
        mv.browseCommands("bondsCommands", commands=["buildBondsByDistance",],package= "Pmv")
        mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
        mv.browseCommands("interactiveCommands", package='Pmv')
        # Don't want to trap exceptions and errors... the user pref is set to 1 by
        # default
        
        mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        mv.browseCommands('msmsCommands', package='Pmv')
        mv.browseCommands('colorCommands',package='Pmv')
        mv.browseCommands('displayCommands',
                          commands=['displaySticksAndBalls',
                                    'undisplaySticksAndBalls',
                                    'displayCPK', 'undisplayCPK',
                                    'displayLines','undisplayLines',
                                    'displayBackboneTrace',
                                    'undisplayBackboneTrace',
                                    'DisplayBoundGeom'
                            ], package='Pmv')
        mv.browseCommands("selectionCommands",commands= ["clearSelection",
                                                         "selectFromString"],
                          package="Pmv")
        import Vision
        mv.browseCommands('coarseMolSurfaceCommands', topCommand=0, package='Pmv')
        mv.readMolecule('Data/1crn.pdb')
        print "self.mv:" , self.mv
        

    def setUp(self):
        """set-up"""
        global mv
        if mv is None:
            self.startViewer()
        else:
           self.mv = mv 

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None
            

    def test_01(self):
        """compute a coarse surface for a molecule using default parameters(resolution, isovalue, gridSize)"""
        nodes = self.mv.getSelection()
        cmd = self.mv.coarseMolSurface
        cmd(nodes = nodes, surfName = surfName, immediate = False)
        self.assertEqual(cmd.surfName, surfName)
        self.assertEqual( cmd.resolution_type , "medium smooth")
        self.assertEqual( cmd.isovalue_type, "fast approximation")
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_01, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 1764)
        self.assertEqual( len(surf.getFaces()), 3524)

    def test_02(self):
        """recompute the surface with a different resolution"""
        nodes = self.mv.getSelection()
        cmd = self.mv.coarseMolSurface
        cmd(nodes = nodes, surfName = surfName, resolution = -0.5,immediate = False )
        self.assertEqual( cmd.isovalue_type, 'fast approximation')
        self.assertEqual( cmd.surf_resolution, -0.5)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_02, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 2220)
        self.assertEqual( len(surf.getFaces()), 4436)

    def test_03(self):
        """recompute the surface with different isovalue """
        cmd = self.mv.coarseMolSurface
        nodes = self.mv.getSelection()
        cmd(nodes = nodes, surfName = surfName, isovalue = 2.0, resolution = -0.5, immediate = False)
        self.assertEqual( cmd.isovalue, 2.0)
        self.assertEqual( cmd.isovalue_type, 'custom')
        self.assertEqual( cmd.surf_resolution, -0.5)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_03, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 2652)
        self.assertEqual( len(surf.getFaces()), 5300)

    def test_04(self):
        "build the surface for a given set of atoms, compute precise isovalue"
        
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        cmd = self.mv.coarseMolSurface
        cmd(nodes= nodes, surfName = surfName, isovalue='precise value', resolution =-0.5,
            immediate=False, perMol=False)
        self.assertEqual(cmd.surf_resolution, -0.5)
        self.assertEqual( cmd.isovalue_type, 'precise value')
        print "in test_4 presize isovalue =", cmd.isovalue
        d = cmd.isovalue[0] - 3.67
        assert (d <= 0.01 and d >= 0 )
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 1738)
        self.assertEqual( len(surf.getFaces()), 3472)

    def test_05(self):
        """tests setting a resolution value that is out of range for computing
        fast approximated isovalue."""
        
        self.mv.clearSelection(log=0)
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        cmd = self.mv.coarseMolSurface
        cmd(nodes= nodes, surfName = surfName, isovalue="fast approximation",
              resolution =-0.5, immediate=False, perMol=False)
        cmd(resolution=-3.5)

        self.assertEqual( cmd.surf_resolution, -3.5)
        self.assertEqual( cmd.isovalue_type, 'custom')
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_05, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 3302)
        self.assertEqual( len(surf.getFaces()), 6612)


    def test_06(self):
        """tests computing the molecule surface (perMole = True, bindGeom = False)
        with a selected set of atoms"""
        self.mv.clearSelection(log=0)
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        from MolKit.molecule import Atom
        atoms = nodes.findType(Atom)
        self.assertEqual(len(atoms), 69)
        cmd = self.mv.coarseMolSurface
        cmd(nodes= nodes, surfName = surfName, isovalue="fast approximation",
            resolution =-0.5, immediate=False, perMol=True, bindGeom = False)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_06, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 1618)
        self.assertEqual( len(surf.getFaces()), 3232)


    def test_07(self):
        """tests computing the suraface with binding the geometry to the molecule"""
        
        self.mv.clearSelection(log=0)
        cmd = self.mv.coarseMolSurface
        cmd(nodes="1crn",  surfName = surfName, gridSize=32, bindGeom=True,
            isovalue='fast approximation', immediate=False, perMol=True,
            resolution=-0.3, log=0)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_07, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 1764)
        self.assertEqual( len(surf.getFaces()), 3524)
        self.assertEqual(hasattr(surf, "mol"), True)
        cmd = self.mv.bindGeomToMolecularFragment
        #print cmd.data.keys()
        if self.mv.hasGui: 
            self.assertEqual(cmd.data.has_key('root|1crn|1crnCS'), True)
        else:
            self.assertEqual(cmd.data.has_key('1crn|1crnCS'), True)

    def test_08(self):
        """tests computing the molecule surface (perMole = True, bindGeom = True),
        displaying  a selected set of atoms"""
        self.mv.selectFromString('','','1-10', '', negate=False,silent=True)
        nodes = self.mv.getSelection()
        from MolKit.molecule import Atom
        atoms = nodes.findType(Atom)
        self.assertEqual(len(atoms), 69)
        cmd = self.mv.coarseMolSurface
        cmd(nodes= nodes, surfName = surfName, isovalue="fast approximation",
            resolution =-0.5, immediate=False, perMol=True, bindGeom=True)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_08, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 1618)
        self.assertEqual( len(surf.getFaces()),  3232)
        
        
    def test_09(self):
        """tests computing  the surface with bindGeom turned off"""
        cmd = self.mv.coarseMolSurface
        cmd(nodes="1crn", surfName="S1", gridSize=32, bindGeom=False,
            isovalue='fast approximation', immediate=False,
            perMol=True, resolution=-0.3, log=0)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "test_09, verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 1764)
        self.assertEqual( len(surf.getFaces()), 3524)
        self.assertEqual(hasattr(surf, "mol"), False)
        cmd = self.mv.bindGeomToMolecularFragment
        if self.mv.hasGui:
            #print "cmd.data.keys():", cmd.data.keys(), surf.fullName
            self.assertFalse(cmd.data.has_key(surf.fullName))
            #self.assertEqual(len(cmd.data.keys()), 0)

    def test_10(self):
        """tests setting a new grid size """
        
        self.mv.clearSelection()
        cmd = self.mv.coarseMolSurface
        cmd( surfName = surfName, gridSize=40, bindGeom=False, isovalue='fast approximation',
            immediate=False, padding=0.0, perMol=True, nodes="1crn", resolution=-0.3, log=0)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName(surfName)
        surf = cmd.surfaces[0]
        print "verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        
        self.assertEqual( len(surf.getVertices()), 2800)
        self.assertEqual( len(surf.getFaces()), 5596)

    def test_11(self):
        """tests setting of padding option"""
        self.mv.clearSelection()
        cmd = self.mv.coarseMolSurface
        cmd(bindGeom=False, gridSize=32, surfName='s1', isovalue=0.1, immediate=False, padding=0.0, perMol=True, nodes="1crn", resolution=-0.1, log=0)
        #surf = self.mv.GUI.VIEWER.GUI.objectByName('s1')
        surf = cmd.surfaces[0]
        #print "verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        self.assertEqual( len(surf.getVertices()), 4151)
        self.assertEqual( len(surf.getFaces()), 8230)
        #set padding
        self.mv.coarseMolSurface(bindGeom=False, gridSize=32, surfName='s1', isovalue=0.1, immediate=False, padding=2.0, perMol=True, nodes="1crn", resolution=-0.1, log=0)
        #print "verts: " , len(surf.getVertices()),  "faces: ", len(surf.getFaces())
        #surf = self.mv.GUI.VIEWER.GUI.objectByName('s1')
        surf = cmd.surfaces[0]
        self.assertEqual( len(surf.getVertices()), 3666)
        self.assertEqual( len(surf.getFaces()), 7328)
        

if __name__ == '__main__':
    unittest.main()
        
