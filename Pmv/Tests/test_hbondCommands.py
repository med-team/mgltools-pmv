#
#
#  $Id: test_hbondCommands.py,v 1.26.2.1 2016/02/11 21:29:44 annao Exp $
#
#
import unittest, glob, os, Pmv.Grid, string,sys
import time
from string import split,strip,find
from MolKit.molecule import Atom
from MolKit.protein import Residue
from DejaVu.Shapes import Circle2D

mv = None
ct = 0
totalCt = 112
klass = None
d = {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class hbond_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(trapExceptions=False, withShell=0, customizer='./.empty' ,
                                gui=hasGUI)
            mv.browseCommands('fileCommands', commands=['readMolecule',],package= 'Pmv')
            mv.loadModule("bondsCommands")
            mv.loadModule("colorCommands")
            mv.loadModule("editCommands")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines',
                                       'colorByAtomType'],
                                      topCommand=0)

            mv.browseCommands('deleteCommands',commands =['deleteMol',], package='Pmv')
            if hasGUI:
                if ct > 0:
                    # reload hbonds Command when we start the Viewer again:
                    from Pmv import hbondCommands
                    reload(hbondCommands)
                
                mv.loadModule('hbondCommands', 'Pmv')
            else:
                mv.browseCommands('hbondCommands', commands=['buildHBonds','addHBond','getHBDonors',
                                                            'getHBAcceptors', 'getHBondEnergies',
                                                            'removeHBond', 'writeHBondAssembly',
                                                             'writeIntermolHBonds', 'addHBondHs',
                                                             'readIntermolHBonds'],
                                  package='Pmv')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            #need access to error messages
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
            
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
       

    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt, mv

        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)

        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
                del self.mv
            mv = None
        #mol=mv.readMolecule('hsg1.pdb')[0]
      


class hbond1_hbondTests(hbond_BaseTests):
    
#buildHBondsGC
    def test_hbonds_build_hbondsGC(self):
        """checks build hbondsGC built hbnds via geometry
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.buildHBondsGC
        ats = self.mv.allAtoms
        c(ats,ats,d)
        n=self.mv.showHBonds
        n(ats)
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        self.assertEqual(len(h)/3,len(n.lines.vertexSet)/2)
        n.ifd.form.root.withdraw()
        n.dismiss_cb()
        

#buildHBonds
    def test_hbonds_build_hbonds(self):
        """checks build hbonds
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.buildHBonds
        ats = self.mv.allAtoms
        c(ats,ats,d)
        if self.mv.hasGui:
            n=self.mv.showHBonds
            n(ats)
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        if self.mv.hasGui:
            self.assertEqual(len(h)/3,len(n.lines.vertexSet)/2)
            n.ifd.form.root.withdraw()
            n.dismiss_cb()
        else:
            self.assertEqual(len(h) > 0, True)
#end buildHBonds


#showHBDA
    def test_hbonds_showHBDA_widget1(self):
        """checks show HBDA widget1
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats = self.mv.allAtoms
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        self.assertEqual(c.form2.root.winfo_ismapped(),1)
        c.cancel_cb()
        
         
    def test_hbonds_showHBDA_sp2D_off_visiblity(self):
        """checks showhbda sp2D off
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.ifd2.entryByName['donor2']['widget'].deselect()
        c.ifd2.entryByName['donor2']['wcfg']['command']()
        self.assertEqual(c.hbdonor2.visible,0)
        c.cancel_cb()


    def test_hbonds_showHBDA_sp3D_off_visiblity(self):
        """checks showhbda sp3D off
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.ifd2.entryByName['donor3']['widget'].deselect()
        c.ifd2.entryByName['donor3']['wcfg']['command']()
        self.assertEqual(c.hbdonor3.visible,0)
        c.cancel_cb()

     
    def test_hbonds_showHBDA_sp2A_off_visiblity(self):
        """checks showhbda  sp2A off
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.ifd2.entryByName['accept2']['widget'].deselect()
        c.ifd2.entryByName['accept2']['wcfg']['command']()
        self.assertEqual(c.hbacceptor2.visible,0)
        c.cancel_cb()


    def test_hbonds_showHBDA_sp3A_off_visiblity(self):
        """checks showhbda sp3A off
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.ifd2.entryByName['accept3']['widget'].deselect()
        c.ifd2.entryByName['accept3']['wcfg']['command']()
        self.assertEqual(c.hbacceptor3.visible,0)
        c.cancel_cb()


    def test_hbonds_showHBDA_donors_acceptors_number(self):
        """checks number of donors,acceptors vs geometries
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        self.assertEqual(len(c.hbacceptor2.vertexSet.vertices.array),54)
        self.assertEqual(len(c.hbacceptor3.vertexSet.vertices.array),16)
        c.cancel_cb()
            

    def test_hbonds_showHBDA__sp2A_visible(self):
        """geom of hbacceptor2 visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.select_cb('sp2A','sp2D','sp3A','sp3D')
        self.assertEqual(c.hbacceptor2.visible, 1)
        c.cancel_cb()

    
    def test_hbonds_showHBDA__sp3A_visible(self):
        """geom of sp3A visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.select_cb('sp2A','sp2D','sp3A','sp3D')
        self.assertEqual(c.hbacceptor3.visible,1)
        c.cancel_cb()


    def test_hbonds_showHBDA__sp2D_visible(self):
        """geom of sp2D visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.select_cb('sp2A','sp2D','sp3A','sp3D')
        self.assertEqual(c.hbdonor2.visible,1)
        c.cancel_cb()


    def test_hbonds_showHBDA__sp3D_visible(self):
        """geom of sp3D visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        c.select_cb('sp2A','sp2D','sp3A','sp3D')
        self.assertEqual(c.hbdonor3.visible,1)
        c.cancel_cb()
#end showHBDA
    

#getHBDonors
    def test_hbonds_get_hbondDonors_returnvalues(self):
        """check return values for get_hbondDonors
        """
        self.mv.readMolecule("Data/1crn_hs.pdb")
        sp2Ats, sp3Ats = self.mv.getHBDonors("1crn_hs")
        #49, 17 for this molecule
        totalDonors = len(sp2Ats) + len(sp3Ats)
        self.assertEqual(totalDonors, 66)


    def test_hbonds_get_hbondDonors_invalid_input(self):
        """get hbondsdonors invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.getHBDonors
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=self.mv.getHBDonors("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_hbonds_get_hbondDonors_empty_input(self):
        """ get hbondsdonors empty input        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.getHBDonors
        #self.assertRaises(AssertionError, c, " ")
        returnvalue=self.mv.getHBDonors(" ")
        self.assertEqual(returnvalue,"ERROR")
#end getHBDonors


#getHBAcceptors
    def test_hbonds_get_hbondAcceptors_returnvalues(self):
        """check return values for get_hbondAcceptors
        """
        self.mv.readMolecule("Data/1crn_hs.pdb")
        sp2Ats, sp3Ats = self.mv.getHBAcceptors("1crn_hs")
        #54, 16 for this molecule
        totalAcceptors = len(sp2Ats) + len(sp3Ats)
        self.assertEqual(totalAcceptors, 70)


    def test_hbonds_get_hbondsAcceptors_invalid_input(self):
        """get hbondAcceptors invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.getHBAcceptors
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=self.mv.getHBAcceptors("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_hbonds_get_hbondsAcceptors_empty_input(self):
        """get hbondAcceptors empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.getHBAcceptors
        #self.assertRaises(AssertionError, c, " ")
        returnvalue=self.mv.getHBAcceptors(" ")
        self.assertEqual(returnvalue,"ERROR")
#end getHBAcceptors


#getHBondEnergies
    def test_hbonds_get_hbondsEnergies_invalid_input(self):
        """get hbondEnergies invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.getHBondEnergies
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=self.mv.getHBondEnergies("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_hbonds_get_hbondsEnergies_empty_input(self):
        """get hbondAcceptors empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.getHBondEnergies
        #self.assertRaises(AssertionError, c, "    ")
        returnvalue=self.mv.getHBondEnergies(" ")
        self.assertEqual(returnvalue,"ERROR")


    def test_hbonds_get_hbondEnergies_energy_value(self):
        """check value of hbond energy is correct
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        if self.mv.hasGui: 
            self.mv.buildHBondsGC(ats,ats,d)
        else:
            self.mv.buildHBonds(ats,ats,d)
        energy_value = self.mv.getHBondEnergies("small_1crn_hs")
        #NB: energy  of first hbond is is -5.341
        self.assertEqual(energy_value[0], -5.341)
#end getHBondEnergies

class hbond2_hbondTests(hbond_BaseTests):
#showHBonds
    def test_showHBonds_widget(self):
        """showHBonds widget check 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        self.assertEqual(c.ifd.form.root.withdraw(),'') 
        c.dismiss_cb()

        
    def test_showHBonds_invalid_input(self):
        """showHBonds invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBonds
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=self.mv.showHBonds("hfdh")
        self.assertEqual(returnvalue,"ERROR")
                

    def test_showHBonds_empty_input(self):
        """showHBonds empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBonds
        #self.assertRaises(AssertionError, c, " ")
        returnvalue=self.mv.showHBonds(" ")
        self.assertEqual(returnvalue,"ERROR")
        

    def test_showHBonds_linevertices(self):
        """showHBonds vertices =hbond atoms vertices
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        self.assertEqual(round(c.lineVerts[0][0]),round(ats[-1]._coords[0][0]))
        self.assertEqual(round(c.lineVerts[0][1]),round(ats[-1]._coords[0][1]))
        self.assertEqual(round(c.lineVerts[0][2]),round(ats[-1]._coords[0][2]))
        self.assertEqual(round(c.lineVerts[1][0]),round(ats[2]._coords[0][0]))
        self.assertEqual(round(c.lineVerts[1][1]),round(ats[2]._coords[0][1]))
        self.assertEqual(round(c.lineVerts[1][2]),round(ats[2]._coords[0][2]))
        c.ifd.form.root.withdraw()
        c.dismiss_cb()

    
    def test_showHBonds_energy(self):
        """show HBonds energy=mv.getHBondEnergies(ats) 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        c = self.mv.showHBonds
        self.mv.buildHBondsGC(ats,ats,d)
        c.hasEnergies = 0
        c.dispEnergy.set(1)
        c(ats)
        x=c.ifd.entryByName['showEnBut']
        x['widget'].select()
        c.dispEnergy.set(1)
        c.hasEnergies = 0
        x['wcfg']['command']()
        c.ifd.entryByName['selAllBut']['wcfg']['command']()        
        self.assertEqual(float(c.e_labs[0]),self.mv.getHBondEnergies(ats)[0])
        c.ifd.form.root.withdraw()
        c.dismiss_cb()
    

    def xtest_showHBonds_distance(self):
        """verifies show HBonds hbond distance =mv.measureDistanceGC
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        self.assertEqual(float(c.dlabs[0]),self.mv.measureDistanceGC("small_1crn_hs: :PRO41:O;small_1crn_hs: :TYR44:HN"))
        c.ifd.form.root.withdraw()
        c.dismiss_cb()


    def test_showHBonds_selecting_nodes_widget(self):
        """verifies show HBonds selected nodes in widget 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        x=c.ifd.entryByName['atsLC']['widget']
        x.lb.selection_set('0')
        self.assertEqual(x.lb.selection_includes('0'),True)
        c.ifd.form.root.withdraw()
        c.dismiss_cb()
    

    def test_showHBonds_dist_on(self):
        """checks show HBonds show dist on
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        x=c.ifd.entryByName['showDistBut']['widget']
        x.select()
        self.assertEqual(c.hasDist,1)
        c.ifd.form.root.withdraw()
        c.dismiss_cb()

        
    def test_showHBonds_energies_on(self):
        """checks show HBonds show Energy on 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.showHBonds
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c.dispEnergy.set(1)
        c.hasEnergies = 0
        c(ats)
        x=c.ifd.entryByName['showEnBut']
        x['widget'].select()
        x['wcfg']['command']() 
        c.ifd.entryByName['selAllBut']['wcfg']['command']()  
        self.assertEqual(c.hasEnergies,1)
        c.ifd.form.root.withdraw()
        c.dismiss_cb()

        
    def test_showHBonds_theta_phi_on(self):
        """checks show HBonds show angles on 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        c = self.mv.showHBonds
        self.mv.buildHBondsGC(ats,ats,d)
        c.hasAngles = 0
        c.dispTheta.set(1)
        c(ats)
        x=c.ifd.entryByName['showThetaBut']
        x['widget'].select()
        x['wcfg']['command']()
        c.ifd.entryByName['selAllBut']['wcfg']['command']() 
        self.assertEqual(c.hasAngles,1)
        c.ifd.form.root.withdraw()
        c.dismiss_cb()
        

    def test_showHBonds_lines_visible(self):
        """checks show HBonds lines visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats) 
        self.assertEqual(c.lines.visible,1)
        c.ifd.form.root.withdraw()
        c.dismiss_cb()
        

    def test_showHBonds_atoms_hasattr(self):
        """checks show HBonds atoms has attr hbonds
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        self.assertEqual(hasattr(ats[-1],"hbonds"),True)
        self.assertEqual(hasattr(ats[2],"hbonds"),True)
        c.ifd.form.root.withdraw()
        c.dismiss_cb()
#end showHBonds


class hbond3_hbondTests(hbond_BaseTests):
#hbondsAsSpheres
    def test_hbonds_as_spheres_widget(self):
        """hbonds as spheres widget check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        c = self.mv.hbondsAsSpheres
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        self.assertEqual(c.ifd.form.root.withdraw(),'') 
    
    
    def test_hbonds_as_spheres_radii(self):
        """hbonds as spheres radii check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        c = self.mv.hbondsAsSpheres
        c.ifd.entryByName['radii']['widget'].set(0.2)
        self.assertEqual(c.radii,c.ifd.entryByName['radii']['widget'].get())
        

    def test_hbonds_as_spheres_spacing(self):
        """hbonds as spheres spacing  check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        c = self.mv.hbondsAsSpheres
        c.ifd.entryByName['spacing']['widget'].set(0.375)
        self.assertEqual(float(c.spacing),c.ifd.entryByName['spacing']['widget'].get())


    def test_hbonds_as_spheres_quality(self):
        """hbonds as spheres quality  check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        c = self.mv.hbondsAsSpheres
        c.ifd.entryByName['quality']['widget'].set(3)
        self.mv.GUI.ROOT.update()
        self.assertEqual(c.quality, c.ifd.entryByName['quality']['widget'].get())


    def test_hbonds_as_spheres_visible(self):
        """hbonds as spheres are  visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        c = self.mv.hbondsAsSpheres
        self.assertEqual(c.spheres.visible ,1)
    

    def test_hbonds_hbonds_as_spheres_nodes(self):
        """hbonds as spheres valid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres("small_1crn_hs")
        self.assertEqual(self.mv.Mols[0].name,"small_1crn_hs")


    def test_hbonds_hbonds_as_spheres_invalid_input(self):
        """hbonds as spheres invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.hbondsAsSpheres
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=c("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_hbonds_hbonds_as_spheres_empty_input(self):
        """hbonds as spheres empty input
        
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.hbondsAsSpheres        
        #self.assertRaises(AssertionError, c, "    ")
        returnvalue=c(" ")
        self.assertEqual(returnvalue,"ERROR")
#end hbondsAsSpheres


#hbondsAsCylinders
    def test_hbonds_as_cylinders_radii(self):
        """hbonds as cylinders radii check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsCylinders(ats)
        c = self.mv.hbondsAsCylinders
        c.ifd.entryByName['radii']['widget'].set(0.20)
        self.assertEqual(c.radii,c.ifd.entryByName['radii']['widget'].get())
        

    def test_hbonds_as_cylinders_length(self):
        """hbonds as cylinders length check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsCylinders(ats)
        c = self.mv.hbondsAsCylinders
        c.ifd.entryByName['length']['widget'].set(1.00)
        self.assertEqual(c.length,c.ifd.entryByName['length']['widget'].get())

    
    def test_hbonds_as_cylinders_visible(self):
        """hbonds as cylinders are visible
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsCylinders(ats)
        c = self.mv.hbondsAsCylinders
        self.assertEqual(c.cylinders.visible ,1)


    def test_hbonds_as_cylinders_widget(self):
        """hbonds as cylinders widget check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsCylinders(ats)
        c = self.mv.hbondsAsCylinders
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        self.assertEqual(c.ifd.form.root.withdraw(),'') 


    def test_hbonds_hbonds_as_cylinders_nodes(self):
        """hbonds as cylinders valid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.hbondsAsCylinders("small_1crn_hs")
        self.assertEqual(self.mv.Mols[0].name,"small_1crn_hs")


    def test_hbonds_hbonds_as_cylinders_invalid_input(self):
        """hbonds as cylinders invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.hbondsAsCylinders
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=c("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_hbonds_hbonds_as_cylinders_empty_input(self):
        """hbonds as cylinders empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.hbondsAsCylinders        
        #self.assertRaises(AssertionError, c, "    ")
        returnvalue=c(" ")
        self.assertEqual(returnvalue,"ERROR")
#end hbondsAsCylinders
        

#extrudeHBonds
    def test_extrude_hbonds_nodes(self):
        """extrude hbonds  valid input 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats = self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.extrudeHBonds("small_1crn_hs")
        self.assertEqual(self.mv.Mols[0].name,"small_1crn_hs")


    def test_extrude_hbonds_invalid_input(self):
        """extrude hbonds invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats = self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.extrudeHBonds
        #self.assertRaises(AssertionError, c, "hfdh ")
        returnvalue=c("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_extrude_hbonds_empty_input(self):
        """extrude hbonds  empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats = self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.extrudeHBonds        
        #self.assertRaises(AssertionError, c, " ")
        returnvalue=c(" ")
        self.assertEqual(returnvalue,"ERROR")


    def test_extrude_hbonds_widget(self):
        """extrude hbonds widget check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.extrudeHBonds 
        c(ats)
        self.assertEqual(c.form.root.winfo_ismapped(),1)
        self.assertEqual(c.form.root.withdraw(),'') 


    def test_extrude_hbonds_geometry(self):
        """extrude hbonds widget check
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.extrudeHBonds 
        c(ats)
        self.assertEqual(len(c.geoms), 1)
#end extrudeHBonds


#addHBondGC
    def test_add_hbonds_valid_input(self):
        """add hbondsGC valid input
        """
        
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        orig_hbond_ats = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        self.assertEqual(len(orig_hbond_ats), 0)
        if self.mv.hasGui: 
            self.mv.addHBondGC(self.mv.allAtoms)
        else:
            self.mv.addHBond("small_1crn_hs: :PRO41:CA", "small_1crn_hs: :PRO41:C")
        new_hbond_ats = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        self.assertEqual(len(new_hbond_ats)>len(orig_hbond_ats), True)
        self.assertEqual(self.mv.Mols[0].name,"small_1crn_hs")


    def test_add_hbondsGC_invalid_input(self):
        """add hbondsGC invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.addHBondGC
        #self.assertRaises(AssertionError, c, "hfdh")
        returnvalue=c("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_add_hbondsGC_empty_input(self):
        """add hbondsGC  empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.addHBondGC        
        self.assertRaises(ValueError, c, ("  ",))
        #returnvalue=c(" ")
        #self.assertEqual(returnvalue,"ERROR")
    

    def test_add_hbonds_added(self):
        """checks for added hbonds
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.addHBondGC("small_1crn_hs: :PRO41:HG2", log=0)
        self.mv.addHBondGC("small_1crn_hs: :ASP43:OD1")
        c = self.mv.showHBonds
        c(ats)
        x=c.ifd.entryByName['atsLC']['widget']
        x.lb.selection_set('0')
        self.assertEqual(c.lines.visible,1)
        c.dismiss_cb()
#end addHBondGC
        
    
#addHBond
    def test_add_hbonds_invalid_input(self):
        """add hbonds invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.addHBond
        l = ("hfdh", "fjgjh")
        self.assertRaises(ValueError, c, ("hfdh","fjgjh"), {})
        #returnvalue=c("hfdh","fjgjh")
        #self.assertEqual(returnvalue,"ERROR")


    def test_add_hbonds_empty_input(self):
        """add hbonds empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.addHBond        
        self.assertRaises(ValueError, c, ("  ","  "), {})
        #returnvalue=c(" "," ")
        #self.assertEqual(returnvalue,"ERROR")
#end addHBond
    

#removeHBondGC
    def Ztest_remove_hbondsGC_added(self):
        """checks for removed hbonds via geometry
        8/30/2004: no redraw occurs in the test...
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.addHBondGC("small_1crn_hs: :ASP43:OD1,HG2")
        self.mv.removeHBondGC("small_1crn_hs: :ASP43:OD1")
        ats=self.mv.allAtoms 
        c = self.mv.showHBonds
        c(ats)
        self.assertEqual(len(c.lines.vertices.vertexSet.array),0)
        #self.assertEqual(len(c.lineVerts),0)
        c.dismiss_cb()
        

    def test_remove_hbondsGC_nodes(self):
        """remove hbondsGC valid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.removeHBondGC("small_1crn_hs")
        self.assertEqual(self.mv.Mols[0].name,"small_1crn_hs")
        has_hbond_ats = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        self.assertEqual(len(has_hbond_ats), 0)


    def test_remove_hbondsGC_invalid_input(self):
        """remove hbondsGC invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        if not self.mv.hasGui: return
        c = self.mv.removeHBondGC
        #self.assertRaises(AssertionError, c, ("hfdh"))
        returnvalue=c("hfdh")
        self.assertEqual(returnvalue,"ERROR")


    def test_remove_hbondsGC_empty_input(self):
        """remove hbondsGC empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.removeHBondGC        
        #self.assertRaises(AssertionError, c, ("    "))
        returnvalue=c(" ")
        self.assertEqual(returnvalue,"ERROR")
#end removeHBondGC
 
    
#removeHBond
    def test_remove_hbonds_invalid_input(self):
        """remove hbonds invalid input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.removeHBond
        self.assertRaises(ValueError, c, ("hfdh","fghjg",), {})
        #returnvalue=c("hfdh","fghjg")
        #self.assertEqual(returnvalue,"ERROR")


    def test_remove_hbonds_empty_input(self):
        """remove hbonds empty input
        """
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.removeHBond        
        self.assertRaises(ValueError, c, ("   ", "  "), {})
        #returnvalue=c(" "," ")
        #self.assertEqual(returnvalue,"ERROR")
#end removeHBond



#limitHBonds
    def test_limit_hbonds_empty_input(self):
        """limit hbonds empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.limitHBonds
        #self.assertRaises(AssertionError, c, ("  "))
        returnvalue=c(" ")
        self.assertEqual(returnvalue,"ERROR")
        

    def test_limit_hbonds_invalid_input(self):
        """limit hbonds invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        c = self.mv.limitHBonds
        #self.assertRaises(AssertionError, c, ("sasf"))
        returnvalue=c("sasf")
        self.assertEqual(returnvalue,"ERROR")
    

    def test_limit_hbonds_nodes(self):
        """limit hbonds energy < value in widget
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        print "all MOls:", self.mv.Mols
        self.mv.addHBond("small_1crn_hs: :PRO41:HG2", "small_1crn_hs: :ASP43:OD1")
        self.mv.limitHBonds("small_1crn_hs: :PRO41:HG2")
        d=self.mv.limitHBonds
        ats=self.mv.allAtoms
        #NB: energy is -5.341
        if not self.mv.hasGui: return
        widget = d.ifd.entryByName['energy']['widget']
        widget.set(-8)
        self.assertEqual(len(d.lines.vertexSet.vertices.array),2)
        d.Delete_cb()
        self.assertEqual(len(d.lines.vertexSet.vertices.array),0)
        self.assertEqual(len(d.everts),0)
    

    def Ztest_limit_hbonds_widget(self):
        """limit hbonds widget is visible check
        8/30/2004: this window is not mapped
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.addHBond("small_1crn_hs: :PRO41:HG2", "small_1crn_hs: :ASP43:OD1")
        c = self.mv.limitHBonds 
        #self.mv.limitHBonds(ats)
        c(ats)
        c.energyWid.wait_visibility(c.energyWid)
        self.assertEqual(c.form2.root.winfo_ismapped(),1)
        self.assertEqual(c.form2.root.withdraw(),'') 
#end limitHBonds

class hbond4_hbondTests(hbond_BaseTests):
#writeHBondAssembly
    def test_write_hbond_assembly_compare_two_files(self):
        """checks write hbond assembly file vs reference file
        """
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.readMolecule("Data/barrel_2.pdb")
        filename = "Data/test_barrel_1_2_hbonds.hbnd"
        cmd = "rm -f " + filename
        if self.mv.hasGui:
            self.mv.buildHBondsGC("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeHBondAssembly("Data/test_barrel_1_2_hbonds.hbnd",self.mv.allAtoms)
        fptr=open("Data/test_barrel_1_2_hbonds.pdb",'r')
        alllines=fptr.readlines()
        fptr1=open("Data/barrel_1_2_hbonds.pdb",'r')
        alllines1=fptr1.readlines()
        self.assertEqual(alllines,alllines1)


    def test_write_hbond_assembly(self):
        """checks writing assembly of hbonds produces a file
        """
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.readMolecule("Data/barrel_2.pdb")
        filename = "Data/test_barrel_1_2_hbonds.hbnd"
        cmd = "rm -f " + filename
        os.system(cmd)
        if self.mv.hasGui:
            self.mv.buildHBondsGC("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeHBondAssembly("Data/test_barrel_1_2_hbonds.hbnd",self.mv.allAtoms)
        self.assertEqual(os.path.exists(filename), True)


    def test_write_hbond_assembly_number_of_hbonds(self):
        """checks number of  hbonds formed by write hbond assembly
        """
        filename = "Data/test_barrel_1_2_hbonds.hbnd"
        cmd = "rm -f " + filename
        os.system(cmd)
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.readMolecule("Data/barrel_2.pdb")
        if self.mv.hasGui:
            self.mv.buildHBondsGC("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0) 
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        self.mv.writeHBondAssembly("Data/test_barrel_1_2_hbonds.hbnd",self.mv.allAtoms)
        fptr=open("Data/test_barrel_1_2_hbonds.hbnd")
        alllines=fptr.readlines()
        y=0
        for k in range(0,5):
            if  find(alllines[k],'HYDBND')==0:
                   y+=1
        self.assertEqual(len(h)/3,y)
#end writeHBondAssembly


#writeIntermolHBonds
    def test_write_intermol_hbond(self):
        """checks hbonds formed by write intermol hbonds
        """
        filename = "Data/test_part12.hbnd"
        cmd = "rm -f " + filename
        os.system(cmd)
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        if self.mv.hasGui:
            self.mv.buildHBondsGC("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeIntermolHBonds(filename, self.mv.allAtoms)
        self.assertEqual(os.path.exists(filename), True)


    def test_write_intermol_hbond_files(self):
        """checks write intermol hbond files are same
        """
        filename = "Data/test_part12.hbnd"
        cmd = "rm -f " + filename
        os.system(cmd)
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        if self.mv.hasGui:
            self.mv.buildHBondsGC("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeIntermolHBonds("Data/test_part12.hbnd", self.mv.allAtoms)
        fptr=open("Data/part1_part2.hbnd",'r')
        alllines=fptr.readlines()
        fptr1=open(filename,'r')
        alllines1=fptr1.readlines()
        self.assertEqual(alllines,alllines1)


    def test_write_intermol_hbonds_number(self):
        """checks number of hbonds in write intermol hbonds
        """
        filename = "Data/test_part12.hbond"
        cmd = "rm -f " + filename
        os.system(cmd)
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        if self.mv.hasGui:
            self.mv.buildHBondsGC("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeIntermolHBonds("Data/test_part12.hbnd", self.mv.allAtoms)
        fptr=open("Data/part1_part2.hbnd",'r')
        alllines=fptr.readlines()
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        #NB: there is only 1 hbond formed so file has only 1 line
        self.assertEqual(len(h)/3,len(alllines))
#end writeIntermolHBonds


#readIntermolHBonds
    def test_read_intermol_hbond_visible(self):
        """check hbonds formed in read intermol hbond
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        self.mv.readIntermolHBonds("Data/part1_part2.hbnd")
        c = self.mv.showHBonds
        c("part1_hs,part2_hs:::")
        self.assertEqual(c.lines.visible,1)
        c.dismiss_cb()
        

    def test_read_intermol_hbonds_number(self):
        """checks number of hbonds formed
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        self.mv.readIntermolHBonds("Data/part1_part2.hbnd")
        fptr=open("Data/part1_part2.hbnd",'r')
        alllines=fptr.readlines()
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        #NB: there is only 1 hbond formed so file has only 1 line
        self.assertEqual(len(h)/3,len(alllines))


    def test_read_intermol_hbond_invalid(self):
        """check invalid i/p read intermol hbond
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        c = self.mv.readIntermolHBonds
        self.assertRaises(IOError,c,"jdslkhfl.hbnd")
#end readIntermolHBonds


#addHBondHs
    def test_addhbonds_hsGC_simple(self):
        """checks number of atoms after adding hs to hbonds
        """
        self.mv.readMolecule("Data/hbond-barrel.pdb")
        orig_num_ats = len(self.mv.allAtoms)
        
        self.mv.buildHBonds("hbond_barrel:::", "hbond_barrel:::", {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1)
        if self.mv.hasGui:
            self.mv.addHBondHsGC("hbond_barrel:::")
        else:
            self.mv.addHBondHs("hbond_barrel:::")
        new_num_ats = len(self.mv.allAtoms)
        self.assertEqual(orig_num_ats<new_num_ats, True)


    def test_addhbonds_hsGC_number(self):
        """checks number of hbonds formed in addhbondsHsGC
        """
        self.mv.readMolecule("Data/hbond-barrel.pdb")
        self.mv.buildHBonds("hbond_barrel:::", "hbond_barrel:::", {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1)
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        if self.mv.hasGui:
            self.mv.addHBondHsGC("hbond_barrel:::")
        else:
            self.mv.addHBondHs("hbond_barrel:::")
        h1 = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        #this command added 1 hydrogenbond 
        self.assertEqual(len(h1)>len(h),True)


    def test_addhbonds_hsGC_visible(self):
        """checks addhbondsHsGC creates viewable hydrogen bonds
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hbond-barrel.pdb")
        self.mv.buildHBonds("hbond_barrel:::", "hbond_barrel:::", {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.addHBondHsGC("hbond_barrel:::")
        c = self.mv.showHBonds
        c("hbond_barrel:::")
        self.assertEqual(c.lines.visible,1)
        c.dismiss_cb()

        
    def test_addhbonds_hs_number(self):
        """checks number of hbonds formed in addhbondsHs
        """
        self.mv.readMolecule("Data/hbond-barrel.pdb")
        self.mv.buildHBonds("hbond_barrel:::", "hbond_barrel:::", {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        h = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        #h is 51 atoms
        self.mv.addHBondHs("hbond_barrel:::")
        h1 = self.mv.allAtoms.get(lambda x: hasattr(x, 'hbonds'))
        #h1 is 77 atoms
        self.assertEqual(len(h1)>len(h),True)
#end addHBondHs

####Log Tests###########


#class hbond_logTests(hbond_BaseTests):

    def test_hbonds_build_hbondsGC_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui:return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.buildHBondsGC
        ats = self.mv.allAtoms
        c(ats,ats,d)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        
        if sys.version.startswith('2.5'):
            comparison_log = 'self.buildHBonds("1crn:::", "1crn:::", {\'d3min\':120.0,\'donorTypes\':[\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'],\'d2max\':180.0,\'a2max\':150.0,\'distCutoff2\':3.0,\'distCutoff\':2.25,\'d3max\':180.0,\'d2min\':120.0,\'a3min\':100.0,\'a2min\':110.0,\'acceptorTypes\':[\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\'],\'distOnly\':0,\'a3max\':150.0}, 1, log=0)'
        else:
            comparison_log = 'self.buildHBonds("1crn:::", "1crn:::", {\'d3min\':120.0,\'donorTypes\':[\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'],\'a2max\':150.0,\'distCutoff2\':3.0,\'d2min\':120.0,\'a3min\':100.0,\'distOnly\':0,\'a3max\':150.0,\'distCutoff\':2.25,\'d3max\':180.0,\'d2max\':180.0,\'a2min\':110.0,\'acceptorTypes\':[\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\']}, 1, log=0)'       
        
        #test_log = split(last_entry, '\n')[0]
        #comp_list = split(comparison_log)
        #test_list = split(test_log)
        #for c, t in zip(comp_list, test_list):
            #print c, c==t, t
        self.assertEqual(split(last_entry,'\n')[0], comparison_log)


    def test_hbonds_build_hbondsGC_log_checks_that_it_runs(self):
        """Checking log string runs 1"""
        self.mv.readMolecule("Data/1crn.pdb")
        if self.mv.hasGui:
            c = self.mv.buildHBondsGC
        else:
            c = self.mv.buildHBonds
        ats = self.mv.allAtoms
        c(ats,ats,d)
        oldself=self
        self =mv
        s = 'self.buildHBonds("1crn:::", "1crn:::", {\'d3min\': 120.0, \'donorTypes\': [\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'], \'d2max\': 180.0, \'a2max\': 150.0, \'distCutoff2\': 3.0, \'distCutoff\': 2.25, \'d3max\': 180.0, \'d2min\': 120.0, \'a3min\': 100.0, \'a2min\': 110.0, \'acceptorTypes\': [\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\'], \'distOnly\': 0, \'a3max\': 150.0}, 1, log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_hbonds_showHBDA_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui:return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/1crn.pdb")
        ats = self.mv.allAtoms
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats) 
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.showHBDA("1crn:::", "1crn:::", [\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'], [\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\'], log=0)')

    def test_hbonds_showHBDA_log_checks_that_it_runs(self):
        """Checking log string runs 2"""
        if not self.mv.hasGui:return
        self.mv.readMolecule("Data/1crn.pdb")
        ats = self.mv.allAtoms
        c = self.mv.showHBDA
        ats = self.mv.allAtoms
        c(ats,ats)
        oldself=self
        self =mv
        s = 'self.showHBDA("1crn:::", "1crn:::", [\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'], [\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_hbonds_get_hbondDonors_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui:return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/1crn_hs.pdb")
        self.mv.getHBDonors("1crn_hs")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.getHBDonors("1crn_hs:::", [\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'], log=0)')

        
    def test_hbonds_get_hbondDonors_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/1crn_hs.pdb")
        oldself=self
        self =mv
        s = 'self.getHBDonors("1crn_hs:::", [\'Nam\', \'Ng+\', \'Npl\', \'N3+\', \'O3\', \'S3\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)


    def test_hbonds_get_hbondAcceptors_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui:return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/1crn_hs.pdb")
        self.mv.getHBAcceptors("1crn_hs")        
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.getHBAcceptors("1crn_hs:::", [\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\'], log=0)')


        
    def test_hbonds_get_hbondAcceptors_log_checks_that_it_runs(self):
        """Checking log string runs 3"""
        self.mv.readMolecule("Data/1crn_hs.pdb")
        self.mv.getHBAcceptors("1crn_hs")
        oldself=self
        self =mv
        s = 'self.getHBAcceptors("1crn_hs:::", [\'O2\', \'O-\', \'Npl\', \'Nam\', \'S3\', \'O3\'], log=0)'
        exec(s)
        oldself.assertEqual(1,1)

            
        
    def test_hbonds_get_hbondEnergies_energy_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui:return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.getHBondEnergies("small_1crn_hs")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.getHBondEnergies("small_1crn_hs: :PRO41:O/+/small_1crn_hs: :TYR44:N/+/small_1crn_hs: :TYR44:HN", log=0)')

        
        
    def test_hbonds_get_hbondEnergies_log_checks_that_it_runs(self):
        """Checking log string runs 4"""
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        if self.mv.hasGui:
            self.mv.buildHBondsGC(ats,ats,d)
        else:
            self.mv.buildHBonds(ats,ats,d)
        oldself=self
        self =mv
        s = 'self.getHBondEnergies("small_1crn_hs: :PRO41:O/+/small_1crn_hs: :TYR44:N/+/small_1crn_hs: :TYR44:HN", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_showHBonds_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui:return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        c = self.mv.showHBonds
        c(ats)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.showHBonds("small_1crn_hs:::", log=0)')
        
    def test_showHBonds_log_checks_that_it_runs(self):
        """Checking log string runs 5"""
        if not self.mv.hasGui:return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        oldself=self
        self =mv
        s = 'self.showHBonds("small_1crn_hs:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
    

    def test_hbonds_as_spheres_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.hbondsAsSpheres("small_1crn_hs:::", log=0)')
    
    def test_hbonds_as_spheres_checks_that_it_runs(self):
        """Checking log string runs 6"""
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsSpheres(ats)
        oldself=self
        self =mv
        s = 'self.hbondsAsSpheres("small_1crn_hs:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_hbonds_as_cylinders_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsCylinders(ats)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.hbondsAsCylinders("small_1crn_hs:::", log=0)')

    def test_hbonds_as_cylinders_log_checks_that_it_runs(self):
        """Checking log string runs 7"""
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats=self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.hbondsAsCylinders(ats)
        oldself=self
        self =mv
        s = 'self.hbondsAsCylinders("small_1crn_hs:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_extrude_hbonds_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats = self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        self.mv.extrudeHBonds("small_1crn_hs")        
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.extrudeHBonds("small_1crn_hs:::", Circle2D(0.1, quality=12, vertDup=0, firstDup=0), capsFlag=0, log=0)')


    def test_extrude_hbonds_log_checks_that_it_runs(self):
        """Checking log string runs 8"""
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        ats = self.mv.allAtoms
        self.mv.buildHBondsGC(ats,ats,d)
        oldself=self
        self =mv
        s = 'self.extrudeHBonds("small_1crn_hs:::", Circle2D(0.1, quality=12, vertDup=0, firstDup=0), capsFlag=0, log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
 
    def test_add_hbondsGC_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.addHBondGC(self.mv.allAtoms)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        from Pmv.Tests import compareArguments
        self.assertTrue(compareArguments(split(last_entry,'\n')[0], 'self.addHBondGC("small_1crn_hs:::", log=0)'))

        
    def test_add_hbondsGC_log_checks_that_it_runs(self):
        """Checking log string runs 9"""
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        oldself=self
        self =mv
        s = 'self.addHBond("small_1crn_hs: :PRO41:CA", "small_1crn_hs: :PRO41:C", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

        
    def test_remove_hbondsGC_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.removeHBondGC("small_1crn_hs")       
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.removeHBondGC("small_1crn_hs", log=0)')

    def test_remove_hbondsGC_log_checks_that_it_runs(self):
        """Checking log string runs 10"""
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        oldself=self
        self =mv
        s = 'self.removeHBondGC("small_1crn_hs", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_limit_hbonds_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.addHBond("small_1crn_hs: :PRO41:HG2", "small_1crn_hs: :ASP43:OD1")
        self.mv.limitHBonds("small_1crn_hs: :PRO41:HG2")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.limitHBonds("small_1crn_hs: :PRO41:HG2", log=0)')

    def test_limit_hbonds_log_checks_that_it_runs(self):
        """Checking log string runs 11"""
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/small_1crn_hs.pdb")
        self.mv.addHBond("small_1crn_hs: :PRO41:HG2", "small_1crn_hs: :ASP43:OD1")
        oldself=self
        self =mv
        s = 'self.limitHBonds("small_1crn_hs: :PRO41:HG2", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_write_hbond_assembly_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.readMolecule("Data/barrel_2.pdb")
        filename = "Data/test_barrel_1_2_hbonds.hbnd"
        cmd = "rm -f " + filename
        self.mv.buildHBondsGC("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeHBondAssembly("Data/test_barrel_1_2_hbonds.hbnd",self.mv.allAtoms)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.writeHBondAssembly(\'Data/test_barrel_1_2_hbonds.hbnd\', "barrel_1,barrel_2:::", log=0)')
        #self.assertEqual(split(last_entry,'\n')[0],'self.writeHBondAssembly(\'Data/test_barrel_1_2_hbonds.hbnd\', "barrel_1;barrel_2:::", log=0)')
        
    def test_write_hbond_log_checks_that_it_runs(self):
        """Checking log string runs 12"""
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.readMolecule("Data/barrel_2.pdb")
        filename = "Data/test_barrel_1_2_hbonds.hbnd"
        cmd = "rm -f " + filename
        if self.mv.hasGui:
            self.mv.buildHBondsGC("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("barrel_1:::","barrel_2:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        oldself=self
        self=mv
        s = 'self.writeHBondAssembly(\'Data/test_barrel_1_2_hbonds.hbnd\', "barrel_1,barrel_2:::", log=0)'
        #s = 'self.writeHBondAssembly(\'Data/test_barrel_1_2_hbonds.hbnd\', "barrel_1;barrel_2:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)


    def test_write_intermol_hbond_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        filename = "Data/test_part12.hbnd"
        cmd = "rm -f " + filename
        os.system(cmd)
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        self.mv.buildHBondsGC("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        self.mv.writeIntermolHBonds(filename, self.mv.allAtoms)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.writeIntermolHBonds(\'Data/test_part12.hbnd\', "part1_hs,part2_hs:::", log=0)')
        #self.assertEqual(split(last_entry,'\n')[0],'self.writeIntermolHBonds(\'Data/test_part12.hbnd\', "part1_hs;part2_hs:::", log=0)')


    def test_write_intermol_hbond_log_checks_that_it_runs(self):
        """Checking log string runs 13"""
        filename = "Data/test_part12.hbnd"
        cmd = "rm -f " + filename
        os.system(cmd)
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        if self.mv.hasGui: 
            self.mv.buildHBondsGC("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        else:
            self.mv.buildHBonds("part1_hs:::","part2_hs:::",{'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1, log=0)
        oldself=self
        self=mv
        s = 'self.writeIntermolHBonds(\'Data/test_part12.hbnd\', "part1_hs,part2_hs:::", log=0)'
        #s = 'self.writeIntermolHBonds(\'Data/test_part12.hbnd\', "part1_hs;part2_hs:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_read_intermol_hbond_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        self.mv.readIntermolHBonds("Data/part1_part2.hbnd")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.readIntermolHBonds(\'Data/part1_part2.hbnd\', log=0)')


    def test_read_intermol_hbond_log_checks_that_it_runs(self):
        """Checking log string runs 14"""
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        oldself=self
        self=mv
        s = 'self.readIntermolHBonds(\'Data/part1_part2.hbnd\', log=0)'
        exec(s)
        oldself.assertEqual(1,1)

        
    def test_addhbonds_hsGC_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/hbond-barrel.pdb")
        orig_num_ats = len(self.mv.allAtoms)
        self.mv.buildHBonds("hbond_barrel:::", "hbond_barrel:::", {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1)
        self.mv.addHBondHsGC("hbond_barrel:::")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.addHBondHs("hbond_barrel:::", log=0)')

    def test_addhbonds_hsGC__log_checks_that_it_runs(self):
        """Checking log string runs 15"""
        self.mv.readMolecule("Data/hbond-barrel.pdb")
        orig_num_ats = len(self.mv.allAtoms)
        self.mv.buildHBonds("hbond_barrel:::", "hbond_barrel:::", {'d3min': 120.0, 'donorTypes': ['Nam', 'Ng+', 'Npl', 'N3+', 'O3', 'S3'], 'a3min': 100.0, 'a2max': 150.0, 'distCutoff2': 3.0, 'distCutoff': 2.25, 'd3max': 180.0, 'd2min': 120.0, 'd2max': 180.0, 'a2min': 110.0, 'acceptorTypes': ['O2', 'O-', 'Npl', 'Nam', 'S3', 'O3'], 'distOnly': 0, 'a3max': 150.0}, 1)
        oldself=self
        self=mv
        s ='self.addHBondHs("hbond_barrel:::", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
## if __name__ == '__main__':
##     unittest.main()


        
if __name__ == '__main__':
    test_cases = [
    'hbond1_hbondTests',
    'hbond2_hbondTests',
    'hbond3_hbondTests',
    'hbond4_hbondTests',

    ]
    unittest.main( argv=([__name__,] + test_cases) )


