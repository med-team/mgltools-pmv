#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_getNodesByMolecule.py,v 1.8 2009/11/10 22:59:02 annao Exp $
#
# $Id: test_getNodesByMolecule.py,v 1.8 2009/11/10 22:59:02 annao Exp $
#

import sys
import unittest, Tkinter

mv= None
ct = 0
totalCt = 1
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

    
class Pmv_test_getNodesByMoleculeTests(unittest.TestCase):

    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, withShell=0, gui=hasGUI)
            mv.browseCommands('fileCommands', commands= ['readMolecule',],package= 'Pmv')
            mv.browseCommands('deleteCommands', commands=['deleteMol',],package= 'Pmv')
            mv.browseCommands("bondsCommands", commands= ["buildBondsByDistance"],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands("interactiveCommands", package='Pmv')
            # Don't want to trap exceptions and errors... the user pref is set to 1 by
            # default
            #need access to error messages
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        

    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv  
   

    def test_getNodesByMolecule(self):
        """
        test a bug in getNodesByMolecule that caused it to return duplicates
        """
        mv.readMolecule('Data/1crn.pdb')
        from MolKit.molecule import Atom
        from MolKit.protein import Residue
        mv.setIcomLevel(Atom, log = 0, KlassSet = None)
        res = mv.getNodesByMolecule(mv.allAtoms, Residue)
        self.assertEqual(len(res[1][0]), 46)
        


if __name__ == '__main__':
    unittest.main()

