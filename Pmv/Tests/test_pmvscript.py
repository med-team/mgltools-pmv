#
# Authors : Sowjanya Karnati,Michel F Sanner, Ruth Huey, Sargis Dallakyan
#
# $Id:
import unittest, time, os
from Pmv import setdmode , setcmode

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

from Pmv import runPmv

class PmvScriptBaseTest(unittest.TestCase):
    def setdmode(self,mode, mv):
        """
        load display commands for mode and set them as default command for new molecule
        """
        setdmode(mode, mv)

    def setcmode(self, mode, mv):
        """
    load color commands for mode and set them as default command for new molecule
        """
        setcmode(mode, mv)
    
    def callpmv(self,dmode,cmode,mol):
        self.mol=mol
        from Pmv.moleculeViewer import MoleculeViewer
        
        mv = MoleculeViewer(logMode="no", customizer="./empty", verbose=False,
                            withShell=0, gui=hasGUI
                            )

        mv.browseCommands('fileCommands',package= 'Pmv')
        
        mv.browseCommands('deleteCommands',package='Pmv') 
        mv.browseCommands('colorCommands',package='Pmv')
        mv.browseCommands("interactiveCommands",package='Pmv')
        mv.browseCommands("displayCommands",
                          commands=['displaySticksAndBalls',
                                    'undisplaySticksAndBalls',
                                    'displayCPK', 'undisplayCPK',
                                    'displayLines','undisplayLines',
                                    'displayBackboneTrace',
                                    'undisplayBackboneTrace',
                                    'DisplayBoundGeom'
                                    ], package = 'Pmv')
        mv.browseCommands("bondsCommands",
                          commands=['buildBondsByDistance'],
                          package='Pmv', log = 0)
        
        mv.setOnAddObjectCommands(['buildBondsByDistance',
                                       'displayLines'],
                                  log=0)
        self.mv=mv
        #self.mv.readMolecule("Data/1crn.pdb")
        
        if dmode is not None or cmode is not None:
        # save current list of commands run when a molecule is loaded
            
            addCmds = self.mv.getOnAddObjectCmd()
	    # remove them
	    for c in addCmds:
	        print c
            self.mv.removeOnAddObjectCmd(c[0])
	    # set the mode
	    #self.setdmode('lines',self.mv)
        self.setdmode(dmode, self.mv)

        if cmode is not None:
	        # set the mode
	        self.setcmode(cmode, self.mv)
	        
        mv.readMolecule(self.mol)

        if dmode is not None or cmode is not None:
            # get current list of commands run when a molecule is loaded
            cmds = self.mv.getOnAddObjectCmd()
	    # remove them
	    for c in cmds:
	        self.mv.removeOnAddObjectCmd(c[0])
	    # restore original list of commands
	    for c in addCmds:
	        self.mv.addOnAddObjectCmd( *c )

    def setUp(self):
        if not hasGUI: return
        # create ".registartion" file in /home/user/.mgltools/VERSION
        # folder if such file does not exist. This is needed to prevent Pmv 
        # from asking the user to register. The tearDown() will remove this file.
        from mglutil.util.packageFilePath import getResourceFolderWithVersion
        rcWithVersion = getResourceFolderWithVersion()
        self.registration = None
        self.ownregistration = True
        if rcWithVersion is not None:
            self.registration = rcWithVersion + os.sep + '.registration'
            if not os.path.exists(self.registration):
                try:
                    f = open(self.registration, "w")
                    f.close()
                    self.ownregistration = False
                except:
                    pass
        #print "Registration:", self.registration, self.ownregistration


    def tearDown(self):
        if hasGUI:
            #time.sleep(2)
            if not self.ownregistration and os.path.exists(self.registration):
                try:
                    os.remove(self.registration)
                except:
                    pass
        if self.mv.hasGui:
            self.mv.Exit(0)
        

class PmvModeTests(PmvScriptBaseTest):


    def test_pmvscript_mode_cpk(self):
        """checks starting pmv with displaymode 'cpk' and colormode coloratoms by
        davidgoodsell colors 'cdg'"""
        dmode = 'cpk'
        cmode='cdg'
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s', '--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-c', cmode, '--die', '--noGUI', mol],) , **{'ownInterpreter': True})
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        cpkats = mv.Mols[0].geomContainer.atoms['cpk'] 
        cpkvertices = mv.Mols[0].geomContainer.geoms['cpk'].vertexSet
        #checks command loaded
        self.assertEqual(hasattr(mv,'displayCPK'),True)
        #checks cpk is displayed
        self.assertEqual(len(cpkats),327)
        #checks cpkgeometry vertices >0
        self.assertEqual(len(cpkvertices)>0,True)
        mod.__dict__.pop('self')


    def test_pmvscript_mode_ss(self):
        """checks starting pmv with displaymode secondary structure 'ss' and colormode colorBySecondarystructure 'css'"""
        dmode = 'ss'
        cmode='css'
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s', '--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s', '--die','--noGUI',  mol],) , **{'ownInterpreter': True})
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        #checks command loaded
        self.assertEqual(hasattr(mv,"ribbon"),True)
        #checks geomC has key secondary structure 
        self.assertEqual(mv.Mols[0].geomContainer.atoms.has_key("secondarystructure"),True)
        #checks chain has attr secondarystructureset and their length
        self.failUnless( hasattr(mv.Mols[0].chains[0], 'secondarystructureset') and \
                         len(mv.Mols[0].chains[0].secondarystructureset)==10)
        mod.__dict__.pop('self')


    def test_pmvscript_mode_bt(self):
        """checks starting pmv with displaymode backbone trace 'bt' and colormode colorByResidueType 'cr'"""
        dmode = 'bt'
        cmode='cr'
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True})
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        ballvert = mv.Mols[0].geomContainer.geoms['CAballs'].vertexSet
        stickvert = mv.Mols[0].geomContainer.geoms['CAsticks'].vertexSet
        #checks command loaded
        self.assertEqual(hasattr(mv,"displayBackboneTrace"),True)
        #checks geomC has key CAballs and CAsticks
        self.assertEqual(mv.Mols[0].geomContainer.atoms.has_key("CAballs"),True) 
        self.assertEqual(mv.Mols[0].geomContainer.atoms.has_key("CAsticks"),True)
        #checks caballgeometry vertices >0,castickgeometry vertices >0
        self.assertEqual(len(ballvert)>0,True)
        self.assertEqual(len(stickvert)>0,True)
        mod.__dict__.pop('self')

    def test_pmvscript_mode_lines(self):
        """checks starting pmv with displaymode 'lines' and colormode
        colorByAtomType 'ca'"""
        dmode = 'lines'
        cmode='ca'
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True})
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        linevert = mv.Mols[0].geomContainer.geoms['bonded'].vertexSet
        #checks command loaded
        self.assertEqual(hasattr(mv,"displayLines"),True)
        #checks lines displayed
        self.assertEqual(len(mv.Mols[0].geomContainer.atoms['bonded'])>0,True)
        #checks linegeometry vertices >0
        self.assertEqual(len(linevert)>0,True)
        mod.__dict__.pop('self')


    def test_pmvscript_mode_sb(self):
        """checks starting pmv with displaymode SticksandBalls 'sb' and colormode colorByChain 'cc'"""
        dmode = 'sb'
        cmode='cc'
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True})
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        ballvert = mv.Mols[0].geomContainer.geoms['balls'].vertexSet
        stickvert = mv.Mols[0].geomContainer.geoms['sticks'].vertexSet
        #checks command loaded
        self.assertEqual(hasattr(mv,"displaySticksAndBalls"),True)
        #checks sticks and balls displayed
        self.assertEqual(len(mv.Mols[0].geomContainer.atoms['sticks'])>0,True)
        self.assertEqual(len(mv.Mols[0].geomContainer.atoms['balls'])>0,True)
        #checks ballgeometry vertices >0,stickgeometry vertices >0
        self.assertEqual(len(ballvert)>0,True)
        self.assertEqual(len(stickvert)>0,True)
        mod.__dict__.pop('self')


    def test_pmvscript_mode_sp(self):
        """checks starting pmv with displaymode spline 'sp' and colormode color by residues shapely 'cs'"""
        dmode = 'sp'
        cmode='cs'
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
           runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True}) 
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        CA_atoms = self.mv.allAtoms.get(lambda x : x.name == 'CA')
        #checks command loaded
        self.assertEqual(hasattr(self.mv,"displayExtrudedSpline"),True)
        #checks geomC has key spline
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms.has_key("spline "),True)
        self.assertEqual(len(self.mv.Mols[0].chains[0].spline['spline '][1]),len(CA_atoms))
        mod.__dict__.pop('self')


    def test_pmvscript_mode_sssb(self):
        """checks starting pmv with displaymode displaySecondarystructure and color mode None"""
        dmode = 'sssb'
        cmode=None
        mol="Data/1crn_test.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True}) 
        mod = __import__('__main__')
        self.mv = mod.__dict__['self']
        #checks command loaded
        self.assertEqual(hasattr(self.mv,"displaySSSB"),True)
        #checks geomC has key secondarystructure
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms.has_key("secondarystructure"),True)
        #checks chain has attr secondarystructureset and their length
        self.failUnless( hasattr(self.mv.Mols[0].chains[0], 'secondarystructureset') and \
                         len(self.mv.Mols[0].chains[0].secondarystructureset)==10)
        mod.__dict__.pop('self')


    def test_pmvscript_mode_ca(self):
        "checks starting pmv with displaymode catrace 'ca' and cmode is colorByMolecule 'cm'"
        dmode = 'ca'
        cmode='cm'
        mol="Data/1crn_test.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
            runPmv(*(['', '-d', dmode, '-c', cmode,'-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True})
        mod = __import__('__main__')
        mv = self.mv = mod.__dict__['self']
        #checks command loaded
        self.assertEqual(hasattr(mv,"displayTrace"),True) 
        #checks geomC has key trace,CATrace and CATrace geomtry vertices >0
        self.assertEqual(mv.Mols[0].geomContainer.atoms.has_key("trace"),True)
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms.has_key("CATrace"),True)
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['CATrace '].vertexSet.vertices.array)>0,True)
        mod.__dict__.pop('self')


    def test_pmvscript_mode_ms(self):
        "checks starting pmv with displaymode msms 'ms' and colormode is None"
        dmode = 'ms'
        cmode=None
        mol="Data/1crn.pdb"
        #self.callpmv(dmode,cmode,mol)
        if hasGUI:
            runPmv(*(['', '-d', dmode, '-s','--die',  mol],) , **{'ownInterpreter': True})
        else:
           runPmv(*(['', '-d', dmode, '-s','--die', '--noGUI', mol],) , **{'ownInterpreter': True}) 
        mod = __import__('__main__')
        self.mv = mod.__dict__['self']
        #checks command loaded
        self.assertEqual(hasattr(self.mv,"displayMSMS"),True)
        #checks geomC has key MSMS-MOL
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms.has_key("MSMS-MOL"),True)
        self.assertEqual(self.mv.Mols[0].geomContainer.msmsAtoms["MSMS-MOL"],self.mv.Mols[0].allAtoms)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms["MSMS-MOL"].vertexSet)>0,True)
        mod.__dict__.pop('self')


if __name__ == '__main__':
    unittest.main()
