#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_displayCommands.py,v 1.55.2.2 2016/03/03 00:56:04 annao Exp $
#
# $Id: test_displayCommands.py,v 1.55.2.2 2016/03/03 00:56:04 annao Exp $
#

#############################################################################
#
# Authors: Sowjanya KARNATI, Sophie COON
#
#############################################################################


"""
Test module for the Pmv module: displayCommands
"""
## ############################################################################
## ### BASE TEST CLASS FOR THE DISPLAY MODULE
## ############################################################################

from exceptions import ValueError, AssertionError
import sys, unittest,time,Pmv
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from string import split
mv =None
klass = None
ct = 0
totalCt = 85

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class DisplayBaseTest(unittest.TestCase):
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from MolKit import Read
            import Tkinter 
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty',
                                 logMode = 'no',
                                 trapExceptions=False,
                                 withShell=0, gui=hasGUI)
                                
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance',
                                        'displayLines'], log=False)
            mv.browseCommands("interactiveCommands", package='Pmv')
        # Don't want to trap exceptions and errors...
        # the user pref is set to 1 by
        # default
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        
            ## mv.browseCommands('displayCommands',
##                               commands=['displaySticksAndBalls','undisplaySticksAndBalls',
##                                         'displayCPK', 'undisplayCPK',
##                                         'displayLines','undisplayLines',
##                                         'displayBackboneTrace','undisplayBackboneTrace',
##                                         'DisplayBoundGeom'
##                                         ],package='Pmv', topCommand=0)
            mv.browseCommands('displayCommands',package='Pmv', topCommand=0)

        self.mv = mv
        self.mv.undo = mv.NEWundo


    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv


## ############################################################################
## ### DISPLAYLINES TESTS
## ############################################################################

class DisplayLines(DisplayBaseTest):
#Invalid Input for displayLines   
    
    def test_displaylines_invalid_nodes(self):
        """tests invalid nodes  for displayLines
        """
        self.mv.readMolecule('Data/7ins.pdb')
        
        c = self.mv.displayLines
        returnValue = c("abcd")
        self.assertEqual(returnValue,None)


        
    def test_displaylines_invalid_linewidth(self):
        """tests invalid input for linewidth in displaylines commnad********
        """
        # Reading the test molecule
        self.mv.readMolecule('Data/7ins.pdb')
        # Test body
        returnValue=self.mv.displayLines("7ins",lineWidth = -5)
        self.assertEqual(returnValue, 'ERROR')
        
    
#end invalid input for displayLines                         

#valid input for displayLines

    def test_displaylines_valid_input_nodes(self):
        """
        Testing input through selction
        """
        # Reading the test molecule
        self.mv.readMolecule('Data/7ins.pdb')
        # Test body
        self.mv.setSelectionLevel(Molecule)
        self.mv.select('7ins.pdb')
        self.mv.displayLines(self.mv.getSelection())
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['bonded'],
                         self.mv.Mols[0].allAtoms)
          
    def test_displaylines_negate(self):
        """
        Testing __call__ on 7ins with only = 0 and negate = 1
        """
        # Reading the test molecule
        self.mv.readMolecule('Data/7ins.pdb')
        # Test body
        self.mv.displayLines(self.mv.getSelection(), negate=1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['bonded']),
                         0)

    def test_displaylines_only(self):
        """
        Testing __call__ on 7ins with only = 1 and negate = 0
        """
        # Reading the test molecule
        self.mv.readMolecule('Data/7ins.pdb')
        # Test body
        self.mv.setSelectionLevel(Chain)
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.displayLines(self.mv.getSelection(), only = 1)
        from MolKit.molecule import Atom
        # need to assert something about the lines geom as well.
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['bonded']),
                         len(self.mv.Mols[0].chains[0].findType(Atom)))

    def test_displaylines_linewidth(self):
        """tests valid input for linewidth in displaylines commnad
        """
        # Reading the test molecule
        self.mv.readMolecule('Data/7ins.pdb')
        # Test body
        self.mv.displayLines("7ins", lineWidth=5, displayBO=1)
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['bonded'].lineWidth,5)

#end valid input for displayLines    
    
    def test_displaylines_selected(self):
        
        # Reading the test molecule
        self.mv.readMolecule('Data/7ins.pdb')
        # Test body
        self.mv.setSelectionLevel(Atom)
        self.mv.select(self.mv.allAtoms[:15])
        self.mv.displayLines(self.mv.getSelection())
        geomsbonded = self.mv.Mols[0].geomContainer.geoms['bonded'].vertexSet
        atomsbonded = self.mv.Mols[0].geomContainer.atoms['bonded']
        self.assertEqual(len(geomsbonded),len(atomsbonded))

    def test_displaylines_emptyviewer(self):
        """
        Testing __call__ on empty viewer
        """
        # here we test if the command can be called with nodes arg only
        if self.mv.displayLines.flag & 1:
            self.mv.displayLines(self.mv.getSelection())
        self.assertEqual(self.mv.Mols.geomContainer.geoms.lines.visible,[])
        
    
    def test_displaylines_bug33(self):
        """
        Test written for the bug #33 reported in MGLBUGZILLA by Daniel.
        only does not work when multiple molecule and selection only on
        one mol.
        """
        # Reading the test molecules
        self.mv.readMolecule('Data/7ins.pdb')
        self.mv.readMolecule('Data/1crn.pdb')

        # Test body
        self.mv.setSelectionLevel(Residue)
        self.mv.select(self.mv.Mols[0].chains[0].residues[1:15])
        nodes = self.mv.getSelection()
        self.mv.displayLines(nodes, only=1)
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['bonded'],
                         nodes.atoms.uniq())
        self.assertEqual(len(self.mv.Mols[1].geomContainer.atoms['bonded']),
                         0)
        
    def test_displayLines_Undo_1(self):
        """
        Test the displayLines commands only=false, negate=false
        """
        # don't want to display the molecules by lines
        self.mv.setOnAddObjectCommands(['buildBondsByDistance'], log=0)
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection",
                                         "select"] ,
                               package="Pmv")
        self.mv.readMolecule("./Data/4fiv.pdb")
        
        # 1- Select a bunch of nodes containing bonded atoms and no bonded
        # atoms.
        self.mv.setSelectionLevel(Residue)
        self.mv.select("4fiv: :HOH312,GLU11,LEU10,HOH359,THR8,ARG13,ILE112,HOH357,THR31,HOH368,ARG113,HOH311,VAL115,HOH373,THR9,HOH320,HOH384,PRO14,HOH381,GLU15,LEU114,LEU29,LYS12,MET116,HOH329,HOH343", negate=False, only=False, log=0)

        # 2- Display them by lines
        self.mv.displayLines(self.mv.getSelection())
        gc = self.mv.Mols[0].geomContainer
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|4fiv|lines|bonded')
        # Lines has 3 components: bonded, nobnds, bondorder.
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = gc.atoms['bonded']
        gatms.sort()
        self.assertEqual(gatms, atms)

        # Check the nobnds:
        nobndAtms = atms.bonds[1]
        nobndAtms.sort()
        nogatms = gc.atoms['nobnds']
        nogatms.sort()
        self.assertEqual(nobndAtms, nogatms)

        # didn't compute the bondorder and didn't display them
        self.assertEqual(len(gc.atoms['bondorder']), 0)

        # Make sure that the undo gets you back to the starting point
        self.mv.undo()
        self.assertEqual(len(gc.atoms['bonded']), 0)
        self.assertEqual(len(gc.atoms['nobnds']), 0)
        self.assertEqual(len(gc.atoms['bondorder']), 0)
        self.assertEqual(len(obj.vertexSet.vertices.array), 0)

        # Test "redo" :
        self.mv.redo()
        gatms = gc.atoms['bonded']
        gatms.sort()
        self.assertEqual(gatms, atms)
        nobndAtms = atms.bonds[1]
        nobndAtms.sort()
        nogatms = gc.atoms['nobnds']
        nogatms.sort()
        self.assertFalse(len(nogatms) == 0)
        self.assertEqual(nobndAtms, nogatms)
        self.assertEqual(len(obj.vertexSet.vertices.array), 136)
        
        

    def test_displayLines_Undo_2(self):
        """
        DisplayLines negate=False, only=False
        With Lines already displayed but not selected any longer.
        """
        self.mv.setOnAddObjectCommands(['buildBondsByDistance'], log=0)
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by SticksAndBalls and clearSelection
        self.mv.displayLines(self.mv.getSelection())
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|1crn|lines|bonded')
        
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['bonded'][:]
        gatms.sort()
        self.assertEqual(gatms, atms)
        self.mv.clearSelection()
        # 2- select another bunch of nodes
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("","","35-40", "", negate=False)
        self.mv.displayLines(self.mv.getSelection())

        atms = self.mv.getSelection()[:].atoms
        atms = atms + gatms
        atms.sort()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['bonded']
        gatms2.sort()
        self.assertEqual(gatms2, atms)

        # 3- Undo
##         undoCmd = ('displaySticksAndBalls("1crn: :ILE35,PRO36,GLY37,ALA38,THR39,CYS40", topCommand=0, setupUndo=0, log=1, scaleFactor=1.0, negate=True, busyIdle=0, redraw=True, quality=10)\n', 'displaySticksAndBalls')
##         self.assertEqual(self.mv.undoCmdStack[-1], undoCmd)
        self.mv.undo()
        ngatms = self.mv.Mols[0].geomContainer.atoms['bonded'][:]
        ngatms.sort()
        self.assertEqual(ngatms, gatms)

    
        
        
    def test_displayLines_Undo_3(self):
        """
        DisplayLines negate=False, only=True 
        """
        self.mv.setOnAddObjectCommands(['buildBondsByDistance'], log=0)
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection",
                                         "select"] ,
                               package="Pmv")
        self.mv.readMolecule("Data/4fiv.pdb")
        from MolKit.molecule import Molecule
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        gc = self.mv.Mols[0].geomContainer
        # Save the starting situation ?
        
        self.mv.displayLines(self.mv.getSelection())
        atms = self.mv.getSelection()[:].allAtoms
        atms.sort()
        gatms = gc.atoms['bonded'][:]
        gatms.sort()
        nobndAtms = atms.bonds[1]
        nobndAtms.sort()
        nogatms = gc.atoms['nobnds']
        nogatms.sort()
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|4fiv|lines|bonded')
        nverts1 = len(obj.vertexSet.vertices.array)

        # 1- Select a bunch of nodes
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.select("4fiv: :HOH312,GLU11,LEU10,HOH359,THR8,ARG13,ILE112,HOH357,THR31,HOH368,ARG113,HOH311,VAL115,HOH373,THR9,HOH320,HOH384,PRO14,HOH381,GLU15,LEU114,LEU29,LYS12,MET116,HOH329,HOH343", negate=False, only=False, log=0)
        # 2- Display them by lines and clearSelection
        self.mv.displayLines(self.mv.getSelection(), only=True)
        nverts2 = len(obj.vertexSet.vertices.array)
        self.assertTrue(nverts2 < nverts1)
        gc = self.mv.Mols[0].geomContainer
        atms2 = self.mv.getSelection()[:].atoms
        atms2.sort()
        gatms2 = gc.atoms['bonded'][:]
        gatms2.sort()
        self.assertEqual(gatms2, atms2)
        nobndAtms2 = atms2.bonds[1]
        nobndAtms2.sort()
        nogatms2 = gc.atoms['nobnds']
        nogatms2.sort()
        self.assertEqual(nobndAtms2, nogatms2)
        
        # Make sure that the undo works properly
        self.mv.undo()
        ngatms3 = gc.atoms['bonded'][:]
        ngatms3.sort()
        self.assertEqual(ngatms3, gatms)
        nogatms3 = gc.atoms['nobnds'][:]
        nogatms3.sort()
        self.assertEqual(nogatms3, nogatms)
        self.assertEqual(len(obj.vertexSet.vertices.array), nverts1)

        # Test "redo" :
        self.mv.redo()
        gatms = gc.atoms['bonded']
        gatms.sort()
        self.assertEqual(gatms, gatms2)
        nobndAtms3 = atms2.bonds[1]
        nobndAtms3.sort()
        nogatms = gc.atoms['nobnds']
        nogatms.sort()
        self.assertEqual(nobndAtms3, nogatms)
        self.assertEqual(len(obj.vertexSet.vertices.array), nverts2)
        

    def test_displayLines_Undo_0(self):
        """
        DisplayLines, only=False, negate=True with lines displayed
        """
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.mv.readMolecule("./Data/4fiv.pdb")
        gc = self.mv.Mols[0].geomContainer
        atms = self.mv.getSelection()[:].allAtoms
        atms.sort()
        gatms = gc.atoms['bonded'][:]
        gatms.sort()
        nobndAtms = atms.bonds[1]
        nobndAtms.sort()
        nogatms = gc.atoms['nobnds']
        nogatms.sort()
        #self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        # 1- Select a bunch of nodes containing bonded atoms and no bonded
        # atoms.
        #self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.select("4fiv: :HOH312,GLU11,LEU10,HOH359,THR8,ARG13,ILE112,HOH357,THR31,HOH368,ARG113,HOH311,VAL115,HOH373,THR9,HOH320,HOH384,PRO14,HOH381,GLU15,LEU114,LEU29,LYS12,MET116,HOH329,HOH343", negate=False, only=False)
        #self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        atms2 = self.mv.getSelection()[:].atoms
        # 2- Undisplay the current selection
        #self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.displayLines(self.mv.getSelection(), negate=True)
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|4fiv|lines|bonded')
        
        # Lines has 3 components: bonded, nobnds, bondorder.
        
        atms2 = atms-atms2
        atms2.sort()
        
        gatms2 = gc.atoms['bonded']
        gatms2.sort()
        self.assertEqual(gatms2, atms2)

        # Check the nobnds:
        nobndAtms2 = atms2.bonds[1]
        nobndAtms.sort()
        nogatms2 = gc.atoms['nobnds']
        nogatms2.sort()
        self.assertEqual(nobndAtms2, nogatms2)

        # didn't compute the bondorder and didn't display them
        #self.assertEqual(len(gc.atoms['bondorder']), 0)

        # Make sure that the undo gets you back to the starting point

                
        self.mv.undo()
        # bonded
        atms3 = gc.atoms['bonded']
        atms3.sort()
        self.assertEqual(atms3, gatms)
        # not bonded
        noatms3 = gc.atoms['nobnds']
        noatms3.sort()
        self.assertEqual(noatms3, nogatms)
        self.assertEqual(len(obj.vertexSet.vertices.array), 1056)

        # bondorder
        #self.assertEqual(len(gc.atoms['bondorder']), 0)
        # test redo:
        self.mv.redo()
        gatms3 = gc.atoms['bonded']
        gatms3.sort()
        self.assertEqual(gatms3, atms2)
        self.assertEqual(len(obj.vertexSet.vertices.array), 920)


    def test_displayLines_Undo_5(self):
        """
        DisplayLines, test displayBO=True on the entire molecule.
        """
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection",
                                         "select"] ,
                               package="Pmv")
        self.mv.readMolecule("Data/4fiv.pdb")
        
        gc = self.mv.Mols[0].geomContainer
        self.mv.browseCommands("editCommands", commands=["typeBonds",],
                               package="Pmv",log=0)
        self.mv.typeBonds(self.mv.getSelection(),1, log=0)
        self.mv.displayLines(self.mv.getSelection(), negate=False,
                             displayBO=True, only=False, log=0, lineWidth=2)
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|4fiv|lines|bonded')
        
        atms = self.mv.Mols[0].allAtoms
        bonds = atms.bonds[0]
        bondsBO = bonds.get(lambda x: x.bondOrder>1)
        boAtms = bondsBO.atom1 + bondsBO.atom2
        boAtms.sort()
        gboAtms = gc.atoms['bondorder']
        gboAtms.sort()
        self.assertEqual(gboAtms, boAtms)
        self.mv.undo()
        #self.assertEqual(len(gc.atoms['bondorder']), 0)

        
    def test_displayLines_Undo_6(self):
        """
        DisplayLines only=False, negate=False, displayBO=True
        on a selection.
        """
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection",
                                         "select"] , package="Pmv")
        self.mv.readMolecule("Data/4fiv.pdb")
        gc = self.mv.Mols[0].geomContainer
        self.mv.browseCommands("editCommands", commands=["typeBonds",],
                               package="Pmv",log=0)
        self.mv.typeBonds(self.mv.getSelection(),1, log=0)
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.select("4fiv: :HOH312,GLU11,LEU10,HOH359,THR8,ARG13,ILE112,HOH357,THR31,HOH368,ARG113,HOH311,VAL115,HOH373,THR9,HOH320,HOH384,PRO14,HOH381,GLU15,LEU114,LEU29,LYS12,MET116,HOH329,HOH343", negate=False, only=False, log=0)
        self.mv.displayLines(self.mv.getSelection(), negate=False,
                             displayBO=True, only=False, log=0, lineWidth=2)
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|4fiv|lines|bonded')
        
        # The displayBO is allways applied to the whole molecule.
        atms = self.mv.Mols[0].allAtoms
        bonds = atms.bonds[0]
        bondsBO = bonds.get(lambda x: x.bondOrder>1)
        boAtms = bondsBO.atom1 + bondsBO.atom2
        boAtms.sort()
        gboAtms = gc.atoms['bondorder']
        gboAtms.sort()
        self.assertEqual(gboAtms, boAtms)
        
        self.mv.undo()
        #self.assertEqual(len(gc.atoms['bondorder']), 0)
        
        
    def test_displayLines_Undo_7(self):
        
        self.mv.setOnAddObjectCommands(['buildBondsByDistance'], log=0)
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection",
                                         "select"] , package="Pmv")

        self.mv.readMolecule("Data/4fiv.pdb")
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.select(self.mv.allAtoms[10:20])
        self.mv.displayLines(self.mv.getSelection(), negate=False,
                             displayBO=True, only=False, log=0, lineWidth=2)
        
        gc = self.mv.Mols[0].geomContainer
        atms = self.mv.Mols[0].allAtoms
        bonds = atms.bonds[0]
        bondsBO = bonds.get(lambda x: x.bondOrder>1)
        boAtms = bondsBO.atom1 + bondsBO.atom2
        boAtms.sort()
        gboAtms = gc.atoms['bondorder']
        gboAtms.sort()
        # Create a current selection. Then undisplay the lines for the current
        # selection but keep the bondOrder displayed.
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.select(self.mv.allAtoms[20:30])
        self.mv.displayLines(self.mv.getSelection(), negate=False,
                             displayBO=True, only=False, log=0, lineWidth=2)
        obj = self.mv.GUI.VIEWER.GUI.objectByName("bonded")
        self.assertEqual(obj.fullName, 'root|4fiv|lines|bonded')
        
        atmsInSel = self.mv.getSelection()
        bondsInSel = atmsInSel.bonds[0]
        BOInSel = bondsInSel.get(lambda x: x.bondOrder>1)
        BOAtmsInSel = BOInSel.atom1 + BOInSel.atom2
        

        atmsDisplayed = atms - atmsInSel
        BODisplayed = atmsDisplayed.bonds[0].get(lambda x: x.bondOrder>1)
        BODisplayed = BODisplayed.atom1+BODisplayed.atom2
        BODisplayed.sort()

        gBOatms = gc.atoms['bondorder']
        gBOatms.sort()
        self.assertEqual(gBOatms, BODisplayed)

        self.mv.undo()
        gBOatms2 = gc.atoms['bondorder']
        gBOatms2.sort()
        self.assertEqual(gBOatms2, gboAtms)

# ############################################################################
# ### DISPLAY STICKS AND BALLS TESTS
# ############################################################################

class DisplaySticksAndBalls(DisplayBaseTest):

    # invalid input for nodes
    def test_display_sticks_and_balls_invalid_input_nodes(self):
        """tests invalid input for nodes in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c = self.mv.displaySticksAndBalls
        returnValue = c("abcd")
        self.assertEqual(returnValue,None)
        
    def test_display_sticks_and_balls_empty_input_nodes(self):
        """tests empty input for nodes in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c = self.mv.displaySticksAndBalls
        returnValue = c(" ")
        self.assertEqual(returnValue,None)
        
    def test_display_sticks_and_balls_invalid_input_only(self):
        """tests invalid input for only in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c = self.mv.displaySticksAndBalls
        returnValue = c("7ins",only = 'hai')
        self.assertEqual(returnValue,None)    


    def test_display_sticks_and_balls_invalid_input_negate(self):
        """tests invalid input for negate in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c = self.mv.displaySticksAndBalls
        returnValue = c("7ins",negate = 'hai')
        self.assertEqual(returnValue,None)
                
       
    def test_display_sticks_and_balls_invalid_input_bRad(self):
        """tests invalid input for BRadii in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c = self.mv.displaySticksAndBalls
        returnValue = c("7ins",bRad = -10)
        self.assertEqual(returnValue,None)
    
           
    def test_display_sticks_and_balls_invalid_input_bscale(self):
        """tests invalid input for bscale in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c = self.mv.displaySticksAndBalls
        returnValue = c("7ins",bScale = -10)
        self.assertEqual(returnValue,None)


    def test_display_sticks_and_balls_invalid_input_cradii(self):
        """tests invalid input for CRadii in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        lsb = self.mv.displaySticksAndBalls("7ins")
        self.assertRaises(TypeError, self.mv.displaySticksAndBalls, cradius = 'hai')


    def test_display_sticks_and_balls_invalid_input_bquality(self):
        """tests invalid input for Quality in display sticks and balls
        """
        self.mv.readMolecule('Data/7ins.pdb')
        lsb = self.mv.displaySticksAndBalls("7ins")
        self.assertRaises(TypeError, self.mv.displaySticksAndBalls, bquality = 'hai')
   
    # end invalid input input for displaySticksAndBalls
    
            
    def test_displaySticksAndBalls(self):
        """tests displaySticksAndBalls
        """
        
        #import pdb;pdb.set_trace()
        
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.mv.select('1crn')
        nodes = self.mv.getSelection()
        self.mv.displaySticksAndBalls(nodes, cquality=4 , bquality=4, sticksBallsLicorice='Sticks and Balls')
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['sticks'].quality, 4)

        
        self.mv.displaySticksAndBalls(nodes, cradius=.50)
        self.mv.Mols[0].geomContainer.geoms['sticks'].Set(radii=.50)
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['sticks'].vertexSet.radii.array[0], .50 )
        #self.assertEqual(self.mv.Mols[0].geomContainer.geoms['sticks'].radii, .50 )
        self.mv.displaySticksAndBalls(nodes, bRad=0.25)
        self.mv.Mols[0].geomContainer.geoms['balls'].Set(radii=.50)
        if self.mv.hasGui:
            self.mv.GUI.VIEWER.Redraw()
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['balls'].radius, .50 )
        #self.assertEqual(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet.radii.array[0], .50 )
        #self.assertEqual(self.mv.Mols[0].geomContainer.geoms['balls'].radii, .50 )
    
    def test_displaysticksballs_emptyviewer(self):
        """
        __call__ on empty viewer
        """
        # here we test if the command can be called with nodes arg only
        if self.mv.displaySticksAndBalls.flag & 1:
            self.mv.displaySticksAndBalls(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.displaySticksAndBalls cannot be called with only self.mv.getSelection()")

    def test_displaysticksballs_bug33(self):
        """
        Test written for the bug #33 reported in MGLBUGZILLA by Daniel.
        only does not work when multiple molecule and selection only on
        one mol.
        """
        # Reading the test molecules
        self.mv.readMolecule('Data/7ins.pdb')
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        # not bondsBOTest body
        self.mv.setSelectionLevel(Chain, KlassSet=None, log=0)
        self.mv.select(self.mv.Mols[0].chains[0].residues[1:15])
        nodes = self.mv.getSelection()
        self.mv.displaySticksAndBalls(nodes, only=1)
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['sticks'],
                         nodes.atoms.uniq())
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['balls'],
                         nodes.atoms.uniq())

        self.assertEqual(len(self.mv.Mols[1].geomContainer.atoms['sticks']), 0)
        self.assertEqual(len(self.mv.Mols[1].geomContainer.atoms['balls']),0)

    def test_displaysticksballs_Undo_1(self):
        """
        Test displaysticksballs with negate=False, only=False
        no other SticksAndBalls displayed
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        # Test a simple display SticksAndBalls
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by SticksAndBalls
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        sticks = self.mv.GUI.VIEWER.GUI.objectByName("sticks")
        self.assertFalse(sticks == None)
        balls = self.mv.GUI.VIEWER.GUI.objectByName("balls")
        self.assertFalse(balls == None)
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['balls']
        gatms.sort()
        self.assertEqual(gatms, atms)
        
        self.mv.undo()
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls']), 0)
        self.assertEqual(sticks.visible, False)
        self.assertEqual(balls.visible, False)

        self.mv.redo()
        self.assertFalse(len(self.mv.Mols[0].geomContainer.atoms['balls']) == 0)
        self.assertEqual(sticks.visible, True)
        self.assertEqual(balls.visible, True)
        
    def test_displaysticksballs_Undo_2(self):
        """
        DisplaySticksAndBalls negate=False, only=False
        With SticksAndBalls already displayed but not selected any longer.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by SticksAndBalls and clearSelection
        
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        sticks = self.mv.GUI.VIEWER.GUI.objectByName("sticks")
        self.assertFalse(sticks == None)
        balls = self.mv.GUI.VIEWER.GUI.objectByName("balls")
        self.assertFalse(balls == None)
        nstvts1 = len(sticks.vertexSet.vertices.array)
        nblvts1 = len(balls.vertexSet.vertices.array)
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        gatms.sort()
        self.assertEqual(gatms, atms)
        self.mv.clearSelection()
        # 2- select another bunch of nodes
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("","","35-40", "", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        nstvts2 = len(sticks.vertexSet.vertices.array)
        nblvts2 = len(balls.vertexSet.vertices.array)
        atms = self.mv.getSelection()[:].atoms
        atms = atms + gatms
        atms.sort()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['balls']
        gatms2.sort()
        self.assertEqual(gatms2, atms)

        # 3- Undo
##         undoCmd = ('displaySticksAndBalls("1crn: :ILE35,PRO36,GLY37,ALA38,THR39,CYS40", topCommand=0, setupUndo=0, log=1, scaleFactor=1.0, negate=True, busyIdle=0, redraw=True, quality=10)\n', 'displaySticksAndBalls')
##         self.assertEqual(self.mv.undoCmdStack[-1], undoCmd)
        self.mv.undo()
        ngatms = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        ngatms.sort()
        self.assertEqual(ngatms, gatms)
        nstvts3 = len(sticks.vertexSet.vertices.array)
        nblvts3 = len(balls.vertexSet.vertices.array)
        self.assertEqual(nstvts3, nstvts1)
        self.assertEqual(nblvts3, nblvts1)
        self.mv.redo()
        nstvts4 = len(sticks.vertexSet.vertices.array)
        nblvts4 = len(balls.vertexSet.vertices.array)
        self.assertEqual(nstvts4, nstvts2)
        self.assertEqual(nblvts4, nblvts2)
        
#display SticksAndBalls

    def test_displaySticksAndBalls_Undo_3(self):
        """
        DisplaySticksAndBalls negate=False, only=True when other SticksAndBalls displayed but
        not selected.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by SticksAndBalls and clearSelection
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        self.mv.readMolecule("Data/1bsr.pdb")
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        gatms.sort()
        self.assertEqual(gatms, atms)
        self.mv.clearSelection()
        # 2- select another bunch of nodes
        self.mv.selectFromString("","","35-40", "", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection(), only=True)
        sticks = self.mv.GUI.VIEWER.GUI.objectByName("sticks")
        self.assertFalse(sticks == None)
        balls = self.mv.GUI.VIEWER.GUI.objectByName("balls")
        self.assertFalse(balls == None)
        nstvts1 = len(sticks.vertexSet.vertices.array)
        nblvts1 = len(balls.vertexSet.vertices.array)
        
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['balls']+self.mv.Mols[1].geomContainer.atoms['balls']
        gatms2.sort()
        self.assertEqual(gatms2, atms)

        # 3- Undo
##         undoCmd = ('displaySticksAndBalls("1crn: :SER11,ASN12,PHE13,ASN14,VAL15,CYS16,ARG17,LEU18,PRO19,GLY20", topCommand=0, setupUndo=0, log=1, scaleFactor=1.0, negate=True, busyIdle=0, redraw=True, quality=10)\n', 'displaySticksAndBalls')
##         self.assertEqual(self.mv.undoCmdStack[-1], undoCmd)
        self.mv.undo()
        nstvts2 = len(sticks.vertexSet.vertices.array)
        nblvts2 = len(balls.vertexSet.vertices.array)
        ngatms = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        ngatms.sort()
        self.assertEqual(ngatms, gatms)
        self.assertTrue(nstvts2 != nstvts1)
        self.assertTrue(nblvts2 != nblvts1)
        self.mv.redo()
        nstvts3 = len(sticks.vertexSet.vertices.array)
        nblvts3 = len(balls.vertexSet.vertices.array)
        self.assertTrue(nstvts3 == nstvts1)
        self.assertTrue(nblvts3 == nblvts1)
        

    def test_displaySticksAndBalls_Undo_4(self):
        """
        DisplaySticksAndBalls, only=True, negate=False but no SticksAndBalls displayed.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        # 2- Display them by SticksAndBalls and clearSelection
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        sticks = self.mv.GUI.VIEWER.GUI.objectByName("sticks")
        self.assertFalse(sticks == None)
        balls = self.mv.GUI.VIEWER.GUI.objectByName("balls")
        self.assertFalse(balls == None)
        self.assertTrue(sticks.visible == True)
        self.assertTrue(balls.visible == True)
        gatms = self.mv.Mols[0].geomContainer.atoms['balls']
        gatms.sort()
        self.mv.displaySticksAndBalls(self.mv.getSelection(), negate=True)
        self.assertTrue(sticks.visible == False)
        self.assertTrue(balls.visible == False)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls']), 0)

        self.mv.undo()
        self.assertTrue(sticks.visible == True)
        self.assertTrue(balls.visible == True)
        gatms2 = self.mv.Mols[0].geomContainer.atoms['balls']
        gatms2.sort()
        self.assertEqual(gatms2, atms)
        self.assertEqual(gatms, gatms2)

        self.mv.redo()
        self.assertTrue(sticks.visible == False)
        self.assertTrue(balls.visible == False)
        

    def test_displaySticksAndBalls_Undo_5(self):
        """
        DisplaySticksAndBalls, only=True, negate=False but no SticksAndBalls displayed.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        # 2- Display them by SticksAndBalls and clearSelection
        self.mv.displaySticksAndBalls(self.mv.getSelection(), negate=True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls']), 0)

        self.mv.undo()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        gatms2.sort()
        self.assertEqual(len(gatms2), 0)
   
    def test_displaySticksAndBalls_Undo_6(self):
        """
        DisplaySticksAndBalls negate=True, no SticksAndBalls displayed
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11,15", "CA,O", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        gc = self.mv.Mols[0].geomContainer
        self.assertEqual(len(gc.atoms['balls']), 4)
        self.mv.clearSelection()
        
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "12,13","", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        self.assertEqual(len(gc.atoms['balls']), 23)
        self.mv.clearSelection()

        gatms1 = self.mv.Mols[0].geomContainer.atoms['balls'][:]
       
        self.mv.selectFromString("", "", "11-20","", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection())
#        self.assertEqual(len(gc.atoms['balls']), 100)
        self.mv.undo()
        self.assertEqual(len(gc.atoms['balls']), 23)

        gatms = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        gatms.sort()
        gatms1.sort()
        self.assertEqual(len(gatms), len(gatms1))
        self.assertEqual(gatms, gatms1)

        
    def test_displaySticksAndBalls_Undo_7(self):
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11,15", "CA,O", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection())
        self.mv.clearSelection()
        gatms1 = self.mv.Mols[0].geomContainer.atoms['balls'][:]
       
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20","", negate=False)
        self.mv.displaySticksAndBalls(self.mv.getSelection(), negate=True)
        self.mv.undo()

        gatms = self.mv.Mols[0].geomContainer.atoms['balls'][:]
        gatms.sort()
        gatms1.sort()
        self.assertEqual(gatms, gatms1)

## ############################################################################
## ### DISPLAY CPK TESTS
## ############################################################################
class DisplayCPK(DisplayBaseTest):

    def test_displaycpk_invalid_input_nodes(self):
        """tests invalid input for displaycpk ,nodes
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c=self.mv.displayCPK
        returnValue =c("abcd")
        self.assertEqual(returnValue,None)
    
    def test_displaycpk_invalid_input_only(self):
        """tests invalid input for displaycpk only
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c=self.mv.displayCPK
        returnValue =c("7ins",only = 'abcd')
        self.assertEqual(returnValue,None)
        
    def test_displaycpk_invalid_input_negate(self):
        """tests invalid input for displaycpk negate
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c=self.mv.displayCPK
        returnValue =c("7ins",negate = 'abcd')
        self.assertEqual(returnValue,None)     
         
    def test_displaycpk_invalid_input_ScaleFactor(self):
        """tests invalid input for displaycpk
        ScaleFactor
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c=self.mv.displayCPK
        returnValue=c("7ins",scaleFactor = 'abcd')
        #self.assertEqual(returnValue,ValueError)
        self.assertEqual(returnValue, 'ERROR')
        
    def test_displaycpk_invalid_input_quality(self):
        """tests invalid input for displaycpk quality
        """
        self.mv.readMolecule('Data/7ins.pdb')
        c=self.mv.displayCPK
        returnValue =c("7ins",quality = -5)
        self.assertEqual(returnValue,None)
        
     
    def test_displaycpk_empty_viewer(self):
        """
        __call__ on empty viewer
        """
        # here we test if the command can be called with nodes arg only
        if self.mv.displayCPK.flag & 1   :
            self.mv.displayCPK(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.displayCPK cannot be called with only self.mv.getSelection()")

    def test_displaycpk_bug33(self):
        """
        Test written for the bug #33 reported in MGLBUGZILLA by Daniel. only does
        not work when multiple molecule and selection only on one mol.
        """
        # Reading the test molecules
        self.mv.readMolecule('Data/7ins.pdb')
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.displayCPK(self.mv.getSelection())

        # Test body
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.select(self.mv.Mols[0].chains[0].residues[1:15])
        nodes = self.mv.getSelection()
        self.mv.displayCPK(nodes, only=1)
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['cpk'].sort(),
                         nodes.atoms.uniq().sort())
        self.assertEqual(len(self.mv.Mols[1].geomContainer.atoms['cpk']),0)


    def test_displayCPKWithVariousScaleFactors(self):
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString('', '', '1-10', 'CA', negate=False,
                                 silent=True)
        self.mv.displayCPK(self.mv.getSelection(), scaleFactor=2.0, quality=10)
        self.mv.clearSelection()
        self.mv.selectFromString('', '', '11-20', 'CA', negate=False,
                                 silent=True)
        self.mv.displayCPK(self.mv.getSelection(), scaleFactor=.5, quality=10)
        self.mv.displayCPK('1crn', negate=1)
        self.mv.displayCPK('1crn')

    def test_displayCPKAssignRadii_1(self):
        self.mv.readMolecule('Data/1crn.pdb')
        # no radius assigned
        nodes = self.mv.getSelection()
        wrad = filter(lambda x: hasattr(x, 'radius'), self.mv.allAtoms)
        self.assertEqual(len(wrad), 0)
        self.mv.displayCPK(nodes)
        wrad = filter(lambda x: hasattr(x, 'radius'), self.mv.allAtoms)
        self.assertEqual(len(wrad),len(self.mv.allAtoms))
        self.assertEqual(self.mv.Mols[0].unitedRadii,1)
        self.mv.displayCPK(nodes, negate=1)
        #self.mv.assignAtomsRadii("1crn", united=0, overwrite=1)
        self.mv.displayCPK(nodes, unitedRadii=0)
        self.assertEqual(self.mv.Mols[0].unitedRadii,0)

    
        

    def test_displayCPK_Undo_1(self):
        """
        Test displayCPK with negate=False, only=False
        no other cpk displayed
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        # Test a simple display CPK
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by CPK
        self.mv.displayCPK(self.mv.getSelection())
        
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['cpk']
        gatms.sort()
        self.assertEqual(gatms, atms)
        
        self.mv.undo()
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk']), 0)
        
        

    def test_displayCPK_Undo_2(self):
        """
        DisplayCPK negate=False, only=False
        With CPK already displayed but not selected any longer.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by CPK and clearSelection
        self.mv.displayCPK(self.mv.getSelection())
        
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        gatms.sort()
        self.assertEqual(gatms, atms)
        self.mv.clearSelection()
        # 2- select another bunch of nodes
        self.mv.selectFromString("","","35-40", "", negate=False)
        self.mv.displayCPK(self.mv.getSelection())

        atms = self.mv.getSelection()[:].atoms
        atms = atms + gatms
        atms.sort()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['cpk']
        gatms2.sort()
        self.assertEqual(gatms2, atms)

        # 3- Undo
##         undoCmd = ('displayCPK("1crn: :ILE35,PRO36,GLY37,ALA38,THR39,CYS40", topCommand=0, setupUndo=0, log=1, scaleFactor=1.0, negate=True, busyIdle=0, redraw=True, quality=10)\n', 'displayCPK')
##         self.assertEqual(self.mv.undoCmdStack[-1], undoCmd)
        self.mv.undo()
        ngatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        ngatms.sort()
        self.assertEqual(ngatms, gatms)
        
    def test_displayCPK_Undo_3(self):
        """
        DisplayCPK negate=False, only=True when other CPK displayed but
        not selected.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        # 2- Display them by CPK and clearSelection
        self.mv.displayCPK(self.mv.getSelection())
        
        self.mv.readMolecule("Data/1bsr.pdb")
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        gatms.sort()
        self.assertEqual(gatms, atms)
        self.mv.clearSelection()
        # 2- select another bunch of nodes
        self.mv.selectFromString("","","35-40", "", negate=False)
        self.mv.displayCPK(self.mv.getSelection(), only=True)
        
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['cpk']+self.mv.Mols[1].geomContainer.atoms['cpk']
        gatms2.sort()
        self.assertEqual(gatms2, atms)

        # 3- Undo
##         undoCmd = ('displayCPK("1crn: :SER11,ASN12,PHE13,ASN14,VAL15,CYS16,ARG17,LEU18,PRO19,GLY20", topCommand=0, setupUndo=0, log=1, scaleFactor=1.0, negate=True, busyIdle=0, redraw=True, quality=10)\n', 'displayCPK')
##         self.assertEqual(self.mv.undoCmdStack[-1], undoCmd)
        self.mv.undo()
        ngatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        ngatms.sort()
        self.assertEqual(ngatms, gatms)

    def test_displayCPK_Undo_4(self):
        """
        DisplayCPK, only=True, negate=False but no CPK displayed.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        # 2- Display them by CPK and clearSelection
        self.mv.displayCPK(self.mv.getSelection())
        gatms = self.mv.Mols[0].geomContainer.atoms['cpk']
        gatms.sort()
        self.mv.displayCPK(self.mv.getSelection(), negate=True)

        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk']), 0)

        self.mv.undo()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['cpk']
        gatms2.sort()
        self.assertEqual(gatms2, atms)
        self.assertEqual(gatms, gatms2)
        

    def test_displayCPK_Undo_5(self):
        """
        DisplayCPK, only=True, negate=False but no CPK displayed.
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        if self.mv.Mols[0].geomContainer.atoms.has_key('cpk'):
            self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk']), 0)
        # 1- Select a bunch of nodes
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        atms = self.mv.getSelection()[:].atoms
        atms.sort()
        # 2- Display them by CPK and clearSelection
        self.mv.displayCPK(self.mv.getSelection(), negate=True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk']), 0)
        self.mv.undo()
        gatms2 = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        gatms2.sort()
        self.assertEqual(len(gatms2), 0)
   
    def test_displayCPK_Undo_6(self):
        """
        DisplayCPK negate=True, no CPK displayed
        """
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11,15", "CA,O", negate=False)
        self.mv.displayCPK(self.mv.getSelection())
        self.mv.clearSelection()
        
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "12,13","", negate=False)
        self.mv.displayCPK(self.mv.getSelection())
        self.mv.clearSelection()

        gatms1 = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
       
        self.mv.selectFromString("", "", "11-20","", negate=False)
        self.mv.displayCPK(self.mv.getSelection())
        self.mv.undo()

        gatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        gatms.sort()
        gatms1.sort()
        self.assertEqual(gatms, gatms1)

        
    def test_displayCPK_Undo_7(self):
        self.mv.browseCommands("selectionCommands",
                               commands=["selectFromString","clearSelection"],
                               package="Pmv")
        self.mv.readMolecule("Data/1crn.pdb")
        # 1- Select a bunch of nodes
        from MolKit.molecule import Atom
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11,15", "CA,O", negate=False)
        self.mv.displayCPK(self.mv.getSelection())
        self.mv.clearSelection()
        gatms1 = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
       
        from MolKit.protein import Residue
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20","", negate=False)
        self.mv.displayCPK(self.mv.getSelection(), negate=True)
        self.mv.undo()

        gatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
        gatms.sort()
        gatms1.sort()
        self.assertEqual(gatms, gatms1)

##     def test_displayCPK_scaleFactor_1(self):
##         self.mv.browseCommands("selectionCommands",
##                                commands=['selectFromString',
##                                          'clearSelection'],
##                                package="Pmv")
        
##         self.mv.readMolecule("Data/1crn.pdb")
##         # 1- Select some nodes
##         from MolKit.molecule import Atom
##         self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
##         self.mv.selectFromString("", "", "20-25", "*", negate=False)
##         self.mv.displayCPK(self.mv.getSelection())
##         self.mv.clearSelection()
        
##         self.mv.selectFromString("", "", "1-6", "*", negate=False)
##         self.mv.displayCPK(self.mv.getSelection(), scaleFactor=1.8)
##         self.mv.clearSelection()

##         gatms = self.mv.Mols[0].geomContainer.atoms['cpk'][:]
##         g = self.mv.Mols[0].geomContainer.geoms['cpk']
##         grad = g.vertexSet.radii.array[:,0]
##         import numpy
##         scaleList = grad/numpy.array(gatms.radius)
        
        #self.assertEqual(unscaledgrad.tolist(), gatms.radius)
        
    def test_displayCPK_ByProperty(self):
        self.mv.clearSelection()
        self.mv.readMolecule('Data/1crn.pdb')
        nodes = self.mv.getSelection()
        sc = 1.4
        offset = 0.5
        self.mv.displayCPK("1crn", log=0, scaleFactor=sc, only=False, cpkRad=offset, quality=0,
                           negate=False, byproperty=True, propertyName='bondOrderRadius',
                           propertyLevel = 'Atom')
        if self.mv.hasGui:
            obj = self.mv.GUI.VIEWER.GUI.objectByName('cpk')
        else:
            obj = self.mv.Mols[0].geomContainer.geoms['cpk']
        minrad = min(obj.vertexSet.radii.array.ravel())
        maxrad = max(obj.vertexSet.radii.array.ravel())
        propvals = self.mv.displayCPK.getPropValues(nodes, 'bondOrderRadius', 'Atom')
        #print "propvals, min, max:", min(propvals), max(propvals)
        minval = min(propvals)*sc+offset
        maxval = max(propvals)*sc+offset
        #print minval, minrad, maxval, maxrad
        self.assertAlmostEqual(minval, minrad, 3)
        self.assertAlmostEqual(maxval, maxrad, 3)
        gatms = mv.Mols[0].geomContainer.atoms['cpk'][:]
        self.assertEqual(len(gatms), 327)
        self.mv.undo()
        gatms = mv.Mols[0].geomContainer.atoms['cpk'][:]
        self.assertEqual(len(gatms), 0)

## ############################################################################
## ### UNDISPLAYLINES TESTS
## ############################################################################

class DisplayTests(DisplayBaseTest):
    
    def test_undisplay_lines_invalid_nodes(self):
        """tests undisplay lines ,nodes invalid input
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c = self.mv.undisplayLines
        returnValue = c("abcd")
        self.assertEqual(returnValue ,None)
        
        
    def test_undisplay_lines_empty_nodes(self):
        """tests undisplay lines ,nodes empty input
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        c = self.mv.undisplayLines
        returnValue = c(" ")
        self.assertEqual(returnValue ,None)
        
    def test_undisplay_lines_nodes(self):
        """tests undisplay lines
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.undisplayLines("1crn")
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['bonded'].vertexSet),0)
        
    def test_undisplay_lines_nodes_get_Selection(self):
        """ tests undisplay lines input through get selection
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.select(self.mv.allAtoms[10:20])
        self.mv.undisplayLines(self.mv.allAtoms[10:20])
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lines'].vertexSet),0)     
        
    
## ############################################################################
## ### UNDISPLAYSTICKSANDBALLS TESTS
## ############################################################################


    
    def test_undisplay_sticks_balls_invalid_nodes(self):
        """tests undisplay sticks and balls ,nodes invalid input
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displaySticksAndBalls("1crn")
        c=self.mv.undisplaySticksAndBalls
        returnValue = c("abcd")
        self.assertEqual(returnValue ,None)
        
    def test_undisplay_sticks_balls_empty_nodes(self):
        """tests undisplay sticks and balls ,nodes empty input
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displaySticksAndBalls("1crn")
        c = self.mv.undisplaySticksAndBalls
        returnValue = c(" ")
        self.assertEqual(returnValue ,None)
        
    def test_undisplay_sticks_balls_nodes(self):
        """tests undisplay sticks and balls 
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displaySticksAndBalls("1crn")
        self.mv.undisplaySticksAndBalls("1crn")
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls'].vertexSet),0)

    def test_undisplay_sticks_balls_nodes_get_Selection(self):
        """tests undisplay sticks and balls input through get selection
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displaySticksAndBalls(self.mv.Mols[0])
        len_old_geoms = len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet)
        #undisplaying 10 sticks and balls
        self.mv.undisplaySticksAndBalls(self.mv.allAtoms[10:20])
        len_new_geoms = len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet)
        #by default undisplayed will be displayed as lines
        self.assertEqual(len_old_geoms - len_new_geoms,10)

## ############################################################################
## ### UNDISPLAYCPK TESTS
## ############################################################################
    
    def test_undisplay_cpk_invalid_nodes(self):
        """ tests undisplay cpk ,nodes invalid input
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displayCPK("1crn")
        c = self.mv.undisplayCPK
        returnValue = c("dfg")
        self.assertEqual(returnValue ,None)
        
    def test_undisplay_cpk_empty_nodes(self):
        """ tests undisplay cpk ,nodes empty input
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displayCPK("1crn")
        c = self.mv.undisplayCPK
        returnValue = c(" ")
        self.assertEqual(returnValue ,None)
        
    def test_undisplay_cpk_nodes(self):
        """ tests undisplay cpk 
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.select("1crn")
        self.mv.displayCPK("1crn")
        self.mv.undisplayCPK("1crn")
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk'].vertexSet),0)
        
            
    def test_undisplay_cpk_nodes_get_Selection(self):
        """ tests undisplay cpk input through get selection
        """
        self.mv.readMolecule("./Data/1crn.pdb")
        self.mv.displayCPK("1crn")
        len_old_geoms = len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet)
        #undisplaying 10 cpk
        self.mv.undisplayCPK(self.mv.allAtoms[10:20])
        len_new_geoms = len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet)
        #by default undisplayed will be displayed as lines
        self.assertEqual(len_old_geoms - len_new_geoms,10)

## ############################################################################
## ### SHOWMOLECULES TESTS
## ############################################################################


    def test_showmolecules_emptyviewer(self):
        """
        __call__ on empty viewer
         """
        if self.mv.hasGui:
            from exceptions import ValueError
            if self.mv.showMolecules.flag & 1:
                self.mv.showMolecules(self.mv.getSelection())
            else:
                print "Cannot be called with empty selection"


    def test_showmolecules_visible(self):
        """tests show molecules
        """
        if self.mv.hasGui:
            self.mv.readMolecule("./Data/hsg1.pdb")
            import time
            time.sleep(5)
            self.mv.showMolecules(["hsg1",])
            self.assertEqual(self.mv.Mols[0].geomContainer.geoms['master'].visible,1)

    def test_show_molecules_negate_1(self):
        """tests show molecules negate = 1
        """
        if self.mv.hasGui:
            self.mv.readMolecule("./Data/1crn.pdb")
            self.mv.showMolecules(["1crn",], negate=1)
            self.assertEqual(self.mv.Mols[0].geomContainer.geoms['master'].visible,False)
        
        
    def test_show_molecules_nodes_invalid_input(self):
        """tests show molecules,nodes invalid input
        """
        if self.mv.hasGui:
            self.mv.readMolecule("./Data/1crn.pdb")
            c = self.mv.showMolecules
            returnValue = c("abcd")
            self.assertEqual(returnValue,None)
        
    def test_show_molecules_node_empty_input(self):
        """tests show molecules,nodes empty input
        """
        if self.mv.hasGui:
            self.mv.readMolecule("./Data/1crn.pdb")
            c = self.mv.showMolecules
            returnValue = c(" ")
            self.assertEqual(returnValue,None)
        
    def test_show_molecules_negate_invalid(self):
        """tests show molecules negate,invalid input
        """
        if self.mv.hasGui:
            self.mv.readMolecule("./Data/1crn.pdb")
            c = self.mv.showMolecules
            returnValue = c("1crn",negate = 'hai')
            self.assertEqual(returnValue,None)

##########################################################################
#       DISPLAY BACKBONETRACE
##########################################################################

    def test_display_backbonetrace(self):
        """checks displaying backbone trace"""
        self.mv.readMolecule("Data/test_barrel_1_2_hbonds.pdb")
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.displayBackboneTrace("test_barrel_1_2_hbonds:::")
        self.assertEqual(str(self.mv.Mols[0].geomContainer.geoms['CAballs']),'<DejaVu.Spheres.GLUSpheres> CAballs with 14 vertices')
        self.assertEqual(str(mv.Mols[0].geomContainer.geoms['CAsticks']),'<DejaVu.Cylinders.Cylinders> CAsticks with 14 vertices and 12 faces')
        
        
#    def test_display_backbonetrace_invalid_input(self):
#        """checks cmd with invalid input"""
#        self.mv.readMolecule("Data/test_barrel_1_2_hbonds.pdb")
#        returnValue =self.mv.displayBackboneTrace("ABCD")
#        self.assertEqual(returnValue,'ERROR')
#
#    def test_display_backbonetrace_empty_input(self):
#        """checks cmd with empty input"""
#        self.mv.readMolecule("Data/test_barrel_1_2_hbonds.pdb")
#        returnValue =self.mv.displayBackboneTrace(" ")
#        self.assertEqual(returnValue,'ERROR')        
        
    def test_display_backbonetrace_get_selection(self):
         """checks displaying backbone trace for selected atoms"""
         self.mv.readMolecule("Data/test_barrel_1_2_hbonds.pdb")
         self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
         ats =self.mv.Mols[0].allAtoms[20:40]
         self.mv.select(ats)
         #self.mv.getSelection(ats)
         self.mv.displayBackboneTrace(self.mv.getSelection(),only=True)
         self.assertEqual(str(self.mv.Mols[0].geomContainer.geoms['CAballs']),'<DejaVu.Spheres.GLUSpheres> CAballs with 2 vertices')
         self.assertEqual(str(mv.Mols[0].geomContainer.geoms['CAsticks']),'<DejaVu.Cylinders.Cylinders> CAsticks with 2 vertices and 1 faces')

    def test_display_backbonetrace_two_mols(self):
        """checks displaying backbone trace for two molecules"""
        self.mv.readMolecule("Data/test_barrel_1_2_hbonds.pdb")
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setSelectionLevel(Molecule, KlassSet=None, log=0)
        self.mv.displayBackboneTrace("test_barrel_1_2_hbonds;hsg1", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.assertEqual(str(self.mv.Mols[1].geomContainer.geoms['CAballs']),'<DejaVu.Spheres.GLUSpheres> CAballs with 198 vertices')
        self.assertEqual(str(self.mv.Mols[1].geomContainer.geoms['CAsticks']),'<DejaVu.Cylinders.Cylinders> CAsticks with 198 vertices and 196 faces')
        
        
    def test_display_backbonetrace_negate(self):
        """checks displaying backbone trace when negate = true"""
        self.mv.readMolecule("Data/test_barrel_1_2_hbonds.pdb")
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.displayBackboneTrace("test_barrel_1_2_hbonds:::",negate=True)
        self.assertEqual(str(self.mv.Mols[0].geomContainer.geoms['CAballs']),'<DejaVu.Spheres.GLUSpheres> CAballs with 184 vertices')
        self.assertEqual(str(mv.Mols[0].geomContainer.geoms['CAsticks']),'<DejaVu.Cylinders.Cylinders> CAsticks with 184 vertices and 0 faces')


    def test_display_backbone_trace_gap_residues(self):
        """checks displaying backbbone trace for molecule with gaps"""
        self.mv.readMolecule("Data/barrel_1_2_test.pdb")
        self.mv.setSelectionLevel(Atom, KlassSet=None, log=0)
        self.mv.displayBackboneTrace("barrel_1_2_test:::")
        self.assertEqual(str(self.mv.Mols[0].geomContainer.geoms['CAsticks']),'<DejaVu.Cylinders.Cylinders> CAsticks with 7 vertices and 3 faces')
        self.assertEqual(str(self.mv.Mols[0].geomContainer.geoms['CAballs']),'<DejaVu.Spheres.GLUSpheres> CAballs with 7 vertices')



class DispalyCommandsLogTests(DisplayBaseTest):

    def test_displayLines_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/7ins.pdb')
        self.mv.displayLines("7ins",lineWidth = 1)
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displayLines("7ins", negate=False, displayBO=False, only=False, log=0, lineWidth=1)')


    def test_displayLines_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/7ins.pdb')    
        oldself =self
        self=mv
        s = 'self.displayLines("7ins", negate=False, displayBO=False, only=False, log=0, lineWidth=1)'
        exec(s)
        self=oldself
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['bonded'].vertexSet)>0,True)

    def test_display_sticksandballs_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/7ins.pdb')
        nodes=self.mv.getSelection()
        self.mv.displaySticksAndBalls(nodes)
        if self.mv.hasGui:        
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displaySticksAndBalls("7ins:::", log=0, cquality=0, sticksBallsLicorice=\'Licorice\', bquality=0, cradius=0.2, setScale=True, only=False, bRad=0.3, negate=False, bScale=0.0)')
        

    def test_display_sticksandballs_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/7ins.pdb')    
        oldself =self
        self=mv
        s = 'self.displaySticksAndBalls("7ins:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)'
        exec(s)
        self=oldself
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['sticks'].vertexSet)>0,True)

    def test_display_cpk_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/7ins.pdb')
        nodes=self.mv.getSelection()
        self.mv.displayCPK(nodes)
        if self.mv.hasGui:        
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')  
            self.assertEqual(split(last_entry,'\n')[0],'self.displayCPK("7ins", log=0, cpkRad=0.0, scaleFactor=1.0, setScale=True, only=False, unitedRadii=True, negate=False, quality=0)')
            

    def test_display_CPK_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/7ins.pdb')    
        oldself =self
        self=mv
        s = 'self.displayCPK("7ins", log=0, cpkRad=0.0, scaleFactor=1.0, setScale=True, only=False, negate=False, quality=10)'
        exec(s)
        self=oldself
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet)>0,True)


    def test_display_BackBoneTrace_log_checks_expected_log_string(self):
        """checks expected log string is written"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/test_barrel_1_2_hbonds.pdb')
        nodes=self.mv.getSelection()
        self.mv.displayBackboneTrace("test_barrel_1_2_hbonds:::")
        if self.mv.hasGui:        
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')  
            self.assertEqual(split(last_entry,'\n')[0],'self.displayBackboneTrace("test_barrel_1_2_hbonds:::", log=0, cquality=0, sticksBallsLicorice=\'Licorice\', bquality=0, cradius=0.2, setScale=True, only=False, bRad=0.3, negate=False, bScale=0.0)')   
            
    def test_display_BackBoneTrace_log_checks_that_it_runs(self):
        """Checking log string runs
        """
        self.mv.readMolecule('Data/test_barrel_1_2_hbonds.pdb')
        oldself =self
        self=mv
        s = 'self.displayBackboneTrace("test_barrel_1_2_hbonds:::", log=0, cquality=0, sticksBallsLicorice=\'Sticks and Balls\', bquality=0, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)'
        exec(s)
        self=oldself
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['CAsticks'].vertexSet)>0,True)

if __name__ == '__main__':
    unittest.main()
