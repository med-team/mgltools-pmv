hasGUI = True
ignore = {'test_Misc':['test_SourceLogBug_1','test_SourceLogBug_2',
                       'test_SourceLogBug_3'],
          'test_msmsCommands':['test_coreDump'],
          'test_fileCommands':['test_ReadMolecule_7'],
          'test_trajectoryCommands': [],
          #FIX THIS: ignore interactive commands tests - they freeze
          # on Linux 32-bit and MacOSX platforms
          'test_interactiveCommands':[],
          # amber commands are removed from CVS:
          'test_Amber94MD':[],
          'test_Amber94Minimize':[],
          'test_amberSetUp':[],
          'test_ConstrainAtomsAmber94':[],
          'test_FixAmberNames':[],
          'test_FreezeAtoms_Amber94':[],
          'test_PlayMDTrjAmber94':[],
          #'test_dependencies' : []
     }
import os
import sys
if os.name != 'nt': #sys.platform != "win32":
    uname = os.uname()
    if uname[0] == 'Darwin' and uname[2][0] == '7':
        ignore['test_APBSCommands'] = []
    elif uname[0] == 'Darwin' and uname[-1] == 'i386':
        ignore['test_hbondCommands'] = []
        # the laptop "mslaptop1" freezes on this test
    elif uname[-1] == 'x86_64':
        #ignore['test_PlayMDTrjAmber94'] = [] 
        ignore['test_videoCommands'] = []
else:
    ignore['test_splineCommands']=[] # always crashes on Windows
    ignore['test_pmvscript'] = [] # Fix this.
    #Results in "Py_Eval_RestoreThread:Null tstate" error when Python2.6 interpeter exits.

if not hasGUI:
    ignore['test_interactiveCommands'] = []
    ignore['test_measureCommands'] = []
    ignore['test_videoCommands'] = []
    ignore['test_setangleCommands'] = []
    ignore['test_selectionCommands'] = ['Pmvselection_SelectInSphereSelectTests',
                                        'Pmvselection_SelectFromStringGUITests']
    ignore['test_Misc']=[]
    ignore['test_APBSCommands']=[]
    ignore['test_gridCommands']=[]
    ignore['test_fileCommands']=['WriteVRML', 'WriteSTL']



# the following two functions are used by tests that check Pmv log strings: 

def getArgs(s):
    # takes a command log string and returns a list of command arguments
    
    # find text between opening and closing parenthesis
    args = s[s.index('(')+1:s.rindex(')')]
    # split on  ,
    argList = args.split(',')
    args = {}
    # for each argument
    for arg in argList:
        # if no equal set value to None
        if arg.find('=')==-1:
            args[arg] = None
        else: # split on equal
            k,v = arg.split('=')
            args[k] = v
    return args


def compareArguments(s1,s2):
    # compares arguments returned by getArgs()
    d1 = getArgs(s1)
    d2 = getArgs(s2)
    assert len(d1)==len(d2)
    for k,v in d2.items():
        if d1[k] != v:
            return False
    return True 


