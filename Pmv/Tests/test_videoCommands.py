import unittest,sys,os,time
import stat
mv = None
ct=0
totalCt=4

class videoComTest(unittest.TestCase):
    """Base class for grid3DCommands unittest"""
    def startViewer(self):
        """start Viewer"""

        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                withShell=0, trapExceptions=False)
            #mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.setUserPreference(('trapExceptions', '0'), log = 0)
            mv.loadModule('dejaVuCommands', 'ViewerFramework')
            mv.browseCommands("fileCommands",commands=['readMolecule',],package= 'Pmv')
            mv.browseCommands("colorCommands",package= 'Pmv')
            mv.browseCommands("bondsCommands", commands=["buildBondsByDistance",],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands('displayCommands',package='Pmv')
            mv.readMolecule('Data/1crn.pdb')
            self.mv = mv 

    def setUp(self):
        """set-up"""
        global mv
        if mv is None:
        #if not hasattr(self, 'mv'):
            self.startViewer()
        else:
           self.mv = mv

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv


    def test_01(self):
        """ Test guiCallback of the command"""
        self.failUnless(hasattr(self.mv, "videoCommand"))
        cmd = self.mv.videoCommand
        cmd.guiCallback()
        self.failUnless(cmd.recorder != None)
        self.failUnless(cmd.gui != None)


    def test_02(self):
        """Test start-stop recording """
        
        cmd = self.mv.videoCommand
        cmd.recorder.RecVar.set(1)
        cmd.recorder.record_cb()
        for i in range(30):
            mv.rotateScene(nbSteps=1)
        cmd.recorder.stop_cb()
        filename = cmd.recorder.fileName
        self.failUnless(os.path.exists(filename))
        #check if file is not empty
        
        fsize = os.stat(filename)[stat.ST_SIZE]
        print "fsize:", fsize
        os.remove(filename)
        self.failUnless(fsize > 0)
        

    def test_03(self):
        """ Test selecting new name for the file"""
        
        cmd = self.mv.videoCommand
        newname = "test.mpg"
        cmd.gui.descr.entryByName['filename']['widget'].setentry(newname)
        cmd.recorder.getFileName()
        self.assertEqual(cmd.recorder.fileName, newname)

    def test_04(self):
        """Test start-pause-stop recording """
        
        cmd = self.mv.videoCommand
        filename = cmd.recorder.fileName
        print "filename:", filename
	cmd.recorder.RecVar.set(1)
        cmd.recorder.record_cb()
        for i in range(20):
            mv.rotateScene(nbSteps=1)
        cmd.recorder.PauseVar.set(1)
        cmd.recorder.pause_cb()
        mv.colorByAtomType("1crn", ['lines'])
        cmd.recorder.PauseVar.set(0)
        cmd.recorder.record_cb()
        for i in range(20):
            mv.rotateScene(nbSteps=1)
        cmd.recorder.stop_cb()
        self.failUnless(os.path.exists(filename))
        #check if file is not empty
        fsize = os.stat(filename)[stat.ST_SIZE]
        print "fsize:", fsize
        os.remove(filename)
        self.failUnless(fsize > 0)
