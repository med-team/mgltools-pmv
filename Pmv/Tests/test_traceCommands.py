#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_traceCommands.py,v 1.14 2012/08/20 22:48:16 annao Exp $
#
# $Id: test_traceCommands.py,v 1.14 2012/08/20 22:48:16 annao Exp $
#
import sys
import gc
import unittest
from DejaVu.Shapes import Triangle2D, Circle2D, Rectangle2D, Square2D, Ellipse2D
import string
from string import split

mv =None
klass = None
ct = 0
totalCt = 43

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1
    
"""
This module implements a set of function to test the commands of the
traceCommands module
"""
class TraceBaseTest(unittest.TestCase):
    def setUp(self):
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            print 'setup: destroying mv'
            if mv and mv.hasGui:
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
            

    def startViewer(self):
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                verbose=False, trapExceptions=False,
                                withShell=0, gui=hasGUI)
            #                         verbose=False, withShell=False)
            mv.browseCommands('fileCommands',
                                   commands=['readMolecule',],
                                   package='Pmv')
            mv.browseCommands('deleteCommands',
                                   commands=['deleteMol',], package='Pmv')
            mv.browseCommands("bondsCommands",
                                   commands=["buildBondsByDistance",],
                                   package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance',
                                            'displayLines'], log=0)
            mv.browseCommands("interactiveCommands", package='Pmv')
            # Don't want to trap exceptions and errors...
            # the user pref is set to 1 by default
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            
            mv.browseCommands('traceCommands', package='Pmv')
        self.mv = mv 

    def tearDown(self):
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
        #self.mv.Exit(0)
        #gc.collect()

###########################################################
###
###    TEST COMPUTE TRACE COMMAND.
###
###########################################################
class ComputeTraceTest(TraceBaseTest):
    def test_computeTrace_1(self):
        """
        Test the computeTrace command on an empty viewer
        """
        self.mv.computeTrace(self.mv.getSelection())

    def test_computeTrace_2(self):
        """
        Test the computeTrace command on 1crn.pdb with the default parameters
        """
        # 1- Read in a "good molecule" 1crn for which we know the result.
        self.mv.readMolecule('Data/1crn.pdb')
        from MolKit.protein import Coil, Residue
        # 2- compute the CA trace for that molecule
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        chain = self.mv.Mols[0].chains[0]
        # 3- assert that the chain has a trace
        self.failUnless(isinstance( chain.trace['CATrace'], Coil))
        self.failUnless( not chain.sheet2D['CATrace'] is None)
        # 6- assert that the catrace contains all the residues of the test mol.
        sheet2DRes = chain.sheet2D['CATrace'].resInSheet
        sheet2DRes.sort()
        allRes = chain.residues
        allRes.sort()
        caRes = chain.trace['CATrace'].residues
        caRes.sort()
        self.assertEqual( allRes,caRes)
        self.assertEqual(caRes,sheet2DRes)
        # 7- assert that a sheet2D object has been created and that the sheet2D
        # and the trace have the same residues
        self.mv.deleteMol('1crn')
        self.assertEqual(len(self.mv.Mols), 0)

    def test_computeTrace_3(self):
        """
        Test the computeTrace command on mead.pqr with the following parameters
        traceName = DNATrace
        ctlAtmName = P
        torsAtmName = C1'
        """
        # 1- Read in mead.pqr
        self.mv.readMolecule('Data/mead.pqr')
        from MolKit.protein import Coil, Residue
        # 2- 
        self.mv.select('mead')
        self.mv.computeTrace(self.mv.getSelection(), traceName='DNATrace',
                             ctlAtmName='P', torsAtmName="C1'")
        chain = self.mv.Mols[0].chains[0]
        # 3- assert that the chain has a trace
        self.failUnless( isinstance( chain.trace['DNATrace'], Coil))
        self.failUnless(not chain.sheet2D['DNATrace'] is None)
        # 6- assert that the catrace contains all the residues of the test mol.
        sheet2DRes = chain.sheet2D['DNATrace'].resInSheet
        sheet2DRes.sort()
        allRes = chain.residues
        allRes.sort()
        traceRes = chain.trace['DNATrace'].residues
        traceRes.sort()
        self.assertEqual(traceRes, sheet2DRes )
        # 7- assert that a sheet2D object has been created and that the sheet2D
        # and the trace have the same residues
        self.mv.deleteMol('mead')
        self.assertEqual(len(self.mv.Mols),0)


    def test_computeTrace_4(self):
        """
        Test the computeTrace command on 1crnAlt.pdb with the default
        parameters
        """
        # the input molecule is 1crnAlt.pdb and the CYS40 doesn't have an
        # Oxygen, therefore the catrace for that chain should only contain
        # the first 39 residues

        # 1- Read in 1crnAlt.pdb
        self.mv.readMolecule('Data/1crnAlt.pdb')

        # 2- select compute the CA trace for that molecule
        self.mv.select('1crnAlt')
        self.mv.computeTrace(self.mv.getSelection())
        chain = self.mv.Mols[0].chains[0]
        # 3- assert that this object is an instance of Coil class
        from MolKit.protein import Residue, Coil
        self.failUnless(isinstance(chain.trace['CATrace'], Coil))
        self.failUnless( hasattr(chain, 'sheet2D'))

        # 5- assert that the catrace only contains the first 39 residues
        # stopped at the residue CYS40 that doesn't have a O.

        #### this is no longer true - see Pmv/extrusuinCommands.py
        #### revision 1.46  (in getSheet2DRes() the loop continues)
        
        ## res = chain.residues.get('THR1-THR39')
        ## res.sort()
        ## cares = chain.trace['CATrace'].residues
        ## cares.sort()
        ## sheet2Res = chain.sheet2D['CATrace'].resInSheet
        ## sheet2Res.sort()
        ## self.assertEqual( cares, res)
        ## self.assertEqual(res, sheet2Res)
        self.mv.deleteMol('1crnAlt')
        self.assertEqual(len(self.mv.Mols), 0)

    def test_computeTrace_5(self):
        """
        Test that the computeCATrace command behaves properly when the
        molecule's chains cannot have a CATrace for example mead.pqr
        which a DNA or RNA
        """
        # 1- Read in a "bad mol" mead.pqr for which we know that no
        # CATrace can be computed.
        self.mv.readMolecule('Data/mead.pqr')
        # 2- compute the CA trace for that molecule 
        self.mv.computeTrace(self.mv.getSelection())
        # 3- assert that the chain has a catrace
        self.failUnless( self.mv.Mols[0].chains[0].trace['CATrace'] is None)
        self.mv.deleteMol('mead')
        self.assertEqual(len(self.mv.Mols),0)

    def test_computeTrace_6(self):
        """
        This test function test if 
        """
        # 1- Read in a "bad mol" mead.pqr for which we know that no
        # CATrace can be computed.
        self.mv.readMolecule('Data/mead.pqr')
        # 2- compute the CA trace for that molecule 
        self.mv.computeTrace(self.mv.getSelection(), 'DNATrace', 'P', "O1'")
        # 3- assert that the chain has a catrace
        self.failUnless(self.mv.Mols[0].chains[0].trace['DNATrace'] is None)
        self.mv.deleteMol('mead')
        self.assertEqual(len(self.mv.Mols), 0)

###########################################################
###
###    TEST EXTRUDETRACE COMMAND.
###
###########################################################
    
class ExtrudeTraceTest(TraceBaseTest):
    def test_extrudeTrace_1(self):
        """ Test if the extrudeTrace behaves properly when no molecule has
        been loaded in the viewer"""
        self.mv.extrudeTrace(self.mv.getSelection(), 'CATrace')

    def test_extrudeTrace_2(self):
        """
        Test extrudeTrace with default parameters on 1crn.pdb
        """
        from DejaVu.IndexedPolygons import IndexedPolygons
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute trace on the selection
        self.mv.computeTrace(self.mv.getSelection())
        # 4- Extrude the default shape on the default traceName. ('CATrace')
        self.mv.extrudeTrace(self.mv.getSelection())
        # 5- Need to verify that the geom has been created properly
        gc = self.mv.Mols[0].geomContainer
        self.failUnless(gc.geoms.has_key('CATrace '))
        g = gc.geoms['CATrace ']
        self.failUnless(isinstance(g, IndexedPolygons))
        self.assertEqual( g.vertexSet.vertices.array.shape, (2234, 3))
        self.assertEqual( g.faceSet.faces.array.shape, (2220, 4))
        self.failUnless(gc.atoms.has_key('CATrace '))
        from MolKit.protein import Residue
        self.assertEqual(gc.atoms['CATrace '],
                         self.mv.Mols[0].findType(Residue))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual(len(self.mv.Mols), 0)

    def test_extrudeTrace_3(self):
        """
        Test extrudeTrace with default parameters on 1crn.pdb
        """
        from DejaVu.IndexedPolygons import IndexedPolygons
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute trace on the selection
        self.mv.computeTrace(self.mv.getSelection())
        # 4- Extrude the default shape on the default traceName. ('CATrace')
        self.mv.extrudeTrace(self.mv.getSelection(), display=0)
        # 5- Need to verify that the geom has been created properly
        gc = self.mv.Mols[0].geomContainer
        self.failUnless(gc.geoms.has_key('CATrace '))
        g = gc.geoms['CATrace ']
        self.failUnless(isinstance(g, IndexedPolygons))
        self.assertEqual( g.vertexSet.vertices.array.shape, (2234, 3))
        self.assertEqual( g.faceSet.faces.array.shape, (2220, 4))
        self.failUnless(gc.atoms.has_key('CATrace '))
        self.assertEqual( len(gc.atoms['CATrace ']), 0)
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual(len(self.mv.Mols), 0)

    def test_extrudeTrace_4(self):
        """
        Test the extrudeTrace command with a trace name that doesn't exist.
        """
        # Test extrusion.... Creation of the right geometries...
        # test good and bad input : ))
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute trace on the selection
        self.mv.computeTrace(self.mv.getSelection())
        # 4- Extrude the default shape for the trace : 'catrace' instead of
        # 'CATrace'
        self.mv.extrudeTrace(self.mv.getSelection(), traceName='catrace')
        gc = self.mv.Mols[0].geomContainer
        self.failUnless(not gc.geoms.has_key('catrace '))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols),0)

    def test_extrudeTrace_5(self):
        """
        Test the extrudeTrace command with a new shape Rectangle 2D
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), frontCap=1, endCap=0)
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['CATrace']
        self.failUnless(gc.geoms.has_key('CATrace '))
        print trace.exElt.cap1
        print trace.exElt.cap2
        self.assertEqual(trace.exElt.cap1,1)
        self.assertEqual(trace.exElt.cap2, 0)
        self.assertEqual(gc.geoms['CATrace '].vertexSet.vertices.array.shape,
                         (2221, 3))
        self.assertEqual(gc.geoms['CATrace '].faceSet.faces.array.shape,
                         (2208, 4))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual(len(self.mv.Mols),0)

    def test_extrudeTrace_6(self):
        """
        Test the extrudeTrace command with a new shape Rectangle 2D
        """
        from DejaVu.Shapes import Rectangle2D
        shape = Rectangle2D(1.2, 0.2, firstDup=0, vertDup=1)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), frontCap=0, endCap=1)
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['CATrace']
        self.assertEqual(trace.exElt.cap1, 0)
        self.assertEqual(trace.exElt.cap2, 1)
        self.failUnless(gc.geoms.has_key('CATrace '))
        self.assertEqual(gc.geoms['CATrace '].vertexSet.vertices.array.shape,
                         (2221, 3))
        self.assertEqual(gc.geoms['CATrace '].faceSet.faces.array.shape,
                         (2208, 4))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual(len(self.mv.Mols), 0)

    def test_extrudeTrace_7(self):
        """
        Test the extrudeTrace command with a new shape Rectangle 2D
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), frontCap=0, endCap=0)
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['CATrace']
        self.assertEqual( trace.exElt.cap1, 0)
        self.assertEqual( trace.exElt.cap2, 0)
        self.failUnless(gc.geoms.has_key('CATrace '))
        self.assertEqual(gc.geoms['CATrace '].vertexSet.vertices.array.shape,
                         (2208, 3))
        self.assertEqual(gc.geoms['CATrace '].faceSet.faces.array.shape,
                         (2196, 4))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual(len(self.mv.Mols),0)

    def test_extrudeTrace_8(self):
        """
        Test the extrudeTrace command with a new shape Rectangle 2D
        """
        from DejaVu.Shapes import Rectangle2D
        shape = Rectangle2D(1.2, 0.2, firstDup=0, vertDup=1)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), shape2D = shape)
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['CATrace']
        self.assertEqual(trace.exElt.shape,shape)
        self.failUnless( gc.geoms.has_key('CATrace '))
        self.assertEqual(gc.geoms['CATrace '].vertexSet.vertices.array.shape,
                         (1490, 3))
        self.assertEqual(gc.geoms['CATrace '].faceSet.faces.array.shape,
                         (740, 4))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual(len(self.mv.Mols),0)

    def test_extrudeTrace_9(self):
        """
        Test the extrudeTrace command with a new shape Rectangle 2D
        """
        from DejaVu.Shapes import Square2D
        shape = Square2D(1.2, firstDup=0, vertDup=1)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), shape2D = shape)
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['CATrace']
        self.assertEqual( trace.exElt.shape, shape)
        self.failUnless( gc.geoms.has_key('CATrace '))
        self.assertEqual( gc.geoms['CATrace '].vertexSet.vertices.array.shape,
                          (1490, 3))
        self.assertEqual( gc.geoms['CATrace '].faceSet.faces.array.shape,
                          (740, 4))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols), 0)


    def test_extrudeTrace_10(self):
        """
        Test the extrudeTrace command with a new shape Rectangle 2D
        """
        from DejaVu.Shapes import Ellipse2D
        shape = Ellipse2D(0.5, 0.2)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), shape2D = shape)
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['CATrace']
        self.assertEqual( trace.exElt.shape, shape)
        self.failUnless(gc.geoms.has_key('CATrace '))
        self.assertEqual( gc.geoms['CATrace '].vertexSet.vertices.array.shape,
                          (2234, 3))
        self.assertEqual( gc.geoms['CATrace '].faceSet.faces.array.shape ,
                          (2220, 4))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols), 0)


    def test_extrudeTrace_11(self):
        """
        Test the extrudeTrace command on mead.pqr using the log 
        """
        self.mv.readMolecule('Data/mead.pqr')
        self.mv.select('mead')
        self.mv.computeTrace(self.mv.getSelection(), traceName='DNATrace', ctlAtmName='P',
                        torsAtmName="C1'")
        assert not self.mv.Mols[0].chains[0].trace['DNATrace'] is None
        from DejaVu.Shapes import Circle2D
        self.mv.extrudeTrace("mead", frontCap=1, endCap=1, log=0,
                             shape2D=Circle2D(0.1, quality=12,
                                              vertDup=0, firstDup=0),
                             traceName='DNATrace')
        gc = self.mv.Mols[0].geomContainer
        trace = self.mv.Mols[0].chains[0].trace['DNATrace']
        self.failUnless( gc.geoms.has_key('DNATraceUNK'))
        g = gc.geoms['DNATraceUNK']
        self.assertEqual( g.vertexSet.vertices.array.shape,(698, 3))
        self.assertEqual( g.faceSet.faces.array.shape, (684, 4))
        #  - Delete the molecule
        self.mv.deleteMol('mead')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols), 0)
    
###########################################################
###
###    TEST DISPLAYTRACE COMMAND.
###
###########################################################
class DisplayTraceTest(TraceBaseTest):
    def test_displayTrace_1(self):
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.displayTrace(self.mv.getSelection(), 'CATrace')


    def test_displayTrace_2(self):
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), display=0)
        self.mv.displayTrace(self.mv.getSelection(), 'CATrace')
        from MolKit.protein import Residue
        gc = self.mv.Mols[0].geomContainer
        self.assertEqual( gc.atoms['CATrace '],
                          self.mv.Mols[0].findType(Residue))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols),
                          0)

    def test_displayTrace_3(self):
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), display=1)
        from MolKit.protein import Residue
        gc = self.mv.Mols[0].geomContainer
        self.assertEqual( gc.atoms['CATrace '],
                          self.mv.Mols[0].findType(Residue))
        self.mv.displayTrace(self.mv.getSelection(), 'CATrace', negate=1)
        self.assertEqual( len(self.mv.Mols[0].geomContainer.atoms['CATrace ']),
                          0)
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols), 0)


    def test_displayTrace_4(self):
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), display=0)
        self.mv.displayTrace(self.mv.getSelection(), 'catrace', negate=1)
        self.failUnless( not self.mv.Mols[0].geomContainer.atoms.has_key('catrace'))
        #  - Delete the molecule
        self.mv.deleteMol('1crn')
        #  - Make sure that the molecule has been deleted.
        self.assertEqual( len(self.mv.Mols),0)


#####################################################################
##
##   CUSTOM TRACE COMMAND TESTS
##
#####################################################################

class CustomTraceTest(TraceBaseTest):

    def test_customtrace_default_aguements(self):
        """tests custom trace command with default arguements"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        self.mv.customTrace(self.mv.getSelection())
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms.has_key('CATrace '),True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CATrace '])!=0,True)

    def test_customtrace_undo(self):
        """tests custom trace command with default arguements and undo custom trace"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        self.mv.customTrace(self.mv.getSelection())
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms.has_key('CATrace '),True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CATrace '])!=0,True)
        self.mv.customTrace(self.mv.getSelection(),display=0)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CATrace '])!=0,False)
        
    def test_customtrace_with_allarguements_circle(self):
        """tests custom trace when shape is circle"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        shape = Circle2D(1.2)
        self.mv.customTrace(self.mv.getSelection(),traceName='CTrace',shape2D=shape,frontCap=1, endCap=1,ctlAtmName='C',torsAtmName='O',nbchords=6,display=1)  
        
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CTrace '])!=0,True)
        
    def test_customtrace_with_allarguements_rectangle(self):
        """tests custom trace when shape is rectangle"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        shape = Rectangle2D(1.2, 0.2, firstDup=0, vertDup=1)
        self.mv.customTrace(self.mv.getSelection(),traceName='CTrace',shape2D=shape,frontCap=1, endCap=1,ctlAtmName='C',torsAtmName='O',nbchords=6,display=1)  
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CTrace '])!=0,True)

    def test_customtrace_with_allarguements_ellipse(self):
        """tests custom trace when shape is ellipse"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        shape = Ellipse2D(1.2, 0.2)
        self.mv.customTrace(self.mv.getSelection(),traceName='CTrace',shape2D=shape,frontCap=1, endCap=1,ctlAtmName='C',torsAtmName='O',nbchords=6,display=1)  
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CTrace '])!=0,True)
        
    def test_customtrace_with_allarguements_square(self):
        """tests custom trace when shape is square"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        shape = Square2D(1.2,vertDup=1)
        self.mv.customTrace(self.mv.getSelection(),traceName='CTrace',shape2D=shape,frontCap=1, endCap=1,ctlAtmName='C',torsAtmName='O',nbchords=6,display=1)  
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CTrace '])!=0,True)
    
    
    def test_customtrace_with_allarguements_triangle(self):
        """tests custom trace when shape is triangle"""
        self.mv.readMolecule('Data/barrel_1.pdb') 
        shape = Triangle2D(1.2,vertDup=1)
        self.mv.customTrace(self.mv.getSelection(),traceName='CTrace',shape2D=shape,frontCap=1, endCap=1,ctlAtmName='C',torsAtmName='O',nbchords=6,display=1)  
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CTrace '])!=0,True)
        
    def test_customtrace_for_selected(self):
        """tests custom trace for selected atoms"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.select(self.mv.allAtoms[:20])
        self.mv.customTrace(self.mv.getSelection())
        ctraceres = self.mv.Mols[0].geomContainer.atoms['CATrace ']
        self.assertEqual(len(ctraceres)==2,True)
   

    ##invalid Arguements
    def test_customtrace_invalid_shape2D(self):
        """tests custom trace with invalid shape arguement"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.assertRaises(AttributeError,self.mv.customTrace,self.mv.getSelection(),shape2D="hello")


    def test_customtrace_invalid_nodes(self):
        """tests custom trace with invalid nodes"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        rval=self.mv.customTrace("hello")
        self.assertEqual(rval,'ERROR')    



#############################################################################
##
##  COMPUTE EXTRUDE TRACE COMMAND TESTS
##
############################################################################


class ComputeExtrudeTraceTest(TraceBaseTest):

    def test_compute_extrude_trace_1(self):
        """tests computeExtrude Trace with default aguements"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.computeExtrudeTrace(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CATrace '])!=0,True)


    def test_compute_extrude_trace_undo(self):
        """tests computeExtrude Trace with negate =1"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.computeExtrudeTrace(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CATrace '])!=0,True)
        self.mv.computeExtrudeTrace(self.mv.getSelection(),negate=1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['CATrace '])!=0,False)
        

    def test_compute_extrude_trace_invalid_nodes(self):
        """compute extrude trace with invalid nodes"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        rval=self.mv.computeExtrudeTrace("hello")
        self.assertEqual(rval,'ERROR')



##################################################################
### LOG TESTS
##################################################################


class TraceCommnadsLogBaseTest(TraceBaseTest):
    
    
    def test_computeTrace_log_checks_expected_log_string(self):
        """
        checks expected log string is written
        """    
        # 1- Read in a "good molecule" 1crn for which we know the result.
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        from MolKit.protein import Coil, Residue
        # 2- compute the CA trace for that molecule
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeTrace("1crn", torsAtmName=\'O\', nbchords=4, ctlAtmName=\'CA\', log=0, traceName=\'CATrace\')')

    def test_computeTrace_log_checks_that_it_runs(self):
        """
        Checking log string runs
        """    
        # 1- Read in a "good molecule" 1crn for which we know the result.
        self.mv.readMolecule('Data/1crn.pdb')
        from MolKit.protein import Coil, Residue
        # 2- compute the CA trace for that molecule
        self.mv.select('1crn')
        oldself=self
        self=self.mv
        s = "self.computeTrace(self.getSelection())"    
        exec(s)
        oldself.assertEqual(1,1)

        
    def test_extrudeTrace_checks_expected_log_string(self):
        """
        checks expected log string is written
        """
        from DejaVu.IndexedPolygons import IndexedPolygons
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute trace on the selection
        self.mv.computeTrace(self.mv.getSelection())
        # 4- Extrude the default shape on the default traceName. ('CATrace')
        self.mv.extrudeTrace(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.extrudeTrace("1crn", log=0, shape2D=None, frontCap=True, endCap=True, display=True, traceName=\'CATrace\')')
        

    def test_extrudeTrace_log_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        from DejaVu.IndexedPolygons import IndexedPolygons
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute trace on the selection
        self.mv.computeTrace(self.mv.getSelection())
        # 4- Extrude the default shape on the default traceName. ('CATrace')
        oldself=self
        self=self.mv
        s ="self.extrudeTrace(self.getSelection())"
        exec(s)
        oldself.assertEqual(1,1)

    
    def test_displayTrace_checks_expected_log_string(self):
        """
        checks expected log string is written
        """
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), display=0)
        self.mv.displayTrace(self.mv.getSelection(), 'CATrace')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displayTrace("1crn", negate=False, only=False, log=0, traceName=\'CATrace\')')
        

    def test_displayTrace_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeTrace(self.mv.getSelection())
        self.mv.extrudeTrace(self.mv.getSelection(), display=0)
        oldself =self
        self=self.mv
        s = "self.displayTrace(self.getSelection(), 'CATrace')"
        exec(s)
        oldself.assertEqual(1,1)
        

    def test_customtrace_checks_expected_log_string(self):
        """checks expected log string is written """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/barrel_1.pdb') 
        self.mv.customTrace(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-4.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.customTrace("barrel_1", torsAtmName=\'O\', nbchords=4, log=0, shape2D=None, frontCap=1, ctlAtmName=\'CA\', traceName=\'CATrace\', display=1, endCap=1)')

    def test_customtrace_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/barrel_1.pdb') 
        oldself =self
        self=self.mv
        s ="self.customTrace(self.getSelection())"
        exec(s)
        oldself.assertEqual(1,1)


    def test_compute_extrude_trace_checks_expected_log_string(self):
        """checks expected log string is written """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.computeExtrudeTrace(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-3.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeExtrudeTrace("barrel_1", negate=False, only=False, log=0)')
        
        
    def xitest_compute_extrude_trace_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/barrel_1.pdb') 
        oldself =self
        self=self.mv
        s ="self.computeExtrudeTrace('1crn', negate=False, only=False, log=0)"
        exec(s)
        oldself.assertEqual(1,1)


    

if __name__ == '__main__':
    unittest.main()
