#
#
#  $Id: test_repairCommands.py,v 1.36.2.1 2016/02/11 21:29:44 annao Exp $
#
#

import unittest, glob, os, Pmv.Grid, string,sys
import time
import Tkinter
from string import split,strip,find
from MolKit.molecule import Atom
from MolKit.protein import Residue
mv = None
klass = None
ct = 0
totalCt = 83
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class Pmvrepair_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            print 'startViewer'
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            #need access to error messages
            mv = MoleculeViewer(trapExceptions=False,
                                withShell=0, customizer='./.empty',
                                gui=hasGUI)
            mv.browseCommands('fileCommands', commands=['readMolecule',],package= 'Pmv')
            mv.browseCommands('colorCommands', package= 'Pmv')
            mv.loadModule("bondsCommands")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines',
                                       'colorByAtomType'], topCommand=0)
            if ct > 0:
                from Pmv import repairCommands
                reload(repairCommands)
            mv.loadModule('repairCommands', 'Pmv')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            mv.loadModule('editCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
            
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)


    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt

        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)

        ct = ct + 1
        global mv
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None

class Pmvrepair_editHistTests(Pmvrepair_BaseTests):
#editHist_h,editHist_hGC

    def test_repair_editHistGC_widget(self):
        """ check editHist_h widget is displayed 
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c=self.mv.editHist_hGC
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69': 'HE2'})
        self.assertEqual(c.form.root.winfo_ismapped(),1)
        c.dismiss_cb()
   

    def test_repair_editHist_invalid_input(self):
        """editHist_h invalid input
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        c=self.mv.editHist_h
        returnvalue = c({'hsg1:A:HIS123': 'HE', 'hsg1:B:HIS123':
        'HE'})
        self.assertEqual(returnvalue,"ERROR")
    

    def test_repair_editHist_empty_input(self):
        """editHist_h empty input
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        c=self.mv.editHist_h
        returnvalue = c({})
        self.assertEqual(returnvalue,"ERROR")

    
    def test_repair_editHistGC_invalid_input(self):
        """editHist_hGC invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        c=self.mv.editHist_hGC
        #NB: HE2 exists but not HE 
        returnvalue = c("hsg1:A:HIS69: 'HE',:B:HIS69:'HE'",mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.assertEqual(returnvalue,"ERROR")
    

    def test_repair_editHistGC_empty_input(self):
        """editHist_hGC empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        c=self.mv.editHist_hGC
        returnvalue = c(" ",mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.assertEqual(returnvalue,"ERROR")

    
#RADIOBUTTON select TESTS
    def test_repair_editHistGC_A_HD1(self):
        """checks editHistGC: selecting a chain A radiobutton HD1 sets entry in cbDict
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69':
        'HE2'})
        c=self.mv.editHist_hGC
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        x=c.ifd.entryByName['hsg1:A:HIS691']['widget']
        #selecting sets the radiobutton HD1 ON for chain A
        #THIS SETS HD1 as the desired HISTIDINE hydrogen for chain A
        x.select()
        #this checks that the correct radiobutton is on
        self.assertEqual(c.cbDict['hsg1:A:HIS69'].get(),'HD1')
        c.dismiss_cb()


    def test_repair_editHistGC_his_HD1_B(self):
        """checks editHistGC: selecting a chain B radiobutton HD1 sets entry in cbDict
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69':
        'HE2'})
        c=self.mv.editHist_hGC
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        x=c.ifd.entryByName['hsg1:B:HIS691']['widget']
        x.select()
        self.assertEqual(c.cbDict['hsg1:B:HIS69'].get(),'HD1')
        c.dismiss_cb()
 

    def test_repair_editHistGC_A_HE2(self):
        """checks editHistGC: selecting a chain A radiobutton HE2 sets entry in cbDict
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1', 'hsg1:B:HIS69': 'HD1'})
        c=self.mv.editHist_hGC
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        x=c.ifd.entryByName['hsg1:A:HIS692']['widget']
        x.select()
        self.assertEqual(c.cbDict['hsg1:A:HIS69'].get(),'HE2')
        c.dismiss_cb()
 

    def test_repair_editHistGC_B_HE2(self):
        """checks editHistGC: selecting a chain B radiobutton HE2 sets entry in cbDict
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")

        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': ''})
        c=self.mv.editHist_hGC
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        x=c.ifd.entryByName['hsg1:B:HIS692']['widget']
        x.select()
        self.assertEqual(c.cbDict['hsg1:B:HIS69'].get(),'HE2')
        c.dismiss_cb()
        

    def test_repair_editHistGC_A_1(self):
        """checks editHistGC: selecting a chain A radiobutton +1 sets entry in cbDict
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69':
        'HE2'})
        c=self.mv.editHist_hGC
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        x=c.ifd.entryByName['hsg1:A:HIS693']['widget']
        x.select()
        self.assertEqual(c.cbDict['hsg1:A:HIS69'].get(),'HD1HE2')
        c.dismiss_cb()
    
    
    def test_repair_editHistGC_B_1(self):
        """checks editHistGC: selecting a chain B radiobutton +1 sets entry in cbDict
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69':
        'HE2'})
        c=self.mv.editHist_hGC
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        x=c.ifd.entryByName['hsg1:A:HIS693']['widget']
        x.select()
        self.assertEqual(c.cbDict['hsg1:A:HIS69'].get(),'HD1HE2')
        c.dismiss_cb()
#end RADIOBUTTON select TESTS


#TESTS that one hydrogen removed when HD1 OR HE2 is chosen
    def test_repair_editHistGC_HD1_A(self):
        """check editHistGC chain A: HD1 active removes atom HE2
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HD1HE2'}, log=0)
        
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        if self.mv.hasGui:
            c=self.mv.editHist_hGC
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
            c.dismiss_cb()
        atoms=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1', 'hsg1:B:HIS69':
        'HD1HE2'})
        #c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        #c.dismiss_cb()
        atoms1=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        self.assertEqual(len(atoms)>len(atoms1),True)
        at=atoms.name
        at.sort()
        at1=atoms1.name+['HE2']
        at1.sort()
        self.assertEqual(at,at1)


        
    def test_repair_editHistGC_HD1_B(self):
        """check editHistGC chain B: HD1 active removes atom HE2
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HD1HE2'}, log=0)
        
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        atoms=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        if self.mv.hasGui:
            c=self.mv.editHist_hGC
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HD1'})
        if self.mv.hasGui:
            c.dismiss_cb()
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        atoms1=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        self.assertEqual(len(atoms)>len(atoms1),True)
        at=atoms.name
        at.sort()
        at1=atoms1.name+['HE2']
        at1.sort()
        self.assertEqual(at,at1)
        if self.mv.hasGui:
            c.dismiss_cb()

        
    def test_repair_editHistGC_HE2_A(self):
        """check editHistGC chain A: HE2 active removes atom HD1
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HD1HE2'}, log=0)
        
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        atoms=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        if self.mv.hasGui:
            c=self.mv.editHist_hGC
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69':'HD1HE2'})
        if self.mv.hasGui:
            c.dismiss_cb()
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        atoms1=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        self.assertEqual(len(atoms)>len(atoms1),True)
        at=atoms.name
        at.sort()
        at1=atoms1.name+['HD1']
        at1.sort()
        self.assertEqual(at,at1)
        if self.mv.hasGui:
            c.dismiss_cb()

        
    def test_repair_editHistGC_HE2_B(self):
        """check editHistGC chain B: HE2 active removes atom HD1
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HD1HE2'}, log=0)

        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        atoms=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        if self.mv.hasGui:
            c=self.mv.editHist_hGC
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HE2'})
        if self.mv.hasGui:
            c.dismiss_cb()
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        atoms1=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        self.assertEqual(len(atoms)>len(atoms1),True)
        at=atoms.name
        at.sort()
        at1=atoms1.name+['HD1']
        at1.sort()
        self.assertEqual(at,at1)
        if self.mv.hasGui:
            c.dismiss_cb()

    
    def test_repair_editHistGC_HD1HE2(self):
        """editHistGC HD1HE2 on checks no change in length of atoms or their
        names
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69': 'HD1HE2'}, log=0)
        
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        if self.mv.hasGui:
            c=self.mv.editHist_hGC
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        resset=self.mv.Mols[0].chains.residues
        atomsrs1=resset.get((lambda x:x.type=='HIS'))[0]
        ats1=atomsrs1.get(lambda z: z.name =='HD1' or z.name=='HE2')
        atomsrs2=resset.get((lambda x:x.type=='HIS'))[1]
        ats2=atomsrs2.get(lambda z: z.name =='HD1' or z.name=='HE2')
        self.mv.editHist_h({'hsg1:A:HIS69': 'HD1HE2', 'hsg1:B:HIS69':'HD1HE2'})
        if self.mv.hasGui:
            c.dismiss_cb()
            c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        atoms1=self.mv.Mols[0].allAtoms.get(lambda z: z.name =='HD1' or z.name=='HE2')
        at=ats1.name+ats2.name
        at.sort()
        at1=atoms1.name
        at1.sort()
        self.assertEqual(len(at),len(at1))
        self.assertEqual(at,at1)
        if self.mv.hasGui:
            c.dismiss_cb()
#end TESTS that one hydrogen removed when HD1 OR HE2 is chosen
#end editHist_h,editHist_hGC


class Pmvrepair_checkForMissingAtomTests(Pmvrepair_BaseTests):
#checks for missing Atoms

    def test_checks_for_missing_atoms_widget(self):
        """checks for missing atoms displays widget displays
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/hsg1.pdb')    
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #delete some atoms 
        self.mv.select("hsg1:A:PRO39:CB,CA,C,O;hsg1:A:GLY40:HN,CA", negate=False, only=False, log=0)
        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
        #check that some atoms are missing
        self.mv.checkForMissingAtomsGC(self.mv.getSelection(),log=0)
        c=self.mv.checkForMissingAtomsGC
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        c.dismiss_cb()
    

    def test_checks_for_missing_atoms_res1(self):
        """checks for missing atoms for LYS70 chain A
        """
        self.mv.readMolecule('Data/hsg1.pdb')    
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.select("hsg1:A:LYS70:CE,NZ,HZ1,HZ2,HZ3", negate=False, only=False, log=0)
        self.mv.select("hsg1:A:ARG41:NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0)
        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
        c1=self.mv.checkForMissingAtoms
        if self.mv.hasGui:
            self.mv.checkForMissingAtomsGC(self.mv.getSelection(),log=0)
            c=self.mv.checkForMissingAtomsGC
            x=c.ifd.entryByName['resLC']['widget']
            x.lb.selection_clear('0','end')
            #selecting LYS only... it should be second in the list, but maynot be
            if find(x.lb.get('0'), 'LYS')<0:
                x.lb.selection_set('1')
            else:
                x.lb.selection_set('0')
            #invoking the command puts labels on current curselection of lb
            #['NZ', 'CE', 'HZ3', 'HZ2', 'HZ1']
            c.ifd.entryByName['selCurrBut']['wcfg']['command']()
            #c.showMissingRes(mv.checkForMissingAtoms(mv.allAtoms))
            # missingAtLabels consists of ATOMS missing from current curselection residue
            missingAtNames=self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
            c.dismiss_cb()
        else:
            misat = self.mv.checkForMissingAtoms("hsg1::;", log=0)
            missingAtNames = []
            for r, names in misat.items():
                if r.name.startswith("LYS"):
                    st = "'%s'"%names[0]
                    for n in names[1:]:
                        st = st+", '%s'"% n
                    missingAtNames.append(st)
        print 'missingAtNames', missingAtNames
        #get a residueSet containing LYS70
        LYSresidues=self.mv.Mols[0].chains.residues.get(lambda x: x.name=='LYS70')
        stdAtNames=c1.resAts['LYS']
        stdAtNames.sort()
        allAtNames=LYSresidues[0].atoms.name+['NZ', 'CE', 'HZ3', 'HZ2', 'HZ1']
        allAtNames.sort()
        self.assertEqual(stdAtNames , allAtNames)
        #this compares the entire string "'NZ', 'CE', 'HZ3', 'HZ2', 'HZ1'"
        #with the analogous string ("'NZ', 'CE', 'HZ3', 'HZ2', 'HZ1'",)[0]
        if self.mv.hasGui:
            self.assertEqual(LYSresidues[0].missing , missingAtNames[0])
    

#    def test_checks_for_missing_atoms_res2(self):
#        """checks for missing atoms for ARG41 chain A
#        """
#        self.mv.readMolecule('Data/hsg1.pdb')    
#        self.mv.setICOM(self.mv.select, modifier=None, log=0)
#        from MolKit.molecule import Atom
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.select("hsg1:A:LYS70:CE,NZ,HZ1,HZ2,HZ3", negate=False, only=False, log=0)
#        self.mv.select("hsg1:A:ARG41:NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0) 
#        c1=self.mv.checkForMissingAtoms
#        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
#        if self.mv.hasGui:
#            self.mv.checkForMissingAtomsGC(self.mv.getSelection(),log=0)
#            c=self.mv.checkForMissingAtomsGC
#            x=c.ifd.entryByName['resLC']['widget']
#            x.lb.selection_clear('0','end')
#            if find(x.lb.get('0'), 'ARG')<0:
#                x.lb.selection_set('1')
#            else:
#                x.lb.selection_set('0')
#            #order in lb appears to be platform dependent
#            #x.lb.selection_set('0')
#            c.ifd.entryByName['selCurrBut']['wcfg']['command']()
#            #c.showMissingRes(mv.checkForMissingAtoms(mv.allAtoms))
#            missingAtNames=self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
#            c.dismiss_cb()
#        else:
#            misat = self.mv.checkForMissingAtoms("hsg1::;", log=0)
#            missingAtNames = []
#            for r, names in misat.items():
#                if r.name.startswith("ARG"):
#                    st = "'%s'"%names[0]
#                    for n in names[1:]:
#                        st = st+", '%s'"% n
#                    missingAtNames.append(st)
#        print 'missingAtNames', missingAtNames
#        LYSresidues=self.mv.Mols[0].chains.residues.get(lambda x: x.name=='ARG41')
#        stdAtNames=c1.resAts['ARG']
#        stdAtNames.sort()
#        allAtNames=LYSresidues[0].atoms.name+['HH21', 'HH12', 'HH11', 'NH2', 'NH1']
#        allAtNames.sort()
#        self.assertEqual(stdAtNames , allAtNames )
#        if self.mv.hasGui:
#            self.assertEqual(LYSresidues[0].missing, missingAtNames[0])

        

#    def test_checks_for_missing_atoms_res1_res2(self):
#        """checks for missing atoms for both LYS70 A and ARG41 A using 'Select All'
#        button
#        """
#        if not self.mv.hasGui: return
#        self.mv.readMolecule('Data/hsg1.pdb')    
#        self.mv.setICOM(self.mv.select, modifier=None, log=0)
#        from MolKit.molecule import Atom
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.select("hsg1:A:LYS70:CE,NZ,HZ1,HZ2,HZ3", negate=False, only=False, log=0)
#        self.mv.select("hsg1:A:ARG41:NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0) 
#        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
#        self.mv.checkForMissingAtomsGC(self.mv.getSelection(),log=0)
#        c1=self.mv.checkForMissingAtoms
#        c=self.mv.checkForMissingAtomsGC
#        x=c.ifd.entryByName['resLC']['widget']
#        x.lb.selection_clear('0','end')
#        x.lb.selection_set('0')
#        x.lb.selection_set('1')
#        #invoke the Select All button's command:
#        c.ifd.entryByName['selCurrBut']['wcfg']['command']()
#        #c.showMissingRes(self.mv.checkForMissingAtoms(self.mv.allAtoms))
#        missingAtNames=self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
#        LYSresidues1=self.mv.Mols[0].chains.residues.get(lambda x: x.name=='LYS70')
#        LYSresidues2=self.mv.Mols[0].chains.residues.get(lambda x: x.name=='ARG41')
#        self.assertEqual(LYSresidues1[0].missing+LYSresidues2[0].missing , missingAtNames[1]+missingAtNames[0])
#        c.dismiss_cb()


    def test_checks_for_missing_atoms_invalid_input(self):
        """checks for missing atoms: invalid input returns ERROR
        """
        self.mv.readMolecule('Data/hsg1.pdb')  
        c=self.mv.checkForMissingAtoms
        returnvalue=c("hsg1:A:LYS0,ARG0", log=0)
        self.assertEqual(returnvalue,'ERROR')


    def test_checks_for_missing_atoms_empty_input(self):
        """checks for missing atoms: empty input returns ERROR
        """
        self.mv.readMolecule('Data/hsg1.pdb')  
        c=self.mv.checkForMissingAtoms
        returnvalue=c(" ")
        self.assertEqual(returnvalue,'ERROR')
    

    def test_checks_for_missing_atomsGC_invalid_input(self):
        """checks for invalid input in checks for missingGC atoms
        """
        self.mv.readMolecule('Data/hsg1.pdb')  
        c=self.mv.checkForMissingAtomsGC
        returnvalue=c("hsg1:A:PRO900")
        self.assertEqual(returnvalue,'ERROR')
    

    def test_checks_for_missing_atomsGC_empty_input(self):
        """checks for empty input in checks for missing atomsGC
        """
        self.mv.readMolecule('Data/hsg1.pdb')  
        c=self.mv.checkForMissingAtomsGC
        returnvalue=c(" ")
        self.assertEqual(returnvalue,'ERROR')

#end check for missing atoms

class Pmvrepair_checkForCloseContactsTests(Pmvrepair_BaseTests):
#check for close contacts

            
    def test_check_for_close_contacts_widget(self):
        """widget check for checkForCloseContactsGC
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        self.mv.checkForCloseContactsGC("ind:::","ind:::",1.0)
        c=self.mv.checkForCloseContactsGC
        self.assertEqual(c.ifd_results.form.root.winfo_ismapped(),1)
        c.dismiss_cb()


    def test_check_for_close_contactsGC_invalid_input(self):
        """invalid input for for checkForCloseContactsGC
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        c=self.mv.checkForCloseContactsGC
        returnvalue=c("ind_xxx:::","ind_xxx:::",1.0)
        self.assertEqual(returnvalue,'ERROR')
        c.dismiss_cb()
        

    def test_check_for_close_contactsGC_empty_input(self):
        """empty input for checkForCloseContactsGC
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        c=self.mv.checkForCloseContactsGC
        returnvalue=c(" "," ",1.0)
        self.assertEqual(returnvalue,'ERROR')
        c.dismiss_cb()


    def test_check_for_close_contacts_invalid_input(self):
        """checkForCloseContacts invalid input
        """
        self.mv.readMolecule('Data/ind.pdb')  
        c=self.mv.checkForCloseContacts
        returnvalue=c("ind_xxx:::","ind_xxx:::",1.0)
        self.assertEqual(returnvalue,'ERROR')
        

    def test_check_for_close_contacts_empty_input(self):
        """checkForCloseContacts empty input
        """
        self.mv.readMolecule('Data/ind.pdb')  
        c=self.mv.checkForCloseContacts
        returnvalue=c(" "," ",1.0)
        self.assertEqual(returnvalue,'ERROR')


    def test_check_for_close_contacts_one_line(self):
        """check close contacts when one selected: its lines geometry's visiblity
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        self.mv.checkForCloseContactsGC("ind:::","ind:::",1.0)
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        x=c.ifd_results.entryByName['atsLC']['widget']
        x.lb.selection_clear('0','end')
        x.lb.selection_set('0')
        c.showCloseContacts(c.atDict,c.distDict,event=None)
        #the command's geometry is its 'lines' attribute
        self.assertEqual(c.lines.visible,1)
        c.dismiss_cb()


    def test_check_for_close_contacts_show_all(self):
        """check close contacts visibility of command's geometry 
        when its "show all" button's command is invoked
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        self.mv.checkForCloseContactsGC("ind:::","ind:::",1.0)
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        x=c.ifd_results.entryByName['atsLC']['widget']
        x.lb.selection_clear('0','end')
        c.ifd_results.entryByName['showAllBut']['wcfg']['command']()
        c.showAllCloseContacts(c.atDict,c.distDict,event=None)
        self.assertEqual(c.lines.visible,1)
        c.dismiss_cb()
        

    def test_check_for_close_contacts_show_all_labels(self):
        """check close contacts all labels are displayed
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        self.mv.checkForCloseContactsGC("ind:::","ind:::",1.0)
        c=self.mv.checkForCloseContactsGC
        #self.assertEqual(len(c.labels.labels),0)
        c.buildForm()
        x=c.ifd_results.entryByName['atsLC']['widget']
        x.lb.selection_clear('0','end')
        c.ifd_results.entryByName['showAllBut']['wcfg']['command']()
        #showall command puts a label on practically every bond
        c.showAllCloseContacts(c.atDict,c.distDict,event=None)
        #the very many (!) labels look like red fuzz
        self.assertEqual(len(c.labels.labels),76)
        c.dismiss_cb()


    def test_check_for_close_contacts_value(self):
        """check close contacts 1 distance's displayed value
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        self.mv.checkForCloseContactsGC("ind:::","ind:::",1.0)
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        x=c.ifd_results.entryByName['atsLC']['widget']
        x.lb.selection_clear('0','end')
        #pick the first entry to label
        x.lb.selection_set('0')
        c.showCloseContacts(c.atDict,c.distDict,event=None)
        self.assertEqual(c.labels.labels,['2.878', '2.866', '2.776'])
        c.dismiss_cb()
    

    def test_check_for_close_contacts_number_less_percentcut_off(self):
        """checks  0 atoms with close contacts 0.3 is percent cut off
        """
        self.mv.readMolecule('Data/ind.pdb')
        if self.mv.hasGui:
            c=self.mv.checkForCloseContactsGC
            c("ind:::","ind:::",0.3)
            c.dismiss_cb()
            distDict = c.distDict
        else:
            atDict, distDict = self.mv.checkForCloseContacts("ind:::","ind:::",0.3)
        self.assertEqual(len(distDict),0)
        

    def test_check_for_close_contacts_number(self):
        """checks 48 atoms with close contacts
        """
        self.mv.readMolecule('Data/ind.pdb')
        if self.mv.hasGui:
            c=self.mv.checkForCloseContactsGC
            c("ind:::","ind:::",1.0)
            distDict = c.distDict
            c.dismiss_cb()
        else:
            atDict, distDict = self.mv.checkForCloseContacts("ind:::","ind:::",1.0)
        self.assertEqual(len(distDict),48)

        

#tests for changes suggested by Michel 1/2005
    def test_check_for_cleanup_after_zero_result(self):
        """checks display is cleared following zero-result
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c("ind:::","ind:::",1.0)
        #then get an empty result
        c("ind:::","ind:::",0.3)
        #self.assertEqual(len(c.distDict),0)
        #the lines should be cleared
        self.assertEqual(len(c.lines.vertexSet.vertices.array),0)
        #the labels should be cleared
        self.assertEqual(len(c.labels.vertexSet.vertices.array),0)
        #how to check that the listbox is destroyed??
        x = c.ifd_results.entryByName['atsLC']['widget']
        #this raises an error because the widget has been destroyed
        self.assertRaises(Tkinter.TclError, x.lb.see, '0')
        c.dismiss_cb()


    def test_check_for_default_display_all(self):
        """checks display is shows all contacts by default
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c("ind:::","ind:::",1.0)
        #the lines should be built
        self.assertEqual(len(c.lines.vertexSet.vertices.array), 124)
        #the labels should be built
        self.assertEqual(len(c.labels.vertexSet.vertices.array), 76)
        c.dismiss_cb()

    def test_display_crosses(self):
        """checks display crossSet
        """
        if not self.mv.hasGui: return
        
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::",1.0)
        #the lines should be built
        c.key_ss.showCurrent.set(1)
        c.key_ss.Show_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),92)
        c.dismiss_cb()


    def test_undisplay_all_crosses_when_close_form(self):
        """checks undisplay crossSet when close form
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::",1.0)
        #the lines should be built
        c.key_ss.showCurrent.set(1)
        c.key_ss.Show_cb()
        c.cancel_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),0)


    def test_save_sets_of_keys_and_close_values(self):
        """checks sets of keys and close atoms are saved
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::", 1.0)
        self.assertEqual(hasattr(c, 'atDict'), True)
        self.assertEqual('closeContact_keys' in self.mv.sets.keys(), False)
        self.assertEqual('closeContact_values' in self.mv.sets.keys(), False)
        c.save_sets()
        self.assertEqual('closeContact_keys' in self.mv.sets.keys(), True)
        self.assertEqual('closeContact_values' in self.mv.sets.keys(), True)
        c.dismiss_cb()

#cancel_cb doesn't change crossSet display
    def test_donot_change_crosses_when_recalculate_with_crosses(self):
        """
        checks donot_change_crosses_when_recalculate_with_crosses
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::",1.0)
        #the lines should be built
        c.key_ss.showCurrent.set(1)
        c.key_ss.Show_cb()
        c.ok_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),92)
        c.ok_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),92)
        c.dismiss_cb()


    def test_donot_change_crosses_when_recalculate_with_no_crosses(self):
        """
        checks donot_change_crosses_when_recalculate_with_no_crosses
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::",1.0)
        #the lines should be built
        c.key_ss.showCurrent.set(0)
        c.key_ss.Show_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),0)
        c.ok_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),0)
        c.dismiss_cb()


#ok_cb doesn't change crossSet display
    def test_redisplay_crosses_when_reopen_form_with_crosses(self):
        """checks redisplay crossSet when open form with crosses
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::",1.0)
        #the lines should be built
        c.key_ss.showCurrent.set(1)
        c.key_ss.Show_cb()
        c.cancel_cb()
        c.guiCallback() 
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),92)
        c.dismiss_cb()


    def test_donot_display_crosses_when_reopen_form_with_no_crosses(self):
        """checks do not display crossSet when open form with no_crosses
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/ind.pdb')  
        #first find and display some result
        c=self.mv.checkForCloseContactsGC
        c.buildForm()
        c("ind:::","ind:::",1.0)
        #the lines should be built
        c.key_ss.showCurrent.set(0)
        c.key_ss.Show_cb()
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),0)
        c.cancel_cb()
        self.assertEqual(c.keyss_oldvalue, 0)
        c.guiCallback() 
        self.assertEqual(len(c.key_ss.showCross.vertexSet.vertices.array),0)
        c.dismiss_cb()


#end check for close contacts


class Pmvrepair_repairMissingAtomTests(Pmvrepair_BaseTests):
#Repair Missing atoms

    def test_repair_missing_atoms_invalid_input(self):
        """repair missing atoms invalid input
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        c=self.mv.repairMissingAtoms
        returnvalue=c("hsg1:A:ARG41:fdsmh",log=0)
        self.assertEqual(returnvalue,'ERROR')


    def test_repair_missing_atoms_empty_input(self):
        """repair missing atoms empty input
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        c=self.mv.repairMissingAtoms
        returnvalue=c(" ",log=0)
        self.assertEqual(returnvalue,'ERROR')
        

    def test_repair_missing_atomsGC_invalid_input(self):
        """repair missing atomsGC invalid input
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        c=self.mv.repairMissingAtomsGC
        returnvalue=c("hsg1:A:ARG41:",log=0)
        self.assertEqual(returnvalue,'ERROR')


    def test_repair_missing_atomsGC_empty_input(self):
        """repair missing atomsGC empty input 
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        c=self.mv.repairMissingAtomsGC
        returnvalue=c(" ",log=0)
        self.assertEqual(returnvalue,'ERROR')

    
#    def test_repair_missing_atoms_number_of_atoms(self):
#        """all atoms before deletion==all atoms after repairing missing atoms
#        """
#        self.mv.readMolecule('Data/hsg1.pdb')  
#        len_oldats=len(self.mv.allAtoms)
#        from MolKit.molecule import Atom
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.select("hsg1:A:ARG41:CZ,NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0)
#        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
#        self.mv.repairMissingAtomsGC(self.mv.getSelection(),log=0)
#        len_newats=len(mv.allAtoms)
#        self.assertEqual(len_oldats,len_newats)
#        c=self.mv.checkForCloseContactsGC
#        c.dismiss_cb()


#    def test_repair_missing_atoms_number_of_line_visible(self):
#        """repair missing atoms: after repair one close contact line is visible
#        """
#        self.mv.readMolecule('Data/hsg1.pdb')  
#        from MolKit.molecule import Atom
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.select("hsg1:A:ARG41:CZ,NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0)
#        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
#        self.mv.repairMissingAtomsGC(self.mv.getSelection(),log=0)
#        c=self.mv.checkForCloseContactsGC
#        x=c.ifd_results.entryByName['atsLC']['widget']
#        x.lb.selection_clear('0','end')
#        x.lb.selection_set('0')
#        c.showCloseContacts(c.atDict,c.distDict,event=None)
#        self.assertEqual(c.lines.visible, 1)
#        c.dismiss_cb()


#    def test_repair_missing_atoms_number_of_labels_lines(self):
#        """repair missing atoms: after repair no. of close contact lines==no. of labels 
#        """
#        self.mv.readMolecule('Data/hsg1.pdb')  
#        from MolKit.molecule import Atom
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.select("hsg1:A:ARG41:CZ,NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0)
#        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
#        self.mv.repairMissingAtomsGC(self.mv.getSelection(),log=0)
#        c=self.mv.checkForCloseContactsGC
#        x=c.ifd_results.entryByName['atsLC']['widget']
#        x.lb.selection_clear('0','end')
#        x.lb.selection_set('0')
#        c.showCloseContacts(c.atDict,c.distDict,event=None)
#        self.assertEqual(len(c.lines.vertexSet)-1,len(c.labels.labels))
#        c.dismiss_cb()


#    def test_repair_missing_atoms_number_of_lines_visible(self):
#        """checks repair missing atoms: 'Show All' button makes no. of lines visible
#        """
#        self.mv.readMolecule('Data/hsg1.pdb')  
#        from MolKit.molecule import Atom
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.select("hsg1:A:ARG41:CZ,NH1,HH11,HH12,NH2,HH21", negate=False, only=False, log=0)
#        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
#        self.mv.repairMissingAtomsGC(self.mv.getSelection(),log=0)
#        c=self.mv.checkForCloseContactsGC
#        x=c.ifd_results.entryByName['atsLC']['widget']
#        x.lb.selection_clear('0','end')
#        c.ifd_results.entryByName['showAllBut']['wcfg']['command']()
#        c.showAllCloseContacts(c.atDict,c.distDict,event=None)
#        self.assertEqual(c.lines.visible,1)
#        c.dismiss_cb()
#end repair missing atoms


class Pmvrepair_add_oxtTests(Pmvrepair_BaseTests):
#add_oxtGC

    def test_add_oxtGC_bonds(self):
        """ checks that add_oxtGC adds a bond to each terminal amino acid's 'C'
           atom
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        c_atoms=res.atoms.get(lambda x: x.name =='C' and len(x.bonds)==2)
        #for i in range(0,len(re)):
	    #    f=re.get(lambda x : len(x.bonds) == 2)
        self.assertEqual(len(c_atoms.bonds),2)
        if self.mv.hasGui:
            self.mv.add_oxtGC('hsg1:A:PHE99:C',log=0)
            self.mv.add_oxtGC('hsg1:B:PHE99:C',log=0)
        else:
            self.mv.add_oxt('hsg1:A:PHE99:C',log=0)
            self.mv.add_oxt('hsg1:B:PHE99:C',log=0)
        
        #for i in range(0,len(c_atoms)):
	    #    f1 = res.get(lambda x : len(x.bonds) == 2)
        after_atoms=res.atoms.get(lambda x: x.name =='C' and len(x.bonds)==2)
        #NB: after_atoms is an empty AtomSet
        self.assertEqual(len(after_atoms),0)

    
    def test_add_oxtGC_bonds_invalid_input(self):
        """checks add_oxtGC invalid input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        c=self.mv.add_oxtGC
        returnvalue=c('hsg1:A:PHE99:S6536')
        self.assertEqual(returnvalue,'ERROR')


    def test_add_oxt_bonds_invalid_input(self):
        """checks add_oxt invalid input
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        c=self.mv.add_oxt
        returnvalue=c('hsg1:A:PHE99:S47456')
        self.assertEqual(returnvalue,'ERROR')


    def test_add_oxtGC_bonds_empty_input(self):
        """checks add_oxtGC empty input
        """
        if not self.mv.hasGui: return
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        c=self.mv.add_oxtGC
        returnvalue=c(' ')
        self.assertEqual(returnvalue,'ERROR')


    def test_add_oxt_bonds_empty_input(self):
        """checks add_oxt empty input
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        c=self.mv.add_oxt
        returnvalue=c('  ')
        self.assertEqual(returnvalue,'ERROR')

        
    def test_add_oxtGC_bonds_added_bonds_1st(self):
        """checks length of bonds of 1st "C"  atom before and after add_oxt
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        re=res.atoms.get(lambda x: x.name =='C')
        for i in range(0,len(re)):
	        f=re.get(lambda x : len(x.bonds) == 2)
        self.assertEqual(len(f[0].bonds),2)
        if self.mv.hasGui:
            self.mv.add_oxtGC('hsg1:A:PHE99:C',log=0)
        else:
            self.mv.add_oxt('hsg1:A:PHE99:C',log=0) 
        self.assertEqual(len(f[0].bonds),3)
    

    def test_add_oxtGC_bonds_added_bonds_2nd(self):
        """checks  length of bonds of 2nd "C" atom after,before add_oxt
       """
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        re=res.atoms.get(lambda x: x.name =='C')
        for i in range(0,len(re)):
	        f=re.get(lambda x : len(x.bonds) == 2)
        self.assertEqual(len(f[1].bonds),2)
        if self.mv.hasGui:
            self.mv.add_oxtGC('hsg1:B:PHE99:C',log=0)
        else:
            self.mv.add_oxt('hsg1:B:PHE99:C',log=0)
        self.assertEqual(len(f[1].bonds),3)


    def test_add_oxtGC_bonds_added_atom_for_2nd(self):
        """checks for added bond and atom is oxt in 2 nd "C" atom
        """
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        re=res.atoms.get(lambda x: x.name =='C')
        for i in range(0,len(re)):
	        f=re.get(lambda x : len(x.bonds) == 2)
        if self.mv.hasGui:
            self.mv.add_oxtGC('hsg1:B:PHE99:C',log=0)
        else:
            self.mv.add_oxt('hsg1:B:PHE99:C',log=0)
        self.assertEqual(str(f[1].bonds[2].atom2),'<Atom instance> hsg1:B:PHE99:OXT')


    def test_add_oxtGC_bonds_addedAtom_for_1st(self):
        """checks for added bond and atom is oxt in 1st "C" atom
        """
        
        self.mv.readMolecule('Data/hsg1.pdb') 
        res=self.mv.Mols[0].chains.residues
        re=res.atoms.get(lambda x: x.name =='C')
        for i in range(0,len(re)):
	        f=re.get(lambda x : len(x.bonds) == 2)
        if self.mv.hasGui:        
            self.mv.add_oxtGC('hsg1:A:PHE99:C',log=0)
        else:
            self.mv.add_oxt('hsg1:A:PHE99:C',log=0)
        self.assertEqual(str( f[0].bonds[2].atom2),'<Atom instance> hsg1:A:PHE99:OXT')

#end add oxt_GC    


class Pmvrepair_modify_termini_Tests(Pmvrepair_BaseTests):
#modify termini

    def setUp(self):
      Pmvrepair_BaseTests.setUp(self)
      mv = self.mv
      mv.readMolecule('Data/piece_modterm.pdbqs')
      at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
      b1 = [at1.bonds[0]]
      b2 = [at1.bonds[4]]
      b3 = [at1.bonds[1]]
      mv.removeBonds(b1, log=0)
      mv.removeBonds(b2, log=0)
      mv.removeBonds(b3, log=0)
      at2 = mv.expandNodes("piece_modterm:B:THR12:HC")[0]
      mv.removeBonds([at2.bonds[1]], log=0)
      mol = mv.Mols[0]
      at1 = mol.NodesFromName("piece_modterm:B:ILE13:N")
      at2 = mol.NodesFromName("piece_modterm:B:ILE13:HN1")
      mv.addBonds([(at1, at2)])


          
    def test_modify_terminus_number_of_bonds(self):
        """checks number of bonds before and after modifyTermini
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        old_len=len(mv.allAtoms.bonds[0])
        self.mv.modifyTermini(self.mv.getSelection())
        new_len = len(mv.allAtoms.bonds[0])
        print old_len, new_len
        self.assertNotEqual(old_len, new_len)
        #self.assertEqual(old_len==new_len+3)
        
    
    def test_modify_termini_coords(self):
        """checks coords before and after modifyTermini
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #oldcoords=self.mv.allAtoms[17].coords[:]
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)
        oldcoords=self.mv.allAtoms[17].coords[:]
        self.mv.modifyTermini(self.mv.getSelection())
        newcoords = self.mv.allAtoms[17].coords[:]
        self.assertNotEqual(oldcoords,newcoords)


    def test_modify_termini_number_of_atoms(self):
        """checks # of atoms before and after modifyTermini
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        oldatoms = self.mv.allAtoms
        self.mv.modifyTermini(self.mv.getSelection())
        newatoms = self.mv.allAtoms
        #this command removes some (3??) hydrogens and build one new one
        self.assertNotEqual(len(oldatoms),len(newatoms))
        
    
    def test_modify_termini_atom_names(self):
        """checks atoms names before and after modifyTermini
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        resSet=self.mv.Mols[0].chains.residues
        oldnames=mv.allAtoms.name
        self.mv.modifyTermini(self.mv.getSelection())
        newnames = mv.allAtoms.name
        #this command removes some (3??) hydrogens and build one new one
        self.assertNotEqual(oldnames,newnames)

            
    def test_modify_terminus_H_deleted(self):
        """checks atoms deleted are H after modifyTermini
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)  
        self.mv.modifyTermini(self.mv.getSelection()) 
        newnames=mv.allAtoms.name
        for i in newnames:
            s1=find('HC',i)
        #this atom is deleted so its name is not in the list of atom names
        #after call to modifyTermini
        #FIND returns -1 for a string not found
        self.assertEqual(s1, -1)
#end modifyTermini


class Pmvrepair_modify_C_terminus_Tests(Pmvrepair_BaseTests):
#modifyCTerminus

    def setUp(self):
      Pmvrepair_BaseTests.setUp(self)
      self.mv.readMolecule('Data/piece_modterm.pdbqs')
      at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
      b1 = [at1.bonds[0]]
      b2 = [at1.bonds[4]]
      b3 = [at1.bonds[1]]
      self.mv.removeBonds(b1, log=0)
      self.mv.removeBonds(b2, log=0)
      self.mv.removeBonds(b3, log=0)
      #at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
      #self.mv.removeBonds([at2.bonds[1]], log=0)


    def test_modify_C_terminus_bonds(self):
        """checks number of bonds before and after modifyCTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        len_old=len(mv.allAtoms.bonds[0])
        resSet=self.mv.Mols[0].chains.residues

        self.mv.modifyCTerminus(resSet)

        len_new = len(mv.allAtoms.bonds[0])
        print len_old, len_new
        self.assertNotEqual(len_old, len_new)
        

    def test_modify_C_terminus_coords(self):
        """checks coords before and after modifyCTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #old = mv.allAtoms[17].coords[:]
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)
        old = mv.allAtoms[17].coords[:]
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyCTerminus(resSet)
        new = mv.allAtoms[17].coords[:]
        self.assertNotEqual(old,new)


    def test_modify_C_terminus_H_deleted(self):
        """checks H deleted after modifyCTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        old_names=mv.allAtoms.name
        old_names.sort()
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyCTerminus(resSet)
        new_names = mv.allAtoms.name
        #add back the 2 deleted atoms' names
        new_names = new_names+['HC', 'HC']
        new_names.sort()
        self.assertEqual(old_names, new_names)
    

    def test_modify_C_terminus_atom_names(self):
        """checks atoms names before and after modifyCTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        old_names=mv.allAtoms.name
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyCTerminus(resSet)
        new_names = mv.allAtoms.name
        self.assertNotEqual(old_names,new_names)


    def test_modify_C_terminus_number_of_atoms(self):
        """checks number of atoms before and after modifyCTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        len_old=len(mv.allAtoms)
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyCTerminus(resSet)
        len_new = len(mv.allAtoms)
        self.assertNotEqual(len_old,len_new)
#end modify Terminus


class Pmvrepair_modify_N_terminus_Tests(Pmvrepair_BaseTests):
#modify N Terminus

    def setUp(self):
      Pmvrepair_BaseTests.setUp(self)
      self.mv.readMolecule('Data/piece_modterm.pdbqs')
      at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
      b1 = [at1.bonds[0]]
      b2 = [at1.bonds[4]]
      b3 = [at1.bonds[1]]
      # FIX THIS (removing the following bonds breaks the tests)
      #self.mv.removeBonds(b1, log=0)
      #self.mv.removeBonds(b2, log=0)
      #self.mv.removeBonds(b3, log=0)
      #at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
      #self.mv.removeBonds([at2.bonds[1]], log=0)

    def test_modify_N_terminus_bonds(self):
        """checks bonds before and after modifyNTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)
        len_old = len(mv.allAtoms.bonds[0])
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyNTerminus(resSet)
        len_new = len(mv.allAtoms.bonds[0])
        #print "len_old:", len_old, len_new
        self.assertNotEqual(len_old,len_new)
        
    
    def test_modify_N_terminus_coords(self):
        """checks coords before and after modifyNTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #old=mv.allAtoms[17].coords[:]
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)
        old=mv.allAtoms[17].coords[:]
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyNTerminus(resSet)
        new = mv.allAtoms[17].coords[:]
        self.assertNotEqual(old,new)


    def test_modify_N_terminus_atoms_name(self):
        """checks atoms name before and after modifyNTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        old_names=mv.allAtoms.name
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyNTerminus(resSet)
        new_names = mv.allAtoms.name
        self.assertNotEqual(old_names,new_names)


    def test_modify_N_terminus_number_of_atoms(self):
        """checks number of atoms before and after modifyNTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        len_old=len(mv.allAtoms)
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyNTerminus(resSet)
        len_new = len(mv.allAtoms)
        self.assertNotEqual(len_old,len_new)
        

    def test_modify_N_terminus_H_deleted(self):
        """checks H deleted after modifyNTerminus
        """
        #self.mv.readMolecule('Data/piece_modterm.pdbqs')
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        #self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyNTerminus(resSet)
        new_names=mv.allAtoms.name
        for i in new_names:
            s=find('HN2',i)
            s1=find('HN3',i)
            
        #Check that find returned -1 for each name of atoms which are
        #deleted by modifyNTerminus call
        self.assertEqual(s, -1)
        self.assertEqual(s1, -1)
# end N Terminus
        
### LogTests

class Pmvrepair_LogTests(Pmvrepair_BaseTests):

    
    def test_repair_editHistGC_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c=self.mv.editHist_hGC
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        self.mv.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69': 'HE2'})
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],"self.editHist_h({'hsg1:A:HIS69':'HE2','hsg1:B:HIS69':'HE2'}, log=0)")


    def test_repair_editHistGC_log_checks_that_it_runs(self):
        """Checking log string runs """
        if not self.mv.hasGui: return
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString(log=0, res='HIS*', mols='', atoms='', negate=0, chains='')
        c=self.mv.editHist_hGC
        c(self.mv.getSelection(),mv.Mols[0].chains.residues.get(lambda x: x.type =='HIS'))
        oldself=self
        self =mv
        s =  "self.editHist_h({'hsg1:A:HIS69': 'HE2', 'hsg1:B:HIS69': 'HE2'}, log=0)"   
        oldself.assertEqual(1,1)

        
    def test_checks_for_missing_atoms_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return 
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/hsg1.pdb')    
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #delete some atoms 
        self.mv.select("hsg1:A:PRO39:CB,CA,C,O;hsg1:A:GLY40:HN,CA", negate=False, only=False, log=0)
        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
        #check that some atoms are missing
        self.mv.checkForMissingAtomsGC(self.mv.getSelection(),log=0)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.checkForMissingAtoms("hsg1::;", log=0)')        
        
    def test_checks_for_missing_atoms_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/hsg1.pdb')    
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #delete some atoms 
        self.mv.select("hsg1:A:PRO39:CB,CA,C,O;hsg1:A:GLY40:HN,CA", negate=False, only=False, log=0)
        self.mv.deleteAtomSet(self.mv.getSelection(),log=0)
        oldself=self
        self =mv
        s = 'self.checkForMissingAtoms("hsg1::;", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
        
        

    def test_check_for_close_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if not self.mv.hasGui: return
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/ind.pdb')  
        self.mv.checkForCloseContactsGC("ind:::","ind:::",1.0)
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.checkForCloseContacts("ind:::", "ind:::", 1.0, 3.0, distSelectorClass=CloserThanVDWSelector, constant=0.0, log=0)')


    
    def test_check_for_close_log_checks_that_it_runs(self):
        """Checking log string runs """
        from MolKit.distanceSelector import CloserThanVDWSelector
        self.mv.readMolecule('Data/ind.pdb')
        oldself=self
        self =mv
        s = 'self.checkForCloseContacts("ind:::", "ind:::", 1.0, 3.0, distSelectorClass=CloserThanVDWSelector, log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_modify_terminus_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/piece_modterm.pdbqs')
        at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
        b1 = [at1.bonds[0]]
        b2 = [at1.bonds[4]]
        b3 = [at1.bonds[1]]
        #self.mv.removeBonds(b1, log=0)
        #self.mv.removeBonds(b2, log=0)
        #self.mv.removeBonds(b3, log=0)
        at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
        #self.mv.removeBonds([at2.bonds[1]], log=0)
        old_len=len(mv.allAtoms.bonds[0])
        self.mv.modifyTermini(self.mv.getSelection())
        if self.mv.hasGui: 
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')
            self.assertEqual(split(last_entry,'\n')[0],'self.modifyTermini("piece_modterm", 1, log=0)')


    def test_modify_terminus_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/piece_modterm.pdbqs')
        at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
        b1 = [at1.bonds[0]]
        b2 = [at1.bonds[4]]
        b3 = [at1.bonds[1]]
        #self.mv.removeBonds(b1, log=0)
        #self.mv.removeBonds(b2, log=0)
        #self.mv.removeBonds(b3, log=0)
        #at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
        #self.mv.removeBonds([at2.bonds[1]], log=0)
        old_len=len(mv.allAtoms.bonds[0])
        oldself=self
        self =mv
        s = 'self.modifyTermini("piece_modterm:::", 1, log=0)'
        exec(s)
        oldself.assertEqual(1,1)


    def test_modify_C_terminus_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/piece_modterm.pdbqs')
        at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
        #self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        b1 = [at1.bonds[0]]
        b2 = [at1.bonds[4]]
        b3 = [at1.bonds[1]]
        self.mv.removeBonds(b1, log=0)
        self.mv.removeBonds(b2, log=0)
        self.mv.removeBonds(b3, log=0)
        at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
        #self.mv.removeBonds([at2.bonds[1]], log=0)
        len_old=len(mv.allAtoms.bonds[0])
        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyCTerminus(resSet)
        if self.mv.hasGui: 
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-3.0)
            last_entry = tx.get(last_entry_index, 'end')
            self.assertEqual(split(last_entry,'\n')[0],'self.modifyCTerminus("piece_modterm:::", log=0)')


    #def Xtest_modify_C_terminus_log_checks_that_it_runs(self):
        # """Checking log string runs DOESN'T WORK FIX THIS"""
        # self.mv.readMolecule('Data/piece_modterm.pdbqs')
        # self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN1", log=0)
        # self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:N", log=0)
        # self.mv.removeBonds("piece_modterm:B:THR12:C","piece_modterm:B:ILE13:HN2", log=0)
        # self.mv.removeBonds("piece_modterm:B:THR12:HC","piece_modterm:B:ILE13:N", log=0)      
        # oldself=self
        # self =mv
        # #self.deleteAtomSet("piece_modterm:B:THR12:HC;piece_modterm:B:ILE13:HC;", log=0)
        # s = 'self.modifyCTerminus("piece_modterm:::", log=0)'
        # exec(s)
        # oldself.assertEqual(1,1)
           
    
    def test_modify_N_terminus_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/piece_modterm.pdbqs')
        at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
        b1 = [at1.bonds[0]]
        b2 = [at1.bonds[4]]
        b3 = [at1.bonds[1]]
        self.mv.removeBonds(b1, log=0)
        self.mv.removeBonds(b2, log=0)
        #self.mv.removeBonds(b3, log=0)
        at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
        #self.mv.removeBonds([at2.bonds[1]], log=0)

        resSet=self.mv.Mols[0].chains.residues
        self.mv.modifyNTerminus(resSet)
        if self.mv.hasGui: 
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-4.0)
            last_entry = tx.get(last_entry_index, 'end')
            self.assertEqual(split(last_entry,'\n')[0],'self.modifyNTerminus("piece_modterm:::", 1, log=0)')      

    def test_modify_N_terminus_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/piece_modterm.pdbqs')
        at1 = self.mv.expandNodes("piece_modterm:B:THR12:C")[0]
        b1 = [at1.bonds[0]]
        b2 = [at1.bonds[4]]
        b3 = [at1.bonds[1]]
        #self.mv.removeBonds(b1, log=0)
        #self.mv.removeBonds(b2, log=0)
        #self.mv.removeBonds(b3, log=0)
        at2 = self.mv.expandNodes("piece_modterm:B:THR12:HC")[0]
        #self.mv.removeBonds([at2.bonds[1]], log=0)
        oldself=self
        self =mv
        
        s = 'self.modifyNTerminus("piece_modterm::", 1, log=0)'
        exec(s)
        oldself.assertEqual(1,1)

        

if __name__ == '__main__':
    test_cases = [
       'Pmvrepair_editHistTests',
       'Pmvrepair_checkForMissingAtomTests',
       'Pmvrepair_checkForCloseContactsTests',
       'Pmvrepair_repairMissingAtomTests',
       'Pmvrepair_add_oxtTests',
       'Pmvrepair_modify_termini_Tests',
       'Pmvrepair_modify_C_terminus_Tests',
       'Pmvrepair_modify_N_terminus_Tests',
       'Pmvrepair_LogTests'
        ]
    
    unittest.main( argv=([__name__ ,] + test_cases) )

    #unittest.main()

