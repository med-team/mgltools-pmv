#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_deleteCommands.py,v 1.20 2011/11/02 23:09:11 annao Exp $
#
# $Id: test_deleteCommands.py,v 1.20 2011/11/02 23:09:11 annao Exp $
#

import sys,unittest,Pmv,time
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from string import split
mv = None
klass = None
ct = 0
totalCt = 44
#totalCt = 101 ???
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class DeleteBaseTests(unittest.TestCase):

    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                    trapExceptions=False, withShell=0, gui=hasGUI)
            #mv = MoleculeViewer(customizer = './.empty', logMode = 'no', withShell=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands',package= 'Pmv')
            mv.browseCommands('deleteCommands',package='Pmv') 
            mv.browseCommands('colorCommands',package='Pmv')
            mv.browseCommands("interactiveCommands",package='Pmv')
            mv.browseCommands("displayCommands",
                              commands=['displaySticksAndBalls','undisplaySticksAndBalls',
                                        'displayCPK', 'undisplayCPK',
                                        'displayLines','undisplayLines',
                                        'displayBackboneTrace','undisplayBackboneTrace',
                                        'DisplayBoundGeom'
                                        ], package = 'Pmv')
            mv.browseCommands("bondsCommands",
                          commands=['buildBondsByDistance'],
                          package='Pmv', log = 0)
        
            mv.setOnAddObjectCommands(['buildBondsByDistance',
                                       'displayLines'],
                                  log=0)
        self.mv = mv

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
            
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for i in range(len(self.mv.Mols)):
            self.mv.deleteMol(self.mv.Mols[-1])
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv


################################################################
#### DELETE MOL 
################################################################
class DeleteMol(DeleteBaseTests):
    def test_deleteMol1(self):
       """ tests reading one molecule and deleting it
       """
       self.mv.readMolecule('./Data/7ins.pdb')
       self.assertEqual( len(mv.Mols) , 1)
       self.assertEqual( mv.Mols[0].name , '7ins')
       self.mv.deleteMol(mv.Mols[0])
       self.assertEqual( len(mv.Mols) , 0)
       
    def test_deleteMol_read_two_delete_one(self):
        """tests reading two mols and deleting one mol
        """
        self.mv.readMolecule('./Data/7ins.pdb')    
        self.mv.readMolecule('./Data/1crn.pdb')    
        self.mv.deleteMol(mv.Mols[0])
        self.assertEqual( len(mv.Mols) , 1)
   
    def test_deleteMol_read_one_delete_other(self):
        """reading one mol and deleting some other mol
        """
        self.mv.readMolecule('./Data/7ins.pdb')    
        self.mv.deleteMol("1crn")
        self.assertEqual( len(mv.Mols) , 1)
 
        
    def test_deleteMol_delete_two_times(self):
       """tests deleting mol two times
       """
       self.mv.readMolecule('./Data/7ins.pdb')
       self.assertEqual( len(mv.Mols) , 1)
       self.assertEqual( mv.Mols[0].name , '7ins')
       self.mv.deleteMol(mv.Mols[0])
       c=self.mv.deleteMol
       returnValue = c("7ins")
       self.assertEqual(returnValue ,None)
    
    def test_deleteMol_invalid_input(self):
       """tests invalid nodes for deletemol
       """
       c=self.mv.deleteMol
       returnValue = c("abcd")
       self.assertEqual(returnValue ,None)    

    def test_deleteMol_empty_input(self):
       """tests empty nodes for delte mol
       """
       c=self.mv.deleteMol
       returnValue = c(" ")
       self.assertEqual(returnValue ,None)
       

################################################################
#### DELETE ALL MOLECULES 
################################################################
class DeleteAllMolecules(DeleteBaseTests):
    def test_deleteAllMolecules(self):
        """ tests reading one molecule and deleting all molecules
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(self.mv.Mols) , 1)
        self.assertEqual( self.mv.Mols[0].name , '7ins')
        self.mv.deleteAllMolecules()
        self.assertEqual( len(self.mv.Mols) , 0)
       

    def test_deleteAllMolecules_continue_cb(self):
        """tests deleting all molecules continue_cb method works
        """
        self.mv.readMolecule('./Data/7ins.pdb')    
        self.mv.readMolecule('./Data/1crn.pdb')    
        self.mv.deleteAllMolecules.continue_cb()
        self.assertEqual( len(self.mv.Mols) , 0)
        
    def test_deleteAllMolecules_cancel_cb(self):
        """tests deleting all molecules cancel_cb method works
        """
        self.mv.readMolecule('./Data/7ins.pdb')    
        self.mv.readMolecule('./Data/1crn.pdb')    
        self.mv.deleteAllMolecules.cancel_cb()
        self.assertEqual( len(self.mv.Mols) , 2)
        
    def test_deleteAllMolecules_when_there_are_none(self):
        """tests deleting allMolecules when there are not any
        """
        self.assertEqual( len(self.mv.Mols) , 0)
        returnValue = self.mv.deleteAllMolecules()
        self.assertEqual(returnValue ,None)
    

    def test_deleteMol_invalid_input(self):
        """tests invalid nodes for deletemol
        """
        c=self.mv.deleteMol
        returnValue = c("abcd")
        self.assertEqual(returnValue ,None)    

    def test_deleteMol_empty_input(self):
        """tests empty nodes for delte mol
        """
        c=self.mv.deleteMol
        returnValue = c(" ")
        self.assertEqual(returnValue ,None)
        

################################################################
#### DELETE ATOMSET
################################################################

class DeleteAtomSet(DeleteBaseTests):
    
    def test_deleteAtomSet_1(self):
        """test deleteAtomSet of 0 atoms returns ERROR
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '7ins')
        result = self.mv.deleteAtomSet(AtomSet([]))
        self.assertEqual(result , 'ERROR')
    
        

    def test_deleteAtomSet_2(self):
        """test deleteAtomSet of 1 atom removes one atom
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '7ins')
        ct = len(self.mv.Mols[0].allAtoms)
        at = self.mv.Mols[0].allAtoms[0]
        self.mv.deleteAtomSet(AtomSet([at]))
        self.assertEqual(len(self.mv.Mols[0].allAtoms) , ct - 1)
    


    def test_deleteAtomSet_3(self):
        """test deleteAtomSet of 10 atoms removes ten atoms
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '7ins')
        ct = len(self.mv.Mols[0].allAtoms)
        self.mv.deleteAtomSet(self.mv.Mols[0].allAtoms[:10])
        self.assertEqual(len(self.mv.Mols[0].allAtoms) , ct - 10)
    

        
    def test_deleteAtomSet_4(self):
        """test deleteAtomSet of mv.allAtoms removes all atoms
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '7ins')
        self.mv.deleteAtomSet(self.mv.allAtoms)
        self.assertEqual(len(self.mv.Mols) , 0)
        self.assertEqual(len(self.mv.allAtoms) , 0)

        
    def test_deleteAtomSet_5(self):
        """test deleteAtomSet of molecule.allAtoms removes molecule
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '7ins')
        self.mv.deleteAtomSet(self.mv.Mols[0].allAtoms)
        self.assertEqual(len(self.mv.Mols) , 0)
        self.assertEqual(len(self.mv.allAtoms) , 0)
        

    def test_deleteAtomSet_6(self):
        """test deleteAtomSet of molecule.allAtoms removes molecule but not second mol
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.mv.readMolecule('./Data/1crn.pdb')
        self.assertEqual( len(mv.Mols) , 2)
        self.assertEqual( mv.Mols[0].name , '7ins')
        self.mv.deleteAtomSet(self.mv.Mols[0].allAtoms)
        self.assertEqual( mv.Mols[0].name , '1crn')
        self.assertEqual(len(self.mv.allAtoms) >= 300,True)
        self.mv.deleteMol(self.mv.Mols[0])
        self.assertEqual(len(self.mv.Mols) , 0)


    def test_deleteAtomSet_7(self):
        """test deleteAtomSet of some atoms from each of 2 molecules
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.mv.readMolecule('./Data/1crn.pdb')
        self.assertEqual( len(mv.Mols) , 2)
        self.assertEqual( mv.Mols[0].name , '7ins')
        ct = len(self.mv.allAtoms)
        ats = self.mv.Mols[0].allAtoms[:10] + self.mv.Mols[1].allAtoms[:10]
        self.mv.deleteAtomSet(ats)
        self.assertEqual(len(self.mv.allAtoms) , ct - 20)
        self.assertNotEqual(len(self.mv.allAtoms) , 0)
    


    def test_deleteAtomSet_8(self):
        """test deleteAtomSet of all atoms from each of 2 molecules
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.mv.readMolecule('./Data/1crn.pdb')
        self.assertEqual( len(mv.Mols) , 2)
        self.assertEqual( mv.Mols[0].name , '7ins')
        self.mv.deleteAtomSet(self.mv.Mols[0].allAtoms + self.mv.Mols[1].allAtoms)
        self.assertEqual(len(self.mv.Mols) , 0)
        self.assertEqual(len(self.mv.allAtoms), 0)


    def test_deleteAtomSet_9(self):
        #delete all the atoms in the viewer
        """test deleteAtomSet of all atoms in viewer with 2 molecules
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.mv.readMolecule('./Data/1crn.pdb')
        self.assertEqual( len(mv.Mols) , 2)
        self.assertEqual( mv.Mols[0].name , '7ins')
        ct = self.mv.allAtoms
        self.mv.deleteAtomSet(self.mv.allAtoms)
        self.assertEqual(len(self.mv.Mols) , 0)
        self.assertEqual(len(self.mv.allAtoms), 0)


    def test_deleteAtomSet_10(self):
        """test deleteAtomSet of a selection
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '7ins')
        self.mv.select(self.mv.Mols[0].chains.residues.atoms[:])
        #print 'current selection=', self.mv.getSelection()
        self.mv.deleteAtomSet(self.mv.getSelection())
        self.assertEqual(len(self.mv.allAtoms) , 0)
        self.assertEqual(len(self.mv.Mols) , 0)


    def test_deleteAtomSet_11(self):
        """test deleteAtomSet of a specific atom by name
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , '1crn')
        ct = len(self.mv.allAtoms)
        #cmd calls expandNodes which changes name into an AtomSet
        self.mv.deleteAtomSet('1crn: :THR1:N')
        self.assertEqual(len(self.mv.allAtoms) , ct-1)
        self.mv.deleteMol('1crn')
        self.assertEqual(len(self.mv.Mols),0)
    
    def test_deleteAtomSet_12(self):
        """test deleteAtomSet of all atom in a specific residueSet
        """
        self.mv.readMolecule('./Data/fx.pdb')
        self.assertEqual( len(mv.Mols) , 1)
        self.assertEqual( mv.Mols[0].name , 'fx')
        self.mv.browseCommands("selectionCommands", commands=['selectFromString',],
                      package="Pmv",log=False)
        self.mv.setSelectionLevel(Residue)
        self.mv.selectFromString('', '', 'HOH*', '', negate=False,
                        silent=True, log=False)
        waters = self.mv.getSelection()[:]
        #cmd calls expandNodes which changes name into an AtomSet
        self.mv.deleteAtomSet(waters)
        self.assertEqual(len(self.mv.Mols[0].chains.residues.get(lambda x: x.name[:3]=="HOH")),0)
        mol = self.mv.Mols[0]
        res =  mol.chains[0].residues[:5]
        result = mol.chains[0].residues[5:8]
        self.mv.select(mol.chains[0].residues[:8])
        self.mv.deleteAtomSet(res)
        resInSel = self.mv.getSelection()
        resInSel.sort()
        result.sort()
        self.assertEqual(resInSel , result)
        #self.assertEqual(len(self.mv.allAtoms) == ct-1
        self.mv.deleteMol("fx")
        self.assertEqual(len(self.mv.Mols),0)

    def test_deleteAtomSet_13(self):
        #delete HOH* atoms and check gc.atoms['nobnds'] is updated
        """test deleteAtomSet of all atom in a specific residueSet updates geomContainer
        """
        self.mv.readMolecule('./Data/hsg1_HOHs.pdb')
        mol = self.mv.Mols[0]
        gc = mol.geomContainer
        self.assertEqual( len(gc.atoms['nobnds']), 127)
        self.mv.browseCommands("selectionCommands", commands=['selectFromString',],
                      package="Pmv",log=False)
        self.mv.setSelectionLevel(Residue)
        self.mv.selectFromString('', '', 'HOH*', '', negate=False,
                        silent=True, log=False)
        #cmd calls expandNodes which changes name into an AtomSet
        self.mv.deleteAtomSet(self.mv.getSelection())
        self.assertEqual( len(gc.atoms['nobnds']), 0)
    

    def test_deleteAtomSet_invalid_input(self):
        """tests inavlid input for delete AtomSet
        """
        self.mv.readMolecule('./Data/hsg1.pdb')
        command =mv.deleteAtomSet
        returnValue = command("hai")
        self.assertEqual(returnValue, 'ERROR')
        
    def test_deleteAtomSet_empty_input(self):
        """tests empty input for delete atom set
        """
        self.mv.readMolecule('./Data/hsg1.pdb')
        command =mv.deleteAtomSet
        returnValue = command(" ")
        self.assertEqual(returnValue, 'ERROR')        

class DeleteHydrogens(DeleteBaseTests):

    def test_deleteHydrogens(self):
        """tests delete Hydrogens
        """
        self.mv.readMolecule('./Data/hsg1.pdb')
        all_hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        self.mv.deleteHydrogens(all_hydrogens)
        new_hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        self.assertEqual(len(all_hydrogens),330)
        self.assertEqual(len(new_hydrogens),0)
    
   
    def test_deleteHydrogens_select(self):
        """tests delete hydrogens ,input through selction
        """
        self.mv.readMolecule('./Data/hsg1.pdb')
        all_hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        self.mv.select(all_hydrogens)
        self.mv.deleteHydrogens(self.mv.getSelection())
        new_hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        self.assertEqual(len(new_hydrogens),0)
    
    def test_deleteHydrogens_mol_with_no_hydrogens(self):
        """tests deleteHydrogens,deleting H from mol with no H
        """
        self.mv.readMolecule('./Data/1crn.pdb')    
        all_hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        command = self.mv.deleteHydrogens
        returnValue = command(all_hydrogens)
        self.assertEqual(returnValue,'ERROR')

    def test_deleteHydrogens_invalid_nodes(self):
        """tests invalid input for deleteHydrogens
        """
        command = self.mv.deleteHydrogens
        returnValue = command("hello")
        self.assertEqual(returnValue,'ERROR')    

    def test_deleteHydrogens_empty_nodes(self):
        """tests empty input for deleteHydrogens
        """
        command = self.mv.deleteHydrogens
        returnValue = command(" ")
        self.assertEqual(returnValue,'ERROR')    

    
    def test_deleteHydrogens_read_one_delete_another(self):
        """tests reading one molecule and deleting H from another
        """
        self.mv.readMolecule('./Data/hsg1.pdb')
        command = self.mv.deleteHydrogens
        returnValue = command("1crn")
        self.assertEqual(returnValue,'ERROR')


    def test_deleteHydrogens_two_mols(self):
        """tests reading two mol and deleteHydrogens 
        """
        self.mv.readMolecule('./Data/hsg1.pdb')    
        self.mv.readMolecule('./Data/1crn_hs.pdb')
        self.mv.deleteHydrogens(self.mv.allAtoms)
        hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        self.assertEqual(len(hydrogens),0)


    def test_deleteHydrogens_two_mols_delete_from_one_mol(self):
        """ tests reading two mol and deleteHydrogens from one
        """
        self.mv.readMolecule('./Data/hsg1.pdb')    
        self.mv.readMolecule('./Data/1crn_hs.pdb')
        all_hydrogens = self.mv.Mols[1].allAtoms.get(lambda x : x.element == 'H')
        self.mv.deleteHydrogens(self.mv.allAtoms)
        new_hydrogens = self.mv.Mols[1].allAtoms.get(lambda x : x.element == 'H')
        self.assertEqual(len(new_hydrogens),0)    


class DeleteCommandsLogTest(DeleteBaseTests):
    
    def test_delete_mol_log_checks_expected_log_string(self):
        """checks expected log string is written for deleteMol"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('./Data/trp3_h.pdb')
        self.mv.deleteMol(mv.Mols[0])
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.deleteMol("trp3_h", log=0, undoable=False)')
    
    def test_delete_mol_log_checks_that_it_runs(self):
        """Checking log string runs for deleteMol
        """
        self.mv.readMolecule('./Data/trp3_h.pdb')
        oldself =self
        self=mv
        s = 'self.deleteMol("trp3_h", log=0)'
        exec(s)
        self=oldself
        self.assertEqual(len(mv.Mols),0)
        
    def test_log_delete_all_molecules_checks_expected_log_string(self):
        """checks expected log string is written for deleteAllMolecules"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('./Data/trp3_h.pdb')
        self.mv.deleteAllMolecules()
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.deleteAllMolecules(log=0)')
    
    def test_log_delete_all_molecules_checks_that_it_runs(self):
        """Checking log string runs for deleteAllMolecules
        """
        self.mv.readMolecule('./Data/trp3_h.pdb')
        oldself =self
        self=mv
        s = 'self.deleteAllMolecules(log=0)'
        exec(s)
        self=oldself
        self.assertEqual(len(mv.Mols),0)
        
    def test_delete_AtomSet_log_checks_expected_log_string(self):
        """checks expected log string is written for deleteAtomSet"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('./Data/7ins.pdb')
        self.mv.deleteAtomSet(self.mv.allAtoms)
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.deleteAtomSet("7ins:::", log=0)')
        
     
    def test_delete_AtomSet_log_checks_that_it_runs(self):
        """Checking log string runs for deleteAtomSet
        """
        self.mv.readMolecule('./Data/7ins.pdb')
        oldself =self
        self=mv
        s = 'self.deleteAtomSet("7ins:::", log=0)'
        exec(s)
        self=oldself
        self.assertEqual(len(mv.Mols),0)

    def test_delete_hydrogens_log_checks_expected_log_string(self):
        """checks expected log string is written for deleteHydrogens"""
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('./Data/hsg1.pdb')
        all_hydrogens = self.mv.allAtoms.get(lambda x : x.element == 'H')
        self.mv.select(all_hydrogens[0])
        self.mv.deleteHydrogens(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.deleteHydrogens("hsg1:A:PRO1:HN1", log=0)')
        
        
    def test_delete_hydrogens_log_checks_that_it_runs(self):
        """Checking log string runs for deleteHydrogens
        """
        self.mv.readMolecule('./Data/hsg1.pdb')
        oldself =self
        self=mv
        oldhydrogens =mv.Mols[0].allAtoms.get(lambda x:x.name[0] == 'H')
        s = 'self.deleteHydrogens(oldhydrogens, log=0)'  
        exec(s)
        self=oldself
        newhydrogens =mv.Mols[0].allAtoms.get(lambda x:x.name[0] == 'H')
        self.assertEqual(len(newhydrogens),0)



if __name__ == '__main__':
    test_cases = [
        'DeleteMol',
        'DeleteAllMolecules',
        'DeleteAtomSet',
        'DeleteHydrogens',
        'DeleteCommandsLogTest',
    ]
    #unittest.main( argv=([__name__ , ] + test_cases) )
    #unittest.main( argv=([__name__ , '-v'] + test_cases) )
    unittest.main()
