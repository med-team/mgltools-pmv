#
# $Id: test_gridCommands.py,v 1.13.6.1 2016/02/11 21:29:43 annao Exp $
#
###########################################################################
#
#   Authors:Sowjanya KARNATI, Alex GILLET, Michel F. SANNER             
#   Copyright: M. Sanner TSRI 2004                         
#
###########################################################################

import sys,os
import unittest
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from Pmv.extruder import Sheet2D
"""
The test_extrusionCommands module implements a serie of function to test the
Pmv/extrusionCommands module.
"""
mv_local = None
ct = 0
totalCt = 31

class GridBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv_local
        if mv_local is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv_local = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                 withShell=0,
                                 verbose=False,trapExceptions=False)
                                 #withShell=0,
            #mv.setUserPreference(('trapExceptions', '0'), log = 0)
            mv_local.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv_local.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv_local.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv_local.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv_local.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv_local.browseCommands("interactiveCommands", package='Pmv')
            mv_local.browseCommands("colorCommands", package='Pmv')
            mv_local.browseCommands("selectionCommands", package='Pmv')
            mv_local.browseCommands('gridCommands', package='Pmv')
        self.mv = mv_local 
        for g in self.mv.grids.values():
            if g.geom:
                g.geom.protected = False
            if g.srf:
                g.srf.protected = False
        self.mv.grids = {}
           
        

    def setUp(self):
        """
        clean-up
        """
         
        if not hasattr(self, 'mv'):
            self.startViewer()
        #for g in self.mv.grids.values():
        #    g.geom.protected = False
        #    g.srf.protected = False
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        for k in self.mv.grids.keys():
            g = self.mv.grids.pop(k)
            if g.geom:
                g.geom.protected = False
            if g.srf:
                g.srf.protected = False
            if g.box:
                g.box.protected = False
            for k in g.slices.keys():
                for slice_list in g.slices[k].values():
                    for slice in slice_list:
                        slice.protected = False
            self.mv.GUI.VIEWER.RemoveObject(g.srf)
            #try:
            #    g.geom.protected = False
            #    g.srf.protected = False
            #    g.box.protected = False
            #    self.mv.GUI.VIEWER.RemoveObject(g.srf)
            #except:
            #    pass
            if hasattr(g, 'srf') and g.srf:
                delattr(g, 'srf')
            if hasattr(g, 'surfaceGUI') and g.surfaceGUI:
                delattr(g, 'surfaceGUI')
            if hasattr(g, 'slices') and g.slices:
                for k in g.slices.keys():
                    for slice_list in g.slices[k]:
                        for slice in slice_list:
                            slice.protected = False
                            self.mv.GUI.VIEWER.RemoveObject(slice)
                delattr(g, 'slices')
            del(g)
        self.mv.GUI.VIEWER.Redraw()
        self.mv.grids = {}
            
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for k in self.mv.grids.keys():
            g = self.mv.grids.pop(k)
            if g.geom:
                g.geom.protected = False
            if hasattr(g,'srf') and g.srf:
                g.srf.protected = False
            if hasattr(g,'box') and g.box:
                g.box.protected = False
            if hasattr(g, 'slices') and g.slices:
                for k in g.slices.keys():
                    for slice in g.slices[k]:
                        slice.protected = False
                        self.mv.GUI.VIEWER.RemoveObject(slice)
            if g.srf:
                self.mv.GUI.VIEWER.RemoveObject(g.srf)
            #try:
            #    g.geom.protected = False
            #    g.srf.protected = False
            #    g.box.protected = False
            #    self.mv.GUI.VIEWER.RemoveObject(g.srf)
            #except:
            #    pass
            if hasattr(g, 'srf'):
                delattr(g, 'srf')
            if hasattr(g, 'surfaceGUI'):
                delattr(g, 'surfaceGUI')
            if hasattr(g, 'slices'):
                for l in g.slices.values():
                    for geom in l:
                        geom.protected = False
                        try:
                            self.mv.GUI.VIEWER.RemoveObject(geom)
                        except:
                            pass
                delattr(g, 'slices')
            del(g)
        self.mv.GUI.VIEWER.Redraw()
        self.mv.grids = {}
            
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv

#####################################################
#   Read AutoGrid Command Tests                     #
#####################################################
class ReadAutoGrid(GridBaseTest):
    
    def test_read_auto_grid_file_name(self):
        """tests readAUTOGRID with valid input
        """
        c = self.mv.readAUTOGRID
        c('./Data/hsg1.O.map')
        self.assertEqual(self.mv.grids.keys()[0],'./Data/hsg1.O.map')
        #self.mv.GUI.VIEWER.RemoveObject(self.mv.grids['./Data/hsg1.O.map'])

    def test_read_auto_grid_invalid(self):
        """tests readAUTOGRID with invalid input raises IOError
        """
        c = self.mv.readAUTOGRID
        #rval = c("hello")
        #self.assertEqual(rval,None)
        self.assertRaises(IOError,c,("hello")) 

#    def test_read_auto_grid_empty(self):
#        """tests readAUTOGRID with empty input raises IOError
#        """
#        c = self.mv.readAUTOGRID
#        #rval = c(" ")
#        #self.assertEqual(rval,None)
#        #self.assertRaises(IOError,c,(" "))            
#        self.assertRaises(IndexError,c,(" "))            


#######################################################
#       GET ISOSURFACE COMMAND TESTS                  #
#######################################################
class AutoGridIsoSurface(GridBaseTest):

    def test_Auto_grid_isosurface_file_name(self):
        """tests getIsosurface with valid input
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')   
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        #FIX THIS!
        grid = self.mv.grids.values()[-1]
        self.assertEqual(hasattr(grid, 'srf'),True)
        grid.srf.protected = False
        #self.assertEqual(hasattr(self.mv.grids['./Data/hsg1.0.map'], 'srf'),True)
        c.Close_cb()

    def test_Auto_grid_isosurface_widget(self):
        """tests widget is getIsosurface displayed
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        c.buildForm()
        but = c.ifd.entryByName[0]['widget']
        #but.wait_visibility(but)
        self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        c.Close_cb()

    def test_Auto_grid_isosurface_display_map(self):
        """tests getIsosurface widget button display map
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        grid =self.mv.grids['./Data/hsg1.O.map']
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        c.ifd.entryByName['./Data/hsg1.O.mapVisBut']['widget'].deselect()
        #invoking display map button
        c.ifd.entryByName['./Data/hsg1.O.mapVisBut']['widget'].invoke()
        self.assertEqual(c.ifd.entryByName['./Data/hsg1.O.mapVisBut']['variable'].get(),1)
        #tests grd  surface is visible
        self.assertEqual(grid.srf.visible,1)
        c.Close_cb()

    def test_Auto_grid_isosurface_render_mode(self):
        """tests getIsosurface widget button mapRenderBut 
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        c.ifd.entryByName['./Data/hsg1.O.mapRenderBut']['widget'].deselect()
        #invoking render mode button
        c.ifd.entryByName['./Data/hsg1.O.mapRenderBut']['widget'].invoke()
        self.assertEqual(c.ifd.entryByName['./Data/hsg1.O.mapRenderBut']['variable'].get(),1)
        c.Close_cb()
        
    def test_Auto_grid_isosurface_show(self):
        """tests getIsosurface widget button show
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        grid =self.mv.grids['./Data/hsg1.O.map']
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        c.ifd.entryByName['./Data/hsg1.O.mapBoxBut']['widget'].deselect()
        #invoking show box button
        c.ifd.entryByName['./Data/hsg1.O.mapBoxBut']['widget'].invoke()
        self.assertEqual(c.ifd.entryByName['./Data/hsg1.O.mapBoxBut']['variable'].get(),1)
        #tests grid box is visible
        self.assertEqual(grid.box.visible,1)
        c.Close_cb()


    def test_Auto_grid_isosurface_sampling_value(self):
        """tests getIsosurface widget button sampling value
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        
        c.ifd.entryByName['./Data/hsg1.O.mapsetEnt']['widget'].delete(0)
        #setting value in sampling value
        c.ifd.entryByName['./Data/hsg1.O.mapsetEnt']['widget'].insert(0,'15.0')
        self.assertEqual(c.ifd.entryByName['./Data/hsg1.O.mapsetEnt']['widget'].get(),'15.0')
        c.Close_cb()
        
    def test_Auto_grid_isosurface_iso_value(self):
        """tests getIsosurface widget button iso value
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        #setting value in iso value entry button
        c.ifd.entryByName['./Data/hsg1.O.mapSlider']['widget'].set(0.17)
        self.assertEqual(c.ifd.entryByName['./Data/hsg1.O.mapSlider']['widget'].get(),0.17)
        c.Close_cb()

    def test_Auto_grid_isosurface_set_iso_value(self):
        """tests setIsovalue command
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        #setting value in iso value from log/command
        self.mv.setIsovalue('./Data/hsg1.O.map',0.18)
        self.assertEqual(c.ifd.entryByName['./Data/hsg1.O.mapSlider']['widget'].get(),0.18)
        c.Close_cb()

    def test_Auto_grid_isosurface_set_iso_value_to_wrong_map (self):
        """tests setisovalue to wrong map raises IOError
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        #setting value in iso value from log/command
        self.mv.setIsovalue('./Data/hsg.O.map',0.18)
        self.assertRaises(KeyError,c,("./Data/hsg.O.map")) 
        c.Close_cb()


    def test_Auto_grid_isosurface_grid(self):
        """tests grids on mv is read grid
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        c('./Data/hsg1.O.map')
        self.assertEqual(self.mv.grids.keys()[0],'./Data/hsg1.O.map')
        c.Close_cb()
        
    def test_Auto_grid_isosurface_two_grids(self):
        """tests when two grids are read
        and Isosurfaces command called for both,
        length of keys in form
        """
        #reading first auto grid
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        #getIsosurface for first
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getIsosurface
        #length of keys in input form
        len_old_keys = len(c.ifd.entryByName.keys())
        #reading second auto grid
        self.mv.readAUTOGRID('./Data/hsg1.C.map')
        #getIsosurface for second
        self.mv.getIsosurface('./Data/hsg1.C.map')
        #length of keys in input form
        len_new_keys = len(c.ifd.entryByName.keys())
        self.assertEqual(len_old_keys<len_new_keys,True)
        c.Close_cb()


    def test_Auto_grid_isosurface_invalid_file_name(self):
        """tests getIsosurface with invalid input raises KeyError
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        self.assertRaises(KeyError,c,("reew")) 
    

    def test_Auto_grid_isosurface_empty_file_name(self):
        """tests getIsosurface with empty input raises KeyError
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')    
        c = self.mv.getIsosurface
        self.assertRaises(KeyError,c,(" "))

######################################################
#  GET ORTHO SLICE COMMAND TESTS                     #
######################################################

class OrthoSliceSurface(GridBaseTest):
    
    def test_ortho_slice_file_name(self):
        """tests getOrthoSlice command
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('x')
        self.assertEqual(os.path.exists('Data/hsg1.O.map'),True)
        c.Close_cb()    
        #c1 = self.mv.getIsosurface
        
    
    def test_ortho_slice_lb(self):
        """tests selecting an entry in list box of get orthoslice widget
        """
        #first grid
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        #second grid
        self.mv.readAUTOGRID('./Data/hsg1.C.map')
        self.mv.getIsosurface('./Data/hsg1.C.map')
        c = self.mv.getOrthoSlice
        c('y')
        #clearing selection 
        c.lb.selection_clear(0,'end')
        #selecting an entry
        c.lb.selection_set(0)
        self.assertEqual(c.lb.selection_get(),'./Data/hsg1.O.map')
        c.Close_cb()
        

    def test_ortho_slice_addx(self):
        """tests getOrthoSlice addx button adds an x slice
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('z')
        old_keys = c.ifd.entryByName.keys()
        print "old_keys=", old_keys
        self.assertEqual('x0MaxEnt' in old_keys, True)
        grid = self.mv.grids.values()[0]
        #keys are 'x', 'y','z' values are list of slices 
        x_slices = grid.slices['x']
        self.assertEqual(len(x_slices),0)
        c.lb.selection_set(0)
        c.ifd.entryByName['addX']['widget'].invoke()
        x_slices = grid.slices['x']
        self.assertEqual(len(x_slices),1)
        c.Close_cb()
        
        
    def test_ortho_slice_showall(self):
        """testing getOrthoSlice show all button
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('x')
        c.lb.selection_set(0)
        c.ifd.entryByName['addX']['widget'].invoke()
        c.ifd.entryByName['showAll']['widget'].deselect()
        c.ifd.entryByName['showAll']['widget'].invoke(),
        self.assertEqual(c.ifd.entryByName['showAll']['variable'].get(),1)
        c.Close_cb()
    
    def test_ortho_slice_X0visBut(self):
        """tests getOrthoSlice, x slice visible or not
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        grid =self.mv.grids['./Data/hsg1.O.map']
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('x')
        c.lb.selection_set(0)
        c.ifd.entryByName['addX']['widget'].invoke()
        c.ifd.entryByName['x0VisBut']['widget'].deselect()
        c.ifd.entryByName['x0VisBut']['widget'].invoke()
        #grid = self.mv.grids['./Data/hsg1.O.map']
        self.assertEqual(c.ifd.entryByName['x0VisBut']['variable'].get(),1)
        self.assertEqual(grid.slices['x'][0].visible,True)
        c.Close_cb()
        
    def test_ortho_slice_x0Scale(self):
        """tests getOrthoSlice ,setting X0 scale
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('x')
        c.lb.selection_set(0)
        c.ifd.entryByName['addX']['widget'].invoke()
        c.ifd.entryByName['x0Scale']['widget'].set(20)
        self.assertEqual(c.ifd.entryByName['x0Scale']['widget'].get(),20)
        c.Close_cb()
        
    def test_ortho_slice_x0MinEnt(self):
        """tests getOrthoSlice ,setting x0MinEnt
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('x')
        c.lb.selection_set(0)
        c.ifd.entryByName['addX']['widget'].invoke()
        c.ifd.entryByName['x0MinEnt']['widget'].delete(0,'end')
        c.ifd.entryByName['x0MinEnt']['widget'].insert(0,10)
        self.assertEqual(c.ifd.entryByName['x0MinEnt']['widget'].get(),'10')
        c.Close_cb()
        
    def test_ortho_slice_x0MaxEnt(self):
        """tests getOrthoSlice,setting x0MaxEnt
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('x')
        c.lb.selection_set(0)
        c.ifd.entryByName['addX']['widget'].invoke()
        c.ifd.entryByName['x0MaxEnt']['widget'].delete(0,'end')
        c.ifd.entryByName['x0MaxEnt']['widget'].insert(0,100)
        self.assertEqual(c.ifd.entryByName['x0MaxEnt']['widget'].get(),'100')
        c.Close_cb()
        
    def test_ortho_slice_addY(self):
        """tests getOrthoSlice, add Y button
        """
        pass
#        self.mv.readAUTOGRID('./Data/hsg1.O.map')
#        c = self.mv.getOrthoSlice
#        c('y')
#        len_old_keys = len(c.ifd.entryByName.keys())
#        ###FIX THIS
#       c.lb.selection_set(0)
#       c.ifd.entryByName['addY']['widget'].invoke()
#       #new keys Y0visBut,y0Scale
#       len_new_keys = len(c.ifd.entryByName.keys())
#       self.assertEqual(len_old_keys<len_new_keys,True)
#       c.Close_cb()

    def test_ortho_slice_Y0visBut(self):
        """tests getOrthoSlice, Y slice is visible
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        grid =self.mv.grids['./Data/hsg1.O.map']
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('y')
        c.lb.selection_set(0)
        c.ifd.entryByName['addY']['widget'].invoke()
        c.ifd.entryByName['y0VisBut']['widget'].deselect()
        c.ifd.entryByName['y0VisBut']['widget'].invoke()
        #grid = self.mv.grids['./Data/hsg1.O.map']
        self.assertEqual(c.ifd.entryByName['y0VisBut']['variable'].get(),1)
        self.assertEqual(grid.slices['y'][0].visible,True)
        c.Close_cb()
        
    def test_ortho_slice_y0Scale(self):
        """tests getOrthoSlice,setting Y scale value
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('y')
        c.lb.selection_set(0)
        c.ifd.entryByName['addY']['widget'].invoke()
        c.ifd.entryByName['y0Scale']['widget'].set(12)
        self.assertEqual(c.ifd.entryByName['y0Scale']['widget'].get(),12)
        c.Close_cb()
    

    def test_ortho_slice_addZ(self):
        """tests getorthoslice addZ button
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('z')
        len_old_keys = len(c.ifd.entryByName.keys())
        c.lb.selection_set(0)
        c.ifd.entryByName['addZ']['widget'].invoke()
        #newkeys z0visBut,z0Scale
        len_new_keys = len(c.ifd.entryByName.keys())
        self.assertEqual(len_old_keys<len_new_keys,True)
        c.Close_cb()

    def test_ortho_slice_z0visBut(self):
        """tests getOrthoSlice,Z slice is visible
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        grid =self.mv.grids['./Data/hsg1.O.map']
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('z')
        c.lb.selection_set(0)
        c.ifd.entryByName['addZ']['widget'].invoke()
        c.ifd.entryByName['z0VisBut']['widget'].deselect()
        c.ifd.entryByName['z0VisBut']['widget'].invoke(),
        #grid = self.mv.grids['./Data/hsg1.O.map']
        self.assertEqual(c.ifd.entryByName['z0VisBut']['variable'].get(),1)
        self.assertEqual(grid.slices['z'][0].visible,True)
        c.Close_cb()
    
    def test_ortho_slice_z0Scale(self):
        """tests getOrthoSlice,setting z scale value
        """
        self.mv.readAUTOGRID('./Data/hsg1.O.map')
        self.mv.getIsosurface('./Data/hsg1.O.map')
        c = self.mv.getOrthoSlice
        c('z')
        c.lb.selection_set(0)
        c.ifd.entryByName['addZ']['widget'].invoke()
        c.ifd.entryByName['z0Scale']['widget'].set(12)
        self.assertEqual(c.ifd.entryByName['z0Scale']['widget'].get(),12)
        c.Close_cb()
        
    def test_ortho_slice_invalid(self):
        """tests getOrthoSlice with invalid input raises KeyError
        """
        c = self.mv.getOrthoSlice
        self.assertRaises(KeyError,c,("hello")) 

    def test_ortho_slice_empty(self):
        """tests getOrthoSlice with empty input raises KeyError
        """
        c = self.mv.getOrthoSlice
        self.assertRaises(KeyError,c,(" "))    

if __name__ == '__main__':
    unittest.main()






        
        
