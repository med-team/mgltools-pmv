############################################################################
#
# Author: Michel Sanner
#
# Copyright: M. Sanner TSRI 2011
#
#############################################################################
#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_dashboard.py,v 1.3 2011/09/14 19:58:57 annao Exp $
#
# $Id: test_dashboard.py,v 1.3 2011/09/14 19:58:57 annao Exp $
#
import sys,unittest,Pmv,time
klass = None
mv = None
ct = 0
totalCt = 1

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class DashboardBaseTests(unittest.TestCase):


    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(logMode = 'no',
                                trapExceptions=False, gui=hasGUI,
                                withShell=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)

        self.mv = mv

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            print 'setup: destroying mv'
            if mv and mv.hasGui:
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name 
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv 
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            print "total count:", ct
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None


class DashboardTests(DashboardBaseTests):

    def test_AllMoleculeButtons(self):
        """checks color widget is diplayed
        """
        assert len(self.mv.dashboard.tree.columns)==8
        
        # Reading the test molecule
        self.mv.readMolecule("./Data/1crn.pdb")

        # selection assertions
        def f01():
            self.assertEqual(self.mv.getSelection(), self.mv.Mols[0].allAtoms)
        def f02():
            self.assertEqual(len(self.mv.selection), 0)

        # lines assertions
        def f11():
            self.assertEqual(
                len(self.mv.Mols[0].geomContainer.atoms['bonded']), 327)
        def f12():
            self.assertEqual(
                len(self.mv.Mols[0].geomContainer.atoms['bonded']), 0)

        # balls and sticks
        def f21():
            self.assertEqual(
                len(self.mv.Mols[0].geomContainer.atoms['sticks']), 327)
            self.assertEqual(
                    len(self.mv.Mols[0].geomContainer.atoms['balls']), 327)
        def f22():
            self.assertEqual(
                len(self.mv.Mols[0].geomContainer.atoms['sticks']), 0)
            self.assertEqual(
                    len(self.mv.Mols[0].geomContainer.atoms['balls']), 0)

        # cpk
        def f31():
            self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk']),
                             327)
        def f32():
            self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['cpk']),0)

        # ribbon
        def f41():
            self.assertEqual(
                len(self.mv.Mols[0].geomContainer.geoms['secondarystructure'].children), 10)
        def f42():
            for child in self.mv.Mols[0].geomContainer.geoms['secondarystructure'].children:
                self.assertEqual(len(child.getFaces()), 0)

        # msms
        def f51(): assert True
        def f52(): assert True
        asserts = [ [f01, f02], [f11, f12], [f21, f22], [f31, f32], [f41, f42],
                    [f51, f52]]
        
        # get a handle to top 3 lines in dashboard
        rootNode = self.mv.dashboard.tree.root
        selectionNode = self.mv.dashboard.tree.root.children[0]
        molNode = self.mv.dashboard.tree.root.children[1]
        vi = self.mv.GUI.VIEWER

        vi.stopAutoRedraw()
        # simulate clicking on green and red buttons in 6 first columns
        for node in [rootNode, selectionNode]:
            for col in range(6):
                if node is selectionNode and col==0: continue
                # invoke green button on All Molecules for selection column
                value  = 1
                node.onOffButtonClick( col, value)
                vi.OneRedraw()
                asserts[col][0]()

                # invoke red button on All Molecules for selection column
                value  = 0
                node.onOffButtonClick( col, value)
                vi.OneRedraw()
                asserts[col][1]()

        # simulate clicking on check button twice in 6 first columns
        for node in [molNode]:
            for col in range(6):
                # invoke green button on All Molecules for selection column
                node.buttonClick( col)
                vi.OneRedraw()
                asserts[col][0]()

                # invoke red button on All Molecules for selection column
                node.buttonClick( col)
                vi.OneRedraw()
                asserts[col][1]()

        vi.startAutoRedraw()
