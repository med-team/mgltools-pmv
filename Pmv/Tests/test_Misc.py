#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_Misc.py,v 1.42 2013/01/29 23:25:52 annao Exp $
#
# $Id: test_Misc.py,v 1.42 2013/01/29 23:25:52 annao Exp $
#

import sys
import unittest
from mglutil.regression import testplus

"""
This module implements a set of functions to test miscellaneous things in Pmv
"""


mv = None
ct = 0
totalCt = 1

class LogTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        from Pmv.moleculeViewer import MoleculeViewer
        
        from MolKit import Read
        import Tkinter
        #if mv is None:
        if not hasattr(self, "mv"):
            if mv is not None:
                from Pmv import colorCommands
                # this is to rebuild ColorPalette GUI
                # and avoid error when the color commands are loaded into the viewer.  
                reload(colorCommands)
            mv = MoleculeViewer(trapExceptions=False,
                            logMode='overwrite',
                            withShell=0,
                            #verbose=False
                            )
            mv.setUserPreference(('warningMsgFormat', 'printed'))
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                           package='Pmv')
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            #mv.browseCommands('fileCommands','Pmv')
            mv.loadModule('deleteCommands')
            mv.loadModule("bondsCommands")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)

            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands("helpCommands", package='ViewerFramework')
            # Don't want to trap exceptions and errors... the user pref is set to 1 by
            # default
            mv.setUserPreference(('warningMsgFormat', 'printed'))
            mv.setUserPreference(('Transformation Logging', 'no'))
            #set up links to shared dictionary and current instance
            
            self.mv = mv
            
        


    def setUp(self):
        """
        setUp
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
 

    def tearDown(self):
        """
        cleanUp
        """
        global ct, totalCt
        ct = ct + 1
        if ct==totalCt:
            if hasattr(self, 'mv'):
                print 'destroying mv'
                self.mv.Exit(0)
                del self.mv




class SourceLogTests(LogTests):
#    def test_SourceLogBug_1(self):
#        self.mv.source("./Data/littleScript.py")
#        # Need to make sure that the log string has been added to mvAll.log.py
#        from Tkinter import END
#        logStr = self.mv.GUI.MESSAGE_BOX.tx.get(1.0, END)
#        logList = logStr.split("\n")
#        logList.reverse()
#        ind = 0
#        item = logList[0]
#        #remove possible extra newline characters
#        while item=="":
#            ind = ind + 1
#            item = logList[ind]
#        self.assertEqual(item, 'self.colorByAtomType("1crn", log=0)')
#        self.mv.Exit(0)
#        delattr(self, 'mv')
#
#        #logFile = open("./mvAll.log.py", 'r')
#        #allLogs = logFile.readlines()
#        #self.assertEqual(allLogs[-1], "self.source('./Data/littleScript.py', log = 0)\n")
#        #logFile.close()
    

    def test_SourceTexturedCloudsWithAlpha(self):

        self.mv.GUI.setGeom(20,20,730,600)

        import Image
        import numpy
        from DejaVu.Texture import Texture
        from DejaVu.IndexedPolygons import IndexedPolygons
        from opengltk.OpenGL import GL
        im = Image.open('Data/clouds/image.tif').convert('RGB')
        imageString = ''.join([['\xff','\x00'][int(all([v > 245 for v in p]))] for p in list(im.getdata())])
        im.putalpha(Image.fromstring("L", im.size, imageString))
        im2D = [ord(c) for c in list(im.tostring())]
        im2D = numpy.array(im2D, 'B')
        im2D.shape = im.size+(4,)
        im2D2 = numpy.array(im2D[:1024, :1024])
        t = Texture(enable=1, image=im2D2, format=GL.GL_RGBA)
        # Set up square...
        coords = [[0.0, 0.0, 0.0], [10.0, 0.0, 0.0], [10.0, 10.0, 0.0], [0.0, 10.0, 0.0]]
        indices = [[0,1,2,3]]
        materials = ( (1,1,1,1),(1,1,1,1),(1,1,1,1),(1,1,1,1) )
        texturecoords = [[0,0],[1,0],[1,1],[0,1]]
        text = IndexedPolygons('Image')
        text.Set(texture = t, textureCoords = texturecoords, vertices=coords,faces=indices , materials = materials, inheritMaterial=0, transparent=1)
        text.Set(shading = 'smooth', frontPolyMode='fill', backPolyMode='fill', culling = 0)
        self.mv.GUI.VIEWER.AddObject(text)
        self.mv.GUI.VIEWER.currentCamera.fog.Set(enabled=False)
        self.mv.GUI.VIEWER.OneRedraw()
        #self.mv.setbackgroundcolor([1, 0, 0,], log=0)

        lArray=self.mv.GUI.VIEWER.currentCamera.GrabFrontBufferAsArray()
        lSum = numpy.add.reduce(lArray)
        
        lSumList = [8074748, # i86Linux2
                    2589386, #  i86Linux2, "jacob"
                    4129377, # x86_64Linux2
                    4109060, # i86Darwin9
                    #ppcDarwin9
                    8129704 #x86_64Darwin10
                    ]
        minval = min(lSumList)-1000
        maxval = max(lSumList)+1000
        msg = 'test_SourceTexturedCloudsWithAlpha lSum: %d, minval: %d, maxval: %d'% (lSum, minval, maxval)
        self.assertTrue( lSum  > minval and lSum < maxval , msg=msg )
        print msg
        #self.mv.Exit(0)
        #delattr(self, 'mv')


if __name__ == '__main__':
    test_cases = [
        'SourceLogTests',
        ]
    
    unittest.main( argv=([__name__] + test_cases) )
    #to get verbose output use this line instead:
    #unittest.main( argv=([__name__, '-v'] + test_cases) )

