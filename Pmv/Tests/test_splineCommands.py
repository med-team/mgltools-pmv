#
####################################################################
#   Author:Sowjanya Karnati
####################################################################
#
#$Id: test_splineCommands.py,v 1.24 2012/08/20 21:59:10 annao Exp $
#
import sys
import unittest
import string,Pmv
from string import split
from DejaVu.IndexedPolygons import IndexedPolygons
from DejaVu.Shapes import Shape2D, Triangle2D, Circle2D, Rectangle2D,Square2D, Ellipse2D
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
klass = None
ct = 0
totalCt = 93
from DejaVu.Shapes import Triangle2D, Circle2D, Rectangle2D, Square2D, Ellipse2D
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class SplineBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                trapExceptions=False, withShell=0,
                                gui=hasGUI, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('splineCommands', package='Pmv')
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            if mv and mv.hasGui:
                print 'setup: destroying mv'
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
            
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv


############################################################
# ComputeSplineGC Command Tests                            #
############################################################

class ComputeSplineGC(SplineBaseTest):
    
    def test_computeSplineGC_emptyViewer(self):
        """
        Test if the computeSplineGC behaves properly
        when no molecule has been loaded in the viewer and with
        the default values
        """
        if self.mv.computeSpline.flag & 1:
            self.mv.computeSpline(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.computeSpline cannot be called with only self.mv.getSelection()")


    def test_computeSplineGC_Normal(self):
        """
        Test the normal behavior of the computeSplineGC on 1crn
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command
        self.mv.computeSplineGC("1crn:::", continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        

    def test_computeSplineGC_one_atmtype(self):
        """tests computeSplineGC by selecting C atmtype
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command with atmtype 'C'
        self.mv.computeSplineGC("1crn:::", continuity=2, nbchords=4, log=0,
        closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        #get 'C' atoms
        c_atoms = self.mv.allAtoms.get(lambda x : x.name == 'C')
        #checks 'C' atoms and  spline atoms are the same
        self.assertEqual(self.mv.Mols[0].chains[0].spline['spline '][1],c_atoms)

    def test_computeSplineGC_two_atmtypes(self):
        """tests computeSplineGC by selecting CA,N atmtype
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command with atmtype 'CA,'N'
        self.mv.computeSplineGC("1crn:::", continuity=2, nbchords=4, log=0,
        closedSpline=0, sortAtms=1, atmtype='CA,N', curSel=0)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        CA_atoms = self.mv.allAtoms.get(lambda x : x.name == 'CA')
        N_atoms = self.mv.allAtoms.get(lambda x : x.name == 'N')
        #checks 'CA','N' atoms and  spline atoms are the same
        self.assertEqual(len(self.mv.Mols[0].chains[0].spline['spline '][1]),len(N_atoms)+len(CA_atoms))


    def test_computeSplineGC_select_cursel(self):
        """tests computeSplineGC by selecting a set of atoms
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #select 20 atoms
        self.mv.select(self.mv.allAtoms[10:30])
        self.mv.computeSplineGC(self.mv.getSelection(), continuity=2, nbchords=4, log=0,
        closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        #checks spline is displayed for those atoms only
        self.assertEqual(self.mv.Mols[0].chains[0].spline['spline '][1],self.mv.allAtoms[10:30])


    def test_computeSplineGC_nbchords(self):
        """tests computeSplineGC ,nbchords is 7
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command
        c = self.mv.computeSplineGC
        c("1crn:::", continuity=2, nbchords=7, log=0,
        closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        #checks nbchords = 7
        self.assertEqual(c.getLastUsedValues()['nbchords'],7)
        

    def test_computeSplineGC_continuity(self):
        """tests computeSplineGC ,continuity = 4
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command
        c = self.mv.computeSplineGC
        c("1crn:::", continuity=4, nbchords=7, log=0,
        closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        #checks continuity=4
        self.assertEqual(c.getLastUsedValues()['continuity'],4)


    def test_computeSplineGC_closedSpline(self):
        """tests computeSplineGC ,closedSpline = 1
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command
        c = self.mv.computeSplineGC
        c("1crn:::", continuity=4, nbchords=7, log=0,
        closedSpline=1, sortAtms=1, atmtype='', curSel=1)
        #checks closedSpline=1
        self.assertEqual(c.getLastUsedValues()['closedSpline'],1)

    def test_computeSplineGC_sortAtms(self):
        """tests computeSplineGC ,sortAtms = 0
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command
        c = self.mv.computeSplineGC
        c("1crn:::", continuity=4, nbchords=7, log=0,
        closedSpline=1, sortAtms=0, atmtype='', curSel=1)
        #checks sortAtms=0
        self.assertEqual(c.getLastUsedValues()['sortAtms'], True)

#invalid input computeSplineGC
    def test_computeSplineGC_invalid_nodes(self):
        """checks computeSplineGC ,invalid nodes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue = c("abcd")
        self.assertEqual(returnValue,None)

    def test_computeSplineGC_empty_nodes(self):
        """checks computeSplineGC ,empty nodes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue = c(" ")
        self.assertEqual(returnValue,None)    

    def test_computeSplineGC_invalid_atmtype(self):
        """tests computeSplineGC invalid atom type********************
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue =c("1crn:::",atmtype= 'Z')
        self.assertEqual(returnValue,None)

        
    def test_computeSplineGC_invalid_continuity(self):
        """tests computeSplineGC ,continuity invalid
        continuity is from 0-5
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue =c("1crn:::",continuity=10)
        self.assertEqual(returnValue,None)

    def test_computeSplineGC_invalid_closedspline(self):
        """tests computeSplineGC ,closedspline invalid
        closedspline is 0/1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue =c("1crn:::",closedSpline = 10)
        self.assertEqual(returnValue,None)

    def test_computeSplineGC_invalid_nbchords(self):
        """tests computeSplineGC ,nbchords invalid
        nbchords is 4-10
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue =c("1crn:::",nbchords = 15)
        self.assertEqual(returnValue,None)


    def test_computeSplineGC_invalid_sortAtms(self):
        """tests computeSplineGC ,sortAtms = 1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSplineGC
        returnValue =c("1crn:::",sortAtms = 10)
        self.assertEqual(returnValue,None)
#end invalid input computesplineGC

############################################################
# ComputeSplineCommand Tests                               #
############################################################

class ComputeSpline(SplineBaseTest):
    
    def test_computeSpline_emptyViewer(self):
        """
        Test if the computeSpline behaves properly
        when no molecule has been loaded in the viewer and with
        the default values
        """
        if self.mv.computeSpline.flag & 1:
            self.mv.computeSpline(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.computeSpline cannot be called with only self.mv.getSelection()")


    def test_computeSpline_Normal(self):
        """
        Test the normal behavior of the computeSpline on 1crn
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSpline command
        self.mv.computeSpline("1crn:::", continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        chain = self.mv.Mols[0].chains[0]
        #checks chain has attribute spline
        self.assertEqual(hasattr(chain,'spline'),True)
        

    def test_computeSpline_one_atmtype(self):
        """tests computeSpline by selecting C atmtype
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSpline command
        self.mv.computeSpline("1crn:::", continuity=2, nbchords=4, log=0,
        closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        #compute 'C' atoms
        c_atoms = self.mv.allAtoms.get(lambda x : x.name == 'C')
        #checks spline is displayed for 'C' atoms only
        self.assertEqual(self.mv.Mols[0].chains[0].spline['spline '][1],c_atoms)

    def test_computeSpline_two_atmtypes(self):
        """tests computeSpline by selecting CA,N atmtype
        """
        #read molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSpline command
        self.mv.computeSpline("1crn:::", continuity=2, nbchords=4, log=0,
        closedSpline=0, sortAtms=1, atmtype='CA,N', curSel=0)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        #get 'CA' atoms
        CA_atoms = self.mv.allAtoms.get(lambda x : x.name == 'CA')
        #get 'N' atoms
        N_atoms = self.mv.allAtoms.get(lambda x : x.name == 'N')
        #checks spline is displayed for 'CA' ,'N'atoms only
        self.assertEqual(len(self.mv.Mols[0].chains[0].spline['spline '][1]),len(N_atoms)+len(CA_atoms))


    def test_computeSpline_select_cursel(self):
        """tests computeSpline by selecting a set of atoms
        """
        #read a molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #select a atomset
        self.mv.select(self.mv.allAtoms[10:30])
        self.mv.computeSpline(self.mv.getSelection(), continuity=2, nbchords=4, log=0,
        closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        chain = self.mv.Mols[0].chains[0]
        self.assertEqual(hasattr(chain,'spline'),True)
        #checks spline is displayed for that atom set only
        self.assertEqual(self.mv.Mols[0].chains[0].spline['spline '][1],self.mv.allAtoms[10:30])


    def test_computeSpline_nbchords(self):
        """tests computeSpline ,nbchords is 7
        """
        #read a molecule
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        c("1crn:::", continuity=2, nbchords=7, log=0,
        closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        #checks nbchords = 7
        self.assertEqual(c.getLastUsedValues()['nbchords'],7)
        

    def test_computeSpline_continuity(self):
        """tests computeSpline ,continuity = 4
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        c("1crn:::", continuity=4, nbchords=7, log=0,
        closedSpline=0, sortAtms=1, atmtype='', curSel=1)
        #checks continuity=4
        self.assertEqual(c.getLastUsedValues()['continuity'],4)


    def test_computeSpline_closedSpline(self):
        """tests computeSpline ,closedSpline = 1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        c("1crn:::", continuity=4, nbchords=7, log=0,
        closedSpline=1, sortAtms=1, atmtype='', curSel=1)
        #checks closedSpline=1
        self.assertEqual(c.getLastUsedValues()['closedSpline'],1)

    def test_computeSpline_sortAtms(self):
        """tests computeSpline ,sortAtms = 0
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        c("1crn:::", continuity=4, nbchords=7, log=0,
        closedSpline=1, sortAtms=0, atmtype='', curSel=1)
        #checks sortAtms = 1
        self.assertEqual(c.getLastUsedValues()['sortAtms'],True)

#invalid input computeSpline
    def test_computeSpline_invalid_nodes(self):
        """tests computeSpline invalid nodes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue = c("abcd")
        self.assertEqual(returnValue,None)

    def test_computeSpline_empty_nodes(self):
        """tests computeSpline empty nodes
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue = c(" ")
        self.assertEqual(returnValue,None)    

    def test_computeSpline_invalid_atmtype(self):
        """tests computeSpline invalid atm type *******************
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue =c("1crn:::",atmtype= 'Z')
        self.assertEqual(returnValue,None)


    def test_computeSpline_invalid_cursel(self):
        """tests computeSpline ,cursel 0,*******************
        cursel is 0/1"""
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue =c("1crn:::",curSel=0)
        self.assertEqual(returnValue,None)
        
    def test_computeSpline_invalid_continuity(self):
        """tests computeSpline ,continuity invalid
        continuity is from 0-5
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue =c("1crn:::",continuity=10)
        self.assertEqual(returnValue,None)

    def test_computeSpline_invalid_closedspline(self):
        """tests computeSpline ,closedspline invalid
        closedspline is 0/1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue =c("1crn:::",closedSpline = 10)
        self.assertEqual(returnValue,None)

    def test_computeSpline_invalid_nbchords(self):
        """tests computeSpline ,nbchords invalid
        nbchords is 4-10
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue =c("1crn:::",nbchords = 15)
        self.assertEqual(returnValue,None)


    def test_computeSpline_invalid_sortAtms(self):
        """tests computeSpline ,sortAtms = 1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        c = self.mv.computeSpline
        returnValue =c("1crn:::",sortAtms = 10)
        self.assertEqual(returnValue,None)
#end invalid input computespline    

############################################################
# ExtrudeSplineCommand Tests                               #
############################################################


class ExtrudeSpline(SplineBaseTest):
    
    
    def test_extrudeSpline_emptyViewer(self):
        """
        Test if the extrudeSplineG behaves properly
        when no molecule has been loaded in the viewer and with
        the default values
        """
        if self.mv.extrudeSpline.flag & 1:
            self.mv.extrudeSpline(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.extrudeSpline cannot be called with only self.mv.getSelection()")
    
    def test_extrudeSpline_default_parameters(self):
        """tests extrudeSpline with default parameters
        """
        #read a molecule
        self.mv.readMolecule("Data/1crn.pdb")
        #call computeSplineGC command
        self.mv.computeSplineGC("1crn:::",continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        #call extrudeSpline command 
        c = self.mv.extrudeSpline
        c("1crn")
        self.assertEqual(c.getLastUsedValues(),c.getValNamedArgs())
    

    def test_extrudeSpline_rectangle(self):
        """tests extrudeSpline when shape is rectangle 
        """
        #read a molecule
        self.mv.readMolecule('Data/1crn.pdb')
        #call computeSplineGC command
        self.mv.computeSplineGC("1crn:::",continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        #call extrudeSpline command
        c = self.mv.extrudeSpline
        c("1crn",shape2D = Rectangle2D(width = 1,height =2,vertDup =1), display = 1)
        #checks shape2D ,rectangle
        self.assertEqual(split(str(c.getLastUsedValues()['shape2D']))[0],'<DejaVu.Shapes.Rectangle2D')
                
    def test_extrudeSpline_circle(self):
        """tests extrudeSpline when shape is circle
        """
        #read a molecule
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn:::",continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        c = self.mv.extrudeSpline
        #checks shape2D ,circle
        c("1crn",shape2D = Circle2D(radius = 0.5),display =1)
        self.assertEqual(split(str(c.getLastUsedValues()['shape2D']))[0],'<DejaVu.Shapes.Circle2D')
       
    
    def test_extrudeSpline_triangle(self):
        """tests extrudeSpline when shape is triangle
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn:::",continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        #checks shape2D, triangle
        self.assertEqual(split(str(c.getLastUsedValues()['shape2D']))[0],'<DejaVu.Shapes.Triangle2D')
        
    def test_extrudeSpline_ellipse(self):
        """tests extrudeSpline when shape is ellipse
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn:::",continuity=2, nbchords=4, log=0, closedSpline=0, sortAtms=1, atmtype='C', curSel=0)
        c = self.mv.extrudeSpline
        c("1crn",shape2D = Ellipse2D(demiGrandAxis =1,demiSmallAxis = 1,quality = 12), display = 1) 
        #checks shape2D, ellipse
        self.assertEqual(split(str(c.getLastUsedValues()['shape2D']))[0],'<DejaVu.Shapes.Ellipse2D')
        
    def test_extrudeSpline_display_false(self):
        """tests extrudeSpline when display = false
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D = Ellipse2D(demiGrandAxis =1,demiSmallAxis = 1,quality = 12), display = 0) 
        #checks when display = false
        self.assertEqual(c.getLastUsedValues()['display'],False) 
        
#invalid input for extrudeSpline

    def test_extrudeSpline_invalid_nodes(self):
        """tests extrudeSpline, invalid nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn")
        c = self.mv.extrudeSpline
        returnValue = c("abcd")    
        self.assertEqual(returnValue,None)

        
    def test_extrudeSpline_empty_nodes(self):
        """tests extrudeSpline,empty nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn")
        c = self.mv.extrudeSpline
        returnValue = c(" ")    
        self.assertEqual(returnValue,None)


    def test_extrudeSpline_display(self):
        """tests extrudeSpline invalid input for  display 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSplineGC("1crn")
        c = self.mv.extrudeSpline
        returnValue = c("1crn",display = 'hai')
        self.assertEqual(returnValue,None)
        
#end invalid input for extrudeSpline

################################################################
# DisplayExtrudedSplineCommand Tests                           #
################################################################

class DisplayExtrudedSpline(SplineBaseTest):

    
    def test_displaySpline_emptyViewer(self):
        """ Test if displaySpline behaves properly when no
        molecule has been loaded in the viewer and with the default values
        """
        if self.mv.displayExtrudedSpline.flag & 1:    
            self.mv.displayExtrudedSpline(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.displayExtrudedSpline cannot be called with only self.mv.getSelection()")


    def test_displaySpline_beforeCompute(self):
        """
        Test the display secondarystructurecommands before the
        computing and extruding the secondary structure information
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        #call displayExtrudedSpline command
        self.mv.displayExtrudedSpline("1crn")
        #checks geom is visible
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['spline '].visible, 1)
        

    def test_displaySpline_negate(self):
        """tests displaySpline when negate = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display =0)
        self.mv.displayExtrudedSpline("1crn",negate = 1)
        #checks when negate = 1,geoms of spline are not visible
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['spline '].visible,0)
        
        
        
    def test_displaySpline_only(self):
        """tests displaySpline when only = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSpline("1crn")
        old_len = len(self.mv.Mols[0].geomContainer.atoms['spline '])
        self.mv.displayExtrudedSpline(self.mv.getSelection(),only = 1)
        new_len = len(self.mv.Mols[0].geomContainer.atoms['spline '])
        #old_len = 13,new_len = 4
        self.assertEqual(old_len > new_len, True)
        

    def test_displaySpline_negate_only(self):
        """tests displaySpline when negate = 1,only =1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSpline("1crn",negate = 1,only = 1)
        #checks when only = 1,negate = 1,geoms of spline are not visible
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['spline '].visible,0)

    def test_displaySpline_color(self):
        """tests displaySpline by coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSpline("1crn")
        #before coloring
        old_col = self.mv.Mols[0].chains.residues.atoms.colors['spline ']
        self.mv.colorByAtomType("1crn",geomsToColor = ['spline ']) 
        #after coloring
        new_col = self.mv.Mols[0].chains.residues.atoms.colors['spline ']
        self.assertEqual(old_col!=new_col,True)
        
#invalid input display spline command
    
    def test_displaySpline_invalid_nodes(self):
        """tests displaySpline,invalid nodes as input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSpline
        returnValue = command("1crn")
        self.assertEqual(returnValue,None)
        
    def test_displaySpline_empty_nodes(self):
        """test displaySpline,empty nodes as input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSpline
        returnValue = command(" ")
        self.assertEqual(returnValue,None)


    def test_displaySpline_invalid_only(self):
        """test displaySpline,invalid only
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSpline
        returnValue = command("1crn",only = 'hai')
        self.assertEqual(returnValue,None)        
    
    def test_displaySpline_invalid_negate(self):
        """test displaySpline,invalid negate
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSpline
        returnValue = command("1crn",negate = 'hai')
        self.assertEqual(returnValue,None)

#end invalid input display spline command

############################################################
# Undisplay Spline Command                                  #
############################################################

class UndisplaySpline(SplineBaseTest):
    
    def test_undisplay_spline(self):
        """tests undisplay spline
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSpline("1crn")   
        self.mv.undisplayExtrudedSpline("1crn")
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['spline '].visible,0)   
    
    def test_undisplay_spline_select(self):
        """tests undisplay spline,input through selection
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.extrudeSpline(self.mv.getSelection(),shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSpline(self.mv.getSelection())   
        self.mv.undisplayExtrudedSpline(self.mv.getSelection())
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['spline '].visible,0)
        
    def test_undisplay_spline_invalid_input(self):
        """tests undisplay spline invalid nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        command = self.mv.undisplayExtrudedSpline
        returnValue = command("abcd")
        self.assertEqual(returnValue,None)
        

    def test_undisplay_spline_empty_input(self):
        """tests undisplay spline empty nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        command = self.mv.undisplayExtrudedSpline
        returnValue = command(" ")
        self.assertEqual(returnValue,None)


################################################################
# DisplaySplineAsLineCommand Tests                             #
################################################################


class  DisplaySplineAsLine(SplineBaseTest):

    def test_displaySplineAsLine_emptyViewer(self):
        """ Test if displaySplineAsLine behaves properly when no
        molecule has been loaded in the viewer and with the default values"""
        if self.mv.displaySplineAsLine.flag & 1:    
            self.mv.displaySplineAsLine(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.displaySplineAsLine cannot be called with only self.mv.getSelection()")
            
    def test_display_spline_as_line_has_attr(self):
        """tests displaySplineAsLine has attr linespline
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        self.mv.displaySplineAsLine("1crn",lineWidth = 1)
        self.failUnless(self.mv.Mols[0].geomContainer.geoms.has_key ('lineSpline '))
       
        
    def test_display_spline_as_line_vertices(self):
        """tests displaySplineAsLine ,lineSpline vertices != 0
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        self.mv.displaySplineAsLine("1crn",lineWidth = 1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lineSpline '].vertexSet.vertices)!= 0,True)


    def test_display_spline_as_line_select(self):
        """tests displaySplineAsLine ,input through seletcion
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.displaySplineAsLine(self.mv.getSelection(),lineWidth = 1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lineSpline '].vertexSet.vertices)!= 0,True)

    def test_display_spline_as_line_only(self):
        """tests displaySplineAsLine when only = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")    
        self.mv.displaySplineAsLine("1crn")
        old_linespline = self.mv.Mols[0].geomContainer.geoms['lineSpline '].vertexSet
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.displaySplineAsLine("1crn",lineWidth = 1,only =1)
        new_linespline = self.mv.Mols[0].geomContainer.geoms['lineSpline '].vertexSet
        self.assertEqual(len(old_linespline)>len(new_linespline),True)
    
    
    def test_displaySpline_as_line_negate(self):
        """tests displaySplineAsLine when negate = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline(self.mv.getSelection())        
        self.mv.displaySplineAsLine("1crn")
        self.mv.displaySplineAsLine("1crn",lineWidth = 1,negate = 1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lineSpline '].faceSet),0)
    
    def test_displaySpline_as_line_negate_only(self):
        """tests displaySplineAsLine when negate = 1,only =1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.extrudeSpline("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displaySplineAsLine("1crn",negate = 1,only = 1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lineSpline '].faceSet),0)

    def test_displaySpline_as_line_color(self):
        """tests displaySplineAsLine by coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.extrudeSpline
        c("1crn",shape2D =Triangle2D(side = 1.5),display = 1)
        self.mv.displaySplineAsLine("1crn")
        old_col = self.mv.Mols[0].chains.residues.atoms.colors['lineSpline ']
        self.mv.colorByAtomType("1crn",geomsToColor = ['lineSpline ']) 
        new_col = self.mv.Mols[0].chains.residues.atoms.colors['lineSpline ']
        self.assertEqual(old_col!=new_col,True)
        

    def test_display_spline_as_line_only_invalid(self):
        """tests displaySplineAsLine, only invalid
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.displaySplineAsLine
        returnValue = c("1crn",lineWidth = 1,only = 'hai')
        self.assertEqual(returnValue,None)            

    def test_display_spline_as_line_negate_invalid(self):
        """tests displaySplineAsLine, negate invalid
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSpline("1crn")
        c = self.mv.displaySplineAsLine
        returnValue = c("1crn",lineWidth = 1,negate = 'hai')
        self.assertEqual(returnValue,None)
################################################################
# UnDisplaySplineAsLineCommand Tests                             #
################################################################

class UnDisplaySplineAsLine(SplineBaseTest):       
    
    def test_undisplay_spline_as_line(self):
        """tests undisplay spline as line
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.displaySplineAsLine("1crn")   
        self.mv.undisplaySplineAsLine("1crn")
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lineSpline '].faceSet) == 0,True)   
    
    def test_undisplay_spline_as_line_select(self):
        """tests undisplay spline as line,input through selection
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.displaySplineAsLine(self.mv.getSelection())   
        self.mv.undisplaySplineAsLine(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['lineSpline '].faceSet) == 0,True)
        
    def test_undisplay_spline_as_line_invalid_input(self):
        """tests undisplay spline as line invalid nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.displaySplineAsLine(self.mv.getSelection())
        command = self.mv.undisplaySplineAsLine
        returnValue = command("abcd")
        self.assertEqual(returnValue,None)
        

    def test_undisplay_spline_as_line_empty_input(self):
        """tests undisplay spline as line  empty nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSpline("1crn")
        self.mv.displaySplineAsLine(self.mv.getSelection())
        command = self.mv.undisplaySplineAsLine
        returnValue = command(" ")
        self.assertEqual(returnValue,None)    

####################################################
##
##  CUSTOM SPLINE COMMAND TESTS
##
####################################################

class CustomSplineTest(SplineBaseTest):

    def test_customspline_default(self):
        """tests customSplineCommand with defualt values"""
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.customSpline(self.mv.getSelection())
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['spline ']!=0,True)
    

    def test_customspline_undo(self):
        """tests customSplineCommand, undo"""
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.customSpline(self.mv.getSelection())    
        oldsplineats = self.mv.Mols[0].geomContainer.atoms['spline ']
        self.mv.customSpline(self.mv.getSelection(),display=0)
        newsplineats = self.mv.Mols[0].geomContainer.atoms['spline ']
        self.assertEqual(len(oldsplineats)!=len(newsplineats),True)
        self.assertEqual(len(newsplineats)==0,True)

    def test_customspline_rectangle(self):
        """tests customSplineCommand with shape2D rectangle
        """
        self.mv.readMolecule("Data/barrel_1.pdb")
        shape = Rectangle2D(1.2, 0.2, firstDup=0, vertDup=1)
        self.mv.customSpline(self.mv.getSelection(), atmtype="C", curSel=False, nbchords=4,interp='interpolation', continuity=2, closedSpline=True,sortAtms=True,  shape2D=shape, capsFlag=False, display=True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['spline '])>0,True)
        
    def test_customspline_with_all_arguements(self):
        """tests customSplineCommand with all arguements"""
        self.mv.readMolecule("Data/barrel_1.pdb")
        shape = Circle2D(1.2)
        self.mv.customSpline(self.mv.getSelection(), atmtype="C", curSel=False, nbchords=4,interp='interpolation', continuity=2, closedSpline=True,sortAtms=True,  shape2D=shape, capsFlag=False, display=True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['spline '])>0,True)    
                        
    
    def test_customspline_for_selected(self):
        """tests customSplineCommand  for selected atoms"""
        self.mv.readMolecule("Data/barrel_1.pdb")
        self.mv.select(self.mv.allAtoms[:50])
        self.mv.customSpline(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['spline '])==5,True)

    

    ##invalid Arguments
    def test_customspline_invalid_nodes(self):
        """tests customSplineCommand with invalid nodes"""
        self.mv.readMolecule("Data/barrel_1.pdb")
        rval = self.mv.customSpline("hello")
        self.assertEqual(rval,'ERROR')

    def test_customspline_invalid_shape2D(self):
         """tests customSplineCommand with invalid Shape"""
         self.mv.readMolecule('Data/barrel_1.pdb')
         self.assertRaises(AttributeError,self.mv.customSpline,self.mv.getSelection(),shape2D="hello")
    
        
#################################################################
##
## COMPUTE EXTRUDE SPLINE COMMAND TESTS
##
#################################################################
class ComputeExtrudeSplineTest(SplineBaseTest):
    def test_compute_extrude_spline_1(self):
        """tests computeExtrude Spline with default aguements"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.computeExtrudeSpline(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['spline '])!=0,True)


    def test_compute_extrude_spline_undo(self):
        """tests computeExtrude Spline with negate =1"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.computeExtrudeSpline(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['spline '])!=0,True)
        self.mv.computeExtrudeSpline(self.mv.getSelection(),negate=1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['spline '])!=0,False)
        

    def test_compute_extrude_spline_invalid_nodes(self):
        """compute extrude spline with invalid nodes"""
        self.mv.readMolecule('Data/barrel_1.pdb')
        rval=self.mv.computeExtrudeSpline("hello")
        self.assertEqual(rval,'ERROR')    


###################################################################
##
##   LOG TESTS
##
###################################################################


class SplineCommnadsLogBaseTest(SplineBaseTest):
    
    
    def test_computeSpline_log_checks_expected_log_string(self):
        """
        checks expected log string is written
        """    
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        # 1- Read in a "good molecule" 1crn for which we know the result.
        self.mv.readMolecule('Data/1crn.pdb')
        from MolKit.protein import Coil, Residue
        # 2- compute the CA spline for that molecule
        self.mv.select('1crn')
        self.mv.computeSpline(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeSpline("1crn", continuity=2, nbchords=8, log=0, closedSpline=False, interp=\'interpolation\', atmtype=\'CA\', curSel=False)')
        
    def test_computeSpline_log_checks_that_it_runs(self):
        """
        Checking log string runs
        """    
        # 1- Read in a "good molecule" 1crn for which we know the result.
        self.mv.readMolecule('Data/1crn.pdb')
        from MolKit.protein import Coil, Residue
        # 2- compute the CA spline for that molecule
        self.mv.select('1crn')
        oldself=self
        self=self.mv
        s = "self.computeSpline(self.getSelection())"    
        exec(s)
        oldself.assertEqual(1,1)

        
    def test_extrudeSpline_checks_expected_log_string(self):
        """
        checks expected log string is written
        """
        from DejaVu.IndexedPolygons import IndexedPolygons
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute spline on the selection
        self.mv.computeSpline(self.mv.getSelection())
        # 4- Extrude the default shape on the default splineName. ('CASpline')
        self.mv.extrudeSpline(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.extrudeSpline("1crn", capsFlag=False, display=True, shape2D=None, log=0)')
        

    def test_extrudeSpline_log_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        from DejaVu.IndexedPolygons import IndexedPolygons
        # 1- Read molecule
        self.mv.readMolecule('Data/1crn.pdb')
        # 2- Select molecule
        self.mv.select('1crn')
        # 3- Compute spline on the selection
        self.mv.computeSpline(self.mv.getSelection())
        # 4- Extrude the default shape on the default splineName. ('CASpline')
        oldself=self
        self=self.mv
        s ="self.extrudeSpline(self.getSelection())"
        exec(s)
        oldself.assertEqual(1,1)

    
    def test_displayExtrudedSpline_checks_expected_log_string(self):
        """
        checks expected log string is written
        """
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.extrudeSpline(self.mv.getSelection(), display=0)
        self.mv.displayExtrudedSpline(self.mv.getSelection(), 'CASpline')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displayExtrudedSpline("1crn", negate=False, only=\'CASpline\', log=0)')
        

    def test_displayExtrudedSpline_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.extrudeSpline(self.mv.getSelection(), display=0)
        oldself =self
        self=self.mv
        s = "self.displayExtrudedSpline(self.getSelection(), 'CASpline')"
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_displaySplineAsLine_checks_expected_log_string(self):
        """
        checks expected log string is written
        """
        # Make sure that the chain atomSet and geoms is correct.
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.extrudeSpline(self.mv.getSelection(), display=0)
        self.mv.displaySplineAsLine("1crn")
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displaySplineAsLine("1crn", negate=False, only=False, lineWidth=3, log=0)')
        

    def test_displaySplineAsLine_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSpline(self.mv.getSelection())
        self.mv.extrudeSpline(self.mv.getSelection(), display=0)
        oldself =self
        self=self.mv
        s = "self.displaySplineAsLine('1crn')"
        exec(s)
        oldself.assertEqual(1,1)
        
    def test_customspline_checks_expected_log_string(self):
        """checks expected log string is written """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/barrel_1.pdb') 
        self.mv.customSpline(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-4.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.customSpline("barrel_1", continuity=2, nbchords=4, log=0, shape2D=None, closedSpline=False, sortAtms=True, interp=\'interpolation\', capsFlag=False, atmtype=\'CA\', curSel=False, display=True)')

    def test_customspline_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/barrel_1.pdb') 
        oldself =self
        self=self.mv
        s ="self.customSpline(self.getSelection())"
        exec(s)
        oldself.assertEqual(1,1)


    def test_compute_extrude_spline_checks_expected_log_string(self):
        """checks expected log string is written """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/barrel_1.pdb')
        self.mv.computeExtrudeSpline(self.mv.getSelection())
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-3.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeExtrudeSpline("barrel_1", negate=False, only=False, log=0)')
        
        
    def test_compute_extrude_spline_checks_that_it_runs(self):  
        """
        Checking log string runs
        """
        self.mv.readMolecule('Data/barrel_1.pdb') 
        oldself =self
        self=self.mv
        s ="self.computeExtrudeSpline('1crn', negate=False, only=False, log=0)"
        exec(s)
        oldself.assertEqual(1,1)

    









if __name__ == '__main__':
    unittest.main()





