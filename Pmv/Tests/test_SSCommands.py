#############################################################################
#
# Author: Sophie I. COON, Michel F. SANNER
#
# Copyright: M. Sanner TSRI 2000
#
#############################################################################
#
# $Header: /opt/cvs/python/packages/share1.5/Pmv/Tests/test_SSCommands.py,v 1.27 2011/06/10 22:45:01 annao Exp $
#
# $Id: test_SSCommands.py,v 1.27 2011/06/10 22:45:01 annao Exp $
#
import sys
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from DejaVu.IndexedPolygons import IndexedPolygons
from DejaVu.Shapes import Shape2D, Triangle2D, Circle2D, Rectangle2D,Square2D, Ellipse2D
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule

mv = None
klass = None
ct = 0
totalCt = 81
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1
    
"""
This module implements a set of functions to test the commands of the
secondarystructurecommands module
"""
class SSBaseTest(unittest.TestCase):

    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no', 
                                withShell=False,
                                verbose=False, trapExceptions=False,
                                gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('secondaryStructureCommands', package='Pmv')
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            print 'setup: destroying mv'
            if mv and mv.hasGui:
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            import pdb; pdb.set_trace()
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            try:
                self.mv.deleteMol(m)
            except:
                pass
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None

############################################################
# ComputeSecondaryStructureCommand Tests                   #
############################################################

class ComputeSSTest(SSBaseTest):
    def test_computeSecondaryStructure_emptyViewer(self):
        """
        Test if the computeSecondaryStructure behaves properly
        when no molecule has been loaded in the viewer and with
        the default values
        """
        if self.mv.computeSecondaryStructure.flag & 1:
            self.mv.computeSecondaryStructure(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.computeSecondaryStructure cannot be called with only self.mv.getSelection()")
        

    def test_computeSecondaryStructure_Normal(self):
        """
        Test the normal behavior of the computeSecondaryStructure on 1crn
        """
        # When computing the secondary structure of a 1crn we expect:
        #  - a secondary structure set containing 10 secondary
        #    structure elements
        #  - the selectionCommands.sets__ dictionnary contains 10 extra keys
        #  - a new attribute of the mol has been created called hasSS its value
        # should be ['From File']
        #  - resWithSS ?
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.computeSecondaryStructure("1crn",
                                          molModes = {'1crn':'From File'})
        mol = self.mv.Mols[0]
        self.failUnless(hasattr(mol, 'hasSS') and mol.hasSS == ['From File'])
        self.failUnless(hasattr(mol.chains[0], 'secondarystructureset') and \
               len(mol.chains[0].secondarystructureset)==10)
        self.failUnless(not hasattr(mol,'_ExtrudeSecondaryStructureCommand__hasSSGeom') or \
               mol._ExtrudeSecondaryStructureCommand__hasSSGeom == 0)
##         self.mv.deleteMol("1crn")
##         assert len(self.mv.Mols)==0


    def test_computeSecondaryStructure_nosheet2D(self):
        """
        Test the computeSecondaryStructure on a molecule (1crnnosheet2D)
        created
        using 1crn.pdb (res1-res29) removing all the O atoms, but leaving the
        Secondary Structure information in the file.
        We expect to:
          - create the proper secondarystructureset containing 5 SS elements
          - but no sheet2D
        """
        self.mv.readMolecule('Data/1crnnosheet2D.pdb')
        self.mv.select('1crnnosheet2D')
        self.mv.computeSecondaryStructure(self.mv.getSelection())
        chain = self.mv.Mols[0].chains[0]
        mol = self.mv.Mols[0]
        self.assertEqual(mol.hasSS, ['From File'])
        self.failUnless( hasattr(chain, 'secondarystructureset'))
        self.assertEqual(len(chain.secondarystructureset), 5)
##         self.mv.deleteMol('1crnnosheet2D')
##         assert len(self.mv.Mols) == 0

    def test_computeSecondaryStructure_fileThenStrideOnMol(self):
        """
        ComputeSecondaryStructure on 1crn using the info in the file then
        using stride."""
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select("1crn")
        self.mv.computeSecondaryStructure(self.mv.getSelection(),
                                          {'1crn':'From File'})
        mol = self.mv.Mols[0]
        self.assertEqual( mol.hasSS, ['From File'])
        self.failUnless( hasattr(mol.chains[0], 'secondarystructureset') and \
                         len(mol.chains[0].secondarystructureset)==10)
        if self.mv.computeSecondaryStructure.haveStride:
            self.mv.computeSecondaryStructure(self.mv.getSelection(), {'1crn':'From Stride'})
            self.assertEqual( mol.hasSS, ['From Stride'])
            self.failUnless( hasattr(mol.chains[0], 'secondarystructureset') and \
                             len(mol.chains[0].secondarystructureset)==11)
        else:
            self.mv.computeSecondaryStructure(self.mv.getSelection(), {'1crn':'From Pross'})
            self.assertEqual( mol.hasSS, ['From Pross'])
            self.failUnless( hasattr(mol.chains[0], 'secondarystructureset') and \
                             len(mol.chains[0].secondarystructureset)==11)

##         self.mv.deleteMol('1crn')
##         assert len(self.mv.Mols)==0

    def test_computeSecondaryStructure_twochains(self):
        """
        Test computeSecondaryStructure on a molecule protease.pdb
        with two chains
        """
        self.mv.readMolecule('Data/protease.pdb')
        self.mv.select('protease')
        self.mv.computeSecondaryStructure(self.mv.getSelection())
        mol = self.mv.Mols[0]
        self.assertEqual(mol.hasSS,['From File'])
        self.failUnless(hasattr(mol.chains[0], 'secondarystructureset'))
        self.failUnless(hasattr(mol.chains[1], 'secondarystructureset'))
##         self.mv.deleteMol('protease')
##         assert len(self.mv.Mols) == 0

    def test_secondaryStructure_fileThenStrideOnMol2(self):
        """
        ComputeSecondaryStructure on 1crn using the info in the file then
        using stride."""
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select("1crn")
        self.mv.computeSecondaryStructure(self.mv.getSelection(),
                                          {'1crn':'From File'})
        self.mv.extrudeSecondaryStructure(self.mv.getSelection())
        mol = self.mv.Mols[0]
        self.assertEqual(mol.hasSS, ['From File'])
        self.failUnless(hasattr(mol.chains[0], 'secondarystructureset') and \
                        len(mol.chains[0].secondarystructureset)==10)
        if self.mv.computeSecondaryStructure.haveStride:
            self.mv.computeSecondaryStructure(self.mv.getSelection(),
                                              {'1crn':'From Stride'})
            self.mv.extrudeSecondaryStructure(self.mv.getSelection())
            self.assertEqual(mol.hasSS, ['From Stride'])
            self.failUnless(hasattr(mol.chains[0], 'secondarystructureset') and \
                            len(mol.chains[0].secondarystructureset)==11)
        else:
            self.mv.computeSecondaryStructure(self.mv.getSelection(),
                                              {'1crn':'From Pross'})
            self.mv.extrudeSecondaryStructure(self.mv.getSelection())
            self.assertEqual(mol.hasSS, ['From Pross'])
            self.failUnless(hasattr(mol.chains[0], 'secondarystructureset') and \
                            len(mol.chains[0].secondarystructureset)==11)
            

    def test_secondaryStructure_cleanBeforeExtrude(self):
        """
        This test makes sure that everything is cleaned by the
        clean method. The clean method is called when computing the SS
        using stride after using
        the info in the file.
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure(self.mv.getSelection(),
                                          {'1crn':'From File'})
        mol = self.mv.Mols[0]
        self.mv.computeSecondaryStructure.clean(mol)

    def test_compute_secondaryStructure_invalid_nodes(self):
        """tests compute secondary structure with invalid nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn') 
        command = self.mv.computeSecondaryStructure
        returnValue = command('hello')
        self.assertEqual(returnValue,None)

    def test_compute_secondaryStructure_invalid_mode(self):
        """tests compute secondary structure with invalid mol mode
        value from {'From File'or 'From Stride'}
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.select('1crn') 
        command = self.mv.computeSecondaryStructure
        returnValue = command(self.mv.getSelection(),{'1crn':'hello'})
        self.assertEqual(returnValue,None)
        
    

############################################################
# ExtrudeSecondaryStructureCommand Tests                   #
############################################################
class ExtrudeSSTest(SSBaseTest):
    def test_extrudeSecondaryStructure_emptyViewer(self):
        """ Test if extrudeSecondaryStructure behaves properly when
        no molecule has
        been loaded in the viewer and with the default values"""
        if self.mv.extrudeSecondaryStructure.flag & 1:    
            self.mv.extrudeSecondaryStructure(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.extrudeSecondaryStructure cannot be called with only self.mv.getSelection()")
            
    def test_extrudeSecondaryStructure_noSheet2D(self):
        """
        Testing extrudeSecondaryStructure for a protein 1crnnosheet2D
        with 1 chain
        having a secondarystructureset holding 5 secondary structure elements
        but no sheet2D because no residues have an O.
        """
        self.mv.readMolecule('Data/1crnnosheet2D.pdb')
        self.mv.computeSecondaryStructure(self.mv.getSelection())
        chain = self.mv.Mols[0].chains[0]
        self.mv.extrudeSecondaryStructure(self.mv.getSelection(), display=1)
        self.failUnless(hasattr(chain, 'sheet2D'))
        self.failUnless( chain.sheet2D['ssSheet2D'] is None)


    def test_extrudeSecondaryStructure_defaultParam(self):
        """
        Function to test the extrudeSecondaryStructure command for 1crn with 1
        chain with the default parameters
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure(self.mv.getSelection())
        chain = self.mv.Mols[0].chains[0]
        c = self.mv.extrudeSecondaryStructure
        c(self.mv.getSelection(), display=1)
        self.assertEqual(c.getLastUsedValues(),c.getValNamedArgs())
        
    def test_extrudeSecondaryStructure_rectangle(self):
        """tests extrudeSecondaryStructure when shape is rectangle 
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 = Rectangle2D(width = 1,height =2,vertDup =1),shape2 =Rectangle2D(width = 1,height =2,vertDup =1), display = 1)
        self.assertEqual(split(str(c.getLastUsedValues()['shape1']))[0],'<DejaVu.Shapes.Rectangle2D')
        self.assertEqual(split(str(c.getLastUsedValues()['shape2']))[0],'<DejaVu.Shapes.Rectangle2D')
        
    def test_extrudeSecondaryStructure_circle(self):
        """tests extrudeSecondaryStructure when shape is circle
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 = Circle2D(radius = 0.5),shape2 = Circle2D(radius = 0.5),display =1)
        self.assertEqual(split(str(c.getLastUsedValues()['shape1']))[0],'<DejaVu.Shapes.Circle2D')
        self.assertEqual(split(str(c.getLastUsedValues()['shape2']))[0],'<DejaVu.Shapes.Circle2D')
    
    def test_extrudeSecondaryStructure_triangle(self):
        """tests extrudeSecondaryStructure when shape is triangle
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.assertEqual(split(str(c.getLastUsedValues()['shape1']))[0],'<DejaVu.Shapes.Triangle2D')
        self.assertEqual(split(str(c.getLastUsedValues()['shape2']))[0],'<DejaVu.Shapes.Triangle2D')

    def test_extrudeSecondaryStructure_ellipse(self):
        """tests extrudeSecondaryStructure when shape is ellipse
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 = Ellipse2D(demiGrandAxis =1,demiSmallAxis = 1,quality = 12),shape2 =Ellipse2D(demiGrandAxis =1,demiSmallAxis = 1,quality = 12), display = 1) 
        self.assertEqual(split(str(c.getLastUsedValues()['shape1']))[0],'<DejaVu.Shapes.Ellipse2D')
        self.assertEqual(split(str(c.getLastUsedValues()['shape2']))[0],'<DejaVu.Shapes.Ellipse2D')
    
   
    
#setting values to parameters

    def test_extrudeSecondaryStructure_frontcap(self):
        """tests front cap =0,default is 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),frontcap = 0,display = 1)
        self.assertEqual(c.getLastUsedValues()['frontcap'],0)

    def test_extrudeSecondaryStructure_endcap(self):
        """tests  endcap =0,default is 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),endcap = 0,display = 1)
        
        self.assertEqual(c.getLastUsedValues()['endcap'],0)
    
    def test_extrudeSecondaryStructure_arrow(self):
        """tests   arrow=0,default is 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),arrow = 0 ,display = 1)
        self.assertEqual(c.getLastUsedValues()['arrow'],0)
    
    def test_extrudeSecondaryStructure_nbchords(self):
        """tests   nbchords=5,default is 4
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),nbchords = 5 ,display = 1)
        self.assertEqual(c.getLastUsedValues()['nbchords'],5)        


    def test_extrudeSecondaryStructure_larrow(self):
        """tests   larrow=4,default is 2
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),larrow = 4 ,display = 1)
        self.assertEqual(c.getLastUsedValues()['larrow'],4)
        

    def test_extrudeSecondaryStructure_display(self):
        """tests display=0,default is 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 0)
        self.assertEqual(c.getLastUsedValues()['display'],False)    

    
    def test_extrudeSecondaryStructure_gapEnd(self):
        """tests  gapEnd = True,default is False
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),gapEnd = False ,display = 1)
        self.assertEqual(c.getLastUsedValues()['gapEnd'],False)

    def test_extrudeSecondaryStructure_gapbeg(self):
        """tests  gapBeg = True,default is False
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),gapBeg = False ,display = 1)
        self.assertEqual(c.getLastUsedValues()['gapBeg'],False)
#end set values to parameters

#invalid input for extrude seconadryStructureCommand
    def test_extrudeSecondaryStructure_nodes_invalid(self):
        """tests extrude secondary structure command invalid input nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure    
        returnValue = c("abcd")
        self.assertEqual(returnValue,None)
    
    def test_extrudeSecondaryStructure_nodes_empty(self):
        """tests extrude secondary structure command empty input nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure    
        returnValue = c(" ")
        self.assertEqual(returnValue,None)

    # the following "invalid" tests make no sence
    def xtest_extrudeSecondaryStructure_frontcap_invalid(self):
        """tests extrude secondary structure command invalid input frontcap
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),frontcap = 'hai' ,display = 1)
        self.assertEqual(returnValue,None)
        

    def xtest_extrudeSecondaryStructure_endcap_invalid(self):
        """tests extrude secondary structure command invalid input endcap
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),endcap = 'hai' ,display = 1)
        self.assertEqual(returnValue,None)
    
    def xtest_extrudeSecondaryStructure_arrow_invalid(self):
        """tests extrude secondary structure command invalid input arrow
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),arrow = 'hai' ,display = 1)
        self.assertEqual(returnValue,None)
    
    def xtest_extrudeSecondaryStructure_nbchords_invalid(self):
        """tests extrude secondary structure command invalid input
        nbchords*******************
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),
                            nbchords = 'hai' ,display = 1)

        self.assertEqual(returnValue,None)        


    def xtest_extrudeSecondaryStructure_larrow_invalid(self):
        """tests extrude secondary structure command invalid input larrow
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),larrow = 'hai' ,display = 1)
        self.assertEqual(returnValue,None)
        

    def xtest_extrudeSecondaryStructure_display_invalid(self):
        """tests extrude secondary structure command invalid input display
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 'hai')
        self.assertEqual(returnValue,None)    

    
    def test_extrudeSecondaryStructure_gapEnd_invalid(self):
        """tests extrude secondary structure command invalid input gapEnd
##         self.mv.deleteMol('1crn')
##         assert len(self.mv.Mols) == 0        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),gapEnd = 90 ,display = 1)
        self.assertEqual(returnValue,None)

    def test_extrudeSecondaryStructure_gapbeg_invalid(self):
        """tests extrude secondary structure command gapBeg
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        returnValue = c("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),gapBeg = 90 ,display = 1)
        self.assertEqual(returnValue,None) 
        
#end invalid input for extrude seconadryStructureCommand


################################################################
# DisplayExtrudedSSCommand Tests                               #
################################################################
class DisplayExtrudedSSTest(SSBaseTest):
    def test_displaySecondaryStructure_emptyViewer(self):
        """ Test if displaySecondaryStructure behaves properly when no
        molecule has been loaded in the viewer and with the default values"""
        if self.mv.displayExtrudedSS.flag & 1:    
            self.mv.displayExtrudedSS(self.mv.getSelection())
        else:
            raise ValueError("WARNING: self.mv.displayExtrudedSS cannot be called with only self.mv.getSelection()")
        

    def test_displaySecondaryStructure_beforeCompute(self):
        """
        Test the display secondarystructurecommands before the
        computing and extruding the secondary structure information
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn")
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['secondarystructure'].visible, True)
        

    def test_displaySecondaryStructure_negate(self):
        """tests displaySecondaryStructure when negate = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure(self.mv.getSelection())
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn",negate = 1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['secondarystructure'].vertexSet.vertices),0)
        
        
        
    def test_displaySecondaryStructure_only(self):
        """tests displaySecondaryStructure when only = 1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn")
        old_len = len(self.mv.Mols[0].geomContainer.atoms['Helix1 '])
        self.mv.displayExtrudedSS(self.mv.getSelection(),only = 1)
        new_len = len(self.mv.Mols[0].geomContainer.atoms['Helix1 '])
        #old_len = 13,new_len = 4
        self.assertEqual(old_len > new_len, True)
        

    def test_displaySecondaryStructure_negate_only(self):
        """tests displaySecondaryStructure when negate = 1,only =1
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn",negate = 1,only = 1)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['secondarystructure'].vertexSet.vertices),0)

    def test_dispalySecondaryStructure_color(self):
        """tests displaySecondaryStructure by coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn")
        old_col = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        self.mv.colorByAtomType("1crn",geomsToColor = ['secondarystructure']) 
        new_col = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        self.assertEqual(old_col!=new_col,True)
        
#invalid input display secondary structure command
    
    def test_dispalySecondaryStructure_invalid_nodes(self):
        """tests displaySecondaryStructure,invalid nodes as input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSS
        returnValue = command("1crn")
        self.assertEqual(returnValue,None)
        
    def test_dispalySecondaryStructure_empty_nodes(self):
        """test displaySecondaryStructure,empty nodes as input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSS
        returnValue = command(" ")
        self.assertEqual(returnValue,None)


    def test_dispalySecondaryStructure_invalid_only(self):
        """test displaySecondaryStructure,invalid only
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSS
        returnValue = command("1crn",only = 'hai')
        self.assertEqual(returnValue,None)        
    
    def test_dispalySecondaryStructure_invalid_negate(self):
        """test displaySecondaryStructure,invalid negate
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        c = self.mv.extrudeSecondaryStructure
        c("1crn",shape1 =Triangle2D(side = 1.5),display = 1)
        command= self.mv.displayExtrudedSS
        returnValue = command("1crn",negate = 'hai')
        self.assertEqual(returnValue,None)

#end invalid input display secondary structure command

############################################################
# Undisplay Secondary Structure Command                    #
############################################################

class UndisplaySS(SSBaseTest):
    
    def test_undisplay_secondary_structure(self):
        """tests undisplay secondary structure
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn")   
        self.mv.undisplayExtrudedSS("1crn")
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['secondarystructure'].vertexSet.vertices),0)   
    
    def test_undisplay_secondary_structure_select(self):
        """tests undisplay secondary structure,input through selection
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS(self.mv.getSelection())   
        self.mv.undisplayExtrudedSS(self.mv.getSelection())
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['secondarystructure'].vertexSet.vertices),0)
        
    def test_undisplay_secondary_structure_invalid_input(self):
        """tests undispaly secondary structure invalid nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        command = self.mv.undisplayExtrudedSS
        returnValue = command("abcd")
        self.assertEqual(returnValue,None)
        

    def test_undisplay_secondary_structure_empty_input(self):
        """tests undispaly secondary structure empty nodes
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        command = self.mv.undisplayExtrudedSS
        returnValue = command(" ")
        self.assertEqual(returnValue,None)

############################################################
#     Color By  SS Element Type                            #
############################################################
class ColorBySS(SSBaseTest):

    def test_color_by_ss(self):
        """tests color by SS
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        #self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        old_colors = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        self.mv.colorBySecondaryStructure("1crn",geomsToColor =
        ['secondarystructure'])
        new_colors = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        self.assertEqual(old_colors != new_colors,True)
        
        
    def test_color_by_ss_coil(self):
        """tests colorbySS ,coil color
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.colorBySecondaryStructure("1crn")
        coil_res = self.mv.Mols[0].geomContainer.atoms['Coil5 '].name
        #ALA 45
        res1 = self.mv.Mols[0].chains.residues.get(lambda x: x.type == 'ALA')
        coil_rescol = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         res1[-1].atoms.colors['secondarystructure'])
        self.assertEqual(coil_rescol[1],(1.0,1.0,1.0))
        
        
    def test_color_by_ss_Helix(self):
        """tests color bySS Helix color
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.colorBySecondaryStructure("1crn")
        helix_res = self.mv.Mols[0].geomContainer.atoms['Helix2 '].name
        res1 = self.mv.Mols[0].chains.residues.get(lambda x: x.type == 'GLU')
        helix_col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         res1[0].atoms.colors['secondarystructure'])
        self.assertEqual(helix_col[1],(1.0,0.0,1.0))        

    def test_color_by_ss_Turn(self):
        """tests color bySS Turn color
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.colorBySecondaryStructure("1crn")
        turn_res = self.mv.Mols[0].geomContainer.atoms['Turn1 '].name
        #GLY 42
        res1 = self.mv.Mols[0].chains.residues.get(lambda x: x.type == 'GLY')
        turn_col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         res1[-1].atoms.colors['secondarystructure'])
        self.assertEqual(turn_col[1],(0.0,1.0,1.0))
    
    def test_color_by_ss_strand(self):
        """ tests color bySS strand color
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.colorBySecondaryStructure("1crn")
        strand_res = self.mv.Mols[0].geomContainer.atoms['Strand2 '].name
        #CYS32
        res1 = self.mv.Mols[0].chains.residues.get(lambda x: x.type == 'CYS')
        Strand_col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),
                         res1[-2].atoms.colors['secondarystructure'])
        self.assertEqual(Strand_col[1],(1.0,1.0,0.0))
        
    def test_color_by_ss_invalid_nodes(self):
        """tests color bySS invalid nodes as input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        command = self.mv.colorBySecondaryStructure
        returnValue = command("gfdjs")
        self.assertEqual(returnValue,'ERROR')

    def test_color_by_ss_invalid_empty(self):
        """tests color bySS empty nodes as input
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        command = self.mv.colorBySecondaryStructure
        returnValue = command(" ")
        self.assertEqual(returnValue,'ERROR')
        
############################################################
# RibbonCommand Tests                                      #
############################################################
class RibbonTest(SSBaseTest):
    def test_ribbon_emptyViewer(self):
        """ Test if the ribbon behaves properly when no molecule has
        been loaded in the viewer and with the default values"""
        if self.mv.ribbon.flag & 1:
            self.mv.ribbon(self.mv.getSelection())
            
        else:
            raise ValueError("WARNING: self.mv.ribbon cannot be called with only self.mv.getSelection()")
            
    def test_ribbon_1crn(self):
        """ Test if ribbon on the 1crn with the default param"""
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure("1crn")
        self.mv.ribbon(self.mv.getSelection())
        gc = self.mv.Mols[0].geomContainer.atoms
        self.failUnless(gc.has_key('Coil5 '))
        self.failUnless(gc.has_key('Helix2 '))
        self.failUnless(gc.has_key('Turn1 '))
        self.failUnless(gc.has_key('Strand1 '))
        
    def test_ribbon_only(self):
        """tests ribbon with only = 1
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        #print "current selection is  ", len(mv.getSelection()), " residues"
        self.assertEqual(len(mv.getSelection()), 10)
        #self.mv.computeSecondaryStructure("1crn")
        self.mv.ribbon("1crn")
        old_res_len = len(self.mv.Mols[0].geomContainer.atoms['Helix1 '])
        self.mv.ribbon(self.mv.getSelection(), only=1)
        new_res_len = len(self.mv.Mols[0].geomContainer.atoms['Helix1 '])
        self.assertNotEqual(old_res_len, new_res_len)
        
    def test_ribbon_negate(self):
        """tests ribbon with negate = 1
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select(self.mv.allAtoms)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.ribbon("1crn",negate = 1)
        #print "geomContainer.atoms.keys()=", self.mv.Mols[0].geomContainer.atoms.keys()
        len_atoms = len(self.mv.Mols[0].geomContainer.atoms['Helix1 '])
        self.assertEqual(len_atoms,0)
        
    def test_ribbon_only_negate(self):
        """tests ribbon with only = 1, negate = 1
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select(self.mv.allAtoms[20:40])
        self.mv.computeSecondaryStructure("1crn")
        self.mv.ribbon("1crn",only = 1,negate = 1)
        len_atoms = len(self.mv.Mols[0].geomContainer.geoms['Helix1 '].vertexSet.vertices)
        self.failUnless(len_atoms,0)

    def test_ribbon_invalid_nodes(self):
        """tests ribbon with invalid nodes as input 
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure("1crn")
        command = self.mv.ribbon
        returnValue = command("hello")
        self.assertEqual(returnValue ,None)
        
           
    def test_ribbon_empty_nodes(self):
        """tests ribbon with empty nodes as input
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure("1crn")
        command = self.mv.ribbon
        returnValue = command(" ")
        self.assertEqual(returnValue ,None)
        
    
    def test_ribbon_invalid_only(self):
        """tests ribbon with invalid input for only
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure("1crn")
        command = self.mv.ribbon
        returnValue = command("1crn",only = 'hai')
        self.assertEqual(returnValue ,None)
        
    def test_ribbon_invalid_negate(self):
        """tests ribbon with invalid input for negate
        """
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure("1crn")
        command = self.mv.ribbon
        returnValue = command("1crn",negate = 'hai')
        self.assertEqual(returnValue ,None)
        
    def test_ribbon_color(self):
        """tests ribbon by coloring
        """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        self.mv.ribbon("1crn")
        old_col = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        self.mv.colorByAtomType("1crn",geomsToColor = ['secondarystructure']) 
        new_col = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        self.assertEqual(old_col!=new_col,True)


##### Log Tests

class SSLogTests(SSBaseTest):

    def test_computeSecondaryStructure_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            self.mv.readMolecule("Data/1crn.pdb")
            self.mv.computeSecondaryStructure("1crn",
                                              molModes = {'1crn':'From File'})    
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.computeSecondaryStructure("1crn", molModes={\'1crn\':\'From File\'}, log=0)')



    def test_computeSecondaryStructure_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/1crn.pdb")
        oldself=self
        self =mv
        s = 'self.computeSecondaryStructure("1crn", molModes={\'1crn\': \'From File\'}, log=0)'
        exec(s)
        oldself.assertEqual(1,1)

    def test_extrudeSecondaryStructure_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            self.mv.readMolecule('Data/1crnnosheet2D.pdb')
            self.mv.computeSecondaryStructure(self.mv.getSelection())
            chain = self.mv.Mols[0].chains[0]
            self.mv.extrudeSecondaryStructure(self.mv.getSelection(), display=1)
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.extrudeSecondaryStructure("1crnnosheet2D", nbchords=8, gapBeg=0, gapEnd=0, larrow=2, frontcap=True, shape2=None, shape1=None, arrow=True, endcap=True, display=1, log=0)')



    def test_extrudeSecondaryStructure_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crnnosheet2D.pdb')
        self.mv.computeSecondaryStructure(self.mv.getSelection())
        chain = self.mv.Mols[0].chains[0]
        oldself=self
        self =mv
        s = 'self.extrudeSecondaryStructure("1crnnosheet2D", nbchords=4, gapBeg=0, gapEnd=0, larrow=2, frontcap=True, shape2=None, shape1=None, arrow=True, endcap=True, display=1, log=0)'
        exec(s)
        oldself.failUnless(hasattr(chain, 'sheet2D'))


    def test_displaySecondaryStructure_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            self.mv.readMolecule('Data/1crn.pdb')
            self.mv.computeSecondaryStructure("1crn")
            self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
            self.mv.displayExtrudedSS("1crn")
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displayExtrudedSS("1crn", negate=False, only=False, log=0)')
            
    def test_displaySecondaryStructure_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        oldself=self
        self =mv
        s = 'self.displayExtrudedSS("1crn", negate=False, only=False, log=0)'
        exec(s)     
        oldself.assertEqual(oldself.mv.Mols[0].geomContainer.geoms['secondarystructure'].visible, True)

    def test_undisplay_secondary_structure_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            self.mv.readMolecule('Data/1crn.pdb')
            self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
            self.mv.selectFromString("", "", "11-20", "", negate=False)
            self.mv.computeSecondaryStructure("1crn")
            self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
            self.mv.displayExtrudedSS("1crn")   
            self.mv.undisplayExtrudedSS("1crn")
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.displayExtrudedSS("1crn", negate=1, only=False, log=0)')


    def test_undisplay_secondary_structure_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        self.mv.displayExtrudedSS("1crn")   
        oldself=self
        self =mv
        s = 'self.displayExtrudedSS("1crn", negate=1, only=False, log=0)'
        exec(s)
        oldself.assertEqual(len(oldself.mv.Mols[0].geomContainer.geoms['secondarystructure'].vertexSet.vertices),0)


    def test_color_by_ss_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            self.mv.readMolecule('Data/1crn.pdb')
            self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
            #self.mv.selectFromString("", "", "11-20", "", negate=False)
            self.mv.computeSecondaryStructure("1crn")
            self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
            old_colors = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
            self.mv.colorBySecondaryStructure("1crn",geomsToColor = ['secondarystructure'])
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.colorBySecondaryStructure("1crn:::", [\'secondarystructure\'], log=0)')
        
        

    def test_color_by_ss_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/1crn.pdb')
        self.mv.setSelectionLevel(Residue, KlassSet=None, log=0)
        #self.mv.selectFromString("", "", "11-20", "", negate=False)
        self.mv.computeSecondaryStructure("1crn")
        self.mv.extrudeSecondaryStructure("1crn",shape1 =Triangle2D(side = 1.5),shape2 = Triangle2D(side = 1.5),display = 1)
        old_colors = self.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        oldself=self
        self =mv
        s = 'self.colorBySecondaryStructure("1crn", [\'secondarystructure\'], log=0)'
        exec(s)
        new_colors = oldself.mv.Mols[0].chains.residues.atoms.colors['secondarystructure']
        oldself.assertEqual(old_colors != new_colors,True)
        



    def test_ribbon_1crn_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        if self.mv.hasGui:
            self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
            self.mv.readMolecule('./Data/1crn.pdb')
            self.mv.select('1crn')
            self.mv.computeSecondaryStructure("1crn")
            self.mv.ribbon(self.mv.getSelection())
            gc = self.mv.Mols[0].geomContainer.atoms
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.ribbon("1crn", negate=False, only=False, log=0)')


    def test_ribbon_1crn_log_checks_that_it_runs(self):
        """Checking log string runs """
        
        self.mv.readMolecule('./Data/1crn.pdb')
        self.mv.select('1crn')
        self.mv.computeSecondaryStructure("1crn")
        self.mv.ribbon(self.mv.getSelection())
        gc = self.mv.Mols[0].geomContainer.atoms
        oldself=self
        self =mv
        s = 'self.ribbon("1crn", negate=False, only=False, log=0)'
        exec(s)
        oldself.failUnless(gc.has_key('Coil5 '))        

    def test_ribbon_DNA(self):
        mol = self.mv.readMolecule('./Data/2EZD_.pdb')
        self.mv.extrudeSecondaryStructure.showNucleicAcidsPropertiesGUI  = False
        self.mv.ribbon("2EZD_")
        SS = mol.geomContainer[0].geoms['secondarystructure']
        if not SS:
            self.fail()

    def test_ribbon_RNA(self):
        mol = self.mv.readMolecule('./Data/1HS1.pdb')
        self.mv.extrudeSecondaryStructure.showNucleicAcidsPropertiesGUI  = False        
        self.mv.ribbon("1HS1")
        SS = mol.geomContainer[0].geoms['secondarystructure']
        if not SS:
            self.fail()

if __name__ == '__main__':
    unittest.main()













############################################################
# OTHER BUGS                                               #
############################################################
## def test_DanielBug():
##     self.mv.readMolecule("./Data/fx.pdb")
##     self.mv.loadCommand("selectionCommands", ['selectFromString',], 'Pself.mv')
##     self.mv.loadModule("editCommands",'Pself.mv')
##     self.mv.loadModule("deleteCommands", 'Pself.mv')

##     self.mv.selectFromString('','','','H*',1)
##     self.mv.deleteAtomSet(self.mv.getSelection())
##     self.mv.add_hGC("fx:::", polarOnly = 1, renumber = 1,
##                method = 'noBondOrder', log = 0)
##     self.mv.computeSecondaryStructure("fx")
    



## harness = testplus.TestHarness( __name__,
##                                 connect = setUp,
##                                 # funs = [test_computeSecondaryStructure_fileThenStrideOnMol],
##                                 funs = testplus.testcollect( globals()),
##                                 disconnect = tearDown
##                                 )

## if __name__ == '__main__':
##     testplus.chdir()
##     print harness
##     sys.exit( len( harness))
