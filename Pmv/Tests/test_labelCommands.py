#
# $Id: test_labelCommands.py,v 1.20.2.2 2016/02/11 21:29:44 annao Exp $
#
################################################################################
#
#   Authors:Sowjanya KARNATI, Michel F. SANNER
#   Copyright: M. Sanner TSRI 2004
#
################################################################################

import sys,os,string
import unittest,string
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from Pmv.extruder import Sheet2D
from string import split
mv= None
klass = None
ct = 0
totalCt = 252

try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class LabelBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                 withShell=0, gui=hasGUI,
                                 verbose=False, trapExceptions=False)
            #mv.setUserPreference(('trapExceptions', '0'), log = 0)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('labelCommands', package='Pmv')
            mv.browseCommands('displayCommands',
                              commands=['displaySticksAndBalls',
                                        'undisplaySticksAndBalls',
                                        'displayCPK', 'undisplayCPK',
                                        'displayLines','undisplayLines',
                                        'displayBackboneTrace',
                                        'undisplayBackboneTrace',
                                        'DisplayBoundGeom'
                                        ],  package='Pmv')
        self.mv = mv 
           
        

    def setUp(self):
        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            print 'setup: destroying mv'
            if mv and mv.hasGui:
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name
            
        if not hasattr(self, 'mv'):
            self.startViewer()
        #for m in self.mv.Mols:
        #    self.mv.deleteMol(m)

    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        #for m in self.mv.Mols:
        #    self.mv.deleteMol(m)
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv


####################################################
#  LABEL BY PROPERTY COMMAND TESTS                 #          
####################################################
class LabelByProperty1(LabelBaseTest):

    def setUp(self):
      LabelBaseTest.setUp(self)
      if not len(self.mv.Mols):
          print "READ MOL"
          self.mv.readMolecule("Data/ind.pdb")
      else:
          self.mv.clearSelection()

#_bndIndex_
    def test_label_by_property_Atom_bndindex_1(self):
        """Tests label by property at Atom level ,_bndIndex_:
           labels dispalyed in viewer are atoms bndIndex
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_bndIndex_'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms._bndIndex_
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))
        
        
    def test_label_by_property_Atom_bndindex_2(self):
        """Tests label by property at Atom level ,_bndIndex_:
           checks all arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_bndIndex_'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['_bndIndex_'])


    def test_label_by_property_Atom_bndindex_color(self):
        """Tests label by property at Atom level ,_bndIndex_:
        coloring labels
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_bndIndex_'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)
        

    def test_label_by_property_Atom_bndindex_negate_1(self):
        """Tests label by property at Atom level ,_bndIndex_:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['_bndIndex_'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_bndindex_only_1(self):
        """Tests label by property at Atom level ,_bndIndex_:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['_bndIndex_'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

#atomicNumber
    def test_label_by_property_Atom_atomicNumber_1(self):
        """Tests label by property at Atom level, atomicNumber:
        labels displayed in viewer are atomic numbers
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['atomicNumber'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.atomicNumber
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))


    def test_label_by_property_Atom_atomicNumber_2(self):
        """Tests label by property at Atom level, atomicNumber:
        checks arguements
        """        
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c =self.mv.labelByProperty
        #c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['atomicNumber'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['atomicNumber'])


    def test_label_by_property_Atom_atomicNumber_color(self):
        """Tests label by property at Atom level, atomicNumber:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['atomicNumber'])
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_atomicNumber_negate_1(self):
        """Tests label by property at Atom level, atomicNumber:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['atomicNumber'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_atomicNumber_only_1(self):
        """Tests label by property at Atom level, atomicNumber:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['atomicNumber'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

#_uniqIndex

    def test_label_by_property_Atom_uniqindex_1(self):
        """Tests label by property at Atom level,_uniqIndex:
        labels displayed in the viewer are atoms uniqIndex
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms._uniqIndex
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))


    def test_label_by_property_Atom_uniqindex_2(self):
        """Tests label by property at Atom level,_uniqIndex:
        checks arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['_uniqIndex'])


    def test_label_by_property_Atom_uniqindex_color(self):
        """Tests label by property at Atom level,_uniqIndex:
        checks label colors
        """        
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_uniqindex_negate_1(self):
        """Tests label by property at Atom level,_uniqIndex:
        checks when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['_uniqIndex'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_uniqindex_only_1(self):
        """Tests label by property at Atom level,_uniqIndex:
        checks when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#ballRad

    def test_label_by_property_Atom_ballRad_1(self):
        """Tests label by property at Atom level, ballRad:
        labels displayed in viewer are atoms ballRad 
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballRad'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.ballRad
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],1)),gc[i])


    def test_label_by_property_Atom_ballRad_2(self):
        """Tests label by property at Atom level, ballRad:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        #c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballRad'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['ballRad'])


    def test_label_by_property_Atom_ballRad_color(self):
        """Tests label by property at Atom level,ballRad:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballRad'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_ballRad_negate_1(self):
        """Tests label by property at Atom level, ballRad:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['ballRad'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_ballRad_only_1(self):
        """Tests label by property at Atom level, ballRad:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['ballRad'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)



#ballScale

    def test_label_by_property_Atom_ballScale_1(self):
        """Tests label by property at Atom level, atomicNumber:
        labels displayed in viewer are atoms ballScale
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballScale'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.ballScale
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],1)),gc[i])


    def test_label_by_property_Atom_ballScale_2(self):
        """Tests label by property at Atom level, ballScale:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        #c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballScale'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['ballScale'])


    def test_label_by_property_Atom_ballScale_color(self):
        """Tests label by property at Atom level,ballScale:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballScale'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_ballScale_negate_1(self):
        """Tests label by property at Atom level, ballScale:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['ballScale'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_ballScale_only_1(self):
        """Tests label by property at Atom level, ballScale:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.displaySticksAndBalls("ind:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.mv.select(mv.allAtoms[:20])

        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['ballScale'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#bondOrderRadius

    def test_label_by_property_Atom_bondOrderRadius_1(self):
        """Tests label by property at Atom level, bondOrderRadius:
        labels displayed in viewer are atoms bondOrderRadius
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['bondOrderRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.bondOrderRadius
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],3)),gc[i])


    def test_label_by_property_Atom_bondOrderRadius_2(self):
        """Tests label by property at Atom level, bondOrderRadius:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        #c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['bondOrderRadius'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['bondOrderRadius'])


    def test_label_by_property_Atom_bondOrderRadius_color(self):
        """Tests label by property at Atom level,bondOrderRadius:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['bondOrderRadius'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_bondOrderRadius_negate_1(self):
        """Tests label by property at Atom level, bondOrderRadius:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['bondOrderRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_bondOrderRadius_only_1(self):
        """Tests label by property at Atom level, bondOrderRadius:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['bondOrderRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

#chemElem

    def test_label_by_property_Atom_chemElem_1(self):
        """Tests label by property at Atom level, chemElem:
        labels displayed in viewer are atoms chemElem
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['chemElem'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.chemElem
        for i in range(0,len(gc)):
            self.assertEqual(list[i],gc[i])


    def test_label_by_property_Atom_chemElem_2(self):
        """Tests label by property at Atom level, chemElem:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['chemElem'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['chemElem'])


    def test_label_by_property_Atom_chemElem_color(self):
        """Tests label by property at Atom level,chemElem:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['chemElem'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_chemElem_negate_1(self):
        """Tests label by property at Atom level, chemElem:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['chemElem'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_chemElem_only_1(self):
        """Tests label by property at Atom level, chemElem:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['chemElem'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#conformation

    def test_label_by_property_Atom_conformation_1(self):
        """Tests label by property at Atom level, conformation:
        labels displayed in viewer are atoms conformations
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['conformation'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.conformation
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))


    def test_label_by_property_Atom_conformation_2(self):
        """Tests label by property at Atom level, conformation:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['conformation'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['conformation'])


    def test_label_by_property_Atom_conformation_color(self):
        """Tests label by property at Atom level,conformation:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['conformation'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_conformation_negate_1(self):
        """Tests label by property at Atom level, conformation:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['conformation'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_conformation_only_1(self):
        """Tests label by property at Atom level, conformation:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['conformation'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#covalentRadius

    def test_label_by_property_Atom_covalentRadius_1(self):
        """Tests label by property at Atom level, covalentRadius:
        labels displayed in viewer are atoms covalent radius
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['covalentRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.covalentRadius
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],3)),gc[i])


    def test_label_by_property_Atom_covalentRadius_2(self):
        """Tests label by property at Atom level, covalentRadius:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['covalentRadius'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['covalentRadius'])


    def test_label_by_property_Atom_covalentRadius_color(self):
        """Tests label by property at Atom level,covalentRadius:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['covalentRadius'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_covalentRadius_negate_1(self):
        """Tests label by property at Atom level, covalentRadius:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['covalentRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_covalentRadius_only_1(self):
        """Tests label by property at Atom level, covalentRadius:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['covalentRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#element

    def test_label_by_property_Atom_element_1(self):
        """Tests label by property at Atom level, element:
        labels displayed in viewer are elements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['element'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.element
        for i in range(0,len(gc)):
            self.assertEqual(list[i],gc[i])


    def test_label_by_property_Atom_element_2(self):
        """Tests label by property at Atom level, element:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['element'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['element'])


    def test_label_by_property_Atom_element_color(self):
        """Tests label by property at Atom level,element:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['element'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_element_negate_1(self):
        """Tests label by property at Atom level, element:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['element'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_element_only_1(self):
        """Tests label by property at Atom level, element:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['element'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

#hetatm

    def test_label_by_property_Atom_hetatm_1(self):
        """Tests label by property at Atom level, hetatm:
        labels displayed in viewer are hetatm
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hetatm'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.hetatm
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))


    def test_label_by_property_Atom_hetatm_2(self):
        """Tests label by property at Atom level, hetatm:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hetatm'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['hetatm'])


    def test_label_by_property_Atom_hetatm_color(self):
        """Tests label by property at Atom level,hetatm:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hetatm'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_hetatm_negate_1(self):
        """Tests label by property at Atom level, hetatm:
        labels displayed in viewer are atomic numbers
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['hetatm'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_hetatm_only_1(self):
        """Tests label by property at Atom level, hetatm:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['hetatm'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)      

#maxBonds

    def test_label_by_property_Atom_maxBonds_1(self):
        """Tests label by property at Atom level, maxBonds:
        labels displayed in viewer are max bonds
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['maxBonds'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.maxBonds
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))


    def test_label_by_property_Atom_maxBonds_2(self):
        """Tests label by property at Atom level, maxBonds:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['maxBonds'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['maxBonds'])


    def test_label_by_property_Atom_maxBonds_color(self):
        """Tests label by property at Atom level,maxBonds:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['maxBonds'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_maxBonds_negate_1(self):
        """Tests label by property at Atom level, maxBonds:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['maxBonds'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_maxBonds_only_1(self):
        """Tests label by property at Atom level, maxBonds:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['maxBonds'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20) 


#name

    def test_label_by_property_Atom_name_1(self):
        """Tests label by property at Atom level, Atom name:
        labels displayed in viewer are atom names
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.name
        for i in range(0,len(gc)):
            self.assertEqual(list[i],gc[i])


    def test_label_by_property_Atom_name_2(self):
        """Tests label by property at Atom level, Atom name:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['name'])


    def test_label_by_property_Atom_name_color(self):
        """Tests label by property at Atom level,Atom name:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_name_negate_1(self):
        """Tests label by property at Atom level, Atom name:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['name'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_name_only_1(self):
        """Tests label by property at Atom level, Atom name:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['name'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)       

#normalname

    def test_label_by_property_Atom_normalname_1(self):
        """Tests label by property at Atom level, normal name:
        labels displayed in viewer are atoms normal name
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['normalname'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.normalname
        for i in range(0,len(gc)):
            self.assertEqual(list[i],gc[i])


    def test_label_by_property_Atom_normalname_2(self):
        """Tests label by property at Atom level, normal name:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['normalname'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['normalname'])


    def test_label_by_property_Atom_normalname_color(self):
        """Tests label by property at Atom level,normal name:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['normalname'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_normalname_negate_1(self):
        """Tests label by property at Atom level, normal name:
        when negate = 1
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['normalname'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_normalname_only_1(self):
        """Tests label by property at Atom level, normal name:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['normalname'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)       


#number

    def test_label_by_property_Atom_number_1(self):
        """Tests label by property at Atom level, atom number:
        labels displayed in viewer are atom numbers
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.number
        for i in range(0,len(gc)):
            self.assertEqual(list[i],int(gc[i]))


    def test_label_by_property_Atom_number_2(self):
        """Tests label by property at Atom level, atom number:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['number'])


    def test_label_by_property_Atom_number_color(self):
        """Tests label by property at Atom level,atom number:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_number_negate_1(self):
        """Tests label by property at Atom level, atom number:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['number'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_number_only_1(self):
        """Tests label by property at Atom level, atom number:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['number'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)      


#occupancy

    def test_label_by_property_Atom_occupancy_1(self):
        """Tests label by property at Atom level, atom occupancy:
        labels displayed in viewer are occupancy
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['occupancy'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.occupancy
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]),gc[i])


    def test_label_by_property_Atom_occupancy_2(self):
        """Tests label by property at Atom level, atom occupancy:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['occupancy'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['occupancy'])


    def test_label_by_property_Atom_occupancy_color(self):
        """Tests label by property at Atom level,atom occupancy:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['occupancy'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_occupancy_negate_1(self):
        """Tests label by property at Atom level, atom occupancy:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['occupancy'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_occupancy_only_1(self):
        """Tests label by property at Atom level, atom occupancy:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['occupancy'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)  

#organic

    def test_label_by_property_Atom_organic_1(self):
        """Tests label by property at Atom level, organic:
        labels displayed in viewer are organic
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['organic'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.organic
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]),gc[i])


    def test_label_by_property_Atom_organic_2(self):
        """Tests label by property at Atom level, organic:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['organic'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['organic'])


    def test_label_by_property_Atom_organic_color(self):
        """Tests label by property at Atom level,organic:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['organic'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_organic_negate_1(self):
        """Tests label by property at Atom level, organic:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['organic'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_organic_only_1(self):
        """Tests label by property at Atom level, organic:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['organic'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#segID

    def test_label_by_property_Atom_segID_1(self):
        """Tests label by property at Atom level, segID:
        labels displayed in viewer are segID
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['segID'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.segID
        for i in range(0,len(gc)):
            self.assertEqual(list[i],gc[i])


    def test_label_by_property_Atom_segID_2(self):
        """Tests label by property at Atom level, segID:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['segID'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['segID'])


    def test_label_by_property_Atom_segID_color(self):
        """Tests label by property at Atom level,segID:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['segID'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_segID_negate_1(self):
        """Tests label by property at Atom level, segID:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['segID'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_segID_only_1(self):
        """Tests label by property at Atom level, segID:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['segID'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#temperatureFactor

    def test_label_by_property_Atom_temperatureFactor_1(self):
        """Tests label by property at Atom level, temperatureFactor:
        labels displayed in viewer are temperatureFactor
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['temperatureFactor'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.temperatureFactor
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],2)),gc[i])


    def test_label_by_property_Atom_temperatureFactor_2(self):
        """Tests label by property at Atom level, temperatureFactor:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['temperatureFactor'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['temperatureFactor'])


    def test_label_by_property_Atom_temperatureFactor_color(self):
        """Tests label by property at Atom level,temperatureFactor:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['temperatureFactor'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_temperatureFactor_negate_1(self):
        """Tests label by property at Atom level, temperatureFactor:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['temperatureFactor'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_temperatureFactor_only_1(self):
        """Tests label by property at Atom level, temperatureFactor:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['temperatureFactor'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)


#vdwRadius

    def test_label_by_property_Atom_vdwRadius_1(self):
        """Tests label by property at Atom level, vdwRadius:
        labels displayed in viewer are vdwRadius
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['vdwRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.vdwRadius
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],2)),gc[i])


    def test_label_by_property_Atom_vdwRadius_2(self):
        """Tests label by property at Atom level, vdwRadius:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['vdwRadius'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['vdwRadius'])


    def test_label_by_property_Atom_vdwRadius_color(self):
        """Tests label by property at Atom level,vdwRadius:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['vdwRadius'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_vdwRadius_negate_1(self):
        """Tests label by property at Atom level, vdwRadius:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['vdwRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_vdwRadius_only_1(self):
        """Tests label by property at Atom level, vdwRadius:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['vdwRadius'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

###Residues###

#_uniqIndex

    def test_label_by_property_residue_uniqIndex_1(self):
        """Tests label by property at residue level,_uniqIndex:
        labels innthe viewer are residue _uniqIndex
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        #self.mv.labelByProperty("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues._uniqIndex
        self.assertEqual(list[0] , int(gc[0]))
        
        
        
    def test_label_by_property_residue_2(self):
        """Tests label by property at residue level,_uniqIndex:
        checks arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['_uniqIndex'])
        


    def test_label_by_property_residue_color(self):
        """Tests label by property at Residue level,_uniqIndex:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByResidueType("ind:::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_negate_1(self):
        """Tests label by property at Residue level,_uniqIndex :
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['_uniqIndex'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_only_1(self):
        """Tests label by property at Residue level,_uniqIndex :
        when only = 1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['_uniqIndex'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)
        

#childrenName
    def test_label_by_property_residue_childrenName_1(self):
        """Tests label by property at residue level,childrenName:
        labels innthe viewer are residue childrenName
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.childrenName
        self.assertEqual(list[0] , gc[0])
        
        
        
    def test_label_by_property_residue_childrenName_2(self):
        """Tests label by property at residue level,childrenName:
        checks arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['childrenName'])
        


    def test_label_by_property_residue_childrenName_color(self):
        """Tests label by property at Residue level,childrenName:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels'] 
        self.mv.colorByResidueType("ind::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_childrenName_negate_1(self):
        """Tests label by property at Residue level,childrenName :
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['childrenName'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_childrenName_only_1(self):
        """Tests label by property at Residue level,childrenName :
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['childrenName'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)



#hasBonds

    def test_label_by_property_residue_hasBonds_1(self):
        """Tests label by property at residue level,hasBonds:
        labels innthe viewer are residue hasbonds
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.hasBonds
        self.assertEqual(str(list[0]) , gc[0])
        
        
        
    def test_label_by_property_residue_hasBonds_2(self):
        """Tests label by property at residue level,hasBonds:
        checks arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['hasBonds'])
        


    def test_label_by_property_residue_hasBonds_color(self):
        """Tests label by property at Residue level,hasbonds:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByResidueType("ind::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_hasBonds_negate_1(self):
        """Tests label by property at Residue level, hasBonds:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['hasBonds'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)

class LabelByProperty2(LabelBaseTest):
    
    def setUp(self):
      LabelBaseTest.setUp(self)
      if not len(self.mv.Mols):
          print "READ MOL"
          self.mv.readMolecule("Data/ind.pdb")
          self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
      else:
          self.mv.clearSelection()
#cpkRad

    def test_label_by_property_Atom_cpkRad_1(self):
        """Tests label by property at Atom level, cpkRad:
        labels displayed in viewer are atoms cpkrad 
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkRad'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.cpkRad
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],1)),gc[i])


    def test_label_by_property_Atom_cpkRad_2(self):
        """Tests label by property at Atom level, cpkRad:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkRad'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(0.0, 0.0, 0.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['cpkRad'])


    def test_label_by_property_Atom_cpkRad_color(self):
        """Tests label by property at Atom level,cpkRad:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkRad'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_cpkRad_negate_1(self):
        """Tests label by property at Atom level, cpkRad:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['cpkRad'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_cpkRad_only_1(self):
        """Tests label by property at Atom level, cpkRad:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkRad'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

#cpkScale

    def test_label_by_property_Atom_cpkScale_1(self):
        """Tests label by property at Atom level, cpkScale:
        labels displayed in viewer are cpkScale
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkScale'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        list = self.mv.allAtoms.cpkScale
        for i in range(0,len(gc)):
            self.assertEqual(str(round(list[i],1)),gc[i])


    def test_label_by_property_Atom_cpkScale_2(self):
        """Tests label by property at Atom level, cpkScale:
        checks arguements
        """ 
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkScale'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(0.0, 0.0, 0.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['cpkScale'])


    def test_label_by_property_Atom_cpkScale_color(self):
        """Tests label by property at Atom level,cpkScale:
        checks label colors
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.labelByProperty("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkScale'])
        self.mv.color("ind:::", [(1.0, 1.0, 1.0)], ['AtomLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['AtomLabels']
        self.mv.colorByAtomType("ind:::", ('AtomLabels',), log=0)
        self.mv.allAtoms.colors['AtomLabels']
        new_colors = self.mv.allAtoms.colors['AtomLabels']
        self.assertEqual(old_colors != new_colors,True)



    def test_label_by_property_Atom_cpkScale_negate_1(self):
        """Tests label by property at Atom level, cpkScale:
        when negate = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty("ind:::", textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['cpkScale'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),0)

        

    def test_label_by_property_Atom_cpkScale_only_1(self):
        """Tests label by property at Atom level, cpkScale:
        when only = 1
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.displayCPK("ind:::", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.mv.select(mv.allAtoms[:20])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByProperty(self.mv.getSelection(), textcolor=(0.0, 0.0, 0.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['cpkScale'])
        gc = self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['AtomLabels'].vertexSet),20)

        
class LabelByProperty3(LabelBaseTest):

    def setUp(self):
      LabelBaseTest.setUp(self)
      if not len(self.mv.Mols):
          print "READ MOL"
          self.mv.readMolecule("Data/1crn.pdb")
      else:
          self.mv.clearSelection()
    
    def test_label_by_property_residue_hasBonds_only_1(self):
        """Tests label by property at Residue level,hasBonds :
        when only = 1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['hasBonds'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)

#hasCA

    def test_label_by_property_residue_hasCA_1(self):
        """Tests label by property at residue level,hasCA:
        labels innthe viewer are residue hasCA
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasCA'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.hasCA
        for i in range(0,len(gc)):
            self.assertEqual(str(list[0]) , gc[0])
        
        
        
    def test_label_by_property_residue_hasCA_2(self):
        """Tests label by property at residue level,hasCA:
        checks arguements
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasCA'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['hasCA'])
        


    def test_label_by_property_residue_hasCA_color(self):
        """Tests label by property at Residue level, hasCA:
        checks label colors
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasCA'])
        self.mv.color("1crn::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByResidueType("1crn:::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_hasCA_negate_1(self):
        """Tests label by property at Residue level, hasCA:
        when negate = 1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['hasCA'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_hasCA_only_1(self):
        """Tests label by property at Residue level, hasCA:
        when only = 1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['hasCA'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        #only one residue is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)
#has O

    def test_label_by_property_residue_hasO_1(self):
        """Tests label by property  at Residue level,hasO:
        labels displayed in the viewer are residues having O
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasO'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.hasO
        for i in range(0,len(gc)):
            self.assertEqual(str(list[0]) , gc[0])
        
        
        
    def test_label_by_property_residue_hasO_2(self):
        """Tests label by property  at Residue level,hasO:
        checks arguements
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasO'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['hasO'])
        
    def test_label_by_property_residue_hasO_color(self):
        """Tests label by property  at Residue level,hasO:
        checks label colors
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasO'])
        self.mv.color("1crn::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByAtomType("1crn::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_hasO_negate_1(self):
        """Tests label by property  at Residue level,hasO:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['hasO'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_hasO_only_1(self):
        """Tests label by property  at Residue level,hasO:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['hasO'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        #only one residue is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)



#name

    def test_label_by_property_residue_name_1(self):
        """Tests label by property  at Residue level,name:
        labels displayed in the viewer are residues names
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.name
        for i in range(0,len(gc)):
            self.assertEqual(str(list[0]) , gc[0])
        
        
        
    def test_label_by_property_residue_name_2(self):
        """Tests label by property  at Residue level,name:
        checks arguements
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['name'])
        
    def test_label_by_property_residue_name_color(self):
        """Tests label by property  at Residue level,name:
        checks label colors
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.mv.color("1crn::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByAtomType("1crn::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_name_negate_1(self):
        """Tests label by property  at Residue level,name:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['name'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_name_only_1(self):
        """Tests label by property  at Residue level,name:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['name'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        #only one residue is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)


#number

    def test_label_by_property_residue_number_1(self):
        """Tests label by property  at Residue level,number:
        labels displayed in the viewer are residues numbers
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.number
        for i in range(0,len(gc)):
            self.assertEqual(str(list[0]) , gc[0])
        
        
        
    def test_label_by_property_residue_number_2(self):
        """Tests label by property  at Residue level,number:
        checks arguements
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['number'])
        
    def test_label_by_property_residue_number_color(self):
        """Tests label by property  at Residue level,number:
        checks label colors
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.mv.color("1crn::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByAtomType("1crn::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_number_negate_1(self):
        """Tests label by property  at Residue level,number:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['number'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_number_only_1(self):
        """Tests label by property  at Residue level,number:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['number'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        #only one residue is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)


#type

    def test_label_by_property_residue_type_1(self):
        """Tests label by property  at Residue level,type:
        labels displayed in the viewer are residues types
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByProperty("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['type'])
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        list = self.mv.Mols[0].chains.residues.type
        for i in range(0,len(gc)):
            self.assertEqual(str(list[0]) , gc[0])
        
        
        
    def test_label_by_property_residue_type_2(self):
        """Tests label by property  at Residue level,type:
        checks arguements
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['type'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['type'])
        
    def test_label_by_property_residue_type_color(self):
        """Tests label by property  at Residue level,type:
        checks label colors
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['type'])
        self.mv.color("1crn::", [(1.0, 1.0, 1.0)], ['ResidueLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.mv.colorByAtomType("1crn::", ('ResidueLabels',), log=0)
        self.mv.allAtoms.colors['ResidueLabels']
        new_colors = self.mv.allAtoms.colors['ResidueLabels']
        self.assertEqual(old_colors != new_colors,True)


    def test_label_by_property_residue_type_negate_1(self):
        """Tests label by property  at Residue level,type:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("1crn::", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['type'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)
        
        
    def test_label_by_property_residue_type_only_1(self):
        """Tests label by property  at Residue level,type:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/1crn.pdb")
        self.mv.select(self.mv.Mols[0].chains.residues[0])
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['type'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        #only one residue is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),1)



class LabelByProperty4(LabelBaseTest):

    def setUp(self):
      LabelBaseTest.setUp(self)
      if not len(self.mv.Mols):
          print "READ MOL"
          self.mv.readMolecule("Data/hsg1.pdb")
      else:
          self.mv.clearSelection()
#Chain
    def test_label_by_property_chain_uniq_index_1(self):
        """Tests label by property at chain level:
        labels displayed in the viewer are chains uniqindex
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByProperty("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.Mols[0].chains._uniqIndex
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])
    
    def test_label_by_property_chain_uniq_index_2(self):
        """Tests label by property  at Chain level,uniq index:
        checks arguements
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['_uniqIndex'])    
            
    def test_label_by_property_chain_uniq_index_color(self):
        """Tests label by property  at  level,uniqIndex:
        checks label colors
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['_uniqIndex'])
        self.mv.color("hsg1:", [(1.0, 1.0, 1.0)], ['ChainLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ChainLabels']
        self.mv.colorByAtomType("hsg1:", ('ChainLabels',), log=0)
        self.mv.allAtoms.colors['ChainLabels']
        new_colors = self.mv.allAtoms.colors['ChainLabels']
        self.assertEqual(old_colors != new_colors,True)

    
    def test_label_by_property_chain_uniqIndex_negate_1(self):
        """Tests label by property  at chain level,uniqIndex:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['_uniqIndex'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),0)
        
        
    def test_label_by_property_chain_uniqIndex_only_1(self):
        """Tests label by property  at Chain level,uniqIndex:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['_uniqIndex'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        #only one chain is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),1)        
#childrenName

    def test_label_by_property_chainchildrenName_1(self):
        """Tests label by property at chain level:
        labels displayed in the viewer are chains childrenName
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByProperty("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.Mols[0].chains.childrenName
        
        
        for i in range(0,len(gc)):
            self.assertEqual( list[i] , gc[i] )
    
    def test_label_by_property_chainchildrenName_2(self):
        """Tests label by property  at Chain level,childrenName:
        checks arguements
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['childrenName'])    
            
    def test_label_by_property_chainchildrenName_color(self):
        """Tests label by property  at  level,childrenName:
        checks label colors
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.mv.color("hsg1:", [(1.0, 1.0, 1.0)], ['ChainLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ChainLabels']
        self.mv.colorByAtomType("hsg1:", ('ChainLabels',), log=0)
        self.mv.allAtoms.colors['ChainLabels']
        new_colors = self.mv.allAtoms.colors['ChainLabels']
        self.assertEqual(old_colors != new_colors,True)

    
    def test_label_by_property_chainchildrenName_negate_1(self):
        """Tests label by property  at chain level,childrenName:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['childrenName'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),0)
        
        
    def test_label_by_property_chainchildrenName_only_1(self):
        """Tests label by property  at Chain level,childrenName:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['childrenName'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        #only one chain is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),1)  

#hasBonds

    def test_label_by_property_chainhasBonds_1(self):
        """Tests label by property at chain level:
        labels displayed in the viewer are chains hasBonds
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByProperty("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.Mols[0].chains.hasBonds
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])
    
    def test_label_by_property_chainhasBonds_2(self):
        """Tests label by property  at Chain level,hasBonds:
        checks arguements
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['hasBonds'])    
            
    def test_label_by_property_chainhasBonds_color(self):
        """Tests label by property  at  level,hasBonds:
        checks label colors
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.mv.color("hsg1:", [(1.0, 1.0, 1.0)], ['ChainLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ChainLabels']
        self.mv.colorByAtomType("hsg1:", ('ChainLabels',), log=0)
        self.mv.allAtoms.colors['ChainLabels']
        new_colors = self.mv.allAtoms.colors['ChainLabels']
        self.assertEqual(old_colors != new_colors,True)

    
    def test_label_by_property_chainhasBonds_negate_1(self):
        """Tests label by property  at chain level,hasBonds:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['hasBonds'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),0)
        
        
    def test_label_by_property_chain_hasBonds_only_1(self):
        """Tests label by property  at Chain level,hasBonds:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['hasBonds'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        #only one chain is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),1)

#id

    def test_label_by_property_chain_id_1(self):
        """Tests label by property at chain level:
        labels displayed in the viewer are chains id
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByProperty("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['id'])
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.Mols[0].chains.id
        for i in range(0,len(gc)):
            self.assertEqual(list[i] , gc[i])
    
    def test_label_by_property_chain_id_2(self):
        """Tests label by property  at Chain level,id:
        checks arguements
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['id'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['id'])    
            
    def test_label_by_property_chain_id_color(self):
        """Tests label by property  at  level,id:
        checks label colors
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['id'])
        self.mv.color("hsg1:", [(1.0, 1.0, 1.0)], ['ChainLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ChainLabels']
        self.mv.colorByAtomType("hsg1:", ('ChainLabels',), log=0)
        self.mv.allAtoms.colors['ChainLabels']
        new_colors = self.mv.allAtoms.colors['ChainLabels']
        self.assertEqual(old_colors != new_colors,True)

    
    def test_label_by_property_chain_id_negate_1(self):
        """Tests label by property  at chain level,id:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['id'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),0)
        
        
    def test_label_by_property_chain_id_only_1(self):
        """Tests label by property  at Chain level,id:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['id'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        #only one chain is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),1)

#name

    def test_label_by_property_chain_name_1(self):
        """Tests label by property at chain level:
        labels displayed in the viewer are chains name
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByProperty("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.Mols[0].chains.name
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])
    
    def test_label_by_property_chain_name_2(self):
        """Tests label by property  at Chain level,name:
        checks arguements
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['name'])    
            
    def test_label_by_property_chain_name_color(self):
        """Tests label by property  at  level,name:
        checks label colors
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.mv.color("hsg1:", [(1.0, 1.0, 1.0)], ['ChainLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ChainLabels']
        self.mv.colorByAtomType("hsg1:", ('ChainLabels',), log=0)
        self.mv.allAtoms.colors['ChainLabels']
        new_colors = self.mv.allAtoms.colors['ChainLabels']
        self.assertEqual(old_colors != new_colors,True)

    
    def test_label_by_property_chain_name_negate_1(self):
        """Tests label by property  at chain level,name:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['name'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),0)
        
        
    def test_label_by_property_chain_name_only_1(self):
        """Tests label by property  at Chain level,name:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['name'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        #only one chain is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),1)

#number

    def test_label_by_property_chain_number_1(self):
        """Tests label by property at chain level:
        labels displayed in the viewer are chains number
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByProperty("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.Mols[0].chains.number
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])
    
    def test_label_by_property_chain_number_2(self):
        """Tests label by property  at Chain level,number:
        checks arguements
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['number'])    
            
    def test_label_by_property_chain_number_color(self):
        """Tests label by property  at  level,number:
        checks label colors
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.mv.color("hsg1:", [(1.0, 1.0, 1.0)], ['ChainLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ChainLabels']
        self.mv.colorByAtomType("hsg1:", ('ChainLabels',), log=0)
        self.mv.allAtoms.colors['ChainLabels']
        new_colors = self.mv.allAtoms.colors['ChainLabels']
        self.assertEqual(old_colors != new_colors,True)

    
    def test_label_by_property_chain_number_negate_1(self):
        """Tests label by property  at chain level,number:
        checks negate negate =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c("hsg1:", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0,location='Center', negate=1, font='arial1.glf',properties=['number'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),0)
        
        
    def test_label_by_property_chain_number_only_1(self):
        """Tests label by property  at Chain level,number:
        checks negate only =  1
        """
        #self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.select(self.mv.Mols[0].chains[0])
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)    
        c = self.mv.labelByProperty
        c(self.mv.getSelection(), textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1,location='Center', negate=0, font='arial1.glf',properties=['number'])    
        gc = self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        #only one chain is selected
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ChainLabels'].vertexSet),1)

#molecule
#bondsflag

class LabelByProperty5(LabelBaseTest):

    
    def setUp(self):
      LabelBaseTest.setUp(self)
      if not len(self.mv.Mols):
          print "READ MOL"
          self.mv.readMolecule("Data/ind.pdb")
          self.mv.readMolecule("Data/1bsrsmall.pdb")
    
    def test_label_by_property_molecule_bonds_flag_1(self):
        """Tests label by property  at molecule level, bonds flag:
        checks labels displayed in the viewer are bondsflag
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.labelByProperty("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['bondsflag'])
        gc = self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        list = self.mv.Mols.bondsflag
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])


    def test_label_by_property_molecule_bonds_flag_2(self):
        """Tests label by property  at molecule level, bonds flag:
        checks all the arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['bondsflag'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['bondsflag'])

    def test_label_by_property_molecule_bonds_flag_color(self):
        """Tests label by property  at molecule level, bonds flag:
        checks label colors
        """    
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['bondsflag'])
        self.mv.color("ind;1bsrsmall", [(1.0, 1.0, 1.0)], ['ProteinLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.mv.colorByMolecules("ind;1bsrsmall", ('lines','ProteinLabels'), log=0)
        new_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.assertEqual(old_colors != new_colors,True)

    def test_label_by_property_molecule_bonds_flag_negate_1(self):
        """Tests label by property  at molecule level, bonds flag:
        checks when negate = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['bondsflag'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),0)
    
    
    def test_label_by_property_molecule_bonds_flag_only_1(self):
        """Tests label by property  at molecule level, bonds flag:
        checks when only = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['bondsflag'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),1)

#childrenName
    
    def test_label_by_property_molecule_childrenName_1(self):
        """Tests label by property  at molecule level, childrenName:
        checks labels displayed in the viewer are childrenName
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.labelByProperty("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        gc = self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        list = self.mv.Mols.childrenName
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])


    def test_label_by_property_molecule_childrenName_2(self):
        """Tests label by property  at molecule level, childrenName:
        checks all the arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['childrenName'])

    def test_label_by_property_molecule_childrenName_color(self):
        """Tests label by property  at molecule level, childrenName:
        checks label colors
        """    
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.mv.color("ind;1bsrsmall", [(1.0, 1.0, 1.0)], ['ProteinLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.mv.colorByMolecules("ind;1bsrsmall", ('lines','ProteinLabels'), log=0)
        new_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.assertEqual(old_colors != new_colors,True)

    def test_label_by_property_molecule_childrenName_negate_1(self):
        """Tests label by property  at molecule level, childrenName:
        checks when negate = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['childrenName'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),0)
    
    
    def test_label_by_property_molecule_childrenName_only_1(self):
        """Tests label by property  at molecule level, childrenName:
        checks when only = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['childrenName'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),1)
        
#hasBonds
    
    def test_label_by_property_molecule_hasBonds_1(self):
        """Tests label by property  at molecule level, hasBonds:
        checks labels displayed in the viewer are hasBonds
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.labelByProperty("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        gc = self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        list = self.mv.Mols.hasBonds
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])


    def test_label_by_property_molecule_hasBonds_2(self):
        """Tests label by property  at molecule level, hasBonds:
        checks all the arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['hasBonds'])

    def test_label_by_property_molecule_hasBonds_color(self):
        """Tests label by property  at molecule level, hasBonds:
        checks label colors
        """    
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.mv.color("ind;1bsrsmall", [(1.0, 1.0, 1.0)], ['ProteinLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.mv.colorByMolecules("ind;1bsrsmall", ('lines','ProteinLabels'), log=0)
        new_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.assertEqual(old_colors != new_colors,True)

    def test_label_by_property_molecule_hasBonds_negate_1(self):
        """Tests label by property  at molecule level, hasBonds:
        checks when negate = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['hasBonds'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),0)
    
    
    def test_label_by_property_molecule_hasBonds_only_1(self):
        """Tests label by property  at molecule level, hasBonds:
        checks when only = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['hasBonds'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),1)

#name
    
    def test_label_by_property_molecule_name_1(self):
        """Tests label by property  at molecule level, name:
        checks labels displayed in the viewer are name
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.labelByProperty("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        gc = self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        list = self.mv.Mols.name
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])


    def test_label_by_property_molecule_name_2(self):
        """Tests label by property  at molecule level, name:
        checks all the arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['name'])

    def test_label_by_property_molecule_name_color(self):
        """Tests label by property  at molecule level, name:
        checks label colors
        """    
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.mv.color("ind;1bsrsmall", [(1.0, 1.0, 1.0)], ['ProteinLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.mv.colorByMolecules("ind;1bsrsmall", ('lines','ProteinLabels'), log=0)
        new_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.assertEqual(old_colors != new_colors,True)

    def test_label_by_property_molecule_name_negate_1(self):
        """Tests label by property  at molecule level, name:
        checks when negate = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['name'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),0)
    
    
    def test_label_by_property_molecule_name_only_1(self):
        """Tests label by property  at molecule level, name:
        checks when only = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['name'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),1)


        
#number
    
    def test_label_by_property_molecule_number_1(self):
        """Tests label by property  at molecule level, number:
        checks labels displayed in the viewer are number
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        self.mv.labelByProperty("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        gc = self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        list = self.mv.Mols.number
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])


    def test_label_by_property_molecule_number_2(self):
        """Tests label by property  at molecule level, number:
        checks all the arguements
        """
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        self.assertEqual(c.getLastUsedValues()['textcolor'],(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['properties'],['number'])

    def test_label_by_property_molecule_number_color(self):
        """Tests label by property  at molecule level, number:
        checks label colors
        """    
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.mv.color("ind;1bsrsmall", [(1.0, 1.0, 1.0)], ['ProteinLabels'], redraw=1, log=0,  setupUndo=0)
        old_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.mv.colorByMolecules("ind;1bsrsmall", ('lines','ProteinLabels'), log=0)
        new_colors = self.mv.allAtoms.colors['ProteinLabels']
        self.assertEqual(old_colors != new_colors,True)

    def test_label_by_property_molecule_number_negate_1(self):
        """Tests label by property  at molecule level, number:
        checks when negate = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind;1bsrsmall", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=0, location='Center', negate=1, font='arial1.glf', properties=['number'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),0)
    
    
    def test_label_by_property_molecule_number_only_1(self):
        """Tests label by property  at molecule level, number:
        checks when only = 1"""
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.readMolecule("Data/1bsrsmall.pdb")
        self.mv.setIcomLevel(Molecule, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ProteinLabels'].vertexSet),1)


class LabelByProperty6(LabelBaseTest):

    def setUp(self):
      LabelBaseTest.setUp(self)
      if not len(self.mv.Mols):
          print "READ MOL"
          self.mv.readMolecule("Data/ind.pdb")


#Invalid Input Tests for Label By Property Command
    def test_label_by_property_invalid_nodes(self):
        """tests label by property with invalid nodes
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        returnValue = c("abcd", textcolor=(1.0, 1.0, 1.0), log=0, format=None, only=1, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(returnValue,None)

    def test_label_by_property_invalid_text_color(self):
        """tests label by property with invalid textcolor
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        self.assertRaises(UnboundLocalError,c,"ind:", textcolor=(1.0, 1.0), log=0, format=1, only=1, location='Center', negate=0, font='arial1.glf', properties=['number'])
        
    def test_label_by_property_invalid_font(self):
        """tests label by property with invalid font
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        self.assertRaises(UnboundLocalError,c,"ind:", textcolor=(1.0, 1.0), log=0, format=1, only=1, location='Center', negate=0, font='HELLO', properties=['number'])
        
    def test_label_by_property_invalid_format(self):
        """tests label by property with invalid format
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        #self.assertRaises(AssertionError,c,"ind:", textcolor=(1.0, 1.0,1.0), log=0, format=1, only=1, location='Center', negate=0, font='arial1.glf', properties=['number'])
        rval = c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=1, only=1, location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(rval,None)

    

    def test_label_by_property_invalid_location(self):
        """tests label by property with invalid location
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        #self.assertRaises(AssertionError,c,"ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=1, location='hai', negate=0, font='arial1.glf', properties=['number'])
        rval =c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=1, location='hai', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(rval,None)
        
    def test_label_by_property_invalid_only(self):
        """tests label by property with invalid only
        """
        self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        #self.assertRaises(AssertionError,c,"ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only='hai', location='Center', negate=0, font='arial1.glf', properties=['number'])
        rval = c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only='hai', location='Center', negate=0, font='arial1.glf', properties=['number'])
        self.assertEqual(rval,None)
        
    def test_label_by_property_invalid_negate(self):
        """tests label by property with invalid negate
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        #self.assertRaises(AssertionError,c,"ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate='hai', font='arial1.glf', properties=['number'])
        rval = c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate='hai', font='arial1.glf', properties=['number'])
        self.assertEqual(rval,None)

#End Invalid Input Tests for Label By Property Command


#FONT

    def test_label_by_property_font_1(self):
        """tests label by property with font arial1.glf
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        import time
        from time import sleep
        sleep(2)
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')


    def test_label_by_property_font_2(self):
        """tests label by property with font 9by15
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_property_font_3(self):
        """tests label by property with font 8by13
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_property_font_4(self):
        """tests label by property with font TimesRoman10
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')    

    def test_label_by_property_font_5(self):
        """tests label by property with font TimesRoman24
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_property_font_6(self):
        """tests label by property with font Helvetica10
        """
        #self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_property_font_7(self):
        """tests label by property with font Helvetica18
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind::", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

#LOCATION
    def test_label_by_property_location_1(self):
        """tests label by property with location Center
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        

    def test_label_by_property_location_2(self):
        """tests label by property with location First
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='First', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'First')


    def test_label_by_property_location_3(self):
        """tests label by property with location Last
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Last', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['location'],'Last')


#FORMAT

    def test_label_by_property_format_1(self):
        """tests label by property with format None
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format=None, only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['format'],None)
        
        
    def test_label_by_property_format_2(self):
        """tests label by property with format %d
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format='%d', only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['format'],'%d')

    def test_label_by_property_format_3(self):
        """tests label by property with format %f
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format='%f', only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['format'],'%f')


    def test_label_by_property_format_4(self):
        """tests label by property with format %4.2f
        """
        #self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.labelByProperty
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format='%4.2f', only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        self.assertEqual(c.getLastUsedValues()['format'],'%4.2f')


#Choose Color
    def test_label_by_property_choose_color(self):
        #self.mv.readMolecule("Data/ind.pdb")
        #self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        c = self.mv.labelByProperty
        c.selection = self.mv.Mols
        c.updateChooser()
        c("ind:", textcolor=(1.0, 1.0,1.0), log=0, format='%4.2f', only=0, location='Center', negate= 0, font='arial1.glf', properties=['number'])
        if self.mv.hasGui:
            color = (1.0,1.0,0.0)
            c.showForm('default',modal = 0,blocking = 0)
            c.configureButton(color)
            self.assertEqual(c.textColor,color)
            c.showForm('default',modal = 0,blocking = 0).withdraw()

        
############################################################
#       LABEL  BY EXPRESSION COMMAND TESTS                 #
############################################################

class LabelByExpression(LabelBaseTest):

    def setUp(self):
        LabelBaseTest.setUp(self)
        for m in self.mv.Mols:
            self.mv.deleteMol(m)

    def test_label_by_expression_atom_lambda_function(self):
        """tests label by expression at atom level,lambda function
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByExpression("ind:::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        #fptr = open("file1",'w')
        #for i in range(0,len(mv.allAtoms)):
	    #   x = mv.allAtoms[i].full_name()+','
	    #   fptr.write(x)
        #fptr.close()
        gc =self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        fptr = open("Data/file1",'r')
        alllines =fptr.readlines()
        listats = split(alllines[0],',')
        for i in range(0,len(gc)):
            self.assertEqual(gc[i] , listats[i])
    
    
    def test_label_by_expression_residue_lambda_function(self):
        """tests label by expression at residue level,lambda function
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByExpression("1crn::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        #fptr = open("file2",'w')
        #for i in range(0,len(mv.Mols[0].chains.residues)):
	    #   x = mv.Mols[0].chains.residues[i].full_name()+','
	    #   fptr.write(x)
        #fptr.close()
        gc =self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        fptr = open("Data/file2",'r')
        alllines =fptr.readlines()
        listres = split(alllines[0],',')
        for i in range(0,len(gc)):
            self.assertEqual(gc[i] , listres[i])
    
    def test_label_by_expression_chain_lambda_function(self):
        """tests label by expression at chain level,lambda function
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByExpression("hsg1:", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        #fptr = open("file3",'w')
        #for i in range(0,len(mv.Mols[0].chains)):
	    #   x = mv.Mols[0].chains[i].full_name()+','
	    #   fptr.write(x)
        #fptr.close()
        gc =self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        fptr = open("Data/file3",'r')
        alllines =fptr.readlines()
        listchain = split(alllines[0],',')
        for i in range(0,len(gc)):
            self.assertEqual(gc[i] , listchain[i])

    def test_label_by_expression_molecule_lambda_function(self):
        """tests label by expression at molecule level ,lambda function
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByExpression("hsg1", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        gc =self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        mol = self.mv.Mols[0].full_name()
        self.assertEqual(gc[0] , mol)
        
    def test_label_by_expression_atom_function(self):
        """tests label by expression at atom level, function
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.labelByExpression("ind:::", function='def foo(selection):\n	#values <- foo(node)\n	#values   : list of string used for labelling\n	#selection: current selection.\n	values = []\n	#loop on the current selection\n	for i in xrange(len(selection)):\n		#build a list of values to label the current selection.\n		if not hasattr(selection[i], "number"):\n			values.append("-")\n		else:\n			if selection[i].number > 20:\n				values.append(selection[i].number*2)\n			else:\n				values.append(selection[i].number)\n	# this list of values is then returned.\n	return values\n\n', lambdaFunc=0, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        #fptr = open("file4",'w')
        #for i in range(0,len(mv.allAtoms.number)):
	    #   if mv.getSelection()[i].number<=20:
		#      x = str(mv.getSelection()[i].number)+','
		
	    #   else:
		#       x = str(mv.getSelection()[i].number*2)+','
	    #    fptr.write(x)
 
        gc =self.mv.Mols[0].geomContainer.geoms['AtomLabels'].labels
        fptr = open("Data/file4",'r')
        alllines =fptr.readlines()
        listats = split(alllines[0],',')
        for i in range(0,len(gc)):
            self.assertEqual(gc[i] , listats[i])


    def test_label_by_expression_residue_function(self):
        """tests label by expression at residue level,function
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.labelByExpression('1crn::', function='def foo(selection):\n	#values <- foo(node)\n	#values   : list of string used for labelling\n	#selection: current selection.\n	values = []\n	#loop on the current selection\n	for i in xrange(len(selection)):\n		#build a list of values to label the current selection.\n		if not hasattr(selection[i], "number"):\n			values.append("-")\n		else:\n			if selection[i].number > 20:\n				values.append(selection[i].number*2)\n			else:\n				values.append(selection[i].number)\n	# this list of values is then returned.\n	return values\n\n', lambdaFunc=0, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))

        #fptr = open("file5",'w')
        #for i in range(0,len(mv.Mols[0].chains.residues.number)):
	    #   if mv.getSelection()[i].number<=20:
		#      x = str(mv.getSelection()[i].number)+','
		
	    #   else:
		#       x = str(mv.getSelection()[i].number*2)+','
	    #    fptr.write(x)
		
 
        gc =self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].labels
        fptr = open("Data/file5",'r')
        alllines =fptr.readlines()
        listats = split(alllines[0],',')
        for i in range(0,len(gc)):
            self.assertEqual(gc[i] , listats[i])

    
    def test_label_by_expression_chain_function(self):
        """tests label by expression chain level,function
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        #self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        #self.mv.setSelectionLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByExpression('hsg1:', function='def foo(selection):\n	#values <- foo(node)\n	#values   : list of string used for labelling\n	#selection: current selection.\n	values = []\n	#loop on the current selection\n	for i in xrange(len(selection)):\n		#build a list of values to label the current selection.\n		if not hasattr(selection[i], "number"):\n			values.append("-")\n		else:\n			if selection[i].number > 20:\n				values.append(selection[i].number*2)\n			else:\n				values.append(selection[i].number)\n	# this list of values is then returned.\n	return values\n\n', lambdaFunc=0, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        gc =  self.mv.Mols[0].geomContainer.geoms['ChainLabels'].labels
        list = self.mv.getSelection().findType(Chain).number
        for i in range(0,len(gc)):
            self.assertEqual(str(list[i]) , gc[i])

    def test_label_by_expression_molecule_function(self):
        """tests label by expression molecule level,function
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        self.mv.labelByExpression('hsg1', function='def foo(selection):\n	#values <- foo(node)\n	#values   : list of string used for labelling\n	#selection: current selection.\n	values = []\n	#loop on the current selection\n	for i in xrange(len(selection)):\n		#build a list of values to label the current selection.\n		if not hasattr(selection[i], "number"):\n			values.append("-")\n		else:\n			if selection[i].number > 20:\n				values.append(selection[i].number*2)\n			else:\n				values.append(selection[i].number)\n	# this list of values is then returned.\n	return values\n\n', lambdaFunc=0, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        gc =self.mv.Mols[0].geomContainer.geoms['ProteinLabels'].labels
        mol = self.mv.Mols[0].number
        self.assertEqual(gc[0] , str(mol))

    
    def test_label__by_expression_choose_color(self):
        """tests label by expression,choose color
        """
        self.mv.readMolecule("Data/hsg1.pdb")
        self.mv.setIcomLevel(Chain, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("hsg1:", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        if self.mv.hasGui:
            color = (1.0,1.0,0.0)
            c.showForm('default',modal = 0, blocking = 0)
            c.configureButton(color)
            self.assertEqual(c.textColor,color)
            c.showForm('default',modal = 0, blocking = 0).withdraw()
    
#FONT

    def test_label_by_expression_font_1(self):
        """tests label by expression with font Helvetica12
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        import time
        from time import sleep
        sleep(2)
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')


    def test_label_by_expression_font_2(self):
        """tests label by expression with font 9by15
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_expression_font_3(self):
        """tests label by expression with font 8by13
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_expression_font_4(self):
        """tests label by expression with font TimesRoman10
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')    

    def test_label_by_expression_font_5(self):
        """tests label by expression with font TimesRoman24
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_expression_font_6(self):
        """tests label by expression with font Helvetica10
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

    def test_label_by_expression_font_7(self):
        """tests label by expression with font Helvetica18
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms['ResidueLabels'].font,'arial1.glf')

#LOCATION
    def test_label_by_expression_location_1(self):
        """tests label by expression with location Center
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Center', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['location'],'Center')
        

    def test_label_by_expression_location_2(self):
        """tests label by expression with location First
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='First', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['location'],'First')


    def test_label_by_expression_location_3(self):
        """tests label by expression with location Last
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='Last', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['location'],'Last')


#FORMAT
    
    def test_label_by_expression_format_1(self):
        """tests label by expression with format None
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format=None, only=0, location='First', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['format'],None)
        
        
    def test_label_by_expression_format_2(self):
        """tests label by expression with format %d
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format='%d', only=0, location='First', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['format'],'%d')

    def test_label_by_expression_format_3(self):
        """tests label by expression with format %f
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format='%f', only=0, location='First', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['format'],'%f')


    def test_label_by_expression_format_4(self):
        """tests label by expression with format %4.2f
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("ind::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format='%4.2f', only=0, location='First', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(c.getLastUsedValues()['format'],'%4.2f')    


    def test_label_by_expression_only_1(self):
        """tests label by expression when only =  1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        self.mv.select(self.mv.Mols[0].chains.residues[:3])
        c = self.mv.labelByExpression
        c(self.mv.getSelection(), function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format='%4.2f', only=1, location='First', negate=0, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),len(self.mv.getSelection()))


    def test_label_by_expression_negate_1(self):
        """tests label by expression when negate =  1
        """
        self.mv.readMolecule("Data/1crn.pdb")
        self.mv.setIcomLevel(Residue, KlassSet=None, log=0)
        c = self.mv.labelByExpression
        c("1crn::", function='\nlambda x: str(x.full_name())\n\n', lambdaFunc=1, font='arial1.glf', log=0, format='%4.2f', only=0, location='First', negate=1, textcolor=(1.0, 1.0, 1.0))
        self.assertEqual(len(mv.Mols[0].geomContainer.geoms['ResidueLabels'].vertexSet),0)






if __name__ == '__main__':
    unittest.main()        
