import unittest


class CreateMolViewerTest(unittest.TestCase):
    
    def test_createMolViewer(self):
        """test that we can create an instance of MoleculeViever, exit it and
        then create a second MV instance in the same Python interpreter."""
        # create original viewer 
        from Pmv.moleculeViewer import MoleculeViewer
        mv = MoleculeViewer(logMode = 'no',
                            withShell=False,
                            verbose=False, trapExceptions=False,
                            gui=1)
        # save names of loaded commands
        cmds = mv.commands.keys()
        #exit viewer
        mv.Exit(0)
        mv = None
        # create a new viewer 
        mv = MoleculeViewer(logMode = 'no',
                            withShell=False,
                            verbose=False, trapExceptions=False,
                            gui=1)
        assert mv is not None
        # verify all commands are loaded in new viewer
        ncmds = mv.commands.keys()
        assert len(cmds) == len(ncmds) 


if __name__ == '__main__':
    unittest.main( )
