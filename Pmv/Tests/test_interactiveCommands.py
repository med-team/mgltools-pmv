#
# $Id: test_interactiveCommands.py,v 1.38.6.1 2016/02/11 21:29:44 annao Exp $
#
################################################################################
#
#   Authors:Sowjanya KARNATI, Michel F. SANNER
#   Copyright: M. Sanner TSRI 2004
#
################################################################################ $Id

import sys,os,string
import math
import unittest,string
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
from Pmv.mvCommand import MVPrintNodeNames, MVCenterOnNodes
from Pmv.extruder import Sheet2D
from string import split
from DejaVu.Spheres import Spheres
import ViewerFramework
from DejaVu.IndexedPolylines import IndexedPolylines
from Pmv import selectionCommands

#from MolKit.mmcifParser import MMCIFParser
#from MolKit.mmcifWriter import MMCIFWriter
from mglutil.math.rotax import rotax

mv= None
klass = None
ct = 0
totalCt = 76

class InteractiveBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                 trapExceptions=0, withShell=0, verbose=False)
            #mv.setUserPreference(('trapExceptions', False), log = 0)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('deleteCommands',package='Pmv')
            mv.browseCommands('bondsCommands', package='Pmv')
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('fileCommands', package='Pmv')
            mv.browseCommands('displayCommands', package='Pmv')
            mv.browseCommands('editCommands', package='Pmv')
            mv.browseCommands("dejaVuCommands",commands=['centerGeom','alignGeomsnogui','alignGeoms'],package='ViewerFramework', topCommand = 0) 
            mv.addCommand( MVPrintNodeNames(), 'printNodeNames', None )
            mv.addCommand( MVCenterOnNodes(), 'centerOnNodes', None )
        self.mv = mv 
           
        

    def setUp(self):

        """
        clean-up
        """
        global klass, mv
        name = self.__class__.__name__
        if not klass:
            klass = name
        elif klass != name:
            print " !!!!!!!!!!!!!!!!!", klass, name
            print 'setup: destroying mv'
            if mv:
                mv.Exit(0)
                if hasattr(self, 'mv'):
                    del self.mv
                mv = None
            klass = name 
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    
    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv


####################################################
#       SET ICOM COMMAND TESTS                     #               
####################################################


class setICOM(InteractiveBaseTest):
    
    def test_set_ICOM_alignGeoms(self):
        """tests setICOM alignGeoms,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.readMolecule("Data/part2_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.alignGeoms, modifier=None, log=0)
        coords_old = self.mv.Mols[0].allAtoms.coords
        #call alignGeoms cmd
        self.mv.alignGeomsnogui('root|part1_hs|lines|bonded', [9], 'root|part2_hs|lines|bonded', [6], log=0)        
        coords_new = self.mv.Mols[0].allAtoms.coords
        self.assertEqual(coords_old,coords_new)
        
    def test_set_ICOM_buildBondsByDistance(self):
        """tests setICOM buildBondsByDistance ,modifier = None
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.removeBondsGC(self.mv.allAtoms.bonds[0])
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.buildBondsByDistance, modifier=None, log=0)
        self.mv.buildBondsByDistance("ind")
        self.assertEqual(len(mv.allAtoms.bonds),2)
        

    def test_set_ICOM_centeronNodes(self):
        """tests setICOM centeronNodes ,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.centerOnNodes, modifier=None, log=0)
        atO = mv.allAtoms.get(lambda x : x.name == 'O')
        self.mv.centerGeom('root',atO.coords[0],log=0) 
        self.mv.GUI.VIEWER.currentObject.Set(rotation=rotax( (1,1,0), (0,0,1), math.pi))
        self.mv.GUI.VIEWER.OneRedraw()
        import numpy
        self.assertTrue(numpy.alltrue(self.mv.GUI.VIEWER.currentObject.pivot == atO.coords))
        #self.assertEqual(self.mv.GUI.VIEWER.currentObject.pivot,atO.coords)
        
        
    
    def test_set_ICOM_centerSceneOnVertices(self):
        """tests setICOM centerSceneOnVertices,modifier = None
        """
        sph = Spheres("sp", centers = ((-4,0,0),),radii = (0.5,))#, protected=True)
        triangle=IndexedPolylines('mytriangle',vertices=[[1,0,0],[0,1,0],[2,1,0]],
                                   faces=((0,1),(1,2),(2,0),))#, protected=True)
        self.mv.GUI.VIEWER.AddObject(sph)
        self.mv.GUI.VIEWER.AddObject(triangle)
        self.mv.centerGeom('root', [1.0, 0.0, 0.0],log=0)
        self.mv.GUI.VIEWER.currentObject.Set(rotation=rotax( (0,0,0), (0,0,1), math.pi))
        self.mv.GUI.VIEWER.OneRedraw()
        self.assertEqual(str(mv.GUI.VIEWER.currentObject.pivot),'[ 1.  0.  0.]')
	self.mv.GUI.VIEWER.RemoveObject(sph)
	self.mv.GUI.VIEWER.RemoveObject(triangle)	        
        
        
    def test_set_ICOM_color_atoms_using_DG(self):
        """tests setICOM colorAtomsUsingDG,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorAtomsUsingDG, modifier=None, log=0)
        self.mv.colorAtomsUsingDG("part1_hs: :ASP43:,O", ['lines'], log=0)
        col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),self.mv.allAtoms.get(lambda x : x.name =='O').colors['lines'])
        self.assertEqual(col ,[(1.0,1.0,1.0)])


#    def test_set_ICOM_color_by_atom_type(self):
#        """tests setICOM colorByAtomType,modifier = None
#        """
#        self.mv.readMolecule("Data/part1_hs.pdb")
#        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
#        self.mv.setICOM(self.mv.colorByAtomType, modifier=None, log=0)
#        self.mv.colorByAtomType("part1_hs: :ASP43:,HB1", ['lines'], log=0)
#        col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),self.mv.allAtoms.get(lambda x : x.name =='HB1').colors['lines'])
#        self.assertEqual(col ,[(0.0,1.0,1.0)])
    
    def test_set_ICOM_color_by_chains(self):
        """tests setICOM colorBychains,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByChains, modifier=None, log=0)    
        self.mv.colorByChains("part1_hs: :ASP43:,OD2", ['lines'], log=0)
        col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),self.mv.allAtoms.get(lambda x : x.name =='OD2').colors['lines'])
        self.assertEqual(col ,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_molecules(self):
        """tests setICOM colorBymolecules,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByMolecules, modifier=None, log=0)
        self.mv.colorByMolecules("part1_hs: :ASP43:,HA", ['lines'], log=0)
        col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),self.mv.allAtoms.get(lambda x : x.name =='HA').colors['lines'])
        self.assertEqual(col ,[(0.0, 1.0, 1.0)])

    def test_set_ICOM_color_by_residue_type(self):
        """tests setICOM colorByresidues,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByResidueType, modifier=None, log=0)
        self.mv.colorByResidueType("part1_hs: :ASP43:,CA", ['lines'], log=0)
        col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),self.mv.allAtoms.get(lambda x : x.name =='CA').colors['lines'])
        self.assertEqual(col ,[(1.0, 0.0, 0.0)])


    def test_set_ICOM_color_residues_using_shapely(self):
        """tests setICOM color residues using shapely,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorResiduesUsingShapely, modifier=None, log=0)    
        self.mv.colorResiduesUsingShapely("part1_hs: :ASP43:,CB", ['lines'], log=0)
        col = map(lambda x: (round(x[0]), round(x[1]), round(x[2])),self.mv.allAtoms.get(lambda x : x.name =='CB').colors['lines'])
        self.assertEqual(col ,[(1.0, 0.0, 0.0)])

    def test_set_ICOM_deselect(self):
        """tests setICOM deselect,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        self.mv.select(self.mv.allAtoms)
        self.mv.setICOM(self.mv.deselect, modifier=None, log=0)
        self.mv.select("part1_hs:::", negate=1, only=False, log=0)
        self.assertEqual(len(self.mv.selection),0)


    def test_set_ICOM_displayCPK(self):
        """tests setICOM display CPK,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.setICOM(self.mv.displayCPK, modifier=None, log=0)
        self.mv.select(self.mv.allAtoms[:3])
        self.mv.displayCPK(self.mv.getSelection(), log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=10)
        self.assertEqual(self.mv.Mols[0].geomContainer.geoms.has_key('cpk'),True)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet),3)
    
    def test_set_ICOM_displayLines(self):
        """tests setICOM display lines,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)    
        self.mv.setICOM(self.mv.displayLines, modifier=None, log=0)
        self.mv.displayLines("part1_hs:::", negate=False, displayBO=False, only=False, log=0, lineWidth=2)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['bonded'].vertexSet),len(mv.allAtoms))


    def test_set_ICOM_displaySticksAndBalls(self):
        """tests setICOM displaySticksAndBalls,,modifier = None
        """
        self.mv.readMolecule("Data/part1_hs.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier=None, log=0)
        self.mv.displaySticksAndBalls("part1_hs:::", log=0, cquality=5, bquality=5, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet),len(mv.allAtoms))
        
    
#For One Atom molecule

    def test_set_ICOM_color_by_Atom_DG_do_pick(self):
        """tests setICOM colorAtomsUsingDG,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorAtomsUsingDG, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Atom_do_pick(self):
        """tests setICOM colorByAtomType,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByAtomType, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


        
    def test_set_ICOM_color_by_chains_do_pick(self):
        """tests setICOM colorByChains,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByChains, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_molecules_do_pick(self):
        """tests setICOM colorByMolecules,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByMolecules, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        self.assertEqual(c,[(1.0, 1.0, 1.0)])    


    def test_set_ICOM_color_by_Residue_type_do_pick(self):
        """tests setICOM colorByResidueType,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByResidueType, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Residue_using_shapely_do_pick(self):
        """tests setICOM colorResiduesUsingShapely,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorResiduesUsingShapely, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_display_sticks_and_balls_do_pick(self):
        """tests setICOM displaySticksAndBalls, modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet),1)

    
    def test_set_ICOM_display_cpk_do_pick(self):
        """tests setICOM displayCPK, modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displayCPK, modifier=None, log=0)
        mod = mv.GUI.VIEWER.getModifier()
        if mod is not None:
            mv.GUI.VIEWER.kbdModifier[mod]= 0 
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet),1)

    def test_set_ICOM_select(self):
        """tests setICOM select,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.assertEqual(len(self.mv.getSelection()),1)

    def test_set_ICOM_deselect1(self):
        """tests setICOM deselect, modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.select, modifier=None, log=0)
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.mv.setICOM(self.mv.deselect, modifier=None, log=0)
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.assertEqual(len(self.mv.selection),0)

    def test_set_ICOM_undisplay_cpk_do_pick(self):
        """tests setICOM undisplaycpk, modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displayCPK, modifier=None, log=0)
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.mv.setICOM(self.mv.undisplayCPK, modifier=None, log=0)
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['cpk'],[])

    def test_set_ICOM_undisplay_sticks_and_balls_do_pick(self):
        """tests setICOM undisplaySticksAndBalls, modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.mv.setICOM(self.mv.undisplaySticksAndBalls, modifier=None, log=0)
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls'].vertexSet),0)

    def test_set_ICOM_print_node_names_do_pick(self):
        """tests setICOM printNodeNames ,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printNodeNames ,modifier=None, log=0)
        mod = mv.GUI.VIEWER.getModifier()
        if mod is not None:
            mv.GUI.VIEWER.kbdModifier[mod]= 0 
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'one_atom: :SER54:HB1')
        
    def test_set_ICOM_print_geom_name(self):
        """tests setICOM printGeometryName,modifier=None
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printGeometryName,modifier=None, log=0)
        mod = mv.GUI.VIEWER.getModifier()
        if mod is not None:
            mv.GUI.VIEWER.kbdModifier[mod]= 0 
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.execCmd(ats)
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n\n')[0],'HB1')
    

#For One Atom molecule  shift_L

    def test_set_ICOM_color_by_Atom_DG_do_pick_shift_L(self):
        """tests set ICOM ,coloring atoms using Dg and modifier = Shift_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        #display as cpk
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorAtomsUsingDG, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Atom_do_pick_shift_L(self):
        """tests set ICOM ,coloring by atom type and modifier = Shift_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByAtomType, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])

        
    def test_set_ICOM_color_by_chains_do_pick_shift_L(self):
        """tests set ICOM ,coloring by chains and modifier = Shift_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByChains, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_molecules_do_pick_shift_L(self):
        """tests set ICOM ,coloring by molecule and modifier = Shift_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByMolecules, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])    


    def test_set_ICOM_color_by_Residue_type_do_pick_shift_L(self):
        """tests set ICOM ,coloring by residue type and modifier = Shift_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByResidueType, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Residue_using_shapely_do_pick_shift_L(self):
        """tests set ICOM ,coloring by residue type , modifier = Shift_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorResiduesUsingShapely, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_display_sticks_and_balls_do_pick_shift_L(self):
        """tests set ICOM ,displaySticksAndBalls , modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is diplayed as ball
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet),1)

    
    def test_set_ICOM_display_cpk_do_pick_shift_L(self):
        """tests set ICOM ,displayCPK , modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(100,100,600,600)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is diplayed as cpk
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet),1)

    def test_set_ICOM_select_shift_L(self):
        """tests set ICOM ,select when modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.select, modifier = 'Shift_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(100,100,300,300)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Shift_L
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is selected
        self.assertEqual(len(self.mv.getSelection()),1)

    def test_set_ICOM_deselect_shift_L(self):
        """tests set ICOM ,deselect when modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setICom select
        self.mv.setICOM(self.mv.select, modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setICom deselect
        self.mv.setICOM(self.mv.deselect, modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks length of selected ats is 0
        self.assertEqual(len(self.mv.selection),0)

    def test_set_ICOM_undisplay_cpk_do_pick_shift_L(self):
        """tests set ICOM ,undisplay cpk when modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setIcom to displayCPK
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setIcom to undisplayCPK
        self.mv.setICOM(self.mv.undisplayCPK, modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked ats undisplay cpk
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['cpk'],[])

    def test_set_ICOM_undisplay_sticks_and_balls_do_pick_shift_L(self):
        """tests set ICOM ,undisplay sticks and balls when modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setIcom to displaySticksAndBalls
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setIcom to undisplaySticksAndBalls
        self.mv.setICOM(self.mv.undisplaySticksAndBalls, modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked ats undisplaySticksAndBalls
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls'].vertexSet),0)

    def test_set_ICOM_print_node_names_do_pick_shift_L(self):
        """tests set ICOM ,printNodeNames when modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printNodeNames ,modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked node name is printed
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'one_atom: :SER54:HB1') 
        
    def test_set_ICOM_print_geom_name_shift_L(self):
        """tests set ICOM ,printgeomNames when modifier = Shift_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printGeometryName,modifier = 'Shift_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Shift_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked geom name is printed 
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry)[-1],'HB1')
       


#For One Atom molecule  Control_L

    def test_set_ICOM_color_by_Atom_DG_do_pick_control_L(self):
        """tests set ICOM ,coloring atoms using Dg and modifier = Control_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        #display as cpk
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorAtomsUsingDG, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Atom_do_pick_control_L(self):
        """tests set ICOM ,coloring by atom type and modifier = Control_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByAtomType, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])

        
    def test_set_ICOM_color_by_chains_do_pick_control_L(self):
        """tests set ICOM ,coloring by chains and modifier = Control_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByChains, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_molecules_do_pick_control_L(self):
        """tests set ICOM ,coloring by molecule and modifier = Control_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByMolecules, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])    


    def test_set_ICOM_color_by_Residue_type_do_pick_control_L(self):
        """tests set ICOM ,coloring by residue type and modifier = Control_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByResidueType, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Residue_using_shapely_do_pick_control_L(self):
        """tests set ICOM ,coloring by residue type , modifier = Control_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorResiduesUsingShapely, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_display_sticks_and_balls_do_pick_control_L(self):
        """tests set ICOM ,displaySticksAndBalls , modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is diplayed as ball
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet),1)

    
    def test_set_ICOM_display_cpk_do_pick_control_L(self):
        """tests set ICOM ,displayCPK , modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(100,100,600,600)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is diplayed as cpk
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet),1)

    def test_set_ICOM_select_control_L(self):
        """tests set ICOM ,select when modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.select, modifier = 'Control_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(100,100,300,300)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Control_L
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is selected
        self.assertEqual(len(self.mv.getSelection()),1)

    def test_set_ICOM_deselect_control_L(self):
        """tests set ICOM ,deselect when modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setICom select
        self.mv.setICOM(self.mv.select, modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setICom deselect
        self.mv.setICOM(self.mv.deselect, modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks length of selected ats is 0
        self.assertEqual(len(self.mv.selection),0)

    def test_set_ICOM_undisplay_cpk_do_pick_control_L(self):
        """tests set ICOM ,undisplay cpk when modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setIcom to displayCPK
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setIcom to undisplayCPK
        self.mv.setICOM(self.mv.undisplayCPK, modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked ats undisplay cpk
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['cpk'],[])

    def test_set_ICOM_undisplay_sticks_and_balls_do_pick_control_L(self):
        """tests set ICOM ,undisplay sticks and balls when modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setIcom to displaySticksAndBalls
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setIcom to undisplaySticksAndBalls
        self.mv.setICOM(self.mv.undisplaySticksAndBalls, modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked ats undisplaySticksAndBalls
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls'].vertexSet),0)

    def test_set_ICOM_print_node_names_do_pick_control_L(self):
        """tests set ICOM ,printNodeNames when modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printNodeNames ,modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked node name is printed
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'one_atom: :SER54:HB1') 
        
    def test_set_ICOM_print_geom_name_control_L(self):
        """tests set ICOM ,printgeomNames when modifier = Control_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printGeometryName,modifier = 'Control_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Control_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked geom name is printed 
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry)[-1],'HB1')
        
        

#For One Atom molecule  Alt_L

    def test_set_ICOM_color_by_Atom_DG_do_pick_alt_L(self):
        """tests set ICOM ,coloring atoms using Dg and modifier = Alt_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        #display as cpk
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorAtomsUsingDG, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Atom_do_pick_alt_L(self):
        """tests set ICOM ,coloring by atom type and modifier = Alt_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByAtomType, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])

        
    def test_set_ICOM_color_by_chains_do_pick_alt_L(self):
        """tests set ICOM ,coloring by chains and modifier = Alt_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByChains, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_molecules_do_pick_alt_L(self):
        """tests set ICOM ,coloring by molecule and modifier = Alt_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByMolecules, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])    


    def test_set_ICOM_color_by_Residue_type_do_pick_alt_L(self):
        """tests set ICOM ,coloring by residue type and modifier = Alt_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorByResidueType, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_color_by_Residue_using_shapely_do_pick_alt_L(self):
        """tests set ICOM ,coloring by residue type , modifier = Alt_L 
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.displayCPK("one_atom", log=0, cpkRad=7.0, scaleFactor=2.0, only=False, negate=False, quality=10)
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.colorResiduesUsingShapely, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        c =map(lambda x : (round(x[0]),round(x[1]),round(x[2])),self.mv.allAtoms.colors['cpk'])
        #checking picked atoms are colored
        self.assertEqual(c,[(1.0, 1.0, 1.0)])


    def test_set_ICOM_display_sticks_and_balls_do_pick_alt_L(self):
        """tests set ICOM ,displaySticksAndBalls , modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(200,200)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is diplayed as ball
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['balls'].vertexSet),1)

    
    def test_set_ICOM_display_cpk_do_pick_alt_L(self):
        """tests set ICOM ,displayCPK , modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(100,100,600,600)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is diplayed as cpk
        self.assertEqual(len(self.mv.Mols[0].geomContainer.geoms['cpk'].vertexSet),1)

    def test_set_ICOM_select_alt_L(self):
        """tests set ICOM ,select when modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.select, modifier = 'Alt_L', log=0)
        #picking using DoPick method
        p = self.mv.DoPick(100,100,300,300)
        #finding the picked atoms
        ats = self.mv.findPickedAtoms(p)
        #setting the modifier to Alt_L
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        #executing the cmd in setICOM with picked ats
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked atom is selected
        self.assertEqual(len(self.mv.getSelection()),1)

    def test_set_ICOM_deselect_alt_L(self):
        """tests set ICOM ,deselect when modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setICom select
        self.mv.setICOM(self.mv.select, modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setICom deselect
        self.mv.setICOM(self.mv.deselect, modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,300,300)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks length of selected ats is 0
        self.assertEqual(len(self.mv.selection),0)

    def test_set_ICOM_undisplay_cpk_do_pick_alt_L(self):
        """tests set ICOM ,undisplay cpk when modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setIcom to displayCPK
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setIcom to undisplayCPK
        self.mv.setICOM(self.mv.undisplayCPK, modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked ats undisplay cpk
        self.assertEqual(self.mv.Mols[0].geomContainer.atoms['cpk'],[])

    def test_set_ICOM_undisplay_sticks_and_balls_do_pick_alt_L(self):
        """tests set ICOM ,undisplay sticks and balls when modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        #setIcom to displaySticksAndBalls
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #setIcom to undisplaySticksAndBalls
        self.mv.setICOM(self.mv.undisplaySticksAndBalls, modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(200,200)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked ats undisplaySticksAndBalls
        self.assertEqual(len(self.mv.Mols[0].geomContainer.atoms['balls'].vertexSet),0)

    def test_set_ICOM_print_node_names_do_pick_alt_L(self):
        """tests set ICOM ,printNodeNames when modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printNodeNames ,modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked node name is printed
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'one_atom: :SER54:HB1') 
        
    def test_set_ICOM_print_geom_name_alt_L(self):
        """tests set ICOM ,printgeomNames when modifier = Alt_L
        """
        self.mv.readMolecule("Data/one_atom.pdb")
        self.mv.setIcomLevel(Atom, KlassSet=None, log=0)
        self.mv.setICOM(self.mv.printGeometryName,modifier = 'Alt_L', log=0)
        #picking using dopick method,finding picked ats ,executing the cmd in setICOM
        p = self.mv.DoPick(100,100,600,600)
        ats = self.mv.findPickedAtoms(p)
        self.mv.ICmdCaller.currentModifier = 'Alt_L'
        self.mv.ICmdCaller.execCmd(ats)
        #checks picked geom name is printed 
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry)[-1],'HB1')
        
    


#############################################
#       ICOM BAR COMMAND TESTS              #
#############################################
class IcomBar(InteractiveBaseTest):


    def test_ICOM_bar_No_Mod_Widget(self):
        """tests Icombar ,by setting PCOM to select
        """
        self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.ICOMbar
        c.NoMod_cb('select')
        self.assertEqual(self.mv.ICmdCaller.commands.value[None].__class__,
                         selectionCommands.MVSelectCommand)
        
        
    def test_ICOM_bar_Shift_Widget(self):
        """tests Icombar ,by setting Shift_L modifier values to select
        """
        self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.ICOMbar
        c.Shift_cb('select')
        self.assertEqual(self.mv.ICmdCaller.commands.value['Shift_L'].__class__,
                         selectionCommands.MVSelectCommand)
    
    def test_ICOM_bar_Ctrl_Widget(self):
        """tests Icombar ,by setting,Control_L modifier values to select
        """
        self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.ICOMbar
        c.Ctrl_cb('select')
        self.assertEqual(self.mv.ICmdCaller.commands.value['Control_L'].__class__,
                         selectionCommands.MVSelectCommand)    
    
    def test_ICOM_bar_Alt_Widget(self):
        """tests Icombar ,by setting,Alt_L modifier values to select
        """
        self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.ICOMbar
        c.Alt_cb('select')
        self.assertEqual(self.mv.ICmdCaller.commands.value['Alt_L'].__class__,
                         selectionCommands.MVSelectCommand)
        
##################################################################
#  Bind Cmd To Keys  Command Tests                               #
##################################################################

class Dummyevent:
    
    def __init__(self,keysym = 'None'):
        self.keysym =keysym
    


class BindCmdToKeys(InteractiveBaseTest,Dummyevent):
    
    def test_bind_cmd_to_keys_1(self):
        """tests bindCmdTokey cmd, binding selection command with escape
        key,when modifier = None
        """
        self.mv.readMolecule("Data/ind.pdb")
        c = self.mv.bindCmdToKey
        c('Escape','None',mv.select,(mv.allAtoms[:10],))
        myevent = Dummyevent(keysym = 'Escape')
        c.processRelease_cb(myevent)
        #gets last message printed in message box
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')
        self.assertEqual(split(last_entry,'\n')[0],'self.select("ind:::0-9", negate=False, only=False, xor=False, log=0, intersect=False)')
        #self.assertEqual(split(last_entry,'\n')[0],'self.select("ind:::0-9", negate=False, only=False, log=0)')
        
        
#    def test_bind_cmd_to_keys_2(self):
#        """tests bindCmdTokey cmd, binding printNodeNames command with F1 key ,when modifier = 'Shift_L'
#        """
#        self.mv.readMolecule("Data/ind.pdb")
#        self.mv.setICOM(self.mv.printNodeNames, modifier = 'Shift_L', log=0)
#        c = self.mv.bindCmdToKey
#        c('F1','Shift_L',mv.printNodeNames,(mv.allAtoms[:1],))
#        #setting modifier to Shift_L
#        mv.GUI.VIEWER.kbdModifier['Shift_L'] = 1 
#        myevent = Dummyevent(keysym = 'F1')
#        c.processRelease_cb(myevent)
#        #gets last message printed in message box
#        tx = mv.GUI.MESSAGE_BOX.tx
#        last_index = tx.index('end')
#        last_entry_index = str(float(last_index)-2.0)
#        last_entry = tx.get(last_entry_index, 'end')    
#        self.assertEqual(split(last_entry,'\n')[0],'self.bindCmdToKey(\'F1\', \'Shift_L\', self.printNodeNames, (<AtomSet instance> holding 1 Atom, "ind:::0",), {}, log=0)')
#        mv.GUI.VIEWER.kbdModifier['Shift_L'] = 0


    def test_bind_cmd_to_keys_3(self):
        """tests bindCmdTokey cmd, binding displayCPK command with 'a' key , ,when modifier = 'Control_L'
        """
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setICOM(self.mv.displayCPK, modifier = 'Control_L', log=0)
        c = self.mv.bindCmdToKey
        c('a','Control_L',mv.displayCPK,(mv.allAtoms[:5],))
        #setting modifier to Control_L
        self.mv.GUI.VIEWER.kbdModifier['Control_L'] = 1
        myevent = Dummyevent(keysym = 'a')
        c.processRelease_cb(myevent)
        #gets last message printed in message box
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.displayCPK("ind:::0-4", log=0, cpkRad=0.0, scaleFactor=1.0, only=False, negate=False, quality=0)')
        self.mv.GUI.VIEWER.kbdModifier['Control_L'] = 0
        

    def test_bind_cmd_to_keys_4(self):
        """tests bindCmdTokey cmd, binding displaySticksAndBalls with '6' key ,when modifier = 'Alt_L'
        """        
        self.mv.readMolecule("Data/ind.pdb")
        self.mv.setICOM(self.mv.displaySticksAndBalls, modifier = 'Alt_L', log=0)
        c = self.mv.bindCmdToKey
        c('6','Alt_L',mv.displaySticksAndBalls,(mv.allAtoms[:5],))
        #setting modifier to Alt_L
        self.mv.GUI.VIEWER.kbdModifier['Alt_L'] = 1
        myevent = Dummyevent(keysym = '6')
        c.processRelease_cb(myevent)
        #gets last message printed in message box
        tx = mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.displaySticksAndBalls("ind:::0-4", log=0, cquality=0, sticksBallsLicorice=\'Licorice\', bquality=0, cradius=0.2, only=False, bRad=0.3, negate=False, bScale=0.0)' )
        self.mv.GUI.VIEWER.kbdModifier['Alt_L'] = 0
    
    

    
    
if __name__ == '__main__': 
    unittest.main()
        


