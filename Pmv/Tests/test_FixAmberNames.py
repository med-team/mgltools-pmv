#
#
# $Id: test_FixAmberNames.py,v 1.6 2009/11/10 22:59:02 annao Exp $
#
#############################################################################
#                                                                           #
#   Author:Sowjanya Karnati                                                 #
#   Copyright: M. Sanner TSRI 2000                                          #
#                                                                           #
#############################################################################

import sys,os
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
ct = 0
totalCt = 13
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1


class AmberBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                    trapExceptions=False, withShell=0, gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('amberCommands', package='Pmv')
            #set up links to shared dictionary and current instance
            from Pmv.amberCommands import Amber94Config, CurrentAmber94
            self.Amber94Config = Amber94Config
            self.CurrentAmber94 = CurrentAmber94
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        #for key,value in self.Amber94Config.items():
        #    del self.Amber94Config[key]
        #    try:
        #        del value
        #    except:
        #        print "exception in deleting ", value
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None

        
######################################################################
#           FixAmberHNames COMMAND TESTS                             #
######################################################################

class FixAmberHNames(AmberBaseTest):
    
    
    def test_fix_amber_hnamesGC_empty_viewer(self):
        """checks FixAmberHNames command on empty viewer
        """
        c = self.mv.fixAmberHNamesGC
        returnValue = c("hsg1")
        self.assertEqual(returnValue,'ERROR')
    
    def test_fix_amber_hnames_HN(self):
        """checks FixAmberHNames,n-terminal PRO residues having HN atoms are named 
        H1,H2
        """
        #read molecule
        self.mv.readMolecule("Data/hsg1.pdb")
        residue_old =self.mv.Mols[0].chains.residues.get(lambda x: x.type =='PRO')
        atomsHN_old = residue_old.atoms.get(lambda x: x.name in ['HN1','HN2'])
        self.mv.fixAmberHNames("hsg1")
        residue_new =self.mv.Mols[0].chains.residues.get(lambda x: x.type =='PRO')
        atomsHN_new = residue_old.atoms.get(lambda x: x.name in ['HN1','HN2'])
        self.assertEqual(len(atomsHN_old), 4)
        self.assertEqual(len(atomsHN_new), 0)
        
    def test_fix_amber_hnames_invalid(self):
        """checks FixAmberHNames,invalid key entry
        """
        #read molecule
        self.mv.readMolecule("Data/hsg1.pdb")    
        c = self.mv.fixAmberHNames
        returnValue = c("abcd")
        self.assertEqual(returnValue,'ERROR')

    def test_fix_amber_hnames_empty(self):
        """checks FixAmberHNames,empty key entry
        """
        #read molecule
        self.mv.readMolecule("Data/hsg1.pdb")    
        c = self.mv.fixAmberHNames
        returnValue = c(" ")
        self.assertEqual(returnValue,'ERROR')

    
######################################################################
#           FixAmberHNamesGC COMMAND TESTS                           #
######################################################################


class FixAmberHNamesGC(AmberBaseTest):
    
    def test_fix_amber_hnamesGC_empty_viewer(self):
        """checks FixAmberHNamesGC command on empty viewer
        """
        c = self.mv.fixAmberHNamesGC
        returnValue = c("hsg1")
        self.assertEqual(returnValue,'ERROR')

    def test_fix_amber_hnamesGC_HN(self):
        """checks FixAmberHNamesGC,n-terminal PRO residues having HN atoms are named as
        H1,H2
        """
        #read molecule
        self.mv.readMolecule("Data/hsg1.pdb")
        residue_old =self.mv.Mols[0].chains.residues.get(lambda x: x.type =='PRO')
        atomsHN_old = residue_old.atoms.get(lambda x: x.name in ['HN1','HN2'])
        self.mv.fixAmberHNamesGC("hsg1")
        residue_new =self.mv.Mols[0].chains.residues.get(lambda x: x.type =='PRO')
        atomsHN_new = residue_old.atoms.get(lambda x: x.name in ['HN1','HN2'])
        self.assertEqual(len(atomsHN_old), 4)
        self.assertEqual(len(atomsHN_new), 0)
        

    def test_fix_amber_hnames_invalid(self):
        """checks FixAmberHNamesGC,invalid key entry
        """
        #read molecule
        self.mv.readMolecule("Data/hsg1.pdb")    
        c = self.mv.fixAmberHNamesGC
        returnValue = c("abcd")
        self.assertEqual(returnValue,'ERROR')

    def test_fix_amber_hnames_empty(self):
        """checks FixAmberHNamesGC,empty key entry
        """
        #read molecule
        self.mv.readMolecule("Data/hsg1.pdb")    
        c = self.mv.fixAmberHNamesGC
        returnValue = c(" ")
        self.assertEqual(returnValue,'ERROR')



###########################################################
#       FixAmberResNamesOrder COMMAND  TESTS              #
###########################################################


class FixAmberResNamesOrder(AmberBaseTest):
    
    def test_fix_amber_res_names_empty_viewer(self):
        """checks FixAmberResNamesOrder on empty viwer
        """
        c = self.mv.fixAmberResNamesOrder
        returnValue =c("1crn_hs:")
        self.assertEqual(returnValue,'ERROR')
    
    def test_fix_amber_res_names_order(self):
        """checks CYS residues with  HN atoms and changes residue name to CYM 
        """
        self.mv.readMolecule("Data/1crn_hs.pdb")
        res =self.mv.Mols[0].chains.residues.get(lambda x :x.type == 'CYS')
        self.mv.fixAmberResNamesOrder("1crn_hs:")
        res1 =self.mv.Mols[0].chains.residues.get(lambda x :x.amber_type == 'CYM')
        self.assertEqual(res.atoms.name,res1.atoms.name)
        

    def test_fix_amber_res_names_order_ats(self):
        """checks the order of atoms in ASN residue before and after calling
        the FixAmberResNamesOrder command
        """
        self.mv.readMolecule("Data/1crn.pdb")
        res =self.mv.Mols[0].chains.residues.get(lambda x :x.type == 'ASN')    
        c = self.mv.fixAmberResNamesOrder
        c("1crn:")
        self.assertEqual(c.atNames['ASN']!=res.atoms.name,True)
        self.assertEqual(res.atoms.name,['N', 'CA', 'CB', 'CG', 'OD1', 'ND2', 'C', 'O', 'N', 'CA', 'CB', 'CG', 'OD1', 'ND2', 'C', 'O', 'N', 'CA', 'CB', 'CG', 'OD1', 'ND2', 'C', 'O', 'OXT'])
        #atoms are reordered to confirm amberConventions
        self.assertEqual(c.atNames['ASN'],['N', 'H', 'H1', 'H2', 'H3', 'CA','HA', 'CB', 'HB2', 'HB3', 'CG', 'OD1', 'ND2', 'HD21', 'HD22', 'C','O', 'OXT'])
        

    def test_fix_amber_res_names_order_invalid(self):
        """checks FixAmberResNamesOrder with invalid nodes
        """
        self.mv.readMolecule("Data/1crn_hs.pdb")
        c = self.mv.fixAmberResNamesOrder
        returnValue = c("hdjf")
        self.assertEqual(returnValue,'ERROR')

    def test_fix_amber_res_names_order_empty(self):
        """checks FixAmberResNamesOrder with empty nodes 
        """
        self.mv.readMolecule("Data/1crn_hs.pdb")
        c = self.mv.fixAmberResNamesOrder
        returnValue = c(" ")
        self.assertEqual(returnValue,'ERROR')



if __name__ == '__main__':
    test_cases = [
        'FixAmberHNames',
        'FixAmberHNamesGC',
        'FixAmberResNamesOrder',
        ]
    
    unittest.main( argv=([__name__ ] + test_cases) )




#if __name__ == '__main__':
#    unittest.main()






        
