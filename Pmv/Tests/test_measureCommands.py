#
#
#  $Id: test_measureCommands.py,v 1.15.4.1 2016/02/11 21:29:44 annao Exp $
#
#

import unittest, glob, os, Pmv.Grid, string,sys
import time
from string import split,strip,find
from MolKit.molecule import Atom
from MolKit.protein import Residue
mv = None
ct = 0
totalCt = 39


class measure_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(trapExceptions=False, withShell=0)
            mv.browseCommands('measureCommands', package='Pmv')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.setUserPreference(('Number of Measure Distances','4'),log=0)
            mv.setUserPreference(('Number of Measure Angles','4'),log=0)
            mv.setUserPreference(('Measured Torsions','4'),log=0)
            #need access to error messages
        self.mv = mv
        #self.distCmdGC = self.mv.measureDistanceGC
        # measureDistanceGC (version with snake) is not used anymore
        self.angleCmdGC = self.mv.measureAngleGC
        self.torCmdGC = self.mv.measureTorsionGC
        self.distCmd = self.mv.measureDistance
        self.angleCmd = self.mv.measureAngle
        self.torCmd = self.mv.measureTorsion


    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()


    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt
        self.mv.setICOM(self.mv.printNodeNames, modifier=None)

        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)

        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv       
        #mol=mv.readMolecule('hsg1.pdb')[0]


class measure_measureTests(measure_BaseTests):

    def xtest_measure_distanceGC_value(self):
        """checks one  distanceGC
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.distCmdGC, modifier=None)
        self.distCmdGC("ind:I:IND201:N1")
        self.distCmdGC("ind:I:IND201:C1")
        self.assertEqual(round(self.distCmdGC("ind:I:IND201:N1,C1"),2),1.49)

    
    def xtest_measure_distance_spheres_visible(self):
        """checks one distance geom visible 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.distCmdGC, modifier=None)
        self.distCmdGC("ind:I:IND201:N1")
        self.distCmdGC("ind:I:IND201:C1")
        self.assertEqual(self.distCmdGC.spheres.visible,1)


    def xtest_measure_distance_spheres_vertices(self):
        """checks one distance geom correct 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.distCmdGC, modifier=None)
        self.distCmdGC("ind:I:IND201:N1")
        ats=self.mv.allAtoms[0]._coords
        self.assertEqual(round(self.distCmdGC.spheres.vertexSet.vertices.array[0][0]),round(ats[0][0]))
        self.assertEqual(round(self.distCmdGC.spheres.vertexSet.vertices.array[0][1]),round(ats[0][1]))
        self.assertEqual(round(self.distCmdGC.spheres.vertexSet.vertices.array[0][2]),round(ats[0][2]))

            
    def xtest_measure_distance_spheres_vertexset(self):
        """checks distance geom updated
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureDistanceGC, modifier=None)
        self.distCmdGC("ind:I:IND201:N1,C1,C2,C3,O1")
        oldvertices=self.distCmdGC.lines.vertexSet.vertices.array[0]
        self.distCmdGC("ind:I:IND201:N2")
        newvertices=self.distCmdGC.lines.vertexSet.vertices.array[0]
        self.assertEqual(oldvertices[0]!=newvertices[0],True)
        self.assertEqual(oldvertices[1]!=newvertices[1],True)
        self.assertEqual(oldvertices[2]!=newvertices[2],True)


    def xtest_measure_distance_empty_atom_entry_in_select_string(self):
        """distanceGC input empty string raises ERROR
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None)
        ats=self.mv.selectFromString(log=0, res='', mols='', atoms=' ',negate=0, chains='')
        self.mv.setICOM(self.distCmdGC, modifier=None)
        returnvalue=self.distCmdGC(ats)
        self.assertEqual(returnvalue,"ERROR") 


    def xtest_measure_distance_spheres_number_of_distances(self):
        """verify four distances from  five input nodes
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.distCmdGC, modifier=None)
        self.distCmdGC("ind:I:IND201:N1,C1,C2,C3,O1")
        self.assertEqual(len(self.distCmdGC.lines.vertexSet.vertices.array),self.distCmdGC.snakeLength+1)


    def xtest_measure_distance_spheres_number_of_distances_6atoms(self):
        """verify four distances from  > five input nodes
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.distCmdGC, modifier=None)
        self.distCmdGC("ind:I:IND201:N1,C1,C2,C3,O1,N3")
        self.assertEqual(len(self.distCmdGC.lines.vertexSet.vertices.array),self.distCmdGC.snakeLength+1)
    
    def xtest_measure_distanceGC_spheres_number_of_distances_select_string(self):
        """verify four distances from five input nodes string input 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None)
        ats=self.mv.selectFromString(log=0, res='', mols='', atoms='N1,C1,C2,C3,O1', negate=0, chains='')
        self.mv.setICOM(self.distCmdGC, modifier=None)
        self.distCmdGC(ats)
        self.assertEqual(len(self.distCmdGC.lines.vertexSet.vertices.array),self.distCmdGC.snakeLength+1)
        
    
    def xtest_measure_distanceGC_spheres_invalid_atom_entry(self):
        """distanceGC input invalid nodes raises ERROR
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.distCmdGC, modifier=None)
        returnValue=self.distCmdGC("ind:I:IND201:N132")   
        self.assertEqual(returnValue,"ERROR")


    def test_measure_angleGC_spheres_value(self):
        """checks one angleGC

        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.assertEqual(round(self.angleCmdGC("ind:I:IND201:H40,C28,C29")),119.0)


    def test_measure_angle_spheres_visible(self):
        """checks angle geom visible
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.angleCmdGC("ind:I:IND201:H40")
        self.assertEqual(self.angleCmdGC.spheres.visible,1)

    
    def test_measure_angle_spheres_vertices(self):
        """checks angle geom correct 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.angleCmdGC("ind:I:IND201:N1")
        ats=mv.allAtoms[0]._coords
        self.assertEqual(round(self.angleCmdGC.spheres.vertexSet.vertices.array[0][0]),round(ats[0][0]))
        self.assertEqual(round(self.angleCmdGC.spheres.vertexSet.vertices.array[0][1]),round(ats[0][1]))
        self.assertEqual(round(self.angleCmdGC.spheres.vertexSet.vertices.array[0][2]),round(ats[0][2]))

    
    def test_measure_angle_spheres_vertexset(self):
        """checks angle geom updated
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.angleCmdGC("ind:I:IND201:H39,H40,H41,C22,H34,H44,C35,C36,H47,C19,C20,H31")
        oldvertices=self.angleCmdGC.lines.vertexSet.vertices.array[3]
        self.angleCmdGC("ind:I:IND201:H1,N1,C2")
        newvertices=self.angleCmdGC.lines.vertexSet.vertices.array[0]
        self.mv.GUI.VIEWER.Redraw()
        self.assertEqual(oldvertices[0],newvertices[0])
        self.assertEqual(oldvertices[1],newvertices[1])
        self.assertEqual(oldvertices[2],newvertices[2])
        
    def test_measure_angle_spheres_number_of_angles(self):
        """verify 4 angles from 12 nodes
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.angleCmdGC("ind:I:IND201:H39,H40,H41,C22,H34,H44,C35,C36,H47,C19,C20,H31")
        self.assertEqual(len(self.angleCmdGC.lines.vertexSet.vertices.array)/3,self.angleCmdGC.snakeLength)
      
    
    def test_measure_angle_spheres_number_of_angles_select_string(self):
        """ verify 4 angles from 12 nodes string input 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None)
        ats=self.mv.selectFromString(log=0, res='', mols='', atoms='H39,H40,H41,C22,H34,H44,C35,C36,H47,C19,C20,H31',negate=0, chains='')
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.angleCmdGC(ats)
        self.assertEqual(len(self.angleCmdGC.lines.vertexSet.vertices.array)/3,self.angleCmdGC.snakeLength)
           
    
    def test_measure_angle_spheres_number_of_angles_select_string_13atoms(self):
        """verify 4 angles from >12 nodes string input
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        self.angleCmdGC("ind:I:IND201:H39,H40,H41,C22,H34,H44,C35,C36,H47,C19,C20,H31,H1")
        self.assertEqual(len(self.angleCmdGC.lines.vertexSet.vertices.array)/3,self.angleCmdGC.snakeLength-1)
       
    
    def test_measure_anglesGC_empty_atom_entry_in_select_string(self):
        """angleGC input empty string raises ERROR
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None)
        ats=self.mv.selectFromString(log=0, res='', mols='', atoms=' ',negate=0, chains='')
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        returnvalue=self.angleCmdGC(ats)
        self.assertEqual(returnvalue,"ERROR") 
        
 
    def test_measure_angleGC_spheres_invalid_atom_entry(self):
        """angleGC input invalid string raises ERROR
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.angleCmdGC, modifier=None)
        returnValue=self.angleCmdGC("ind:I:IND201:N132")   
        self.assertEqual(returnValue,"ERROR")


    def test_measure_torsionGC_value(self):
        """checks  one torsionGc 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        c=self.torCmdGC("ind:I:IND201:N1,C1,C2,C3")
        self.assertEqual(round(c),-178)


    def test_measure_torsion_spheres_visible(self):
        """checks torsion  geom  visible 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        self.torCmdGC("ind:I:IND201:N1")
        self.torCmdGC("ind:I:IND201:C1")
        self.assertEqual(self.torCmdGC.spheres.visible,1)


    def test_measure_torsion_spheres_vertices(self):
        """checks torsion geom correct
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        self.torCmdGC("ind:I:IND201:N1")
        ats=mv.allAtoms[0]._coords
        self.assertEqual(round(self.torCmdGC.spheres.vertexSet.vertices.array[0][0]),round(ats[0][0]))
        self.assertEqual(round(self.torCmdGC.spheres.vertexSet.vertices.array[0][1]),round(ats[0][1]))
        self.assertEqual(round(self.torCmdGC.spheres.vertexSet.vertices.array[0][2]),round(ats[0][2]))

            
    def test_measure_torsion_spheres_vertexset(self):
        """checks torsion geom updated
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        self.torCmdGC("ind:I:IND201:N1,C1,C2,C3,O1,N2,C4,C5,C6,C7,N3,C8,C9,C10,C11,O2", log=0)
        oldvertices=self.torCmdGC.lines.vertexSet.vertices.array[0]
        self.torCmdGC("ind:I:IND201:H38,C26,C27,H39")
        newvertices=self.torCmdGC.lines.vertexSet.vertices.array[0]
        self.assertEqual(oldvertices[0]!=newvertices[0],True)
        self.assertEqual(oldvertices[1]!=newvertices[1],True)
        self.assertEqual(oldvertices[2]!=newvertices[2],True)


    def test_measure_torsion_spheres_number_of_torsions(self):
        """checks four torsions from 16 nodes  
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        self.torCmdGC("ind:I:IND201:H8,H13,H10,H9,H45,C34,H46,C35,H44,C33,C32,C31,H41,H33,C30,C29")
        self.assertEqual(len(self.torCmdGC.lines.vertexSet.vertices.array),self.torCmdGC.snakeLength*4)

    
    def test_measure_torsion_spheres_number_of_torsions_17atoms(self):
        """checks four torsions from >16 nodes input
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        self.torCmdGC("ind:I:IND201:H8,H13,H10,H9,H45,C34,H46,C35,H44,C33,C32,C31,H41,H33,C30,C29,C1")
        self.assertEqual(len(self.torCmdGC.spheres.vertexSet.vertices.array), 1)
        # since snakeLength is 4 we loose the first torsion when we add atom 17
        self.assertEqual(len(self.torCmdGC.lines.faceSet.faces.array), 3)

    
    def test_measure_torsion_spheres_number_of_torsions_select_string(self):
        """verify four torsions from 16 string input
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None)
        ats=self.mv.selectFromString(log=0, res='', mols='', atoms='H8,H13,H10,H9,H45,C34,H46,C35,H44,C33,C32,C31,H41,H33,C30,C29',negate=0, chains='')
        self.mv.setICOM(self.torCmdGC, modifier=None)
        self.torCmdGC(ats)
        self.assertEqual(len(self.torCmdGC.lines.vertexSet.vertices.array)/4,self.torCmdGC.snakeLength)
        

    def test_measure_torsionGC_spheres_invalid_atom(self):
        """torsionGC input invalid nodes raises ERROR  
        """
        self.mv.readMolecule('id.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.torCmdGC, modifier=None)
        returnValue=self.torCmdGC("ind:I:IND201:N132")   
        self.assertEqual(returnValue,"ERROR")   
        
    def test_measure_torsionGC_spheres_empty_atom_entry_in_select_string(self):
        """torsionGC input empty string raises ERROR
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setSelectionLevel(Atom, KlassSet=None)
        ats=self.mv.selectFromString(log=0, res='', mols='', atoms=' ',negate=0, chains='')
        self.mv.setICOM(self.torCmdGC, modifier=None)
        returnvalue=self.torCmdGC(ats)
        self.assertEqual(returnvalue,"ERROR") 
       
        
                
    def test_measure_distance_value(self):
        """checks one distance 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.assertEqual(round(self.distCmd("ind:I:IND201:N1","ind:I:IND201:C1"),2),1.49)


    def test_measure_angle_spheres_value(self):
        """checks one angle 

        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.assertEqual(round(self.angleCmd("ind:I:IND201:H40","ind:I:IND201:C28","ind:I:IND201:C29")),119.0)
   
    
    def test_measure_torsion_value(self):
        """checks one torsion 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        c=self.torCmd("ind:I:IND201:N1","ind:I:IND201:C1","ind:I:IND201:C2","ind:I:IND201:C3")
        self.assertEqual(round(c),-178)
    
    def test_measure_distance_spheres_invalid_atom_entry(self):
        """distance input invalid node raises ERROR 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        returnValue=self.distCmd("ind:I:IND201:N132","ind:I:IND201:C1")   
        self.assertEqual(returnValue,"ERROR")
    
            
    def test_measure_angle_spheres_invalid_atom_entry(self):
        """ angle input invalid node raises ERROR 
        """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        returnValue=self.angleCmd("ind:I:IND201:N132","ind:I:IND201:C1","ind:I:IND201:H1")   
        self.assertEqual(returnValue,"ERROR")

    def test_measure_torsion_spheres_invalid_atom(self):
        """torsion input invalid node raises ERROR 
        """
        self.mv.readMolecule('id.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        returnValue=self.torCmd("ind:I:IND201:N132","ind:I:IND201:C1","ind:I:IND201:H1","ind:I:IND201:C1")   
        self.assertEqual(returnValue,"ERROR")   
        
    
###Log Tests###

class MeasureLogTests(measure_BaseTests):
    
    def xtest_measure_distanceGC_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureDistanceGC, modifier=None)
        self.mv.measureDistanceGC("ind:I:IND201:N1")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.measureDistanceGC("ind:I:IND201:N1", log=0)')

    def xtest_measure_distanceGC_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureDistanceGC, modifier=None)
        oldself=self
        self = mv
        s = 'self.measureDistanceGC("ind:I:IND201:N1", log=0)'
        exec(s)
        oldself.assertEqual(1,1)
    
        
    def test_measure_angleGC_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureAngleGC, modifier=None)
        self.mv.measureAngleGC("ind:I:IND201:H40,C28,C29")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.measureAngleGC("ind:I:IND201:H40,C28,C29", log=0)')

        
    
    def test_measure_angleGC_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureAngleGC, modifier=None)
        oldself=self
        self = mv
        s = 'self.measureAngleGC("ind:I:IND201:H40,C28,C29", log=0)'
        exec(s)
        oldself.assertEqual(round(self.measureAngleGC("ind:I:IND201:H40,C28,C29")),119.0)
        
        

    def test_measure_torsion_spheres_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureTorsionGC, modifier=None)
        self.mv.measureTorsionGC("ind:I:IND201:N1")
        tx = self.mv.GUI.MESSAGE_BOX.tx
        last_index = tx.index('end')
        last_entry_index = str(float(last_index)-2.0)
        last_entry = tx.get(last_entry_index, 'end')    
        self.assertEqual(split(last_entry,'\n')[0],'self.measureTorsionGC("ind:I:IND201:N1", log=0)')


    def test_measure_torsion_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule('Data/ind.pdb')
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.setICOM(self.mv.measureTorsionGC, modifier=None)
        oldself=self
        self = mv
        s = 'self.measureTorsionGC("ind:I:IND201:N1", log=0)'
        exec(s)
        oldself.assertEqual(1,1)


    
if __name__ == '__main__':
    unittest.main()



        
        
