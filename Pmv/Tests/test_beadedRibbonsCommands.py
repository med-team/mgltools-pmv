import unittest,sys,os,time
import stat
mv = None
ct=0
totalCt=3

class beadedRibbonsTest(unittest.TestCase):
    """Base class for beadedRibbonsTest unittest"""
    def startViewer(self):
        """start Viewer"""

        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                withShell=0, trapExceptions=False)
            #mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.setUserPreference(('trapExceptions', '0'), log = 0)
            mv.loadModule('dejaVuCommands', 'ViewerFramework')
            mv.browseCommands("fileCommands",commands=['readMolecule',],package= 'Pmv')
            mv.browseCommands("colorCommands",package= 'Pmv')
            mv.browseCommands("bondsCommands", commands=["buildBondsByDistance",],package= "Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'], log=0)
            mv.browseCommands('displayCommands',package='Pmv')
            mv.browseCommands('beadedRibbonsCommands',package='Pmv')
            
            self.mv = mv 

    def setUp(self):
        """set-up"""
        global mv
        if mv is None:
        #if not hasattr(self, 'mv'):
            self.startViewer()
        else:
           self.mv = mv
        mv.undo = mv.NEWundo

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt
        ct = ct + 1
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv

    def test_01(self):
        """ Tests 1crn"""
        mol = mv.readMolecule('Data/1crn.pdb')
        mv.beadedRibbons("1crn")
        self.failUnless(hasattr(mol, 'beadedRibbonParams'))

    def test_02(self):
        """ Tests ind.pdb"""
        mol = mv.readMolecule('Data/ind.pdb')
        mv.beadedRibbons("ind")
        self.failUnless(hasattr(mol, 'beadedRibbonParams'))

    def test_03(self):
        """test display/undisplay beaded ribbons"""
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        mol = mv.readMolecule('Data/1crn.pdb')
        obj = mv.GUI.VIEWER.FindObjectByName('root|1crn|beadedRibbon')
        # make sure the ribbon has not been built
        self.assertEqual(obj, None)
        self.assertEqual(hasattr(mol, 'beadedRibbonParams'), False)

        #display beaded ribbon
        mv.displayBeadedRibbons("1crn", negate=False, only=False, log=0)
        obj = mv.GUI.VIEWER.FindObjectByName('root|1crn|beadedRibbon')
        self.assertTrue(obj != None)
        self.failUnless(hasattr(mol, 'beadedRibbonParams'))
        

        #undisplay
        mv.displayBeadedRibbons("1crn", negate=True, only=False, log=0)
        self.assertEqual(obj.visible, 0)

        #undo
        mv.undo()
        self.assertEqual(obj.visible, 1)
