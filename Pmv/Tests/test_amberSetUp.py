#
#
# $Id: test_amberSetUp.py,v 1.10 2010/11/01 21:27:02 annao Exp $
#
#############################################################################
#                                                                           #
#   Author:Sowjanya Karnati                                                 #
#   Copyright: M. Sanner TSRI 2000                                          #
#                                                                           #
#############################################################################

import sys,os
import unittest
import string,Pmv
from string import split
from opengltk.OpenGL import GL
from MolKit.molecule import Atom, AtomSet, Bond, BondSet
from MolKit.protein import Chain,Residue,Molecule
mv = None
ct = 0
totalCt = 36
try:
    from Pmv.Tests import hasGUI
except:
    hasGUI=1

class AmberBaseTest(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    def startViewer(self):
        """
        start Viewer
        """
        global mv
        if mv is None:
            from Pmv.moleculeViewer import MoleculeViewer
            from MolKit import Read
            import Tkinter
            mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                                 trapExceptions=False, withShell=0,
                                gui=hasGUI)
                                 #withShell=0, verbose=False)
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            mv.browseCommands('fileCommands', commands=['readMolecule',],
                               package='Pmv')
            mv.browseCommands('deleteCommands',commands=['deleteMol',],
                               package='Pmv')
            mv.browseCommands("bondsCommands",
                               commands=["buildBondsByDistance",],
                               package="Pmv")
            mv.setOnAddObjectCommands(['buildBondsByDistance','displayLines'])
            mv.browseCommands("interactiveCommands", package='Pmv')
            mv.browseCommands("colorCommands", package='Pmv')
            mv.browseCommands("selectionCommands", package='Pmv')
            mv.browseCommands('amberCommands', package='Pmv')
            #set up links to shared dictionary and current instance
            from Pmv.amberCommands import Amber94Config, CurrentAmber94
            self.Amber94Config = Amber94Config
            self.CurrentAmber94 = CurrentAmber94
        self.mv = mv 

    def setUp(self):
        """
        clean-up
        """
        
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
    

    def tearDown(self):
        """
        clean-up
        """
        global ct, totalCt, mv
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        #for key,value in self.Amber94Config.items():
        #    del self.Amber94Config[key]
        #    try:
        #        del value
        #    except:
        #        print "exception in deleting ", value
        ct = ct + 1
        if ct==totalCt:
            if self.mv.hasGui:
                print 'destroying mv'
                self.mv.Exit(0)
            del self.mv
            mv = None


##############################################
#   SetUpAmber94 COMMAND TESTS               #
##############################################


class SetUpAmber94(AmberBaseTest):
            
    def test_set_up_amber94_default_arguments(self):        
        """
        Test the default arguments
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        c("trp3_h:", 'test1')
        self.assertEqual(hasattr(c,'CurrentAmber94'),True)
        self.assertEqual(c.CurrentAmber94.atoms, self.mv.Mols[0].allAtoms)
        self.assertEqual(c.CurrentAmber94.key,'test1')

    
    def test_set_up_amber94_empty_viewer(self):
        """
        Test if the SetUpAmber94 behaves properly
        when no molecule has been loaded in the viewer and with
        the default values
        """
        c = self.mv.setup_Amber94
        returnValue = c("trp3_h:", 'test2')
        self.assertEqual(returnValue,'ERROR')
        

    def test_setup_amber94_fileName(self):
        """
        Test specifying filename 
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        c("trp3_h:", 'test3', 'Data/trp3_h.prmtop', log=0)
        self.assertEqual(c.getLastUsedValues()['filename'],'Data/trp3_h.prmtop')


    def test_setup_amber94_dataDict(self):
        """
        Test specifying dataDict 
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        c("trp3_h:", 'test4', None, dataDict={'allDictList': ['all_amino94_dat'], 'ntDictList': ['all_aminont94_dat'], 'ctDictList': ['all_aminoct94_dat']}, log=0)
        self.assertEqual(c.getLastUsedValues()['dataDict'],{'allDictList':['all_amino94_dat'], 'ntDictList': ['all_aminont94_dat'],'ctDictList': ['all_aminoct94_dat']})       
#end checking values to arguments


#invalid values to arguments
    def test_setup_amber94_invalid_nodes(self):
        """
        Test specifying invalid molecule
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        returnValue = c("hai", 'test5')
        self.assertEqual(returnValue,'ERROR')

        
    def test_setup_amber94_keystr_invalid(self):
        """
        Test specifying invalid id
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        returnValue = c("trp3_h:",' ',None)    
        self.assertEqual(returnValue,None)
        
        
    def setup_amber94_file_name_invalid(self):
        """
        FIX THIS: not correct syntax!
        Test specifying invalid filename
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        self.assertRaises(AssertionError, c,('trp3_h:','test7','hai'))
            
        
    def test_setup_amber94_dataDict_invalid(self):
        """
        Test specifying invalid data_dictionary
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        c = self.mv.setup_Amber94
        returnValue =c("trp3_h:",'test8','Data/trp3_h.prmtop',dataDict = 7)
        self.assertEqual(returnValue,None)


#end invalid input for setUpAmber94

######################################################
#   SetMinimOptsAmber94 COMMAND TESTS                #
######################################################
 
class SetMinimOptsAmber94(AmberBaseTest):

    def test_set_minim_opt_amber94_widget_check(self):
        """checks minim_opt form is displayed
        FIX THIS
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        #self.mv.setup_Amber94("trp3_h:",'mintest1','Data/trp3_h.prmtop')
        #c = self.mv.setminimOpts_Amber94
        #c('mintest1')
        #c.buildForm()
        ##but = c.ifd.entryByName['dield_cb']['widget']
        ##but.wait_visibility(but)
        #self.assertEqual(c.ifd.form.root.winfo_ismapped(),1)
        #c.Accept_cb()

    def test_set_minim_opt_amberId_button(self):
        """checks minim_opt form has right label in entry
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        self.mv.setup_Amber94("trp3_h:",'mintest2','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        st=c('mintest2')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            amber94But = c.ifd.entryByName['amberIds']['widget']
            amber94But.setentry('mintest2')
            self.assertEqual(amber94But.get(),'mintest2')
            c.Accept_cb()
        else:
            apply(c, ('mintest2',), {'ntpr': 50, 'nsnb': 25,
                                    'cut': 8.0, 'verbosemm': 1,
                                    'dield': 1})

        
    def test_set_minim_opt_amber94_print_summary(self):
        """checks minim_opt verbose_cb entry has correct text
        """
        self.mv.readMolecule("Data/trp3_h.pdb")
        self.mv.setup_Amber94("trp3_h:",'mintest3','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        st=c('mintest3')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            x = c.ifd.entryByName['verbose_cb']['widget']
            x.setentry('no print out')
            self.assertEqual(x.get(),'no print out')
            c.Accept_cb()
        else:
            apply(c, ('mintest3',), {'ntpr': 50, 'nsnb': 25, 'cut': 8.0,
                                    'verbosemm': 0, 'dield': 1})
    
    def test_set_minim_opt_amber94_dield(self):
        """checks toggle dield widget in setminimopts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest4','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        #call setminimopts command
        st=c('mintest4')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #toggle on  dield 
            c.ifd.entryByName['dield_cb']['widget'].select()
            self.assertEqual(c.dield.get(),1)
            c.Accept_cb()
        else:
            apply(c, ('mintest4',), {'ntpr': 50, 'nsnb': 25, 'cut': 8.0,
                                     'verbosemm': 1, 'dield': 1})
    
    def test_set_minim_opt_amber94_cut_tw_thumb_wheel(self):
        """checks cut_tw -cut off distance non bonded interactions in setminimopts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest5','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        #call setminimopts command
        st=c('mintest5')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting cut_tw to 400.0
            c.ifd.entryByName['cut_tw']['widget'].set(400)
            self.assertEqual(c.ifd.entryByName['cut_tw']['widget'].get(),400.0)
            c.Accept_cb()
        else:
            apply(c, ('mintest5',), {'ntpr': 50, 'nsnb': 25, 'cut': 400.0,
                                     'verbosemm': 1, 'dield': 1})
    
    def test_set_minim_opt_amber94_nsnb_tw_thumb_wheel(self):
        """checks nsnb_tw number of steps between nonbonded pair list updates
        in setminimopts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest6','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        #call setminimopts command
        st=c('mintest6')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting nsnb_tw to 40.0
            c.ifd.entryByName['nsnb_tw']['widget'].set(40.0)    
            self.assertEqual(c.ifd.entryByName['nsnb_tw']['widget'].get(),40.0)
            c.Accept_cb()
        else:
            apply(c, ('mintest6',), {'ntpr': 50, 'nsnb': 40,
                                    'cut': 8.0, 'verbosemm': 1, 'dield': 1.})
    

    def test_set_minim_opt_amber94_ntpr_tw_thumb_wheel(self):
        """checks setting  ntpr_tw -numberof steps between print
        out of energy information in setminimopts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest7','Data/trp3_h.prmtop')
        #call setminimopts command
        c = self.mv.setminimOpts_Amber94
        st=c('mintest7')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting ntpr_tw to 60.0
            c.ifd.entryByName['ntpr_tw']['widget'].set(60.0)    
            self.assertEqual(c.ifd.entryByName['ntpr_tw']['widget'].get(),60.0)    
            c.Accept_cb()
        else:
            apply(c,('mintest7',), {'ntpr': 60, 'nsnb': 25,
                                    'cut': 8.0, 'verbosemm': 1, 'dield': 1})
        

#end widget entries check    
           

    def test_set_minim_opt_amber94_normal(self):
        """
        tests setminimopts command 
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest8','Data/trp3_h.prmtop')
        #call setminimopts command
        c = self.mv.setminimOpts_Amber94
        c('mintest8')
        self.assertEqual(hasattr(c,'CurrentAmber94'),True)


    def test_set_minim_opt_amber94_invalid_key(self):
        """
        tests setminimopts command  with invalid key
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        self.mv.setup_Amber94("trp3_h:",'mintest9','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        error = 'ERROR'
        self.assertEqual(c('gjsd'), error) 
        

        
#########################################################
#   setmdOpts_Amber94 COMMAND TESTS                     #
#########################################################


class SetMDOpts(AmberBaseTest):
    
    def test_set_md_opts_widget(self):
        """checks setmdpts widget is displayed
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest1','Data/trp3_h.prmtop')
        #call setmdopts command
        c = self.mv.setmdOpts_Amber94
        st = c('mdtest1')
        if self.mv.hasGui:
            c.buildForm()
            self.assertEqual(c.ifd.form.root.winfo_exists(),1)
            c.Accept_cb()
        self.assertEqual(st, None)
        
    def test_set_md_opts_amber_ids(self):
        """checks setting amberids in setmdpts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest2','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest2')
        if self.mv.hasGui:
            c.buildForm()
            #amberids button
            AmberIdBut = c.ifd.entryByName['amberIds']['widget']
            AmberIdBut.setentry('mdtest2')
            self.assertEqual(AmberIdBut.get(),'mdtest2')
            c.Accept_cb()
        self.assertEqual(st, None)
        
    def test_set_md_opts_print_summary(self):
        """checks setmdpts widget entry for printing summary
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest3','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest3')
        if self.mv.hasGui:
            c.buildForm()
            x = c.ifd.entryByName['verbose_cb']['widget']
            #setting to no print out
            x.setentry('no print out')
            self.assertEqual(x.get(),'no print out')
            c.Accept_cb()
        self.assertEqual(st, None)
            
    def test_set_md_opts_dt_tw_thumb_wheel(self):
        """checks setting dt_tw - timesteps in setmdpts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest4','Data/trp3_h.prmtop')
        #call setmdopts command
        c = self.mv.setmdOpts_Amber94
        st=c('mdtest4')
        if self.mv.hasGui:
            c.buildForm()
            #setting dt_tw thumb wheel value to 0.4
            c.ifd.entryByName['dt_tw']['widget'].set(0.4)
            self.assertEqual(c.ifd.entryByName['dt_tw']['widget'].get(),0.4)
            c.Accept_cb()
        self.assertEqual(st, None)


    def test_set_md_opts_t_tw_thumb_wheel(self):
        """checks setting t_tw - initial time in setmdpts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest5','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest5')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting t_tw thumb wheel value to 4.00
            c.ifd.entryByName['t_tw']['widget'].set(4.00)
            self.assertEqual(c.ifd.entryByName['t_tw']['widget'].get(),4.00)
            c.Accept_cb()


    def test_set_md_opts_ntpr_md_tw_thumb_wheel(self):
        """checks setting ntpr_md_tw-frequency in setmdpts widget
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest6','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st = c('mdtest6')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting ntpr_md_tw to 4
            c.ifd.entryByName['ntpr_md_tw']['widget'].set(4)
            self.assertEqual(c.ifd.entryByName['ntpr_md_tw']['widget'].get(),4)
            c.Accept_cb()


    def test_set_md_opts_tautp_tw_thumb_wheel(self):
        """checks setting tautp_tw -temp.coupling params
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest7','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest7')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting tautp_tw to 0.4
            c.ifd.entryByName['tautp_tw']['widget'].set(0.4)
            self.assertEqual(c.ifd.entryByName['tautp_tw']['widget'].get(),0.4)
            c.Accept_cb()

    def test_set_md_opts_ntwx_tw_thumb_wheel(self):
        """checks setting ntwx_tw trajectory snap shot frequency
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest8','Data/trp3_h.prmtop')
        #call setmdopts command
        c = self.mv.setmdOpts_Amber94
        st=c('mdtest8')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting tautp_tw to 0.4
            c.ifd.entryByName['ntwx_tw']['widget'].set(4)
            self.assertEqual(c.ifd.entryByName['ntwx_tw']['widget'].get(),4)
            c.Accept_cb()
    

    def test_set_md_opts_idum_tw_thumb_wheel(self):
        """checks setting idum_tw random number seed
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest9','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest9')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting idum_tw to 1
            c.ifd.entryByName['idum_tw']['widget'].set(1)
            self.assertEqual(c.ifd.entryByName['idum_tw']['widget'].get(),1)
            c.Accept_cb()


    def test_set_md_opts_temp0_tw_thumb_wheel(self):
        """checks setting temp0_tw target temperature
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest10','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest10')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting temp0_tw to 400
            c.ifd.entryByName['temp0_tw']['widget'].set(400)
            self.assertEqual(c.ifd.entryByName['temp0_tw']['widget'].get(),400)
            c.Accept_cb()


    def test_set_md_opts_tempi_tw_thumb_wheel(self):
        """checks setting tempi_tw initial temperature
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest11','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest11')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #setting tempi_tw to 10
            c.ifd.entryByName['tempi_tw']['widget'].set(10)
            self.assertEqual(c.ifd.entryByName['tempi_tw']['widget'].get(),10)
            c.Accept_cb()

    def test_set_md_opts_zerov(self):
        """checks toggle zerov- use zero initial velocities
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest12','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st=c('mdtest12')
        self.assertEqual(st, None)
        if self.mv.hasGui:
            c.buildForm()
            #toggle on zerov button
            c.ifd.entryByName['zerov_cb']['widget'].select()
            self.assertEqual(c.zerov.get(),1)
            c.Accept_cb()

#end widget entries check
    
    
    
    def test_set_md_opt_amber94_normal(self):
        """checks setmdopts_Amber94
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest13','Data/trp3_h.prmtop')
        #call setmdopts_Amber94
        c = self.mv.setmdOpts_Amber94
        c('mdtest13')
        self.assertEqual(hasattr(c,'CurrentAmber94'),True)
        self.assertEqual(c.CurrentAmber94.key, "mdtest13")


    def test_set_md_opt_amber94_invalid_key(self):
        """checks setmdopts_Amber94 with non-existent key
        """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        self.mv.setup_Amber94("trp3_h:",'mdtest14','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        error = 'ERROR'
        self.assertEqual(c('gjdsg'), error) 


class LogAmberSetUpTest(AmberBaseTest):
    
    def test_SetUpAmber94_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        self.mv.readMolecule("Data/trp3_h.pdb")
        st = self.mv.setup_Amber94("trp3_h:",'mdtest14','Data/trp3_h.prmtop')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],'self.setup_Amber94("trp3_h:", \'mdtest14\', dataDict={}, log=0, filename=\'Data/trp3_h.prmtop\')')
        self.assertEqual(st , None)

    def test_SetUpAmber94_log_checks_that_it_runs(self):
        """Checking log string runs """
        self.mv.readMolecule("Data/trp3_h.pdb")
        oldself=self
        self =mv
        s="self.setup_Amber94('trp3_h:', 'mdtest14', dataDict={}, log=0, filename='Data/trp3_h.prmtop')"
        exec(s)
        self =oldself
        c = self.mv.setup_Amber94
        self.assertEqual(hasattr(c,'CurrentAmber94'),True)
        self.assertEqual(c.CurrentAmber94.atoms, self.mv.Mols[0].allAtoms)
        self.assertEqual(c.CurrentAmber94.key,'mdtest14')
        
    def test_SetMinimOptsAmber94_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest6','Data/trp3_h.prmtop')
        c = self.mv.setminimOpts_Amber94
        #call setminimopts command
        st = c('mintest6')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.setminimOpts_Amber94('mintest6', log=0)")
        self.assertEqual(st, None)
    
    def test_SetMinimOptsAmber94_log_checks_that_it_runs(self):
        """Checking log string runs """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mintest6','Data/trp3_h.prmtop')
        oldself=self
        self =mv
        s="self.setminimOpts_Amber94('mintest6', log=0)"
        exec(s)
        #c = self.mv.setminimOpts_Amber94
        oldself.assertEqual(1,1)
        
    def test_SetMDOpts_log_checks_expected_log_string(self):
        """checks expected log string is written
        """
        self.mv.showHideGUI('MESSAGE_BOX', 1, redraw=0, log=0)
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest14','Data/trp3_h.prmtop')
        c = self.mv.setmdOpts_Amber94
        #call setmdopts command
        st = c('mdtest14')
        if self.mv.hasGui:
            tx = self.mv.GUI.MESSAGE_BOX.tx
            last_index = tx.index('end')
            last_entry_index = str(float(last_index)-2.0)
            last_entry = tx.get(last_entry_index, 'end')    
            self.assertEqual(split(last_entry,'\n')[0],"self.setmdOpts_Amber94('mdtest14', log=0)")
        self.assertEqual(st, None)

            
    def test_SetMDOpts_log_log_checks_that_it_runs(self):
        """Checking log string runs """
        #read molecule
        self.mv.readMolecule("Data/trp3_h.pdb")
        #call setup_Amber94 command
        self.mv.setup_Amber94("trp3_h:",'mdtest14','Data/trp3_h.prmtop')    
        oldself=self
        self =mv       
        s = "self.setmdOpts_Amber94('mdtest14', log=0)"
        exec(s)
        #c = self.mv.setmdOpts_Amber94
        oldself.assertEqual(1,1)



        

if __name__ == '__main__':
    test_cases = [
        'SetUpAmber94',
        'SetMinimOptsAmber94',
        'SetMDOpts',  #303, 544 None instead of 'ERROR'
        'LogAmberSetUpTest',
        ]
    
    unittest.main( argv=([__name__ ] + test_cases) )




#if __name__ == '__main__':
#    unittest.main()






        
